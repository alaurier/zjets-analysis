## This is an analysis framework for Z+Jets Run2
```
## To checkout from Git:
mkdir ZJets
cd ZJets
mkdir source build run
cd source
git clone https://gitlab.cern.ch/alaurier/zjets-analysis.git
```
## How to compile and run
```
#1. Set up atlas tools and analysisBase
cd source
setupATLAS
asetup 21.2.85,AnalysisBase
#2. Compile code. To compile faster on lxplus, make -j12 instead.
cd ../build
cmake ../source
make
#3. Link the compiled code
cd ../run
source ../build/*/setup.sh
```

### Running the code
```
To run the code, the executable is called runZJets. The steering code is found in the util folder of the package.
 

Most of the configurables for the jobs are found in different config files which correspond to configs for different 'years'. These are found in the zjets-analysis folder and are called *.config .

The option BaseConfig is required for all jobs and simply links the base tools needed in the analysis. They are based around the 2017 recommendations so MC16d and data2017 samples require less inputs. For samples other than MC16d/data2017, you must also include another config file specific to the sample's year. See example below.

The other configurables include the channel type (recommended '3' for both electrons and muon channels at one), JetAlg (recommended PFlow).
```
##### Example local run command
#1. On a 2017 data sample
```
runZJets /eos/user/a/alaurier/ZJets/data17Example.root JetAlg: PFlow Channel: 3 BaseConfig: zjets-analysis/ZJets.config
```

#2. On a 2018 SherpaZJets sample
```
runZJets /eos/user/a/alaurier/ZJets/Sherpa2018Example.root JetAlg: PFlow Channel: 3 BaseConfig: zjets-analysis/ZJets.config zjets-analysis/ZJets_2018.config

This will create an output directory called `ZJetsanalysis_DATE` where `DATE` is automatically replaced by the date and time. You can specify a different output directory with the option `OutputDir`.
```
#3. Running on the Grid
```
First set up the proper panda and rucio packages.

Two new inputs are required for running on the grid when compared to running locally. GridDS is the input dataset and OutputDS is the panda output dataset.
```
##### Example Grid run command
```
#1. Data 2017 sample
runZJets GridDS: data17_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3984 OutputDS: user.<UserName>.<Date>.%in:name[1]%.%in:name[2]%.%in:name[6]% Channel: 3 JetAlg: PFlow BaseConfig: vbfz-analysis/VBFZ.config

For the other samples, follow the same rules shown above in the 'Run locally' section.
Not that the <userName> and <Date> are placeholders that need to be changed!
```
##### Running on a full sample on the grid
```
Instead of running on a single dataset, you can run on all the samples of a sample set (data, SherpaZJets, MGZJets, etc). An extra configurable is needed called SampleName. The samples for each sample name are found in the data folder in either "data_samples.config" or "mcX_samples.config". The MC samples are seperated into their years according to the file name.
Running the following script will then run all the W+Jets mc16a samples outlined in "mc16a_samples.config".

#1. W+Jets mc16a example
runZJets zjets-analysis/ZJets_2016.config SampleName: WJets GridRunAll: 1 JetAlg: PFlow Channel: 3 BaseConfig: zjets-analysis/ZJets.config OutputDS: user.<UserName>.<Date>.Nominal.WJets2016.%in:name[1]%.%in:name[2]%.%in:name[6]% Include: zjets-analysis/mc16a_samples.config
```
##### Running over all the samples on the grid
```
The file "runCommands.txt" found in the data folder holds the commands for running each sample on the grid. If this is copied in the run folder, you can simply use:

while read line; do $line; done < runCommands.txt

and then this will submit jobs for each and every sample for the analysis. This will submit a few hundred jobs.
```
##### Histogram and plot making
```
The histogram and plot making script are found in the same file in the macro folder, ZJetsplots.cxx which is steered by the ZJetsplots.data file. 
```
### Cross sections
```
All the information needed for the cross sections is found in the macro folder in the file called mc16_xsecs.conf
```
