#include "zjets-analysis/ZJetsmuonTools.h"

ZJetsmuonTools::ZJetsmuonTools(const char *name, bool isMC)
    : asg::AsgMessaging( name )
    , m_name(name)
    , m_isMC(isMC)
{}

StatusCode ZJetsmuonTools::Initialize(Config &config){
  m_applyTriggerSF = config.getBool(m_name+".MuonTriggerScaleFactors.ApplyTriggerSF");
  m_diMuonTriggerLeg1 = config.getStr(m_name+".Trigger.DiMuonTriggerLeg1");
  m_diMuonTriggerLeg2 = config.getStr(m_name+".Trigger.DiMuonTriggerLeg2");
  m_emuTrigger1 = config.getStr("ZJetseventTools.Trigger.SingleTrigger1");

  //Config parameters
  m_pTCut           = config.getNum(m_name+".Selection.pTCut");
  m_MaxAbsEta       = config.getNum(m_name+".Selection.MaxAbsEta");
  m_d0BySigd0Max    = config.getNum(m_name+".Selection.d0BySigd0Max");
  m_z0Max           = config.getNum(m_name+".Selection.z0Max");

  ANA_CHECK(InitializeMuonTools(config));

  return StatusCode::SUCCESS;
}

void ZJetsmuonTools::checkSyst(const CP::SystematicSet &syst) {
  // this is really silly, there is only one var per syst.
  for (auto var : syst) {
    if (m_muonCalibrationAndSmearingTool->isAffectedBySystematic(var))
      printf("Syst %s is affecting muon scale+reso tool!\n",syst.name().c_str());
    if ( m_muonRecoSFTool->isAffectedBySystematic(var) )
      printf("Syst %s is affecting muon recoSF tool.\n",syst.name().c_str());
    if ( m_muonIsoSFTool->isAffectedBySystematic(var) )
      printf("Syst %s is affecting muon isoSF tool.\n",syst.name().c_str());
    if ( m_muonTTVASFTool->isAffectedBySystematic(var) )
      printf("Syst %s is affecting muon TTVA tool.\n",syst.name().c_str());
    if ( m_muonTriggerSFTool->isAffectedBySystematic(var) )
      printf("Syst %s is affecting muon trigger SF tool.\n",syst.name().c_str());
  }

}

StatusCode ZJetsmuonTools::ApplySystematicVariation(const CP::SystematicSet &syst){

    ANA_CHECK(m_muonCalibrationAndSmearingTool->applySystematicVariation(syst));
    ANA_CHECK(m_muonRecoSFTool->applySystematicVariation(syst));
    ANA_CHECK(m_muonIsoSFTool->applySystematicVariation(syst));
    ANA_CHECK(m_muonTTVASFTool->applySystematicVariation(syst));
    ANA_CHECK(m_muonTriggerSFTool->applySystematicVariation(syst));

  return StatusCode::SUCCESS;
}

StatusCode ZJetsmuonTools::InitializeMuonTools(Config &config){
  // Muon selection tool
  m_muonSelectionTool = new CP::MuonSelectionTool("MuonSelectionTool");
  ANA_CHECK(m_muonSelectionTool->setProperty("MaxEta", config.getNum(m_name+".MuonSelectionTool.MaxEta")));
  ANA_CHECK(m_muonSelectionTool->setProperty("MuQuality", config.getInt(m_name+".MuonSelectionTool.MuQuality")));
  ANA_CHECK(m_muonSelectionTool->initialize());

  // Muon Isolation selection tool
  m_isoSelectionTool = new CP::IsolationSelectionTool("IsolationSelectionTool_muon");
  ANA_CHECK(m_isoSelectionTool->setProperty("MuonWP", config.getStr(m_name+".IsolationSelectionTool.MuonWP").Data()));
  ANA_CHECK(m_isoSelectionTool->initialize());

  // Muon reconstruction SF
  m_muonRecoSFTool = new CP::MuonEfficiencyScaleFactors("MuonEfficiencyScaleFactors_Reco");
  ANA_CHECK(m_muonRecoSFTool->setProperty("WorkingPoint", config.getStr(m_name+".MuonEfficiencyScaleFactors.WorkingPointReco").Data()));
  //ANA_CHECK(m_muonRecoSFTool->setProperty("CalibrationRelease", config.getStr(m_name+".MuonEfficiencyScaleFactors.CalibrationRelease").Data()));
  ANA_CHECK(m_muonRecoSFTool->initialize());

  // Muon isolation SF
  m_muonIsoSFTool = new CP::MuonEfficiencyScaleFactors("MuonEfficiencyScaleFactors_Iso");
  ANA_CHECK(m_muonIsoSFTool->setProperty("WorkingPoint", config.getStr(m_name+".MuonEfficiencyScaleFactors.WorkingPointIso").Data()));
  //ANA_CHECK(m_muonIsoSFTool->setProperty("CalibrationRelease", config.getStr(m_name+".MuonEfficiencyScaleFactors.CalibrationRelease").Data()));
  ANA_CHECK(m_muonIsoSFTool->initialize());

  // Muon track to vertex association SF
  m_muonTTVASFTool = new CP::MuonEfficiencyScaleFactors("MuonEfficiencyScaleFactors_TTVA");
  ANA_CHECK(m_muonTTVASFTool->setProperty("WorkingPoint", config.getStr(m_name+".MuonEfficiencyScaleFactors.WorkingPointTTVA").Data()));
  //ANA_CHECK(m_muonTTVASFTool->setProperty("CalibrationRelease", config.getStr(m_name+".MuonEfficiencyScaleFactors.CalibrationRelease").Data()));
  ANA_CHECK(m_muonTTVASFTool->initialize());

  // Muon trigger SF
  m_muonTriggerSFTool = new CP::MuonTriggerScaleFactors("MuonTriggerScaleFactors");
  ANA_CHECK(m_muonTriggerSFTool->setProperty("MuonQuality", config.getStr(m_name+".MuonTriggerScaleFactors.MuonQuality").Data()));
 // ANA_CHECK(m_muonTriggerSFTool->setProperty("UseExperimental", config.getBool(m_name+".MuonTriggerScaleFactors.UseExperimental")));
  ANA_CHECK(m_muonTriggerSFTool->initialize());

  // Muon calibration tool
  m_muonCalibrationAndSmearingTool = new CP::MuonCalibrationAndSmearingTool("MuonCalibrationAndSmearingTool");
  ANA_CHECK(m_muonCalibrationAndSmearingTool->setProperty("Year", config.getStr(m_name+".MuonCalibrationAndSmearingTool.Year").Data()));
  ANA_CHECK(m_muonCalibrationAndSmearingTool->setProperty("StatComb", config.getBool(m_name+".MuonCalibrationAndSmearingTool.StatComb")));
  ANA_CHECK(m_muonCalibrationAndSmearingTool->setProperty("SagittaRelease", config.getStr(m_name+".MuonCalibrationAndSmearingTool.SagittaRelease").Data()));
  ANA_CHECK(m_muonCalibrationAndSmearingTool->setProperty("SagittaCorr", config.getBool(m_name+".MuonCalibrationAndSmearingTool.SagittaCorr")));
  ANA_CHECK(m_muonCalibrationAndSmearingTool->setProperty("doSagittaMCDistortion", config.getBool(m_name+".MuonCalibrationAndSmearingTool.doSagittaMCDistortion")));
  ANA_CHECK(m_muonCalibrationAndSmearingTool->setProperty("SagittaCorrPhaseSpace", config.getBool(m_name+".MuonCalibrationAndSmearingTool.SagittaCorrPhaseSpace")));
  ANA_CHECK(m_muonCalibrationAndSmearingTool->setProperty("Release", config.getStr(m_name+".MuonCalibrationAndSmearingTool.Release").Data()));
  ANA_CHECK(m_muonCalibrationAndSmearingTool->setProperty("do2StationsHighPt", config.getBool(m_name+".MuonCalibrationAndSmearingTool.do2StationsHighPt")));
  ANA_CHECK(m_muonCalibrationAndSmearingTool->setProperty("doExtraSmearing", config.getBool(m_name+".MuonCalibrationAndSmearingTool.doExtraSmearing")));
  ANA_CHECK(m_muonCalibrationAndSmearingTool->initialize());

  return StatusCode::SUCCESS;
}

// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisWinterMC16#Muon_track_to_vertex_association
// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2#Electron_d0_and_z0_cut_definitio
bool ZJetsmuonTools::passTTVA(const xAOD::Muon* muon, const xAOD::Vertex *vtxHS, const xAOD::EventInfo *eventInfo) {
  const xAOD::TrackParticle *trk = muon->primaryTrackParticle();
  if ( !vtxHS || !trk ) return false;
  double d0sig = xAOD::TrackingHelpers::d0significance( trk,
							eventInfo->beamPosSigmaX(),
							eventInfo->beamPosSigmaY(),
							eventInfo->beamPosSigmaXY());
  if ( std::abs(d0sig) > m_d0BySigd0Max ) return false;
  double Dz0 = std::abs(trk->z0() + trk->vz() - vtxHS->z()) * sin(trk->theta());
  if ( Dz0 > m_z0Max) return false;
  return true;
}

double ZJetsmuonTools::getRecoSF(const xAOD::Muon* mu1, const xAOD::Muon* mu2, const CP::SystematicSet &syst) {
  float sf1 = 1.0, sf2 = 1.0;
  m_muonRecoSFTool->getEfficiencyScaleFactor(*mu1,sf1);
  m_muonRecoSFTool->getEfficiencyScaleFactor(*mu2,sf2);
  return sf1*sf2;
}

double ZJetsmuonTools::getIsoSF(const xAOD::Muon* mu1, const xAOD::Muon* mu2, const CP::SystematicSet &syst) {
  float sf1 = 1.0, sf2 = 1.0;
  m_muonIsoSFTool->getEfficiencyScaleFactor(*mu1,sf1);
  m_muonIsoSFTool->getEfficiencyScaleFactor(*mu2,sf2);
  return sf1*sf2;
}

double ZJetsmuonTools::getTTVASF(const xAOD::Muon* mu1, const xAOD::Muon* mu2, const CP::SystematicSet &syst) {
  float sf1 = 1.0, sf2 = 1.0;
  m_muonTTVASFTool->getEfficiencyScaleFactor(*mu1,sf1);
  m_muonTTVASFTool->getEfficiencyScaleFactor(*mu2,sf2);
  return sf1*sf2;
}

double ZJetsmuonTools::getDimuonSF(const xAOD::Muon* mu1, const xAOD::Muon* mu2, const CP::SystematicSet &syst) {
  return getRecoSF(mu1,mu2,syst)*getIsoSF(mu1,mu2,syst)*getTTVASF(mu1,mu2,syst)*getDimuonTriggerSF(mu1,mu2);
}

StatusCode ZJetsmuonTools::MuonSelection(xAOD::TEvent *event, const xAOD::Vertex* primaryVtx, std::vector<xAOD::Muon*> *calibMuons, std::vector<xAOD::Muon*> *selected, int& count1, int& count2, int& count3){
  int mcount1=0,mcount2=0,mcount3=0;

  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(event->retrieve(eventInfo, "EventInfo"));

  selected->clear();

  for(auto muon : *calibMuons){
    bool isGoodMuon = false;
    bool passSel = false;
    bool passIso = false;
    bool passVtx = false;

    double absEta = fabs(muon->eta());

    if (muon->pt() < 10000 || absEta > m_MaxAbsEta) continue;

    // Track-to-vertex association
    const xAOD::TrackParticle *mutrk = muon->primaryTrackParticle();
    float mu_d0sig = 10. , mu_deltaz0=10., mu_deltaz0new=10.;
    if(mutrk && primaryVtx && muon->pt() > 20000){
        mu_d0sig = xAOD::TrackingHelpers::d0significance( mutrk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
        mu_deltaz0 = fabs(mutrk->z0() + mutrk->vz() - primaryVtx->z());
        mu_deltaz0new = mu_deltaz0 * sin(mutrk->theta());
    }
    if( ( fabs(mu_d0sig) < m_d0BySigd0Max && fabs(mu_deltaz0new) < m_z0Max) ) passVtx = true;

    if (absEta < m_MaxAbsEta && muon->pt() > m_pTCut) mcount1++;

    if( m_muonSelectionTool->accept(muon)) passSel = true;

    if (absEta < m_MaxAbsEta && muon->pt() > m_pTCut && passSel) mcount2++;

    if( m_isoSelectionTool->accept(*muon)) passIso = true;

    if (absEta < m_MaxAbsEta && muon->pt() > m_pTCut && passSel && passIso) mcount3++;

    if (absEta < m_MaxAbsEta && muon->pt() > m_pTCut && passSel && passIso && passVtx) isGoodMuon=true;

    if (!isGoodMuon) continue;

    // Scale factors
    float muonIdSF = 1.0;
    float muonIsoSF = 1.0;
    float muonTTVASF = 1.0;

    if(m_isMC) {
        ANA_CHECK(m_muonRecoSFTool->getEfficiencyScaleFactor(*muon, muonIdSF));
        ANA_CHECK(m_muonIsoSFTool->getEfficiencyScaleFactor(*muon, muonIsoSF));
        ANA_CHECK(m_muonTTVASFTool->getEfficiencyScaleFactor(*muon, muonTTVASF));
    }

    muon->auxdata<float>("muIDSF") = muonIdSF;
    muon->auxdata<float>("muIsoSF") = muonIsoSF;
    muon->auxdata<float>("muTTVASF") = muonTTVASF;
    selected->push_back(muon);
  }

  count1=mcount1;
  count2=mcount2;
  count3=mcount3;

  if( selected->size() > 1 )
    std::sort(selected->begin(), selected->end(), DescendingPt());

  return StatusCode::SUCCESS;
}

// This is a SF, not an efficincy!!
double ZJetsmuonTools::GetMuonTriggerEfficiency(std::vector<xAOD::Muon*> *SelectedMuons){
  if (!m_applyTriggerSF) return 1.0;

  auto mu1 = SelectedMuons->begin();
  auto mu2 = SelectedMuons->begin() + 1;
  return getDimuonTriggerSF(*mu1,*mu2);
}

double ZJetsmuonTools::GetMuonTriggerEfficiencyEmu(std::vector<xAOD::Muon*> *SelectedMuons){
  if (!m_applyTriggerSF) return 1.0;

  auto mu1 = SelectedMuons->begin();
  auto mu2 = SelectedMuons->begin() + 1;
  return getMuonTriggerSF(*mu1,*mu2);
}


double ZJetsmuonTools::getDimuonTriggerSF(const xAOD::Muon* mu1, const xAOD::Muon* mu2) {
  //https://twiki.cern.ch/twiki/bin/view/Atlas/MuonTriggerPhysicsRecommendationsRel212017
  //from above: eff = e22(1)*e22(2) + e22(1)*(1 - e22(2))*e8(2) + e22(2)*(1 - e22(1))*e8(1)

  double ef_data_mu1_leg1, ef_data_mu2_leg1, ef_data_mu1_leg2, ef_data_mu2_leg2;
  double ef_MC_mu1_leg1, ef_MC_mu2_leg1, ef_MC_mu1_leg2, ef_MC_mu2_leg2;

  m_muonTriggerSFTool->getTriggerEfficiency(*mu1, ef_data_mu1_leg1, m_diMuonTriggerLeg1.Data(), true);
  m_muonTriggerSFTool->getTriggerEfficiency(*mu2, ef_data_mu2_leg1, m_diMuonTriggerLeg1.Data(), true);
  m_muonTriggerSFTool->getTriggerEfficiency(*mu1, ef_data_mu1_leg2, m_diMuonTriggerLeg2.Data(), true);
  m_muonTriggerSFTool->getTriggerEfficiency(*mu2, ef_data_mu2_leg2, m_diMuonTriggerLeg2.Data(), true);

  double eff_data = ef_data_mu1_leg1*ef_data_mu2_leg1 + ef_data_mu1_leg1*(1.0 - ef_data_mu2_leg1)*ef_data_mu2_leg2 + ef_data_mu2_leg1*(1.0 - ef_data_mu1_leg1)*ef_data_mu1_leg2;

  m_muonTriggerSFTool->getTriggerEfficiency(*mu1, ef_MC_mu1_leg1, m_diMuonTriggerLeg1.Data(), false);
  m_muonTriggerSFTool->getTriggerEfficiency(*mu2, ef_MC_mu2_leg1, m_diMuonTriggerLeg1.Data(), false);
  m_muonTriggerSFTool->getTriggerEfficiency(*mu1, ef_MC_mu1_leg2, m_diMuonTriggerLeg2.Data(), false);
  m_muonTriggerSFTool->getTriggerEfficiency(*mu2, ef_MC_mu2_leg2, m_diMuonTriggerLeg2.Data(), false);

  double eff_MC = ef_MC_mu1_leg1*ef_MC_mu2_leg1 + ef_MC_mu1_leg1*(1.0 - ef_MC_mu2_leg1)*ef_MC_mu2_leg2 + ef_MC_mu2_leg1*(1.0 - ef_MC_mu1_leg1)*ef_MC_mu1_leg2;

  if (eff_MC != 0.0) return eff_data/eff_MC;
  return 1.0;
}

double ZJetsmuonTools::getMuonTriggerSF(const xAOD::Muon* mu1, const xAOD::Muon* mu2) {
  //https://twiki.cern.ch/twiki/bin/view/Atlas/MuonTriggerPhysicsRecommendationsRel212017
  //from above: eff = e22(1)*e22(2) + e22(1)*(1 - e22(2))*e8(2) + e22(2)*(1 - e22(1))*e8(1)
  double mtrigsf;
  std::string trigger;
  if (m_emuTrigger1 == "HLT_mu26_ivarmedium") trigger = "HLT_mu26_ivarmedium_OR_HLT_mu50";
  else trigger = "HLT_mu20_iloose_L1MU15_OR_HLT_mu50";


  m_muonTriggerSFTool->getTriggerEfficiency(*mu1, mtrigsf, trigger, true);
  // m_muonTriggerSFTool->getTriggerScaleFactor(*mu2, mtrigsf, trigger);

  return mtrigsf;
}


StatusCode ZJetsmuonTools::FinalizeTools(){
  delete m_muonSelectionTool;
  delete m_isoSelectionTool;
  delete m_muonCalibrationAndSmearingTool;
  delete m_muonRecoSFTool;
  delete m_muonIsoSFTool;
  delete m_muonTTVASFTool;
  delete m_muonTriggerSFTool;

  return StatusCode::SUCCESS;
}
