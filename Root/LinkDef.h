#include "zjets-analysis/ZJetsStudy.h"
#include "zjets-analysis/ZJetsFullSysts.h"
#include "zjets-analysis/ZJetsExtraSysts.h"
#include "zjets-analysis/Config.h"

#ifdef __CINT__

  #pragma link off all globals;
  #pragma link off all classes;
  #pragma link off all functions;
  #pragma link C++ nestedclass;

  #pragma link C++ class ZJetsStudy+;
  #pragma link C++ class ZJetsFullSysts+;
  #pragma link C++ class ZJetsExtraSysts+;
  #pragma link C++ class ZJets::Config+;

#endif
