#include "zjets-analysis/ZJetselecTools.h"

ZJetselecTools :: ZJetselecTools(const char *name, bool isMC)
    : asg::AsgMessaging( name )
    , m_name(name)
    , m_isMC(isMC)
{}

StatusCode ZJetselecTools :: Initialize(Config &config) {
  m_doElecTriggerSF = config.getBool(m_name+".AsgElectronEfficiencyCorrectionTool.doElecTriggerSF");

  //Config parameters
  m_pTCut           = config.getNum(m_name+".Selection.pTCut");
  m_MaxAbsEta       = config.getNum(m_name+".Selection.MaxAbsEta");
  m_BarrelMaxAbsEta = config.getNum(m_name+".Selection.BarrelMaxAbsEta");
  m_EndcapMinAbsEta = config.getNum(m_name+".Selection.EndcapMinAbsEta");
  m_d0BySigd0Max    = config.getNum(m_name+".Selection.d0BySigd0Max");
  m_z0Max           = config.getNum(m_name+".Selection.z0Max");

  ANA_CHECK(InitializeElectronTools(config));

  return StatusCode::SUCCESS;
}

double ZJetselecTools::getRecoSF(const xAOD::Electron* e1, const xAOD::Electron* e2, const CP::SystematicSet &syst) {
  double sf1 = 1.0, sf2 = 1.0;
  m_elecRecoSFTool->getEfficiencyScaleFactor(*e1,sf1);
  m_elecRecoSFTool->getEfficiencyScaleFactor(*e2,sf2);
  return sf1*sf2;
}

double ZJetselecTools::getIsoSF(const xAOD::Electron* e1, const xAOD::Electron* e2, const CP::SystematicSet &syst) {
  double sf1 = 1.0, sf2 = 1.0;
  m_elecIsoSFTool->getEfficiencyScaleFactor(*e1,sf1);
  m_elecIsoSFTool->getEfficiencyScaleFactor(*e2,sf2);
  return sf1*sf2;
}

double ZJetselecTools::getIDSF(const xAOD::Electron* e1, const xAOD::Electron* e2, const CP::SystematicSet &syst) {
  double sf1 = 1.0, sf2 = 1.0;
  m_elecIdSFTool->getEfficiencyScaleFactor(*e1,sf1);
  m_elecIdSFTool->getEfficiencyScaleFactor(*e2,sf2);
  return sf1*sf2;
}

double ZJetselecTools::getTrigSF(const xAOD::Electron* e1, const xAOD::Electron* e2, const CP::SystematicSet &syst) {
  double sf1 = 1.0, sf2 = 1.0;
  m_elecTrigSFTool->getEfficiencyScaleFactor(*e1,sf1);
  m_elecTrigSFTool->getEfficiencyScaleFactor(*e2,sf2);
  return sf1*sf2;
}

double ZJetselecTools::getDielectronSF(const xAOD::Electron* e1, const xAOD::Electron* e2, const CP::SystematicSet &syst) {
  double sf = getRecoSF(e1,e2,syst)*getIsoSF(e1,e2,syst)*getIDSF(e1,e2,syst);
  return m_doElecTriggerSF ? sf * getTrigSF(e1,e2,syst) : sf;
}


StatusCode ZJetselecTools::ApplySystematicVariation(const CP::SystematicSet &syst){
    
  ANA_CHECK(m_elecCalibTool->applySystematicVariation(syst));
  ANA_CHECK(m_isoCorrectionTool->applySystematicVariation(syst));
  ANA_CHECK(m_elecRecoSFTool->applySystematicVariation(syst));
  ANA_CHECK(m_elecIdSFTool->applySystematicVariation(syst));
  ANA_CHECK(m_elecIsoSFTool->applySystematicVariation(syst));
  ANA_CHECK(m_elecTrigSFTool->applySystematicVariation(syst));
  ANA_CHECK(m_elecTrigMCEffTool->applySystematicVariation(syst));

  return StatusCode::SUCCESS;
}

StatusCode ZJetselecTools::InitializeElectronTools(Config &config) {

  // Electron calibration tool
  m_elecCalibTool = new CP::EgammaCalibrationAndSmearingTool("EgammaCalibrationAndSmearingTool");
  ANA_CHECK(m_elecCalibTool->setProperty("ESModel", config.getStr(m_name+".EgammaCalibrationAndSmearingTool.ESModel").Data()));
  ANA_CHECK(m_elecCalibTool->setProperty("decorrelationModel", config.getStr(m_name+".EgammaCalibrationAndSmearingTool.decorrelationModel").Data()));
  ANA_CHECK(m_elecCalibTool->initialize());

  // Electron Isolation selection tool
  m_isoSelectionTool = new CP::IsolationSelectionTool("IsolationSelectionTool");
  ANA_CHECK(m_isoSelectionTool->setProperty("ElectronWP", config.getStr(m_name+".IsolationSelectionTool.ElectronWP").Data()));
  ANA_CHECK(m_isoSelectionTool->initialize());

  //Likelihood selection working point
  m_electronLikelihoodWP = config.getStr(m_name+".ElectronLikelihood.WorkingPoint");

  // Isolation Correction tool
  m_isoCorrectionTool = new CP::IsolationCorrectionTool("IsolationCorrectionTool_electron");
  ANA_CHECK(m_isoCorrectionTool->setProperty("IsMC", m_isMC));
  ANA_CHECK(m_isoCorrectionTool->initialize());

  // Data type and Correlation model consistent for all SF/Eff tools
  Str ForceDataType    = config.getStr(m_name+".AsgElectronEfficiencyCorrectionTool.ForceDataType");
  Str CorrelationModel = config.getStr(m_name+".AsgElectronEfficiencyCorrectionTool.CorrelationModel");

  // Electron reconstruction SF tool
  m_elecRecoSFTool = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_Reco");
  std::vector<std::string> fileRecoSF{ PathResolverFindCalibFile(config.getStr(m_name+".AsgElectronEfficiencyCorrectionTool.fileRecoSF").Data()) };
  std::string SFmap = "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/map3.txt";
  ANA_CHECK(m_elecRecoSFTool->setProperty("MapFilePath", SFmap));
  //ANA_CHECK(m_elecRecoSFTool->setProperty("MapFilePath", fileRecoSF));
  ANA_CHECK(m_elecRecoSFTool->setProperty("RecoKey", "Reconstruction"));
  //ANA_CHECK(m_elecRecoSFTool->setProperty("CorrectionFileNameList", fileRecoSF));
  ANA_CHECK(m_elecRecoSFTool->setProperty("ForceDataType", ForceDataType.Data()));
  ANA_CHECK(m_elecRecoSFTool->setProperty("CorrelationModel", CorrelationModel.Data()));
  ANA_CHECK(m_elecRecoSFTool->initialize());

  // Electron identification SF tool
  m_elecIdSFTool = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_Id");
  std::vector<std::string> fileIdSF{ PathResolverFindCalibFile(config.getStr(m_name+".AsgElectronEfficiencyCorrectionTool.fileIdSF").Data()) };
  //ANA_CHECK(m_elecIdSFTool->setProperty("CorrectionFileNameList", fileIdSF));
  //ANA_CHECK(m_elecIdSFTool->setProperty("MapFilePath", fileIdSF));
  ANA_CHECK(m_elecIdSFTool->setProperty("MapFilePath", SFmap));
  ANA_CHECK(m_elecIdSFTool->setProperty("IdKey", "Medium"));
  ANA_CHECK(m_elecIdSFTool->setProperty("ForceDataType", ForceDataType.Data()));
  ANA_CHECK(m_elecIdSFTool->setProperty("CorrelationModel", CorrelationModel.Data()));
  ANA_CHECK(m_elecIdSFTool->initialize());

  // Electron isolation SF tool
  m_elecIsoSFTool = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_Iso");
  std::vector<std::string> fileIsoSF{ PathResolverFindCalibFile(config.getStr(m_name+".AsgElectronEfficiencyCorrectionTool.fileIsoSF").Data()) };
  //ANA_CHECK(m_elecIsoSFTool->setProperty("CorrectionFileNameList", fileIsoSF));
  ANA_CHECK(m_elecIsoSFTool->setProperty("MapFilePath", SFmap));
  //ANA_CHECK(m_elecIsoSFTool->setProperty("MapFilePath", fileIsoSF));
  ANA_CHECK(m_elecIsoSFTool->setProperty("IdKey", "Medium"));
  ANA_CHECK(m_elecIsoSFTool->setProperty("IsoKey", "FCLoose"));
  ANA_CHECK(m_elecIsoSFTool->setProperty("ForceDataType", ForceDataType.Data()));
  ANA_CHECK(m_elecIsoSFTool->setProperty("CorrelationModel", CorrelationModel.Data()));
  ANA_CHECK(m_elecIsoSFTool->initialize());

  // Electron trigger SF tool
  m_elecTrigSFTool = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_Trig");
  std::vector<std::string> fileTrigSF{ PathResolverFindCalibFile(config.getStr(m_name+".AsgElectronEfficiencyCorrectionTool.fileTrigSF").Data()) };
  //ANA_CHECK(m_elecTrigSFTool->setProperty("CorrectionFileNameList", fileTrigSF));
  ANA_CHECK(m_elecTrigSFTool->setProperty("MapFilePath", SFmap));
  //ANA_CHECK(m_elecTrigSFTool->setProperty("MapFilePath", fileTrigSF));
  ANA_CHECK(m_elecTrigSFTool->setProperty("IdKey", "Medium"));
  ANA_CHECK(m_elecTrigSFTool->setProperty("IsoKey", "FCLoose"));
  ANA_CHECK(m_elecTrigSFTool->setProperty("TriggerKey", "DI_E_2015_e12_lhloose_L1EM10VH_2016_e17_lhvloose_nod0_2017_2018_e24_lhvloose_nod0_L1EM20VH"));
  ANA_CHECK(m_elecTrigSFTool->setProperty("CorrelationModel", CorrelationModel.Data()));
  ANA_CHECK(m_elecTrigSFTool->setProperty("ForceDataType", ForceDataType.Data()));
  ANA_CHECK(m_elecTrigSFTool->initialize());

  // Electron trigger MC efficiency tool
  m_elecTrigMCEffTool = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_MCEff");
  std::vector<std::string> fileTrigMCEff{ PathResolverFindCalibFile(config.getStr(m_name+".AsgElectronEfficiencyCorrectionTool.fileTrigMCEff").Data()) };
  ANA_CHECK(m_elecTrigMCEffTool->setProperty("CorrectionFileNameList", fileTrigMCEff));
  ANA_CHECK(m_elecTrigMCEffTool->setProperty("ForceDataType", ForceDataType.Data()));
  ANA_CHECK(m_elecTrigMCEffTool->setProperty("CorrelationModel", CorrelationModel.Data()));
  ANA_CHECK(m_elecTrigMCEffTool->initialize());

  return StatusCode::SUCCESS;
}

//StatusCode ZJetselecTools::InitializeTriggerTools(){
//  // Initialize and configure trigger tools
//  m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool");
//  ToolHandle< TrigConf::ITrigConfigTool > configHandle( m_trigConfigTool );
//  ANA_CHECK(configHandle->initialize());
//
//  m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
//  ANA_CHECK(m_trigDecisionTool->setProperty("ConfigTool", configHandle ));
//  ANA_CHECK(m_trigDecisionTool->setProperty("TrigDecisionKey", "xTrigDecision"));
//  ANA_CHECK(m_trigDecisionTool->initialize());
//
//  m_trigMatching.setTypeAndName("Trig::MatchingTool/MyMatchingTool");
//  ANA_CHECK(m_trigMatching.retrieve());
//
//  return StatusCode::SUCCESS;
//}

// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2#Electron_d0_and_z0_cut_definitio
bool ZJetselecTools::passTTVA(const xAOD::Electron* elec, const xAOD::Vertex *vtxHS, const xAOD::EventInfo *eventInfo) {
  const xAOD::TrackParticle *trk = elec->trackParticle();
  if ( !vtxHS || !trk ) return false;
  double d0sig = xAOD::TrackingHelpers::d0significance( trk, 
							eventInfo->beamPosSigmaX(), 
							eventInfo->beamPosSigmaY(), 
							eventInfo->beamPosSigmaXY());
  if ( std::abs(d0sig) > m_d0BySigd0Max ) return false;
  double Dz0 = std::abs(trk->z0() + trk->vz() - vtxHS->z()) * sin(trk->theta());
  if ( Dz0 > m_z0Max) return false;
  return true;
}

StatusCode ZJetselecTools::ElectronSelection(xAOD::TEvent *event, const xAOD::Vertex* primaryVtx, std::vector<xAOD::Electron*> *calibElecs, std::vector<xAOD::Electron*> *selected, int& count1, int& count2, int& count3){
  int ecount1=0,ecount2=0,ecount3=0;

  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(event->retrieve(eventInfo, "EventInfo"));

  selected->clear();

  for(auto electron : *calibElecs){
    bool isGoodElec = true;
    bool isCrack=false;
    bool passLikely=false;
    bool passIso = false;
    bool passVtx = false;
    
    double absEta = fabs(electron->eta());
    //double absEta = fabs(electron->caloCluster()->etaBE(1));

    if(electron->pt()<20000 || absEta > m_MaxAbsEta) continue;

    if( !electron->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON) ) continue;

    // in Crack
    if( absEta > m_BarrelMaxAbsEta && absEta < m_EndcapMinAbsEta ) isCrack = true;

    if( electron->pt() > m_pTCut && absEta < m_MaxAbsEta && !isCrack) ecount1++;

    if(electron->auxdata<char>(m_electronLikelihoodWP.Data())) passLikely = true;

    if( electron->pt() > m_pTCut && absEta < m_MaxAbsEta && !isCrack && passLikely) ecount2++;

    if(!passLikely) continue;

    if(m_isoSelectionTool->accept(*electron)) passIso = true;

    if( electron->pt() > m_pTCut && absEta < m_MaxAbsEta && !isCrack && passLikely && passIso) ecount3++;
  
    // Track-to-vertex association
    const xAOD::TrackParticle *eltrk = electron->trackParticle();
    float el_d0sig = 10., el_deltaz0 = 10., el_deltaz0new = 10.;
    if( eltrk && primaryVtx ){
      el_d0sig = xAOD::TrackingHelpers::d0significance(eltrk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
      el_deltaz0 = fabs(eltrk->z0() + eltrk->vz() - primaryVtx->z());
      el_deltaz0new = el_deltaz0 * sin(eltrk->theta());
    }
    if( ( fabs(el_d0sig) < m_d0BySigd0Max && fabs(el_deltaz0new) < m_z0Max) ) passVtx = true;

    if(electron->pt() < m_pTCut || absEta > m_MaxAbsEta || isCrack || !passLikely || !passIso || !passVtx) isGoodElec = false;

    if(!isGoodElec) continue;

    // Scale factors
    double elrecosf = 1.0;
    double elidsf = 1.0;
    double elisosf = 1.0;
    double eltrigsf = 1.0;
    double eltrigeff = 1.0;
    if(m_isMC) {
      if(m_elecRecoSFTool->getEfficiencyScaleFactor(*electron,elrecosf) != CP::CorrectionCode::Ok){ continue; }
      if(m_elecIdSFTool->getEfficiencyScaleFactor(*electron,elidsf) != CP::CorrectionCode::Ok){ continue; }
      if(m_elecIsoSFTool->getEfficiencyScaleFactor(*electron,elisosf) != CP::CorrectionCode::Ok){ continue; }
      if(m_doElecTriggerSF){
        if(m_elecTrigSFTool->getEfficiencyScaleFactor(*electron,eltrigsf) != CP::CorrectionCode::Ok){ continue; }
        if(m_elecTrigMCEffTool->getEfficiencyScaleFactor(*electron,eltrigeff) != CP::CorrectionCode::Ok){ continue; }
      }
    }
    electron->auxdecor<double>("elRecoSF") = elrecosf;
    electron->auxdecor<double>("elIDSF") = elidsf;
    electron->auxdecor<double>("elIsoSF") = elisosf;
    electron->auxdecor<double>("elTrigSF") = eltrigsf;
    electron->auxdecor<double>("elTrigEff") = eltrigeff;

    selected->push_back(electron);
  }

  count1=ecount1;
  count2=ecount2;
  count3=ecount3;

  if( selected->size() > 1 )
    std::sort(selected->begin(), selected->end(), DescendingPt());

  return StatusCode::SUCCESS;
}

StatusCode ZJetselecTools::FinalizeTools(){
  delete m_elecCalibTool;
  delete m_isoSelectionTool;
  delete m_isoCorrectionTool;
  delete m_elecRecoSFTool;
  delete m_elecIdSFTool;
  delete m_elecIsoSFTool;
  delete m_elecTrigSFTool;
  delete m_elecTrigMCEffTool;

  return StatusCode::SUCCESS;
}
