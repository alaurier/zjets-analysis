#include "zjets-analysis/ZJetscommon.h"

bool pTorder(const TLorentzVector &l, const TLorentzVector &r) {
  return l.Pt()>r.Pt();
}

void ZJets::fatal(TString msg) {
  printf("\nFATAL\n  %s\n\n",msg.Data());
  abort();
}

ZJets::StrV ZJets::vectorize(TString str, TString sep) {
  StrV result;
  TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) { delete strings; return result; }
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr()) {
    // the number sign and everything after is treated as a comment
    if (os->GetString()[0]=='#') break;
    result.push_back(os->GetString());
  }
  delete strings;
  return result;
}

// convert a text line containing a list of numbers to a vector<double>
ZJets::NumV ZJets::vectorizeNum(TString str, TString sep) {
  NumV result; StrV vecS = vectorize(str,sep);
  for (uint i=0;i<vecS.size();++i)
    result.push_back(atof(vecS[i]));
  return result;
}

TString ZJets::fourVecAsText(const TLorentzVector &p4) {
  return TString::Format("(pT,y,phi,m) = (%6.1f GeV,%6.3f,%6.3f,%5.1f GeV )",
			 p4.Pt()*invGeV,p4.Rapidity(),p4.Phi(),p4.M()*invGeV);
}
TString ZJets::fourVecAsText(const xAOD::IParticle *p) { return fourVecAsText(p->p4()); }


// checks if a given file or directory exist
bool ZJets::fileExist(TString fn) {
  return !(gSystem->AccessPathName(fn.Data()));
}

/*
 *  Methods for histogram creation
 */

TH1F* ZJets::createHist1D(TString hname, TString title, int nbins, int xlow, int xhigh) {
  TH1F* h = new TH1F(hname,title,nbins,xlow,xhigh);
  return h;
}

double ZJets::DRrap(const TLorentzVector &p1, const TLorentzVector &p2) {
  double dy=p1.Rapidity()-p2.Rapidity(), dphi=p1.DeltaPhi(p2);
  return sqrt(dy*dy+dphi*dphi);
}

double ZJets::DRrap_min(const TLV &a, const TLVs &bs) {
  double DRmin=999;
  for (const TLV &b:bs)
    if (DRrap(a,b)<DRmin)
      DRmin=DRrap(a,b);
  return DRmin;
}

double ZJets::DRrap(const xAOD::IParticle *p1, const xAOD::IParticle *p2) {
  return DRrap(p1->p4(),p2->p4());
}

// scalar sum pT
double ZJets::HT(const TLVs &ps) {
  double ht=0;
  for (const auto &p:ps) ht+=p.Pt();
  return ht;
}

/*
 *  Truth methods
 */

/// print details about the truth particle to the screen
void ZJets::printTruthPtcl(const xAOD::TruthParticle *ptcl, TString comment,
			  int childDepth, int parentDepth, int currentDepth) {
  // indentation in case we print decay chain. Three spaces per level
  TString indent(Form(Form("%%%ds",3*currentDepth),""));
  if (ptcl==nullptr) { printf("%sNULL\n",indent.Data()); return; }
  printf("%sTruth part. ID:%5d, status: %2d, %s  %s\n",
	 indent.Data(),ptcl->pdgId(),ptcl->status(),fourVecAsText(ptcl).Data(),comment.Data());
  if (childDepth>0||parentDepth>0) {
    int npar=ptcl->nParents(), nchild=ptcl->nChildren();
    printf("%s-> %d parent and %d children\n",indent.Data(),npar,nchild);
    if (parentDepth>0)
      for (int ip=0;ip<npar;++ip) printTruthPtcl(ptcl->parent(ip),Form("parent %d of ",ip+1)+comment,
						 childDepth-1,parentDepth-1,currentDepth+1);
    if (childDepth>0)
      for (int ic=0;ic<nchild;++ic) printTruthPtcl(ptcl->child(ic),Form("child %d of ",ic+1)+comment,
						   childDepth-1,parentDepth-1,currentDepth+1);
  }
}

bool ZJets::notFromHadron(const xAOD::TruthParticle *ptcl) {
  int ID = ptcl->pdgId();
  // if the particle is a hadron, return false
  if (MCUtils::PID::isHadron(ID)) return false;

  // if there are no parents, not from hadron
  if (ptcl->nParents()==0) return true;

  const xAOD::TruthParticle *parent = ptcl->parent(0);
  int parentID = 0;
  if(parent)
  	parentID = parent->pdgId();
  else //Has nParents > 0 but parent not present, assume from hadron
  	return false;
  if (MCUtils::PID::isHadron(parentID)) return false;

  // recursive call!
  if (MCUtils::PID::isTau(parentID)||parentID==ID) return notFromHadron(parent);

  // if we get here, all is good
  return true;
}

bool ZJets::isStable(const xAOD::TruthParticle *ptcl) {
  return ptcl->status() == 1 && ptcl->barcode() < 200000;
}

bool ZJets::isFromZ(const xAOD::TruthParticle *ptcl) {
  if (MCUtils::PID::isZ(ptcl->pdgId())) return true;
  if (ptcl->parent()==nullptr) return false;
  return isFromZ(ptcl->parent());
}

bool ZJets::isLepton(const xAOD::TruthParticle *ptcl) {
  return MCUtils::PID::isElectron(ptcl->pdgId()) || MCUtils::PID::isMuon(ptcl->pdgId());
}

bool ZJets::isZdecayLepton(const xAOD::TruthParticle *ptcl) {
  return isStable(ptcl) && isLepton(ptcl) && isFromZ(ptcl);
}

bool ZJets::isGoodTruthElectron(const xAOD::TruthParticle *ptcl) {
  return isStable(ptcl) && MCUtils::PID::isElectron(ptcl->pdgId()) && notFromHadron(ptcl);
}

bool ZJets::isGoodTruthMuon(const xAOD::TruthParticle *ptcl) {
  return isStable(ptcl) && MCUtils::PID::isMuon(ptcl->pdgId()) && notFromHadron(ptcl);
}

bool ZJets::isFiducialElectron(const TLV &e) {
  double aeta = std::abs(e.Eta());
  //return e.Pt()>25e3 && aeta<2.5;
  return e.Pt()>25e3 && aeta<2.5 && !(1.37<aeta&&aeta<1.52);
}

bool ZJets::isFiducialMuon(const TLV &mu) {
  return mu.Pt()>25e3 && std::abs(mu.Eta())<2.5;
}

bool ZJets::isFiducialLepton(const Lepton &l) {
  if (MCUtils::PID::isElectron(l.pdgId))
    return isFiducialElectron(l);
  if (MCUtils::PID::isMuon(l.pdgId))
    return isFiducialMuon(l);
  fatal(Form("Input particle to isFiducialLepton has pdgId = %i",l.pdgId));
  return false;
}

ZJets::Leptons ZJets::getDressedLeptons(const xAOD::TruthParticleContainer *truthLeptons) {
  ZJets::Leptons leps;
  for (const auto *l:*truthLeptons) {
    if (MCUtils::PID::isHadron(l->auxdata<int>("motherID"))) continue;

    ZJets::TLV p4;
    p4.SetPtEtaPhiE(l->auxdata<float>("pt_dressed"),
		    l->auxdata<float>("eta_dressed"),
		    l->auxdata<float>("phi_dressed"),
		    l->auxdata<float>("e_dressed"));
    ZJets::Lepton p(p4,l->pdgId(),(p4.Pt()-l->pt())/l->pt(),l->auxdata<int>("nPhotons_dressed"));
    leps.push_back(p);
  }

  std::sort(leps.begin(),leps.end(),pTorder);
  return leps;
}

//ZJets::TruthParticleStruct ZJets::identifyTruthParticles(xAOD::TEvent *event) {
//  using namespace ZJets;
//  ZJets::TruthParticleStruct ptcls;
//  TString tjetColl="AntiKt4TruthJets";
//
//  const xAOD::TruthParticleContainer *truthParticles = nullptr;
//  if ( event->retrieve(truthParticles, "TruthParticles").isFailure() )
//    ZJets::fatal("Cannot access TruthParticles");
//
//  const xAOD::JetContainer *truthJets = nullptr;
//  if ( event->retrieve(truthJets, tjetColl.Data()).isFailure() )
//    ZJets::fatal("Cannot access "+tjetColl);
//
//  for (const xAOD::TruthParticle *ptcl : *truthParticles)
//    if ( MCUtils::PID::isZ(ptcl->pdgId()) && ptcl->status()==62 )
//      add(ptcls.Zs,ptcl);
//  int NZs = ptcls.Zs.size();
//
//  for (const xAOD::TruthParticle *ptcl : *truthParticles) {
//    int id = ptcl->pdgId();
//    if ( isStable(ptcl) && notFromHadron(ptcl) ) {
//      if ( MCUtils::PID::isPhoton(id) )
//          add(ptcls.gams,ptcl);
//
//      if ( MCUtils::PID::isElectron(id) || MCUtils::PID::isMuon(id) ) {
//        if ( isFromZ(ptcl) )
//          add(ptcls.Zleptons,ptcl);
//
//        if ( NZs==0 || isFromZ(ptcl) || std::abs(ptcl->eta())>5 )
//          add(ptcls.leptons,ptcl);
//      }
//    }
//  }
//  for (auto lep:ptcls.leptons) {
//    add(ptcls.dressedLeptons,Lepton(lep->p4(),lep->pdgId()));
//  }
//  std::sort(ptcls.dressedLeptons.begin(),ptcls.dressedLeptons.end(),pTorder);
//  if (ptcls.dressedLeptons.size()>=2&&ptcls.dressedLeptons[0].Pt()<ptcls.dressedLeptons[1].Pt())
//    fatal("sorting failed!!");
//
//  ptcls.NdressingLep1=ptcls.NdressingLep2=-99;
//  ptcls.fracDressingLep1=ptcls.fracDressingLep2=-99;
//  for (size_t i=0;i<ptcls.dressedLeptons.size();++i) {
//    TLV &l=ptcls.dressedLeptons[i];
//    double pT=l.Pt(); int N=0;
//    for (auto gam:ptcls.gams)
//      if (gam->p4().DeltaR(l)<0.1) {
//        l+=gam->p4();
//        ++N;
//      }
//    if (i==0) { ptcls.NdressingLep1 = N; ptcls.fracDressingLep1 = l.Pt()/pT-1.0; }
//    if (i==1) { ptcls.NdressingLep2 = N; ptcls.fracDressingLep2 = l.Pt()/pT-1.0; }
//  }
//  std::sort(ptcls.dressedLeptons.begin(),ptcls.dressedLeptons.end(),pTorder);
//
//  for (auto jet:*truthJets) {
//    TLV j = jet->p4(); j*=invGeV;
//    bool isLepton = false;
//    for (auto lep:ptcls.dressedLeptons){
//      if (DRrap(j,lep)<0.2)
//        isLepton=true;
//    }
//    if (isLepton) continue;
//    if (j.Pt()<30) continue;
//    if (std::abs(j.Rapidity()) > 2.5) continue;
//    add(ptcls.jets30gev,j);
//  }
//
//  bool jetMatched = false;
//  for (auto lep:ptcls.dressedLeptons)
//    if (isFiducialLepton(lep)){
//      jetMatched = false;
//      for (auto j : ptcls.jets30gev) {
//        // Sliding cone OR recommendation
//        double dR=0.04 + 10/(lep.Pt()*1E-3); // 10 and lep pT are in GeV
//        if (dR > 0.4) dR = 0.4;
//        if (DRrap(j,lep)<dR) jetMatched = true;
//      }
//      if (!jetMatched) add(ptcls.fidLeptons,lep);
//    }
//
//  for (auto j : ptcls.jets30gev)
//    if (j.Pt() >= 60)
//      add(ptcls.jets,j);
//
//  return ptcls;
//}





ZJets::TruthParticleStruct ZJets::identifyTruthParticles(xAOD::TEvent *event) {
  using namespace ZJets;
  ZJets::TruthParticleStruct ptcls;
  std::vector<const xAOD::TruthParticle*> TruthLeptons;
  const xAOD::TruthParticleContainer* truthMuons = 0;
  const xAOD::TruthParticleContainer* truthElecs = 0;
  const xAOD::JetContainer* truthJets = 0;
  event->retrieve( truthMuons, "STDMTruthMuons");
  event->retrieve( truthElecs, "STDMTruthElectrons");
  // event->retrieve( truthJets, "AntiKt4TruthWZJets");
  event->retrieve( truthJets, "AntiKt4TruthJets");
  uint particle_type;
  uint particle_origin;
  float pt, E, eta, phi;
  TLV lep;

  // Muons
  for (auto muon : *truthMuons){
    particle_type=0;
    particle_origin=0;
    pt=0;
    E=0;
    eta=0;
    phi=0;
    particle_type = muon->auxdata<uint>("classifierParticleType");
    particle_origin = muon->auxdata<uint>("classifierParticleOrigin");
    pt = muon->auxdata<float>("pt_dressed");
    E = muon->auxdata<float>("e_dressed");
    eta = muon->auxdata<float>("eta_dressed");
    phi = muon->auxdata<float>("phi_dressed");

    if (particle_type == 6 && particle_origin == 13){ // iso Muon && from Z
      if (pt>25e3 && std::abs(eta) < 2.4){
        lep.SetPtEtaPhiE(pt,eta,phi,E);
        add(ptcls.dressedLeptons,Lepton(lep,muon->pdgId()));
      }
    }
  } // End Muons
  // Electrons
  for (auto elec : *truthElecs){
    particle_type=0;
    particle_origin=0;
    pt=0;
    E=0;
    eta=0;
    phi=0;
    particle_type = elec->auxdata<uint>("classifierParticleType");
    particle_origin = elec->auxdata<uint>("classifierParticleOrigin");
    pt = elec->auxdata<float>("pt_dressed");
    E = elec->auxdata<float>("e_dressed");
    eta = elec->auxdata<float>("eta_dressed");
    phi = elec->auxdata<float>("phi_dressed");

    if (particle_type == 2 && particle_origin == 13){ // iso Electron && from Z
      if (pt>25e3 && std::abs(eta) < 2.47){
        lep.SetPtEtaPhiE(pt,eta,phi,E);
        add(ptcls.dressedLeptons,Lepton(lep,elec->pdgId()));
      }
    }
  } // End Electrons
  // Jets
  for (auto jet : *truthJets){
    TLV j = jet->p4();
    j*=invGeV;
    bool isLepton = false;
    // OR with 30 GeV jets
    if (j.Pt()>30 && std::abs(j.Rapidity()) < 2.5){
      for (auto lep : ptcls.dressedLeptons)
        if (DRrap(j,lep) < 0.2) isLepton = true;
      if (!isLepton) add(ptcls.jets,j);
      // else std::cout << "Cleaning jets!" << std::endl;
    }
  } // End Jets
  // We need to remove lepton matched with jets
  std::sort(ptcls.dressedLeptons.begin(),ptcls.dressedLeptons.end(),pTorder);
  bool jetMatched = false;
  for (auto lep : ptcls.dressedLeptons){
    jetMatched = false;
    for (auto j : ptcls.jets) {
      double dR=0.4;
      // Sliding cone OR recommendation
      // double dR=0.04 + 10/(lep.Pt()*1E-3); // 10 and lep pT are in GeV
      // if (dR > 0.4) dR = 0.4;
      if (DRrap(j,lep)<dR) jetMatched = true;
    }
    if (!jetMatched) add(ptcls.fidLeptons,lep);
  }
  return ptcls;
}
