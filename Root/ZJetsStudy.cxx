#include <zjets-analysis/ZJetsStudy.h>
#include <PATInterfaces/SystematicsUtil.h>

using namespace ZJets;

// this is needed to distribute the algorithm to the workers
ClassImp (ZJetsStudy)

ZJetsStudy::ZJetsStudy()
{
}

EL::StatusCode ZJetsStudy::initialize() {
  ANA_CHECK_SET_TYPE(StatusCode);
  ANA_CHECK(xAOD::Init());

  xAOD::TEvent* event = wk()->xaodEvent();

  Info("initialize()", "Number of Events = %lli", event->getEntries());

  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(event->retrieve(eventInfo, "EventInfo"));

  m_isMC = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);
  m_channel = m_config.getInt("Channel");
  m_doPFlow = m_config.getStr("JetAlg") == "PFlow" ? true : false;
  //Initialize all tools
  m_eventTools = new ZJetseventTools("ZJetseventTools", m_isMC);
  ANA_CHECK(m_eventTools->Initialize(m_config));
  m_jetTools   = new ZJetsjetTools("ZJetsjetTools", m_isMC);
  ANA_CHECK(m_jetTools->Initialize(m_config));
  m_muTools    = new ZJetsmuonTools("ZJetsmuonTools", m_isMC);
  ANA_CHECK(m_muTools->Initialize(m_config));
  m_elTools    = new ZJetselecTools("ZJetselecTools", m_isMC);
  ANA_CHECK(m_elTools->Initialize(m_config));
  ANA_CHECK( InitializeOR()); // OverlapRemoval OR

  m_finalMuons = new std::vector<xAOD::Muon*>;
  m_finalElectrons = new std::vector<xAOD::Electron*>;

  // Systematics
  //const CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
  //const CP::SystematicSet& recommendedSystematics = registry.recommendedSystematics();
  //m_sysList = CP::make_systematics_vector(recommendedSystematics);
  CP::SystematicSet LeadingSysts;
  // Muon Smearing Systematics
  LeadingSysts.insert(CP::SystematicVariation("MUON_ID",-1));
  LeadingSysts.insert(CP::SystematicVariation("MUON_ID",+1));
  LeadingSysts.insert(CP::SystematicVariation("MUON_MS",-1));
  LeadingSysts.insert(CP::SystematicVariation("MUON_MS",+1));
  LeadingSysts.insert(CP::SystematicVariation("MUON_SCALE",-1));
  LeadingSysts.insert(CP::SystematicVariation("MUON_SCALE",+1));
  // 10 systematics
  //LeadingSysts.insert(CP::SystematicVariation("EG_SCALE_ALL",+1));
  //LeadingSysts.insert(CP::SystematicVariation("EG_RESOLUTION_ALL",+1));
  //LeadingSysts.insert(CP::SystematicVariation("ET_JER_EffectiveNP_2",+1));
  //LeadingSysts.insert(CP::SystematicVariation("ET_JER_EffectiveNP_3",+1));
  //LeadingSysts.insert(CP::SystematicVariation("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR",+1));
  //LeadingSysts.insert(CP::SystematicVariation("EG_SCALE_ALL",-1));
  //LeadingSysts.insert(CP::SystematicVariation("JET_EffectiveNP_Detector2",-1));
  //LeadingSysts.insert(CP::SystematicVariation("JET_EffectiveNP_Mixed1",-1));
  //LeadingSysts.insert(CP::SystematicVariation("JET_JER_EffectiveNP_9",-1));
  //LeadingSysts.insert(CP::SystematicVariation("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR",-1));
  // 8 systematics +-1 sigma
  //LeadingSysts.insert(CP::SystematicVariation("EG_SCALE_ALL",-1));
  //LeadingSysts.insert(CP::SystematicVariation("EG_SCALE_ALL",+1));
  //LeadingSysts.insert(CP::SystematicVariation("EG_RESOLUTION_ALL",-1));
  //LeadingSysts.insert(CP::SystematicVariation("EG_RESOLUTION_ALL",+1));
  //LeadingSysts.insert(CP::SystematicVariation("ET_JER_EffectiveNP_2",-1));
  //LeadingSysts.insert(CP::SystematicVariation("ET_JER_EffectiveNP_2",+1));
  //LeadingSysts.insert(CP::SystematicVariation("ET_JER_EffectiveNP_3",-1));
  //LeadingSysts.insert(CP::SystematicVariation("ET_JER_EffectiveNP_3",+1));
  //LeadingSysts.insert(CP::SystematicVariation("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR",-1));
  //LeadingSysts.insert(CP::SystematicVariation("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR",+1));
  //LeadingSysts.insert(CP::SystematicVariation("JET_EffectiveNP_Detector2",-1));
  //LeadingSysts.insert(CP::SystematicVariation("JET_EffectiveNP_Detector2",+1));
  //LeadingSysts.insert(CP::SystematicVariation("JET_EffectiveNP_Mixed1",-1));
  //LeadingSysts.insert(CP::SystematicVariation("JET_EffectiveNP_Mixed1",+1));
  //LeadingSysts.insert(CP::SystematicVariation("JET_JER_EffectiveNP_9",-1));
  //LeadingSysts.insert(CP::SystematicVariation("JET_JER_EffectiveNP_9",+1));
  m_sysList = CP::make_systematics_vector(LeadingSysts);
  std::cout << "ALEXANDRE # of sys = " << m_sysList.size() << std::endl;
  int SysNumber = 0;
  for(auto& systematic : m_sysList){
    std::string sname = systematic.name();
    ANA_MSG_INFO("Systematic # " << SysNumber << ": "<<sname);
    SysNumber++;
  }
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsStudy::execute() {

  ANA_CHECK_SET_TYPE(StatusCode);

  xAOD::TEvent* event = wk()->xaodEvent();
  //xAOD::TStore* store = wk()->xaodStore();

  static int Nevts = 0;
  if ((++Nevts % 1000) == 0) Info("execute()", "Processed %5i events", Nevts);

  bool SaveEvent = true;

  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(event->retrieve(eventInfo, "EventInfo"));
  isTruth = false;
  if (m_isMC) {
    m_truthPtcls = identifyTruthParticles(event);
    ANA_CHECK(FillTruthBranches());
  }

  EvtNbr = eventInfo->eventNumber();
  RunNbr = eventInfo->runNumber();

  //Check for duplicate event
  const std::pair<UInt_t, UInt_t> runEvent = std::make_pair(RunNbr, EvtNbr);

  m_w = m_isMC ? eventInfo->mcEventWeight() : 1.0;

  //pileup reweighting
  m_pileupW = 1.0;
  if (m_isMC) {
    ANA_CHECK(m_eventTools->m_PileupReweightingTool->apply(*eventInfo));
    m_pileupW = eventInfo->auxdecor<float>("PileupWeight");
  }
  m_w *= m_pileupW;

  // MC weight times PU weight
  m_initialW = m_w;

  // Cut 1: Duplicate events
  if (m_processedEvents.insert(runEvent).second == false) {
    return EL::StatusCode::SUCCESS;
  }
  m_cutFlow_weighted->Fill(10, m_initialW);
  m_cutFlow_nominal->Fill(10);


  m_mu = m_eventTools->GetCorrectedMu(eventInfo);

  // Cut 2: GRL
  if ( !m_isMC && !m_eventTools->PassRunLB(eventInfo))
    return EL::StatusCode::SUCCESS;
  m_cutFlow_weighted->Fill(11, m_initialW);
  m_cutFlow_nominal->Fill(11);

  // Find the PV
  const xAOD::VertexContainer* pVertices = NULL;
  ANA_CHECK(event->retrieve(pVertices, "PrimaryVertices"));

  const xAOD::Vertex *vtxHS = nullptr;
  for (auto vtx:*pVertices) {
    const auto vtxType = vtx->vertexType();
    if ( vtxType == xAOD::VxType::PriVtx || vtxType == xAOD::VxType::PileUp ) {
      if (vtxHS) continue;
      if (vtxType==xAOD::VxType::PriVtx) vtxHS = vtx;
    }
  }

  if (vtxHS!=nullptr){
    m_cutFlow_weighted->Fill(12, m_initialW);
    m_cutFlow_nominal->Fill(12);
  }
  else SaveEvent = false;

  // Cut 4: Event (Jets) cleaning

  if (!m_eventTools->PassEventCleaning(eventInfo))
    return EL::StatusCode::SUCCESS;
  if(!eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad"))
    SaveEvent = false;
  else{
    m_cutFlow_weighted->Fill(13, m_initialW);
    m_cutFlow_nominal->Fill(13);
  }


  // Cut 5: Trigger Decisions
  m_pass_trigger = false;
  m_pass_trigger =  m_eventTools->PassTrigger();
  bool passElecTrigger = m_eventTools->PassElecTrigger();
  bool passMuonTrigger = m_eventTools->PassMuonTrigger();

  if (m_pass_trigger){
    m_cutFlow_weighted->Fill(14, m_initialW);
    m_cutFlow_nominal->Fill(14);
  }
  else SaveEvent = false;

  if (!SaveEvent){ // If event didnt pass any of the early selection
    if (isTruth) ZjTree[0]->Fill(); // If Truth Z save truth
    return EL::StatusCode::SUCCESS; // Didnt pass, next event
  }

  // if (m_isMC){
  //   const std::vector< float > weights = eventInfo->mcEventWeights();
  //   int size = weights.size();
  //   std::cout << "ALEXANDRE WEIGHTS SIZE= " << weights.size() << std::endl;
  //   if (weights[0] != eventInfo->mcEventWeight()) std::cout << "ALEX WRONG WEIGHTS!!!!!" << std::endl;
  //   std::cout << "ALEX5 = " << weights[5] << std::endl;
  //   std::cout << "ALEX6 = " << weights[6] << std::endl;
  //   std::cout << "ALEX193 = " << weights[193] << std::endl;
  //   std::cout << "ALEX194 = " << weights[194] << std::endl;
  //   std::cout << "ALEX198 = " << weights[198] << std::endl;
  //   std::cout << "ALEX199 = " << weights[199] << std::endl;
  // }
  //   std::cout << "===== " << std::endl;
  // for (auto weight : m_weightTool->getWeightNames()) {
  //   std::cout << weight << std::endl;
    //if (weight==" muR = 0.5, muF = 0.5 ") std::cout << "ALEX ISR 05-05 WEIGHTS5 = " << m_weightTool->getWeight(weight) << std::endl;
    //if (weight==" muR = 2.0, muF = 2.0 ") std::cout << "ALEX ISR 20-20 WEIGHTS6 = " << m_weightTool->getWeight(weight) << std::endl;
    //if (weight=="Var3cUp") std::cout << "ALEX ISR Var3cUp WEIGHTS193 = " << m_weightTool->getWeight(weight) << std::endl;
    //if (weight=="Var3cDown") std::cout << "ALEX ISR Var3cDown WEIGHTS194 = " << m_weightTool->getWeight(weight) << std::endl;
    //if (weight=="isr:muRfac=1.0_fsr:muRfac=2.0") std::cout << "ALEX FSR 10-20 WEIGHTS = " << m_weightTool->getWeight(weight) << std::endl;
    //if (weight=="isr:muRfac=1.0_fsr:muRfac=0.5") std::cout << "ALEX FSR 10-05 WEIGHTS = " << m_weightTool->getWeight(weight) << std::endl;
    //if (weight==" muR = 0.5, muF = 1.0 ") std::cout << "ALEX Top 05-10 WEIGHTS = " << m_weightTool->getWeight(weight) << std::endl;
    //if (weight==" muR = 2.0, muF = 1.0 ") std::cout << "ALEX Top 20-10 WEIGHTS = " << m_weightTool->getWeight(weight) << std::endl;
    //if (m_weightTool->hasWeight(weight)) std::cout << "ALEX WEIGHT  = " << m_weightTool->getWeight(weight) << std::endl;
    //my_histogram_map[weight].Fill(my_variable, m_weightTool->getWeight(weight));
  // }
    // std::cout << "---- " << std::endl;
  // return EL::StatusCode::SUCCESS;


  // Retrieve original container of Jets, Muon, Electrons;
  const xAOD::JetContainer* jets;
  const xAOD::MuonContainer* muons;
  const xAOD::ElectronContainer* electrons;
  ANA_CHECK(event->retrieve(jets, m_jetTools->m_jetCollectionName.Data()));
  ANA_CHECK(event->retrieve(muons, "Muons"));
  ANA_CHECK(event->retrieve(electrons, "Electrons"));
  // Need these two for OR. Since not using, keep them empty
  const xAOD::TauJetContainer* taus = nullptr;
  const xAOD::PhotonContainer* photons = nullptr;

  // Flag for OR
  const ort::inputAccessor_t selAcc(m_flags.inputLabel);
  const ort::inputDecorator_t selDec(m_flags.inputLabel);
  const ort::outputAccessor_t overlapAcc(m_flags.outputLabel);

  // Systematics loop starts here
  int sys=0;
  for (auto& systematic : m_sysList){
    //std::cout << "systematic: " << systematic.name() << std::endl;
    sys++;
    if (sys!=1){
      m_truth_lep_E.clear();
      m_truth_lep_pT.clear();
      m_truth_lep_px.clear();
      m_truth_lep_py.clear();
      m_truth_lep_pz.clear();
      m_truth_lep_id.clear();
      m_truth_jet_E.clear();
      m_truth_jet_pT.clear();
      m_truth_jet_px.clear();
      m_truth_jet_py.clear();
      m_truth_jet_pz.clear();
      m_NTruthJets=0;
    }
    //if (sys>1) continue;
    bool saveEvent = true;
    m_isZEvent = false;

    isDiMuon=false; isDiElec=false; isElMu=false;

    m_Nmuons = 0;
    m_Nelecs = 0;
    m_Njets = 0;
    m_trigger_matched = false;
    m_lep1TotalSF = 1.;
    m_lep2TotalSF = 1.;
    m_trigSF = 1.;
    m_jvtSF = 1.;
    m_fjvtSF = 1.;

//    // Reset all containers for this systematic. Not memory efficient but cannot fail.
    m_jets.clear();
    m_lepton1.Clear();
    m_lepton2.Clear();
    m_lepton1ID=0;
    m_lepton2ID=0;
    m_finalElectrons->clear();
    m_finalMuons->clear();
    // Clear Tree values
    m_lep_E.clear();
    m_lep_pT.clear();
    m_lep_px.clear();
    m_lep_py.clear();
    m_lep_pz.clear();
    m_lep_id.clear();
    m_jet_E.clear();
    m_jet_pT.clear();
    m_jet_px.clear();
    m_jet_py.clear();
    m_jet_pz.clear();

    // Apply systematic variations!
    ANA_CHECK(m_jetTools->ApplySystematicVariation(systematic));
    ANA_CHECK(m_elTools->ApplySystematicVariation(systematic) );
    ANA_CHECK(m_muTools->ApplySystematicVariation(systematic) );
    ANA_CHECK(m_eventTools->m_PileupReweightingTool->applySystematicVariation(systematic));

    //Lepton selection
    int ecount1=0,ecount2=0,ecount3=0;
    int mcount1=0,mcount2=0,mcount3=0;

    //Calibrate jets and fill m_allJets
    std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer(*jets);
    for (auto jet: *jets_shallowCopy.first){
      ANA_CHECK(m_jetTools->m_jetCalibTool->applyCorrection(*jet));
      // In original code, we do applyCalibration.
      //ANA_CHECK(m_jetTools->m_jetCalibTool->applyCalibration(*jet));
      ANA_CHECK(m_jetTools->m_jetUncertainties->applyCorrection(*jet));
      // This would be JetPreSelection
      if ( fabs(jet->rapidity())<m_jetTools->m_rapidityCut && jet->pt() > m_jetTools->m_pTCut)
        selDec(*jet) = true;
      else
        selDec(*jet) = false;
    }
    //Calibrate leptons and fill m_allMuons and m_allElectrons
    // Electrons
    int Nelec=0;
    std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > elecs_shallowCopy = xAOD::shallowCopyContainer(*electrons);
    for (auto elec: *elecs_shallowCopy.first){
      ANA_CHECK(m_elTools->m_elecCalibTool->applyCorrection(*elec));
      // Electron Selection
      selDec(*elec) = false;
      // New, messier method. But more compatible with 100+ systematics
      double absEta = fabs(elec->eta());
      // Kinematic Selection : pT < 25GeV; eta < 2.47
      if (elec->pt()<m_elTools->m_pTCut || absEta > m_elTools->m_MaxAbsEta) continue;
      // Good Electron
      if( !elec->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON) ) continue;
      // In crack
      if( absEta > m_elTools->m_BarrelMaxAbsEta && absEta < m_elTools->m_EndcapMinAbsEta ) continue;
      ecount1++;
      // Pass electron ID likelihood
      if(!elec->auxdata<char>(m_elTools->m_electronLikelihoodWP.Data())) continue;
      ecount2++;
      if (!m_elTools->m_isoSelectionTool->accept(*elec)) continue;
      ecount3++;
      // Track-to-vertex association
      const xAOD::TrackParticle *eltrk = elec->trackParticle();
      float el_d0sig = 10., el_deltaz0 = 10., el_deltaz0new = 10.;
      if( eltrk ){
        el_d0sig = xAOD::TrackingHelpers::d0significance(eltrk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
        el_deltaz0 = fabs(eltrk->z0() + eltrk->vz() - vtxHS->z());
        el_deltaz0new = el_deltaz0 * sin(eltrk->theta());
      }
      // Pass vertex selection
      if( !( fabs(el_d0sig) < m_elTools->m_d0BySigd0Max && fabs(el_deltaz0new) < m_elTools->m_z0Max) ) continue;

      // Scale factors
      double elrecosf = 1.0;
      double elidsf = 1.0;
      double elisosf = 1.0;
      double eltrigsf = 1.0;
      double eltrigeff = 1.0;
      if(m_isMC) {
        if(m_elTools->m_elecRecoSFTool->getEfficiencyScaleFactor(*elec,elrecosf) != CP::CorrectionCode::Ok){ continue; }
        if(m_elTools->m_elecIdSFTool->getEfficiencyScaleFactor(*elec,elidsf) != CP::CorrectionCode::Ok){ continue; }
        if(m_elTools->m_elecIsoSFTool->getEfficiencyScaleFactor(*elec,elisosf) != CP::CorrectionCode::Ok){ continue; }
        if(m_elTools->m_doElecTriggerSF){
          if(m_elTools->m_elecTrigSFTool->getEfficiencyScaleFactor(*elec,eltrigsf) != CP::CorrectionCode::Ok){ continue; }
          if(m_elTools->m_elecTrigMCEffTool->getEfficiencyScaleFactor(*elec,eltrigeff) != CP::CorrectionCode::Ok){ continue; }
        }
      }
      elec->auxdecor<double>("elRecoSF") = elrecosf;
      elec->auxdecor<double>("elIDSF") = elidsf;
      elec->auxdecor<double>("elIsoSF") = elisosf;
      elec->auxdecor<double>("elTrigSF") = eltrigsf;
      elec->auxdecor<double>("elTrigEff") = eltrigeff;
      selDec(*elec) = true; // An electron to be used for OR
      Nelec++;
    }


    // Muons
    int Nmuon=0;
    std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer(*muons);
    for (auto muon: *muons_shallowCopy.first){
      selDec(*muon) = false;
      if (muon->pt() <10000. || fabs(muon->eta()) > 2.4) continue;
      ANA_CHECK(m_muTools->m_muonCalibrationAndSmearingTool->applyCorrection(*muon));
      double absEta = fabs(muon->eta());
      if (muon->pt() < m_muTools->m_pTCut || absEta > m_muTools->m_MaxAbsEta) continue;
      mcount1++;
      // Pass acceptance
      if ( !m_muTools->m_muonSelectionTool->accept(muon) ) continue;
      mcount2++;
      // Pass Isolation
      if ( !m_muTools->m_isoSelectionTool->accept(*muon) ) continue;
      mcount3++;
      // Track-to-vertex association
      const xAOD::TrackParticle *mutrk = muon->primaryTrackParticle();
      float mu_d0sig = 10. , mu_deltaz0=10., mu_deltaz0new=10.;
      if(mutrk){
          mu_d0sig = xAOD::TrackingHelpers::d0significance( mutrk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
          mu_deltaz0 = fabs(mutrk->z0() + mutrk->vz() - vtxHS->z());
          mu_deltaz0new = mu_deltaz0 * sin(mutrk->theta());
      }
      // Pass vertex association
      if (!( fabs(mu_d0sig) < m_muTools->m_d0BySigd0Max && fabs(mu_deltaz0new) < m_muTools->m_z0Max) ) continue;
      // Scale factors
      float muonIdSF = 1.0;
      float muonIsoSF = 1.0;
      float muonTTVASF = 1.0;
      if(m_isMC) {
        ANA_CHECK(m_muTools->m_muonRecoSFTool->getEfficiencyScaleFactor(*muon, muonIdSF));
        ANA_CHECK(m_muTools->m_muonIsoSFTool->getEfficiencyScaleFactor(*muon, muonIsoSF));
        ANA_CHECK(m_muTools->m_muonTTVASFTool->getEfficiencyScaleFactor(*muon, muonTTVASF));
      }
      muon->auxdata<float>("muIDSF") = muonIdSF;
      muon->auxdata<float>("muIsoSF") = muonIsoSF;
      muon->auxdata<float>("muTTVASF") = muonTTVASF;
      selDec(*muon) = true; // A muon to be used for OR
      Nmuon++;
    }

    if (sys==1){
      if (ecount1 > 1){
        m_cutFlow_weighted->Fill(15, m_initialW);
        m_cutFlow_nominal->Fill(15);
        if (ecount2 > 1){
          m_cutFlow_weighted->Fill(16, m_initialW);
          m_cutFlow_nominal->Fill(16);
          if (ecount3 > 1){
            m_cutFlow_weighted->Fill(17, m_initialW);
            m_cutFlow_nominal->Fill(17);
          }
        }
      }
      if (mcount1 > 1){
        m_cutFlow_weighted->Fill(18, m_initialW);
        m_cutFlow_nominal->Fill(18);
        if (mcount2 > 1){
          m_cutFlow_weighted->Fill(19, m_initialW);
          m_cutFlow_nominal->Fill(19);
          if (mcount3 > 1){
            m_cutFlow_weighted->Fill(20, m_initialW);
            m_cutFlow_nominal->Fill(20);
          }
        }
      }
      // at least 2 final leptons before OR
      if (Nmuon + Nelec > 1) {
        m_cutFlow_weighted->Fill(21, m_initialW);
        m_cutFlow_nominal->Fill(21);
      }
    }
    // If not in dilepton phase space
    if (Nmuon + Nelec < 2)
      saveEvent = false;
    if (!saveEvent) {
      delete jets_shallowCopy.first;
      delete jets_shallowCopy.second;
      delete elecs_shallowCopy.first;
      delete elecs_shallowCopy.second;
      delete muons_shallowCopy.first;
      delete muons_shallowCopy.second;
      if (!isTruth)
        continue;
    }
    if (saveEvent){
      // OverlapRemoval OR
      ANA_CHECK(m_toolBox.masterTool->removeOverlaps(elecs_shallowCopy.first,muons_shallowCopy.first,jets_shallowCopy.first,taus,photons));

      // Loop over Jet, electron and Muon
      // Save the ones that passed all the selection and then OR
      TLV part;
      // Loop and keep jets after selection,OR and JVTSelection
      // Dump loop is needed to get JVT SF
      // Needs to be ran on a full container AFTER OR but before JVT selection
      // So I create and erase a temporary container with only the OR jets
      if(m_isMC){
        auto jetsTemp = new xAOD::JetContainer();
        auto jetsTempAux = std::make_unique<xAOD::AuxContainerBase>();
        jetsTemp->setStore(jetsTempAux.get());
        for (auto obj : *jets_shallowCopy.first){
          if(selAcc(*obj))
            if(overlapAcc(*obj) == false){
              xAOD::Jet* tempJet = new xAOD::Jet();
              jetsTemp->push_back(tempJet);
              *tempJet = *obj;
            }
        }
        float sf;
        ANA_CHECK(m_jetTools->m_jvtEffTool->applyAllEfficiencyScaleFactor(jetsTemp, sf));
        m_jetTools->m_jvtSF = sf;
        delete jetsTemp;
      }
      for (auto obj : *jets_shallowCopy.first){
        if(selAcc(*obj))
          if(overlapAcc(*obj) == false){
            // This is JetJVTSelection
            if (!m_jetTools->m_jvtEffTool->passesJvtCut(*obj)) continue;
            part.SetPtEtaPhiE(obj->pt()*invGeV, obj->eta(), obj->phi(), obj->e()*invGeV);
            m_jets.push_back(part);
          }
      }
      delete jets_shallowCopy.first;
      delete jets_shallowCopy.second;
      std::sort(m_jets.begin(),m_jets.end(), DescendingPtTLV());
      // Loop and keep electrons after selection and OR
      for (auto obj : *elecs_shallowCopy.first)
        if(selAcc(*obj))
          if(overlapAcc(*obj) == false)
            m_finalElectrons->push_back(obj);
      m_Nelecs=m_finalElectrons->size();
      // Loop and keep Muons after selection and OR
      for (auto obj : *muons_shallowCopy.first)
        if(selAcc(*obj))
          if(overlapAcc(*obj) == false)
            m_finalMuons->push_back(obj);
      m_Nmuons=m_finalMuons->size();

      // Does nothign for now
      //if(m_isMC)
      //  m_jetTools->GetJVTscaleFactors(m_jvtSF, m_fjvtSF);

      //m_w *= m_jvtSF * m_fjvtSF;

      // Finished event cleaning
      if (m_Nelecs + m_Nmuons > 1 && sys==1) {
        m_cutFlow_weighted->Fill(22, m_initialW);
        m_cutFlow_nominal->Fill(22);
        // Dummy
        m_cutFlow_weighted->Fill(23, m_initialW);
        m_cutFlow_nominal->Fill(23);
      }
      if (m_Nelecs + m_Nmuons !=2) {
        delete muons_shallowCopy.first;
        delete muons_shallowCopy.second;
        delete elecs_shallowCopy.first;
        delete elecs_shallowCopy.second;
        saveEvent = false;
      }
    }
    if (!saveEvent && !isTruth) continue;

    if (saveEvent){
      double mu1IdSf=0;
      double mu2IdSf=0;
      double mu1IsoSf=0;
      double mu2IsoSf=0;
      double mu1TVVASf=0;
      double mu2TVVASf=0;
      double el1recosf =0;
      double el1idsf = 0;
      double el1isosf =0;
      double el2recosf =0;
      double el2idsf = 0;
      double el2isosf =0;
      // Trigger SF & Efficiencies
      m_trigger_matched = false;
      if (m_Nmuons == 2 && m_Nelecs == 0) {
        auto muon1 = *(m_finalMuons->at(0));
        auto muon2 = *(m_finalMuons->at(1));
        m_trigger_matched = m_eventTools->TrigMatchMuon(muon1, muon2);

        mu1IdSf = muon1.auxdata<float>("muIDSF");
        mu2IdSf = muon2.auxdata<float>("muIDSF");
        mu1IsoSf = muon1.auxdata<float>("muIsoSF");
        mu2IsoSf = muon2.auxdata<float>("muIsoSF");
        mu1TVVASf = muon1.auxdata<float>("muTTVASF");
        mu2TVVASf = muon2.auxdata<float>("muTTVASF");

        m_lep1TotalSF = mu1IdSf * mu1IsoSf * mu1TVVASf;
        m_lep2TotalSF = mu2IdSf * mu2IsoSf * mu2TVVASf;

        if(m_isMC)
          m_trigSF = m_muTools->GetMuonTriggerEfficiency(m_finalMuons);
        // DiMuon Selection. 2 oppositly charged muons with no electrons & pass trigger
        isDiMuon = ((muon1.charge() + muon2.charge() ==0) && passMuonTrigger && m_trigger_matched);
        if (isDiMuon){
          m_lepton1.SetPtEtaPhiE(muon1.pt()*invGeV, muon1.eta(), muon1.phi(), muon1.e()*invGeV);
          m_lepton2.SetPtEtaPhiE(muon2.pt()*invGeV, muon2.eta(), muon2.phi(), muon2.e()*invGeV);
          m_lepton1ID= muon1.charge() < 0 ? 13 : -13;
          m_lepton2ID= muon2.charge() < 0 ? 13 : -13;
        }
      }
      else if (m_Nmuons == 0 && m_Nelecs == 2) {
        auto electron1 = *(m_finalElectrons->at(0));
        auto electron2 = *(m_finalElectrons->at(1));
        m_trigger_matched = m_eventTools->TrigMatchElectron(electron1, electron2);

        el1recosf = electron1.auxdata<double>("elRecoSF");
        el1idsf = electron1.auxdata<double>("elIDSF");
        el1isosf = electron1.auxdata<double>("elIsoSF");
        el2recosf = electron2.auxdata<double>("elRecoSF");
        el2idsf = electron2.auxdata<double>("elIDSF");
        el2isosf = electron2.auxdata<double>("elIsoSF");

        m_lep1TotalSF = el1recosf * el1idsf * el1isosf;
        m_lep2TotalSF = el2recosf * el2idsf * el2isosf;

        double el1trigsf = electron1.auxdata<double>("elTrigSF");
        double el2trigsf = electron2.auxdata<double>("elTrigSF");
        m_trigSF = el1trigsf * el2trigsf;

        // DiElec Selection. 2 oppositly charged electrons with no muonss & pass trigger
        isDiElec = ((electron1.charge() + electron2.charge() ==0) && passElecTrigger && m_trigger_matched);
        if (isDiElec){
          m_lepton1.SetPtEtaPhiE(electron1.pt()*invGeV, electron1.eta(), electron1.phi(), electron1.e()*invGeV);
          m_lepton2.SetPtEtaPhiE(electron2.pt()*invGeV, electron2.eta(), electron2.phi(), electron2.e()*invGeV);
          m_lepton1ID= electron1.charge() < 0 ? 11 : -11;
          m_lepton2ID= electron2.charge() < 0 ? 11 : -11;
        }
      }
      //bool doCR = false;
      bool doCR = true;
      if (doCR)
      if (m_Nmuons == 1 && m_Nelecs == 1) { // 1 Elec 1 Muon CR?
        auto muon1 = *(m_finalMuons->at(0));
        auto elec1 = *(m_finalElectrons->at(0));

        el1recosf = elec1.auxdata<double>("elRecoSF");
        el1idsf = elec1.auxdata<double>("elIDSF");
        el1isosf = elec1.auxdata<double>("elIsoSF");
        mu1IdSf = muon1.auxdata<float>("muIDSF");
        mu1IsoSf = muon1.auxdata<float>("muIsoSF");
        mu1TVVASf = muon1.auxdata<float>("muTTVASF");

        m_lep1TotalSF = mu1IdSf * mu1IsoSf * mu1TVVASf;
        m_lep2TotalSF = el1recosf * el1idsf * el1isosf;


        // El + mu & pass muon trigger
        isElMu = passMuonTrigger;
        bool matchMuon= m_eventTools->TrigMatchMuon(muon1,muon1);
        bool matchElec= m_eventTools->TrigMatchElectron(elec1,elec1);
        if (isElMu && (matchMuon || matchElec)){
          m_lepton1.SetPtEtaPhiE(muon1.pt()*invGeV, muon1.eta(), muon1.phi(), muon1.e()*invGeV);
          m_lepton2.SetPtEtaPhiE(elec1.pt()*invGeV, elec1.eta(), elec1.phi(), elec1.e()*invGeV);
          m_lepton1ID= muon1.charge() < 0 ? 13 : -13;
          m_lepton2ID= elec1.charge() < 0 ? 11 : -11;
        }
      }
      delete muons_shallowCopy.first;
      delete muons_shallowCopy.second;
      delete elecs_shallowCopy.first;
      delete elecs_shallowCopy.second;

      m_w *= m_lep1TotalSF * m_lep2TotalSF * m_trigSF;
    }

    // Final Event selection
    bool isDilepton = false;
    isDilepton = (isDiMuon || isDiElec || isElMu);

    bool fillTree = false;
    if (!isDilepton) saveEvent = false;
    if ((saveEvent && isDilepton) || isTruth) fillTree = true;
    TLV zboson = m_lepton1 + m_lepton2;
    // If Dilepton mass in Z range
    if (zboson.M() > m_mmin && zboson.M() < m_mmax)
      m_isZEvent = true;

    // Force Z mass between 50 and 160GeV
    if (fillTree && (zboson.M() > 50 && zboson.M() < 160)) {
      // Loop over Jets & Save Jets over 60GeV pT
      for (auto jet : m_jets){
        if (jet.Pt() > 60){
          m_jet_E.push_back(jet.E());
          m_jet_pT.push_back(jet.Pt());
          m_jet_px.push_back(jet.Px());
          m_jet_py.push_back(jet.Py());
          m_jet_pz.push_back(jet.Pz());
        }
      }
      // Save Leptons
      m_lep_E.push_back(m_lepton1.E());
      m_lep_pT.push_back(m_lepton1.Pt());
      m_lep_px.push_back(m_lepton1.Px());
      m_lep_py.push_back(m_lepton1.Py());
      m_lep_pz.push_back(m_lepton1.Pz());
      m_lep_E.push_back(m_lepton2.E());
      m_lep_pT.push_back(m_lepton2.Pt());
      m_lep_px.push_back(m_lepton2.Px());
      m_lep_py.push_back(m_lepton2.Py());
      m_lep_pz.push_back(m_lepton2.Pz());
      m_lep_id.push_back(m_lepton1ID);
      m_lep_id.push_back(m_lepton2ID);
      m_Njets = m_jet_E.size();

      ZjTree[sys-1]->Fill();
    }

    if (!saveEvent || !isDilepton) continue;

    // In Dilepton phase space
    if (sys==1){
      m_cutFlow_weighted->Fill(24, m_initialW);
      m_cutFlow_nominal->Fill(24);
      // Using final weight
      m_cutFlow_weighted->Fill(25, m_w);
      m_cutFlow_nominal->Fill(25);
    }

    if (!m_isZEvent) continue;


    if (sys==1) {
      // Using final weight
      // Z candidate
      m_cutFlow_weighted->Fill(26, m_w);
      m_cutFlow_nominal->Fill(26);
      // Z + > 1 Jet
      if (m_Njets >0){
        m_cutFlow_weighted->Fill(27, m_w);
        m_cutFlow_nominal->Fill(27);
      }
      // If Z + jet2
      if (m_Njets>1){
        m_cutFlow_weighted->Fill(28, m_w);
        m_cutFlow_nominal->Fill(28);
      }
    }
  }
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsStudy::fileExecute() {
  xAOD::TEvent* event = wk()->xaodEvent();

  ANA_CHECK(m_eventTools->FillSumOfWeights(event, m_sum_of_weights));

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsStudy::FillTruthBranches() {

  isTruth = false;
  m_truth_lep_E.clear();
  m_truth_lep_pT.clear();
  m_truth_lep_px.clear();
  m_truth_lep_py.clear();
  m_truth_lep_pz.clear();
  m_truth_lep_id.clear();
  m_truth_jet_E.clear();
  m_truth_jet_pT.clear();
  m_truth_jet_px.clear();
  m_truth_jet_py.clear();
  m_truth_jet_pz.clear();
  m_NTruthJets=0;

  int Nlep = m_truthPtcls.fidLeptons.size();
  if (Nlep!=2) return EL::StatusCode::SUCCESS;

  // Save stuff about lep 1
  auto lep1 = m_truthPtcls.fidLeptons[0]; // GeV transformation is done in truth calcs
  lep1 *= invGeV;

  // Getting here, we have two leptons
  auto lep2 = m_truthPtcls.fidLeptons[1]; // GeV transformation is done in truth calcs
  lep2 *= invGeV;

  // Dilepton selection first
  // 2 elecs or 2 muons
  if ( lep1.pdgId+lep2.pdgId != 0)
    return EL::StatusCode::SUCCESS;

  if (lep1.Pt()<lep2.Pt()) fatal("Truth leptons are not sorted!!");
  m_truth_lep_E.push_back(lep1.E());
  m_truth_lep_E.push_back(lep2.E());
  m_truth_lep_pT.push_back(lep1.Pt());
  m_truth_lep_pT.push_back(lep2.Pt());
  m_truth_lep_px.push_back(lep1.Px());
  m_truth_lep_px.push_back(lep2.Px());
  m_truth_lep_py.push_back(lep1.Py());
  m_truth_lep_py.push_back(lep2.Py());
  m_truth_lep_pz.push_back(lep1.Pz());
  m_truth_lep_pz.push_back(lep2.Pz());
  m_truth_lep_id.push_back(lep1.pdgId);
  m_truth_lep_id.push_back(lep2.pdgId);

  // At least 2 leptons
  m_cutFlow_weighted->Fill(3, m_initialW);
  m_cutFlow_nominal->Fill(3);

  TLorentzVector ll=lep1+lep2;
  // Born candidates?
  m_cutFlow_weighted->Fill(7, m_initialW);
  m_cutFlow_nominal->Fill(7);

  // Dilepton selection
  //if ( lep1.pdgId+lep2.pdgId != 0 || ll.M() < 71.0 || ll.M() > 111.0 || Nlep!=2 )
  if ( lep1.pdgId+lep2.pdgId != 0 || ll.M() < 50.0 || ll.M() > 160.0 || Nlep!=2 )
    return EL::StatusCode::SUCCESS;

  // Z candidate
  m_cutFlow_weighted->Fill(4, m_initialW);
  m_cutFlow_nominal->Fill(4);


  TLVs jets = m_truthPtcls.jets;

  for (auto jet : jets){
    if (jet.Pt()>60){ // Jets were transfored to GeV in truth info
      m_truth_jet_E.push_back(jet.E());
      m_truth_jet_pT.push_back(jet.Pt());
      m_truth_jet_px.push_back(jet.Px());
      m_truth_jet_py.push_back(jet.Py());
      m_truth_jet_pz.push_back(jet.Pz());
    }
  }

  m_NTruthJets=m_truth_jet_E.size();
  if (m_NTruthJets > 0){
    // At least 1 Jet
    m_cutFlow_weighted->Fill(5, m_initialW);
    m_cutFlow_nominal->Fill(5);
    // Born candidatres??
    m_cutFlow_weighted->Fill(8, m_initialW);
    m_cutFlow_nominal->Fill(8);
    // More than 1 jet
    if (m_NTruthJets>1){
      m_cutFlow_weighted->Fill(6, m_initialW);
      m_cutFlow_nominal->Fill(6);
      // Born candidatres??
      m_cutFlow_weighted->Fill(9, m_initialW);
      m_cutFlow_nominal->Fill(9);
    }
  }
  isTruth = true;
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsStudy::setupJob(EL::Job& job) {
  job.useXAOD();
  ANA_CHECK_SET_TYPE(EL::StatusCode);
  ANA_CHECK(xAOD::Init());
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsStudy::histInitialize() {
  char treename[10];
  for (Int_t i=0; i<nsys; i++) {
    sprintf(treename,"Ztree%d",i);
    ZjTree[i] = new TTree(treename,"ZJets_tree");

    ZjTree[i]->Branch("EventNumber",    &EvtNbr,         "EventNumber/I");
    ZjTree[i]->Branch("RunNumber",      &RunNbr,         "RunNumber/I");
    ZjTree[i]->Branch("initialWeight",  &m_initialW,     "initialWeight/F");
    ZjTree[i]->Branch("pileupWeight",   &m_pileupW,      "pileupWeight/F");
    ZjTree[i]->Branch("finalWeight",    &m_w,            "finalWeight/F");
    ZjTree[i]->Branch("PassTrig",       &m_pass_trigger,  "passTrigger/B");
    ZjTree[i]->Branch("trigSF",         &m_trigSF,       "trigSF/F");
    ZjTree[i]->Branch("lep1TotalSF",    &m_lep1TotalSF,  "lep1TotalSF/F");
    ZjTree[i]->Branch("lep2TotalSF",    &m_lep2TotalSF,  "lep2TotalSF/F");
    ZjTree[i]->Branch("jvtSF",          &m_jvtSF,        "jvtSF/F");
    ZjTree[i]->Branch("fjvtSF",         &m_fjvtSF,       "fjvtSF/F");
    ZjTree[i]->Branch("AverageMu",      &m_mu,           "AverageMu/F");
    ZjTree[i]->Branch("Njets",          &m_Njets);
    ZjTree[i]->Branch("isZEvent",       &m_isZEvent);
    // Leptons
    ZjTree[i]->Branch("lep_E",  &m_lep_E  );
    ZjTree[i]->Branch("lep_pT", &m_lep_pT );
    ZjTree[i]->Branch("lep_px", &m_lep_px );
    ZjTree[i]->Branch("lep_py", &m_lep_py );
    ZjTree[i]->Branch("lep_pz", &m_lep_pz );
    ZjTree[i]->Branch("lep_id", &m_lep_id );
    // Jets
    ZjTree[i]->Branch("jet_E",  &m_jet_E    );
    ZjTree[i]->Branch("jet_pT", &m_jet_pT   );
    ZjTree[i]->Branch("jet_px", &m_jet_px   );
    ZjTree[i]->Branch("jet_py", &m_jet_py   );
    ZjTree[i]->Branch("jet_pz", &m_jet_pz   );
    if (i==0){
      // Truth Leptons
      ZjTree[i]->Branch("truth_Njets",  &m_NTruthJets   );
      ZjTree[i]->Branch("truth_lep_E",  &m_truth_lep_E   );
      ZjTree[i]->Branch("truth_lep_pT", &m_truth_lep_pT  );
      ZjTree[i]->Branch("truth_lep_px", &m_truth_lep_px  );
      ZjTree[i]->Branch("truth_lep_py", &m_truth_lep_py  );
      ZjTree[i]->Branch("truth_lep_pz", &m_truth_lep_pz  );
      ZjTree[i]->Branch("truth_lep_id", &m_truth_lep_id  );
      // Truth Jets
      ZjTree[i]->Branch("truth_jet_E",  &m_truth_jet_E   );
      ZjTree[i]->Branch("truth_jet_pT", &m_truth_jet_pT  );
      ZjTree[i]->Branch("truth_jet_px", &m_truth_jet_px  );
      ZjTree[i]->Branch("truth_jet_py", &m_truth_jet_py  );
      ZjTree[i]->Branch("truth_jet_pz", &m_truth_jet_pz  );
    }
    wk()->addOutput(ZjTree[i]);

  }

  // CutFlow initialization
  m_cutFlow_weighted = new TH1F("CutFlow_weighted", "CutFlow_weighted", 40, -0.5, 39.5);
  m_cutFlow_nominal = new TH1F("CutFlow_nominal", "CutFlow_nominal", 40, -0.5, 39.5);
  int bin=0;
  for (TString lbl:{"T 2 lep, pT, eta","T DR(l,jets)", "T Exactly 2 lep","T 2e or 2mu","T M(ll)","T Z+jets","T Z+2jets","Born 2 lep", "Born M(ll)", "Born Z+Jets","All Evts","GRL","PriVtx exists","Core Flags", "Pass HLT", "2elects, pT, eta", "Elec ID", "Elec Isolation", "2muons, pT, eta", "Muon ID", "Muon Isolation", "2 leptons", "Lep-jet OR", "Exactly 2 lep", "Exactly 2 lep", "M(ll), OS", "Z+Jets", "Z+2Jets", "Z+b", "Z+2b"}) {
    m_cutFlow_weighted->GetXaxis() -> SetBinLabel(++bin,lbl);
    m_cutFlow_nominal->GetXaxis()  -> SetBinLabel(  bin,lbl);
  }

  m_sum_of_weights = new TH1F("sum_of_weights", "sum_of_weights", 6, -0.5, 5.5);
  m_sum_of_weights->GetXaxis()->SetBinLabel(1, "DxAOD");
  m_sum_of_weights->GetXaxis()->SetBinLabel(2, "DxAOD squared");
  m_sum_of_weights->GetXaxis()->SetBinLabel(3, "DxAOD events");
  m_sum_of_weights->GetXaxis()->SetBinLabel(4, "xAOD");
  m_sum_of_weights->GetXaxis()->SetBinLabel(5, "xAOD squared");
  m_sum_of_weights->GetXaxis()->SetBinLabel(6, "xAOD events");

  wk()->addOutput(m_cutFlow_weighted);
  wk()->addOutput(m_cutFlow_nominal);
  wk()->addOutput(m_sum_of_weights);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsStudy::InitializeOR() {
  m_flags.boostedLeptons = true;
  m_flags.doElectrons = true; // by default
  m_flags.doMuons = true; // by defauly
  m_flags.doJets = true; // by default
  m_flags.doTaus = false; // On by default. Should keep?
  m_flags.doPhotons = false; // On by default. Let's see how I deal with this.
  if (m_doPFlow) m_flags.doMuPFJetOR = true;
  ANA_CHECK( ORUtils::recommendedTools(m_flags, m_toolBox) );
  ANA_CHECK( m_toolBox.initialize() );

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsStudy::finalize() {
  delete m_finalMuons;
  delete m_finalElectrons;
  m_jets.clear();
  m_lepton1.Clear();
  m_lepton2.Clear();
  m_lep_E.clear();
  m_lep_pT.clear();
  m_lep_px.clear();
  m_lep_py.clear();
  m_lep_pz.clear();
  m_lep_id.clear();
  m_jet_E.clear();
  m_jet_pT.clear();
  m_jet_px.clear();
  m_jet_py.clear();
  m_jet_pz.clear();

  ANA_CHECK(m_eventTools->FinalizeTools());
  ANA_CHECK(m_jetTools->FinalizeTools()  );
  ANA_CHECK(m_muTools->FinalizeTools()   );
  ANA_CHECK(m_elTools->FinalizeTools()   );

  delete m_eventTools;
  delete m_jetTools;
  delete m_muTools;
  delete m_elTools;

  ANA_MSG_INFO ("Finalize: ");
  ANA_MSG_INFO ("-----------------------------------------------------");
  ANA_MSG_INFO ("Cutflow Reco");
  ANA_MSG_INFO ("All events:      " << m_cutFlow_nominal->GetBinContent(11));
  ANA_MSG_INFO ("Pass GRL:        " << m_cutFlow_nominal->GetBinContent(12));
  ANA_MSG_INFO ("Primary vertex:  " << m_cutFlow_nominal->GetBinContent(13));
  ANA_MSG_INFO ("Core flags:      " << m_cutFlow_nominal->GetBinContent(14));
  ANA_MSG_INFO ("Pass HLT         " << m_cutFlow_nominal->GetBinContent(15));
  ANA_MSG_INFO ("2 elecs, pT,eta: " << m_cutFlow_nominal->GetBinContent(16));
  ANA_MSG_INFO ("Elec ID:         " << m_cutFlow_nominal->GetBinContent(17));
  ANA_MSG_INFO ("Elec isolation:  " << m_cutFlow_nominal->GetBinContent(18));
  ANA_MSG_INFO ("2 muons, pT,eta: " << m_cutFlow_nominal->GetBinContent(19));
  ANA_MSG_INFO ("Muon ID:         " << m_cutFlow_nominal->GetBinContent(20));
  ANA_MSG_INFO ("Muons isolation: " << m_cutFlow_nominal->GetBinContent(21));
  ANA_MSG_INFO ("2 leptons        " << m_cutFlow_nominal->GetBinContent(22));
  ANA_MSG_INFO ("Lep-jet OR:      " << m_cutFlow_nominal->GetBinContent(23));
  ANA_MSG_INFO ("Exactly 2 lep:   " << m_cutFlow_nominal->GetBinContent(25));
  ANA_MSG_INFO ("Exactly 2 lep:   " << m_cutFlow_nominal->GetBinContent(26));
  ANA_MSG_INFO ("M(ll), OS        " << m_cutFlow_nominal->GetBinContent(27));
  ANA_MSG_INFO ("Z+jets           " << m_cutFlow_nominal->GetBinContent(28));
  ANA_MSG_INFO ("Z+2jets           " << m_cutFlow_nominal->GetBinContent(29));
  ANA_MSG_INFO ("----------------");
  ANA_MSG_INFO ("Cutflow Truth");
  ANA_MSG_INFO ("2e or 2mu:       " << m_cutFlow_nominal->GetBinContent(4));
  ANA_MSG_INFO ("M(ll):           " << m_cutFlow_nominal->GetBinContent(5));
  ANA_MSG_INFO ("Z+jets:          " << m_cutFlow_nominal->GetBinContent(6));
  ANA_MSG_INFO ("Z+2jets:         " << m_cutFlow_nominal->GetBinContent(7));

  return EL::StatusCode::SUCCESS;
}
