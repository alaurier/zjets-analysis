#include <zjets-analysis/ZJetsExtraSysts.h>
#include <PATInterfaces/SystematicsUtil.h>

using namespace ZJets;

// this is needed to distribute the algorithm to the workers
ClassImp (ZJetsExtraSysts)

ZJetsExtraSysts::ZJetsExtraSysts()
{
}

EL::StatusCode ZJetsExtraSysts::initialize() {
  ANA_CHECK_SET_TYPE(StatusCode);
  ANA_CHECK(xAOD::Init());

  xAOD::TEvent* event = wk()->xaodEvent();

  Info("initialize()", "Number of Events = %lli", event->getEntries());

  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(event->retrieve(eventInfo, "EventInfo"));

  m_isMC = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);
  //Initialize all tools
  m_eventTools = new ZJetseventTools("ZJetseventTools", m_isMC);
  ANA_CHECK(m_eventTools->Initialize(m_config));
  m_jetTools   = new ZJetsjetTools("ZJetsjetTools", m_isMC);
  ANA_CHECK(m_jetTools->Initialize(m_config));
  m_muTools    = new ZJetsmuonTools("ZJetsmuonTools", m_isMC);
  ANA_CHECK(m_muTools->Initialize(m_config));
  m_elTools    = new ZJetselecTools("ZJetselecTools", m_isMC);
  ANA_CHECK(m_elTools->Initialize(m_config));
  ANA_CHECK( InitializeOR()); // OverlapRemoval OR

  // PMG Truth Weight Tool
  if (m_isMC)
    ANA_CHECK( InitializeEventWeights() );


  m_finalMuons = new std::vector<xAOD::Muon*>;
  m_finalElectrons = new std::vector<xAOD::Electron*>;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsExtraSysts::execute() {

  ANA_CHECK_SET_TYPE(StatusCode);

  // Define what CR we are running
  // CR1 is for multijet background
  // We found the following works best:
  // For electron use CR3 : No sign requirement
  // For muon use CR2: same sign muons only!
  bool CR1 = false; // Drop muon isolation, elec ID, elec vtx cut, !elecIso
  bool CR2 = false; // CR1 + same sign
  bool CR3 = false; // CR1 + no sign requirement
  if (CR2 || CR3) CR1 = true; // if doing any CR, make sure CR1 is on.
  if (CR2 && CR3) fatal("Cant have both charge CRs turned on!");

  xAOD::TEvent* event = wk()->xaodEvent();
  //xAOD::TStore* store = wk()->xaodStore();

  static int Nevts = 0;
  if ((++Nevts % 1000) == 0) Info("execute()", "Processed %5i events", Nevts);

  bool SaveEvent = true;
  m_w=1.;
  m_pileupW=1.;
  m_initialW=1.;
  m_weight=1.;

  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(event->retrieve(eventInfo, "EventInfo"));

  EvtNbr = eventInfo->eventNumber();
  RunNbr = eventInfo->runNumber();

  //Check for duplicate event
  const std::pair<UInt_t, UInt_t> runEvent = std::make_pair(RunNbr, EvtNbr);

  m_w = m_isMC ? eventInfo->mcEventWeight() : 1.0;
  // m_initialW = m_w;


  //pileup reweighting
  m_pileupW = 1.0;
  if (m_isMC) {
    ANA_CHECK(m_eventTools->m_PileupReweightingTool->apply(*eventInfo));
    m_pileupW = eventInfo->auxdecor<float>("PileupWeight");
  }
  // Reweight problematic Sherpa weights.
  int channel = m_config.getInt("Channel",-1);
  if (m_isMC){
    if (channel == 2) // Sherpa
      if (std::abs(m_w) >=100) m_w = 1.;
  }

  // m_w *= m_pileupW;

  // MC weight times PU weight
  m_initialW = m_w * m_pileupW;

  // Get extra event weights for top modelling and PDF systematics.
  if (m_isMC){
    for (unsigned int i=0; i<vector_name_of_weights.size();i++){
      double weight = m_weightTool->getWeight(vector_name_of_weights[i]);
      // std::cout << "did it work? " << i << std::endl;
      vector_of_weights[i] = 0.; // Reinitialize this value just in case!
      vector_of_weights[i] = weight;
      vector_sum_of_weights[i] = vector_sum_of_weights[i] + weight;
    }
  }
  // std::cout << "event weight nominal = " << vector_of_weights[0] << std::endl;
  // std::cout << "nominal = " << m_w << std::endl;
  // if ((++Nevts % 1000) == 0) fatal("lol");
  // return EL::StatusCode::SUCCESS;


  // Cut 1: Duplicate events
  if (m_processedEvents.insert(runEvent).second == false) {
    return EL::StatusCode::SUCCESS;
  }
  m_cutFlow_weighted->Fill(10, m_initialW);
  m_cutFlow_nominal->Fill(10);

  m_mu = m_eventTools->GetCorrectedMu(eventInfo);

  // Cut 2: GRL
  if ( !m_isMC && !m_eventTools->PassRunLB(eventInfo))
    return EL::StatusCode::SUCCESS;
  m_cutFlow_weighted->Fill(11, m_initialW);
  m_cutFlow_nominal->Fill(11);

  // Find the PV
  const xAOD::VertexContainer* pVertices = NULL;
  ANA_CHECK(event->retrieve(pVertices, "PrimaryVertices"));

  const xAOD::Vertex *vtxHS = nullptr;
  for (auto vtx:*pVertices) {
    const auto vtxType = vtx->vertexType();
    if ( vtxType == xAOD::VxType::PriVtx || vtxType == xAOD::VxType::PileUp ) {
      if (vtxHS) continue;
      if (vtxType==xAOD::VxType::PriVtx) vtxHS = vtx;
    }
  }

  if (vtxHS!=nullptr){
    m_cutFlow_weighted->Fill(12, m_initialW);
    m_cutFlow_nominal->Fill(12);
  }
  else SaveEvent = false;

  // Cut 4: Event (Jets) cleaning

  if (!m_eventTools->PassEventCleaning(eventInfo))
    return EL::StatusCode::SUCCESS;
  if(!eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad"))
    SaveEvent = false;
  else{
    m_cutFlow_weighted->Fill(13, m_initialW);
    m_cutFlow_nominal->Fill(13);
  }


  // Cut 5: Trigger Decisions
  m_pass_trigger = false;
  m_pass_trigger =  m_eventTools->PassTrigger();
  bool passElecTrigger = m_eventTools->PassElecTrigger();
  bool passMuonTrigger = m_eventTools->PassMuonTrigger();
  bool passEMuTrigger  = m_eventTools->PassEMuTrigger();

  // If doesn't pass dilepton trigger, can it pass the single lepton trigger for e-mu CR?
  if (!m_pass_trigger) m_pass_trigger = passEMuTrigger;


  if (m_pass_trigger){
    m_cutFlow_weighted->Fill(14, m_initialW);
    m_cutFlow_nominal->Fill(14);
  }
  else SaveEvent = false;


  isTruth = false;
  if (m_isMC) {
    m_truthPtcls = identifyTruthParticles(event);
    ANA_CHECK(FillTruthVariables());
    if (isTruth) ANA_CHECK(FillTruthHistos()); // Dont save if duplicate or not GRL
  }

  if (!SaveEvent){ // If event didnt pass any of the early selection
    return EL::StatusCode::SUCCESS; // Didnt pass, next event
  }
  // return EL::StatusCode::SUCCESS; // Didnt pass, next event


  // Retrieve original container of Jets, Muon, Electrons;
  const xAOD::JetContainer* JETS;
  const xAOD::MuonContainer* muons;
  const xAOD::ElectronContainer* electrons;
  ANA_CHECK(event->retrieve(JETS, m_jetTools->m_jetCollectionName.Data()));
  ANA_CHECK(event->retrieve(muons, "Muons"));
  ANA_CHECK(event->retrieve(electrons, "Electrons"));
  // Need these two for OR. Since not using, keep them empty
  const xAOD::TauJetContainer* taus = nullptr;
  const xAOD::PhotonContainer* photons = nullptr;

  // Flag for OR
  const ort::inputAccessor_t selAcc(m_flags.inputLabel);
  const ort::inputDecorator_t selDec(m_flags.inputLabel);
  const ort::outputAccessor_t overlapAcc(m_flags.outputLabel);


  m_weight = m_w;

  bool saveEvent = true;
  m_isZEvent = false;

  isDiMuon=false; isDiElec=false; isElMu=false;

  m_Nmuons = 0;
  m_Nelecs = 0;
  int nJets = 0;
  double HT=0.;
  m_trigger_matched = false;
  m_lep1TotalSF = 1.;
  m_lep2TotalSF = 1.;
  m_trigSF = 1.;
  m_jvtSF = 1.;
  m_fjvtSF = 1.;

  // Reset all containers for this systematic. Not memory efficient but cannot fail.
  m_jets.clear();
  m_lepton1.Clear();
  m_lepton2.Clear();
  m_lepton1ID=0;
  m_lepton2ID=0;
  m_finalElectrons->clear();
  m_finalMuons->clear();

  //Lepton selection
  int ecount1=0,ecount2=0,ecount3=0;
  int mcount1=0,mcount2=0,mcount3=0;

  //Calibrate jets and fill m_allJets
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer(*JETS);
  for (auto jet: *jets_shallowCopy.first){
    ANA_CHECK(m_jetTools->m_jetCalibTool->applyCorrection(*jet));
    // In original code, we do applyCalibration.
    //ANA_CHECK(m_jetTools->m_jetCalibTool->applyCalibration(*jet));
    ANA_CHECK(m_jetTools->m_jetUncertainties->applyCorrection(*jet));
    // This would be JetPreSelection
    if ( fabs(jet->rapidity())<m_jetTools->m_rapidityCut && jet->pt() > m_jetTools->m_pTCut)
      selDec(*jet) = true;
    else
      selDec(*jet) = false;
  }
  //Calibrate leptons and fill m_allMuons and m_allElectrons
  // Electrons
  int Nelec=0;
  std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > elecs_shallowCopy = xAOD::shallowCopyContainer(*electrons);
  for (auto elec: *elecs_shallowCopy.first){
    ANA_CHECK(m_elTools->m_elecCalibTool->applyCorrection(*elec));
    // Electron Selection
    selDec(*elec) = false;
    // New, messier method. But more compatible with 100+ systematics
    double absEta = fabs(elec->eta());
    // Kinematic Selection : pT < 25GeV; eta < 2.47
    if (elec->pt()<m_elTools->m_pTCut || absEta > m_elTools->m_MaxAbsEta) continue;
    // Good Electron
    if( !elec->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON) ) continue;
    // In crack
    if( absEta > m_elTools->m_BarrelMaxAbsEta && absEta < m_elTools->m_EndcapMinAbsEta ) continue;
    ecount1++;


    if (CR1){ // Control region.
      // Drop elec the ID requirement
      // Anti-isolated electrons only : if it passes iso, skip it
      if (m_elTools->m_isoSelectionTool->accept(*elec)) continue;
    }
    else { // Normal search region
      // if elec doesn't pass ID, skip electron
      if(!elec->auxdata<char>(m_elTools->m_electronLikelihoodWP.Data())) continue;
      ecount2++;
      // if elec doesn't pass isolation requirement, skip electron
      if (!m_elTools->m_isoSelectionTool->accept(*elec)) continue;
    }
    ecount3++;
    // Track-to-vertex association
    const xAOD::TrackParticle *eltrk = elec->trackParticle();
    float el_d0sig = 10., el_deltaz0 = 10., el_deltaz0new = 10.;
    if( eltrk ){
      el_d0sig = xAOD::TrackingHelpers::d0significance(eltrk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
      el_deltaz0 = fabs(eltrk->z0() + eltrk->vz() - vtxHS->z());
      el_deltaz0new = el_deltaz0 * sin(eltrk->theta());
    }
    // Pass vertex selection
    bool passVertex = false;
    passVertex = ( fabs(el_d0sig) < m_elTools->m_d0BySigd0Max && fabs(el_deltaz0new) < m_elTools->m_z0Max );
    // If SR && didnt pass vertex selection, skip elec;
    if (!CR1 && !passVertex) continue;

    // Scale factors
    double elrecosf = 1.0;
    double elidsf = 1.0;
    double elisosf = 1.0;
    double eltrigsf = 1.0;
    double eltrigeff = 1.0;
    if(m_isMC) {
      if(m_elTools->m_elecRecoSFTool->getEfficiencyScaleFactor(*elec,elrecosf) != CP::CorrectionCode::Ok){ continue; }
      if(m_elTools->m_elecIdSFTool->getEfficiencyScaleFactor(*elec,elidsf) != CP::CorrectionCode::Ok){ continue; }
      if(m_elTools->m_elecIsoSFTool->getEfficiencyScaleFactor(*elec,elisosf) != CP::CorrectionCode::Ok){ continue; }
      if(m_elTools->m_doElecTriggerSF){
        if(m_elTools->m_elecTrigSFTool->getEfficiencyScaleFactor(*elec,eltrigsf) != CP::CorrectionCode::Ok){ continue; }
        if(m_elTools->m_elecTrigMCEffTool->getEfficiencyScaleFactor(*elec,eltrigeff) != CP::CorrectionCode::Ok){ continue; }
      }
    }
    elec->auxdecor<double>("elRecoSF") = elrecosf;
    elec->auxdecor<double>("elIDSF") = elidsf;
    elec->auxdecor<double>("elIsoSF") = elisosf;
    elec->auxdecor<double>("elTrigSF") = eltrigsf;
    elec->auxdecor<double>("elTrigEff") = eltrigeff;
    selDec(*elec) = true; // An electron to be used for OR
    Nelec++;
  }


  // Muons
  int Nmuon=0;
  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer(*muons);
  for (auto muon: *muons_shallowCopy.first){
    selDec(*muon) = false;
    if (muon->pt() <10000. || fabs(muon->eta()) > 2.4) continue;
    ANA_CHECK(m_muTools->m_muonCalibrationAndSmearingTool->applyCorrection(*muon));
    double absEta = fabs(muon->eta());
    if (muon->pt() < m_muTools->m_pTCut || absEta > m_muTools->m_MaxAbsEta) continue;
    mcount1++;
    // Pass acceptance
    if ( !m_muTools->m_muonSelectionTool->accept(muon) ) continue;
    mcount2++;
    // Pass Isolation
    if (!CR1) // if not Mjj background
      if ( !m_muTools->m_isoSelectionTool->accept(*muon) ) continue;
    mcount3++;
    // Track-to-vertex association
    const xAOD::TrackParticle *mutrk = muon->primaryTrackParticle();
    float mu_d0sig = 10. , mu_deltaz0=10., mu_deltaz0new=10.;
    if(mutrk){
        mu_d0sig = xAOD::TrackingHelpers::d0significance( mutrk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
        mu_deltaz0 = fabs(mutrk->z0() + mutrk->vz() - vtxHS->z());
        mu_deltaz0new = mu_deltaz0 * sin(mutrk->theta());
    }
    // Pass vertex association
    if (!( fabs(mu_d0sig) < m_muTools->m_d0BySigd0Max && fabs(mu_deltaz0new) < m_muTools->m_z0Max) ) continue;
    // Scale factors
    float muonIdSF = 1.0;
    float muonIsoSF = 1.0;
    float muonTTVASF = 1.0;
    if(m_isMC) {
      ANA_CHECK(m_muTools->m_muonRecoSFTool->getEfficiencyScaleFactor(*muon, muonIdSF));
      ANA_CHECK(m_muTools->m_muonIsoSFTool->getEfficiencyScaleFactor(*muon, muonIsoSF));
      ANA_CHECK(m_muTools->m_muonTTVASFTool->getEfficiencyScaleFactor(*muon, muonTTVASF));
    }
    muon->auxdata<float>("muIDSF") = muonIdSF;
    muon->auxdata<float>("muIsoSF") = muonIsoSF;
    muon->auxdata<float>("muTTVASF") = muonTTVASF;
    selDec(*muon) = true; // A muon to be used for OR
    Nmuon++;
  }

  if (ecount1 > 1){
    m_cutFlow_weighted->Fill(15, m_initialW);
    m_cutFlow_nominal->Fill(15);
    if (ecount2 > 1){
      m_cutFlow_weighted->Fill(16, m_initialW);
      m_cutFlow_nominal->Fill(16);
      if (ecount3 > 1){
        m_cutFlow_weighted->Fill(17, m_initialW);
        m_cutFlow_nominal->Fill(17);
      }
    }
  }
  if (mcount1 > 1){
    m_cutFlow_weighted->Fill(18, m_initialW);
    m_cutFlow_nominal->Fill(18);
    if (mcount2 > 1){
      m_cutFlow_weighted->Fill(19, m_initialW);
      m_cutFlow_nominal->Fill(19);
      if (mcount3 > 1){
        m_cutFlow_weighted->Fill(20, m_initialW);
        m_cutFlow_nominal->Fill(20);
      }
    }
  }
  // at least 2 final leptons before OR
  if (Nmuon + Nelec > 1) {
    m_cutFlow_weighted->Fill(21, m_initialW);
    m_cutFlow_nominal->Fill(21);
  }
  // If not in dilepton phase space
  if (Nmuon + Nelec < 2)
    saveEvent = false;
  if (!saveEvent) {
    delete jets_shallowCopy.first;
    delete jets_shallowCopy.second;
    delete elecs_shallowCopy.first;
    delete elecs_shallowCopy.second;
    delete muons_shallowCopy.first;
    delete muons_shallowCopy.second;
    return EL::StatusCode::SUCCESS;
  }
  if (saveEvent){
    // OverlapRemoval OR
    ANA_CHECK(m_toolBox.masterTool->removeOverlaps(elecs_shallowCopy.first,muons_shallowCopy.first,jets_shallowCopy.first,taus,photons));

    // Loop over Jet, electron and Muon
    // Save the ones that passed all the selection and then OR
    TLV part;
    // Loop and keep jets after selection,OR and JVTSelection
    // Dump loop is needed to get JVT SF
    // Needs to be ran on a full container AFTER OR but before JVT selection
    // So I create and erase a temporary container with only the OR jets
    if(m_isMC){
      auto jetsTemp = new xAOD::JetContainer();
      auto jetsTempAux = std::make_unique<xAOD::AuxContainerBase>();
      jetsTemp->setStore(jetsTempAux.get());
      for (auto obj : *jets_shallowCopy.first){
        if(selAcc(*obj))
          if(overlapAcc(*obj) == false){
            xAOD::Jet* tempJet = new xAOD::Jet();
            jetsTemp->push_back(tempJet);
            *tempJet = *obj;
          }
      }
      float sf;
      ANA_CHECK(m_jetTools->m_jvtEffTool->applyAllEfficiencyScaleFactor(jetsTemp, sf));
      m_jetTools->m_jvtSF = sf;
      delete jetsTemp;
    }
    for (auto obj : *jets_shallowCopy.first){
      if(selAcc(*obj))
        if(overlapAcc(*obj) == false){
          // This is JetJVTSelection
          if (!m_jetTools->m_jvtEffTool->passesJvtCut(*obj)) continue;
          part.SetPtEtaPhiE(obj->pt()*invGeV, obj->eta(), obj->phi(), obj->e()*invGeV);
          m_jets.push_back(part);
        }
    }
    delete jets_shallowCopy.first;
    delete jets_shallowCopy.second;
    std::sort(m_jets.begin(),m_jets.end(), DescendingPtTLV());
    // Loop and keep electrons after selection and OR
    for (auto obj : *elecs_shallowCopy.first)
      if(selAcc(*obj))
        if(overlapAcc(*obj) == false)
          m_finalElectrons->push_back(obj);
    m_Nelecs=m_finalElectrons->size();
    // Loop and keep Muons after selection and OR
    for (auto obj : *muons_shallowCopy.first)
      if(selAcc(*obj))
        if(overlapAcc(*obj) == false)
          m_finalMuons->push_back(obj);
    m_Nmuons=m_finalMuons->size();

    // Does nothign for now
    // if(m_isMC)
    //   m_jetTools->GetJVTscaleFactors(m_jvtSF, m_fjvtSF);

    // if (m_jvtSF !=0.) m_weight *= m_jvtSF;
    // if (m_fjvtSF !=0.) m_weight *= m_fjvtSF;

    // Finished event cleaning
    if (m_Nelecs + m_Nmuons > 1) {
      m_cutFlow_weighted->Fill(22, m_initialW);
      m_cutFlow_nominal->Fill(22);
      // Dummy
      m_cutFlow_weighted->Fill(23, m_initialW);
      m_cutFlow_nominal->Fill(23);
    }
    if (m_Nelecs + m_Nmuons !=2) {
      delete muons_shallowCopy.first;
      delete muons_shallowCopy.second;
      delete elecs_shallowCopy.first;
      delete elecs_shallowCopy.second;
      saveEvent = false;
    }
  }
  if (!saveEvent) return EL::StatusCode::SUCCESS;

  double mu1IdSf=0;
  double mu2IdSf=0;
  double mu1IsoSf=0;
  double mu2IsoSf=0;
  double mu1TVVASf=0;
  double mu2TVVASf=0;
  double el1recosf =0;
  double el1idsf = 0;
  double el1isosf =0;
  double el2recosf =0;
  double el2idsf = 0;
  double el2isosf =0;
  // Trigger SF & Efficiencies
  m_trigger_matched = false;
  if (m_Nmuons == 2 && m_Nelecs == 0) {
    auto muon1 = *(m_finalMuons->at(0));
    auto muon2 = *(m_finalMuons->at(1));
    m_trigger_matched = m_eventTools->TrigMatchMuon(muon1, muon2);

    mu1IdSf = muon1.auxdata<float>("muIDSF");
    mu2IdSf = muon2.auxdata<float>("muIDSF");
    mu1IsoSf = muon1.auxdata<float>("muIsoSF");
    mu2IsoSf = muon2.auxdata<float>("muIsoSF");
    mu1TVVASf = muon1.auxdata<float>("muTTVASF");
    mu2TVVASf = muon2.auxdata<float>("muTTVASF");

    m_lep1TotalSF = mu1IdSf * mu1IsoSf * mu1TVVASf;
    m_lep2TotalSF = mu2IdSf * mu2IsoSf * mu2TVVASf;

    if(m_isMC)
      m_trigSF = m_muTools->GetMuonTriggerEfficiency(m_finalMuons);
    // DiMuon Selection. 2 oppositly charged muons with no electrons & pass trigger
    isDiMuon = ((muon1.charge() + muon2.charge() ==0) && passMuonTrigger && m_trigger_matched);
    if (CR1)
      if (passMuonTrigger && m_trigger_matched) isDiMuon = true;
    if (isDiMuon){
      m_lepton1.SetPtEtaPhiE(muon1.pt()*invGeV, muon1.eta(), muon1.phi(), muon1.e()*invGeV);
      m_lepton2.SetPtEtaPhiE(muon2.pt()*invGeV, muon2.eta(), muon2.phi(), muon2.e()*invGeV);
      m_lepton1ID= muon1.charge() < 0 ? 13 : -13;
      m_lepton2ID= muon2.charge() < 0 ? 13 : -13;
    }
  }
  else if (m_Nmuons == 0 && m_Nelecs == 2) {
    auto electron1 = *(m_finalElectrons->at(0));
    auto electron2 = *(m_finalElectrons->at(1));
    m_trigger_matched = m_eventTools->TrigMatchElectron(electron1, electron2);

    el1recosf = electron1.auxdata<double>("elRecoSF");
    el1idsf = electron1.auxdata<double>("elIDSF");
    el1isosf = electron1.auxdata<double>("elIsoSF");
    el2recosf = electron2.auxdata<double>("elRecoSF");
    el2idsf = electron2.auxdata<double>("elIDSF");
    el2isosf = electron2.auxdata<double>("elIsoSF");

    m_lep1TotalSF = el1recosf * el1idsf * el1isosf;
    m_lep2TotalSF = el2recosf * el2idsf * el2isosf;

    double el1trigsf = electron1.auxdata<double>("elTrigSF");
    double el2trigsf = electron2.auxdata<double>("elTrigSF");
    m_trigSF = el1trigsf * el2trigsf;

    // DiElec Selection. 2 oppositly charged electrons with no muonss & pass trigger
    isDiElec = ((electron1.charge() + electron2.charge() ==0) && passElecTrigger && m_trigger_matched);
    if (CR1)
      if (passElecTrigger && m_trigger_matched) isDiElec = true;
    if (isDiElec){
      m_lepton1.SetPtEtaPhiE(electron1.pt()*invGeV, electron1.eta(), electron1.phi(), electron1.e()*invGeV);
      m_lepton2.SetPtEtaPhiE(electron2.pt()*invGeV, electron2.eta(), electron2.phi(), electron2.e()*invGeV);
      m_lepton1ID= electron1.charge() < 0 ? 11 : -11;
      m_lepton2ID= electron2.charge() < 0 ? 11 : -11;
    }
  }

  if (m_Nmuons == 1 && m_Nelecs == 1) { // 1 Elec 1 Muon CR?
    auto muon1 = *(m_finalMuons->at(0));
    auto elec1 = *(m_finalElectrons->at(0));

    el1recosf = elec1.auxdata<double>("elRecoSF");
    el1idsf = elec1.auxdata<double>("elIDSF");
    el1isosf = elec1.auxdata<double>("elIsoSF");
    mu1IdSf = muon1.auxdata<float>("muIDSF");
    mu1IsoSf = muon1.auxdata<float>("muIsoSF");
    mu1TVVASf = muon1.auxdata<float>("muTTVASF");

    m_lep1TotalSF = mu1IdSf * mu1IsoSf * mu1TVVASf;
    m_lep2TotalSF = el1recosf * el1idsf * el1isosf;


    // El + mu & pass muon trigger
    // isElMu = passEMuTrigger;
    bool matchMuon= m_eventTools->TrigMatchMuonEmu(muon1,muon1);
    bool matchElec= false;
    m_trigSF = 1.;
    // Trig matching is for muon trig matching only!
    // bool matchElec= m_eventTools->TrigMatchElectronEmu(elec1,elec1);
    if (passEMuTrigger && (matchMuon || matchElec)){
      if(m_isMC){
        m_trigSF = m_muTools->GetMuonTriggerEfficiencyEmu(m_finalMuons);
      }
      m_trigSF = m_trigSF * elec1.auxdata<double>("elTrigSF");
      // std::cout << "TRIG SF = " << m_trigSF * elec1.auxdata<double>("elTrigSF") << std::endl;
      m_lepton1.SetPtEtaPhiE(muon1.pt()*invGeV, muon1.eta(), muon1.phi(), muon1.e()*invGeV);
      m_lepton2.SetPtEtaPhiE(elec1.pt()*invGeV, elec1.eta(), elec1.phi(), elec1.e()*invGeV);
      m_lepton1ID= muon1.charge() < 0 ? 13 : -13;
      m_lepton2ID= elec1.charge() < 0 ? 11 : -11;
      isElMu = true;
    }
  }
  delete muons_shallowCopy.first;
  delete muons_shallowCopy.second;
  delete elecs_shallowCopy.first;
  delete elecs_shallowCopy.second;

  // m_weight *= m_lep1TotalSF * m_lep2TotalSF * m_trigSF;


  // Final Event selection
  bool isDilepton = false;
  isDilepton = (isDiMuon || isDiElec || isElMu);
  if (!isDilepton) return EL::StatusCode::SUCCESS;

  int rch=3;
  if (isDiElec) rch=0;
  if (isDiMuon) rch=1;
  if (isElMu)   rch=2;

  TLV zboson = m_lepton1 + m_lepton2;
  double lep1pt=0.,lep2pt=0;
  if (m_lepton1.Pt() < m_lepton2.Pt()){
    lep1pt = m_lepton2.Pt();
    lep2pt = m_lepton1.Pt();
  }
  else{
    lep1pt = m_lepton1.Pt();
    lep2pt = m_lepton2.Pt();
  }

  // If Dilepton mass in Z range
  if (zboson.M() > m_mmin && zboson.M() < m_mmax) // Between 71 and 111 GeV
    m_isZEvent = true;

  bool isGoodZ = false;
  bool isSelected = false;

  // opposite signed leptons requirement is forced here
  isGoodZ = rch < 3 && m_pass_trigger && m_lepton1.Pt()>25. && m_lepton2.Pt()>25. && fabs(m_lepton1.Eta())<2.4 &&  fabs(m_lepton2.Eta())<2.4 && m_lepton1ID*m_lepton2ID < 0;
  // if CR2, just require same sign
  if (CR2) isGoodZ = rch < 3 && m_pass_trigger && m_lepton1.Pt()>25. && m_lepton2.Pt()>25. && fabs(m_lepton1.Eta())<2.4 &&  fabs(m_lepton2.Eta())<2.4 && m_lepton1ID*m_lepton2ID > 0;
  // if CR3, drop sign requirements
  if (CR3) isGoodZ = rch < 3 && m_pass_trigger && m_lepton1.Pt()>25. && m_lepton2.Pt()>25. && fabs(m_lepton1.Eta())<2.4 &&  fabs(m_lepton2.Eta())<2.4;

  isSelected = isGoodZ && m_isZEvent;

  if (!isGoodZ) return EL::StatusCode::SUCCESS;
  std::vector<TLorentzVector> jets, loose_jets;
  TLorentzVector closest_jet, dijet;
  double mjj=0., dpjj=0., minDR=99.;
  float fzjbinr=0.;

  // Force Z mass between 50 and 160GeV
  if (zboson.M() > 50 && zboson.M() < 160) {
    // Loop over Jets & Save Jets over 60GeV pT
    for (auto jet : m_jets){
      if (jet.Pt() >= 60.){
        loose_jets.push_back(jet);
        if (jet.Pt() >= 100.)
          jets.push_back(jet);
      }
    }
  }
  else return EL::StatusCode::SUCCESS;
  nJets = jets.size();

  // Systematics loop starts here
  int sys=0;
  for (sys=1; sys<nsys+1; sys++){
    // sys++;
    // continue;

    double val = 999.;
    if (channel == 2){ // Sherpa signal only!
      val = vector_of_weights[sys-1]; // Nominal + 100 PDF variations + 2 alternate PDF + 2 alpha_S + 6 scale = 111
    }
    else if (channel == 4){ // ttbar
      if (sys < 106){ // Nominal + 100 PDF variations + 2 alternate PDF + 2 alpha_S = 105
        val = vector_of_weights[sys-1];
      }
      else if (sys == 106){ // ISR DOWN
        val = vector_of_weights[105]*vector_of_weights[106]/vector_of_weights[0];
      }
      else if (sys == 107){ // ISR UP
        val = vector_of_weights[107]*vector_of_weights[108]/vector_of_weights[0];
      }
      else if (sys == 108){ // FSR DOWN
        val = vector_of_weights[109];
      }
      else if (sys == 109){ // FSR UP
        val = vector_of_weights[110];
      }
      else val = 0.;
    }
    else fatal("Not Sherpa signal or ttbar!");

    if (val == 999.) fatal("Did not go into getting proper event weight!");
    m_weight = val * m_pileupW * m_lep1TotalSF * m_lep2TotalSF * m_trigSF;

    // Reco histograms && response matrix
    if (isSelected && rch < 2){ // elec or muon channel only!
      // Start with reco histograms first.
      if (loose_jets.size()!=0) // loose jets
        vhistr[1+(rch*nunf) + 2*nunf*(sys-1)]->Fill(loose_jets[0].Pt(),m_weight); // lead jet pT

      if (nJets>0){
        vhistr[0+(rch*nunf) + 2*nunf*(sys-1)]->Fill(zboson.Pt(),m_weight); // Z pT with jets >100GeV
        vhistr[12+(rch*nunf) + 2*nunf*(sys-1)]->Fill(lep1pt,m_weight); // leading lepton
        vhistr[13+(rch*nunf) + 2*nunf*(sys-1)]->Fill(lep2pt,m_weight); // 2nd lepton
        // Jet multiplicities, jet pt > 100 GeV
        for (int i = 0;i<nJets+1;i++)
          vhistr[8+(rch*nunf) + 2*nunf*(sys-1)]->Fill(float(i),m_weight);//incl jet multiplicity
        vhistr[9+(rch*nunf) + 2*nunf*(sys-1)]->Fill(nJets,m_weight);//excl jet multiplicity
        if (nJets>1){
          dijet=jets[0]+jets[1];
          mjj = dijet.M();
          vhistr[3+(rch*nunf) + 2*nunf*(sys-1)]->Fill(mjj,m_weight);
        }
      } // end NJets
      minDR=99.;
      HT=lep1pt+lep2pt;
      for (int i=0; i<nJets; i++){
        HT+=jets[i].Pt();
        float mindr = DRrap(jets[i],zboson);
        if(mindr < minDR) {
          closest_jet=jets[i];
          minDR = mindr;
        }
      }
      if (nJets>0){
        vhistr[2+(rch*nunf) + 2*nunf*(sys-1)]->Fill(HT,m_weight);

        if(jets[0].Pt()>500.){ // Requiring leading jet above 500GeV
          // Jet multiplicities, leading jet pt > 500 GeV
          for (int i = 0;i<nJets+1;i++)
          vhistr[10+(rch*nunf) + 2*nunf*(sys-1)]->Fill(float(i),m_weight);//incl jet multiplicity
          vhistr[11+(rch*nunf) + 2*nunf*(sys-1)]->Fill(nJets,m_weight);//excl jet multiplicity

          vhistr[4+(rch*nunf) + 2*nunf*(sys-1)]->Fill(minDR,m_weight);
          float fzjbin1 = 16.,fzjbin2=0.;
          fzjbinr=0.;
          for (int i=0;i<15;i++){
            if (zboson.Pt()/closest_jet.Pt()> zjbins[i] && zboson.Pt()/closest_jet.Pt()< zjbins[i+1]) fzjbin1 = float(i);
            for (int j=0;j<3;j++)
              if (closest_jet.Pt()> jbins[j] && closest_jet.Pt()< jbins[j+1]) fzjbin2 = float(j);
          }
          //0,1,2, ...20 --> 0,3,6,...60
          fzjbinr = fzjbin1*3.+fzjbin2;
          //cout<<"Test bin: "<<fzjbin1<<" = "<<fzjbin2<<" = "<<fzjbinr<<endl;
          vhistr[5+(rch*nunf) + 2*nunf*(sys-1)]->Fill(fzjbinr,m_weight);
          if(minDR<1.4) vhistr[6+(rch*nunf) + 2*nunf*(sys-1)]->Fill(fzjbinr,m_weight);
          else if(minDR>2.0) vhistr[7+(rch*nunf) + 2*nunf*(sys-1)]->Fill(fzjbinr,m_weight);
          else vhistr[14+(rch*nunf) + 2*nunf*(sys-1)]->Fill(fzjbinr,m_weight);
        } // end of high pT requirement
      }
      // Now look into response matrices
      if (isTruth && m_isSelectedTruth){ // This event also passed truth
        if (isSelected && tch == rch){ // Reco event passed && same channel
          // Start with jet pT with loose jets
          if (loose_jets.size()>0 && m_truth_loose_jets.size()>0)
            vhistm[1+(tch*nunf) + 2*nunf*(sys-1)]->Fill(loose_jets[0].Pt(),m_truth_loose_jets[0].Pt(),m_weight);
          // Real requirement of jet pT > 100GeV
          if (nJets > 0 && m_NTruthJets > 0){
            vhistm[9+(tch*nunf) + 2*nunf*(sys-1)]->Fill(nJets,m_NTruthJets,m_weight); // Exclusive nJets
            vhistm[0+(tch*nunf) + 2*nunf*(sys-1)]->Fill(zboson.Pt(),m_truthZ.Pt(),m_weight); // Z pT
            vhistm[12+(tch*nunf) + 2*nunf*(sys-1)]->Fill(lep1pt,m_truth_lep1.Pt(),m_weight); // lep 1 pT
            vhistm[13+(tch*nunf) + 2*nunf*(sys-1)]->Fill(lep2pt,m_truth_lep2.Pt(),m_weight); // lep 2 pT
            vhistm[2+(tch*nunf) + 2*nunf*(sys-1)]->Fill(HT,m_truth_HT,m_weight); // HT
            if(nJets>1 && m_NTruthJets>1)
              vhistm[3+(tch*nunf) + 2*nunf*(sys-1)]->Fill(mjj,m_truth_dijet.M(),m_weight);
            if (jets[0].Pt()> 500. && m_truth_jets[0].Pt()> 500.){
              vhistm[11+(tch*nunf) + 2*nunf*(sys-1)]->Fill(nJets,m_NTruthJets,m_weight); // Exclusive nJets
              vhistm[4+(tch*nunf) + 2*nunf*(sys-1)]->Fill(minDR,m_truth_minDR,m_weight);
              vhistm[5+(tch*nunf) + 2*nunf*(sys-1)]->Fill(fzjbinr,m_truth_fzjbin,m_weight);
              if(m_truth_minDR<1.4) vhistm[6+(tch*nunf) + 2*nunf*(sys-1)]->Fill(fzjbinr,m_truth_fzjbin,m_weight);
              else if(m_truth_minDR>2.0) vhistm[7+(tch*nunf) + 2*nunf*(sys-1)]->Fill(fzjbinr,m_truth_fzjbin,m_weight);
              else vhistm[14+(tch*nunf) + 2*nunf*(sys-1)]->Fill(fzjbinr,m_truth_fzjbin,m_weight);
            }
          }
        } // End of response matrix
      } // end of truth

    } // End of reco histograms + response matrix


    // Fill Control Plots
    if (sys>=1){
      HT=0;mjj=0;dpjj=0,minDR=99.;
      dijet.SetPtEtaPhiE(0.,0.,0.,0.);
      closest_jet.SetPtEtaPhiE(0.,0.,0.,0.);
      // Start with Good Z, off-peak
      vhist[1+(rch*npl)+ 3*npl*(sys-1)]->Fill(zboson.M(),m_weight); // Z mass
      if(nJets>0){ // Z mass with at least 1 jet > 100GeV
        vhist[10+(rch*npl) + 3*npl*(sys-1)]->Fill(zboson.M(),m_weight);
        if (jets[0].Pt()>=500.)
          vhist[37+(rch*npl)+ 3*npl*(sys-1)]->Fill(zboson.M(),m_weight); // Z mass, lead jet > 500GeV
      }
      if(nJets>=2)
        vhist[38+(rch*npl) + 3*npl*(sys-1)]->Fill(zboson.M(),m_weight);
      if(nJets>=3)
        vhist[39+(rch*npl) + 3*npl*(sys-1)]->Fill(zboson.M(),m_weight);


      if (zboson.Pt()<250.) vhist[32+(rch*npl)+ 3*npl*(sys-1)]->Fill(zboson.M(),m_weight); // Z Mass if Z pT < 250 GeV
      else if (zboson.Pt()>500.) vhist[34+(rch*npl)+ 3*npl*(sys-1)]->Fill(zboson.M(),m_weight); // Z Mass if Z pT > 500 GeV
      else vhist[33+(rch*npl)+ 3*npl*(sys-1)]->Fill(zboson.M(),m_weight); // Z Mass if Z pT between 250 and 500 GeV

      // Good Z && on Peak
      if (isSelected){
        for (int i = 0;i<nJets+1;i++){
          vhist[4+(rch*npl)+ 3*npl*(sys-1)]->Fill(float(i),m_weight);//incl jet multiplicity
          vhist[22+(rch*npl)+ 3*npl*(sys-1)]->Fill(float(i),m_weight);//incl jet multiplicity
        }
        vhist[24+(rch*npl)+ 3*npl*(sys-1)]->Fill(nJets,m_weight); // Exclusive jet multiplicity
        // no jet preselection
        vhist[3+(rch*npl)+ 3*npl*(sys-1)]->Fill(zboson.Pt(),m_weight); //incl Zpt
        vhist[0+(rch*npl)+ 3*npl*(sys-1)]->Fill(m_mu,m_weight);//inclusive average mu

        HT = lep1pt + lep2pt;
        for (int i = 0;i<nJets;i++){
          HT += jets[i].Pt();
          float mindr = DRrap(jets[i],zboson);
          if(mindr < minDR) {
            closest_jet = jets[i];
            minDR = mindr;
          }
        }

        if (nJets>0){ // require at least 1 100GeV jet.
          vhist[7+(rch*npl)+ 3*npl*(sys-1)]->Fill(zboson.Pt(),m_weight); // Z pT
          vhist[5+(rch*npl)+ 3*npl*(sys-1)]->Fill(jets[0].Pt(),m_weight); // lead jet pT
          vhist[6+(rch*npl)+ 3*npl*(sys-1)]->Fill(HT,m_weight); // HT
          vhist[35+(rch*npl)+ 3*npl*(sys-1)]->Fill(lep1pt,m_weight);
          vhist[36+(rch*npl)+ 3*npl*(sys-1)]->Fill(lep2pt,m_weight);

          if(nJets>1){ // at least 2 jets
            dijet = jets[0]+jets[1];
            mjj = dijet.M(); // m_jj
            dpjj = jets[0].DeltaPhi(jets[1]); // delta_phi jj
          }

          // Start of colinear pT requirement : lead jet > 500GeV
          // ALEXANDRE I HAVE NOT INCLDUED THE DR CUT HERE for the sake of having proper control plots
          if (jets[0].Pt()>=500.){
            for (int i = 0;i<nJets+1;i++)
              vhist[23+(rch*npl)+ 3*npl*(sys-1)]->Fill(float(i),m_weight);//incl jet multiplicity
            vhist[25+(rch*npl)+ 3*npl*(sys-1)]->Fill(nJets,m_weight); // exclusive jet mult
            vhist[17+(rch*npl)+ 3*npl*(sys-1)]->Fill(nJets,m_weight); // exclusive jet mult
            //closest jet pT and 2nd-jet pT
            vhist[18+(rch*npl)+ 3*npl*(sys-1)]->Fill(closest_jet.Pt(),m_weight); // closest jet
            if(nJets>1) vhist[19+(rch*npl)+ 3*npl*(sys-1)]->Fill(jets[1].Pt(),m_weight); // 2nd lead

            // DeltaR cuts/
            if(minDR<1.4)
              vhist[15+(rch*npl)+ 3*npl*(sys-1)]->Fill(nJets,m_weight);
            if(minDR>2.0)
              vhist[16+(rch*npl)+ 3*npl*(sys-1)]->Fill(nJets,m_weight);

            // key collinear plots
            double pTZJ=zboson.Pt()/closest_jet.Pt();
            vhist[2+(rch*npl)+ 3*npl*(sys-1)]->Fill(pTZJ,m_weight); // pT(Z) / pT(cl_jet)
            vhist[9+(rch*npl)+ 3*npl*(sys-1)]->Fill(minDR,m_weight); // minDR
            // Plots of pT ratio between many minDR regions
            // We defined colinear region as minDR < 1.4
            // And anti-colinear as minDR > 2.0
            if(minDR < 2.0){ // colinear
              vhist[11+(rch*npl)+ 3*npl*(sys-1)]->Fill(pTZJ,m_weight);
            }else
              vhist[12+(rch*npl)+ 3*npl*(sys-1)]->Fill(pTZJ,m_weight);
            if(minDR < 1.0){
              vhist[26+(rch*npl)+ 3*npl*(sys-1)]->Fill(pTZJ,m_weight);
            }else
              vhist[27+(rch*npl)+ 3*npl*(sys-1)]->Fill(pTZJ,m_weight);
            if(minDR < 1.2){
              vhist[28+(rch*npl)+ 3*npl*(sys-1)]->Fill(pTZJ,m_weight);
            }else
              vhist[29+(rch*npl)+ 3*npl*(sys-1)]->Fill(pTZJ,m_weight);
            if(minDR < 1.4){
              vhist[30+(rch*npl)+ 3*npl*(sys-1)]->Fill(pTZJ,m_weight);
            }else
              vhist[31+(rch*npl)+ 3*npl*(sys-1)]->Fill(pTZJ,m_weight);
            if(minDR>1.4&& minDR < 2.0)
              vhist[40+(rch*npl)+ 3*npl*(sys-1)]->Fill(pTZJ,m_weight);

            if(nJets>1){ // dijet phase space, with leading jet above 500
              vhist[8+(rch*npl)+ 3*npl*(sys-1)]->Fill(mjj,m_weight); // m_jj
              if(minDR<1.){ // colinear
                vhist[13+(rch*npl)+ 3*npl*(sys-1)]->Fill(mjj,m_weight); // colinear m_jj
                vhist[20+(rch*npl)+ 3*npl*(sys-1)]->Fill(dpjj,m_weight); // colinear dphi_jj
              }
              if (minDR>2.){ // anti-colinear
                vhist[14+(rch*npl)+ 3*npl*(sys-1)]->Fill(mjj,m_weight);
                vhist[21+(rch*npl)+ 3*npl*(sys-1)]->Fill(dpjj,m_weight);
              }
            }//2jets
          } // End of lead jet pT > 500
        } // end of 1 jet requirement
      } // End of Good on peak Z
      // Also fill the cut flow tables
      // In Dilepton phase space
      if (sys==135){
        m_cutFlow_weighted->Fill(24, m_initialW);
        m_cutFlow_nominal->Fill(24);
        // Using final weight
        m_cutFlow_weighted->Fill(25, m_weight);
        m_cutFlow_nominal->Fill(25);
        if (!m_isZEvent) continue; // Cut flow, with Z only
        // Using final weight
        // Z candidate
        m_cutFlow_weighted->Fill(26, m_weight);
        m_cutFlow_nominal->Fill(26);
        // Z + > 1 Jet
        if (nJets >0){
          m_cutFlow_weighted->Fill(27, m_weight);
          m_cutFlow_nominal->Fill(27);
        }
        // If Z + jet2
        if (nJets>1){
          m_cutFlow_weighted->Fill(28, m_weight);
          m_cutFlow_nominal->Fill(28);
        }
      }
    } // End of Control plots filling

  } // End of systematics loops
  // fatal("lol");
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsExtraSysts::fileExecute() {
  xAOD::TEvent* event = wk()->xaodEvent();

  ANA_CHECK(m_eventTools->FillSumOfWeights(event, m_sum_of_weights));

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsExtraSysts::FillTruthVariables() {

  isTruth = false;
  m_isSelectedTruth = false;
  tch = 3;
  m_truth_lep1.SetPxPyPzE(0,0,0,0);
  m_truth_lep2.SetPxPyPzE(0,0,0,0);
  m_truth_lep1ID=0;
  m_truth_lep2ID=0;
  m_truth_loose_jets.clear();
  m_truth_jets.clear();
  m_NTruthJets=0;

  int Nlep = m_truthPtcls.fidLeptons.size();
  if (Nlep!=2) return EL::StatusCode::SUCCESS;

  // Save stuff about lep 1
  auto lep1 = m_truthPtcls.fidLeptons[0]; // GeV transformation is done in truth calcs
  lep1 *= invGeV;

  // Getting here, we have two leptons
  auto lep2 = m_truthPtcls.fidLeptons[1]; // GeV transformation is done in truth calcs
  lep2 *= invGeV;

  // Dilepton selection first
  // 2 elecs or 2 muons
  if ( lep1.pdgId + lep2.pdgId != 0)
    return EL::StatusCode::SUCCESS;

  // if (lep1.Pt()<lep2.Pt()) fatal("Truth leptons are not sorted!!");
  m_truth_lep1.SetPxPyPzE(lep1.Px(),lep1.Py(),lep1.Pz(),lep1.E());
  m_truth_lep2.SetPxPyPzE(lep2.Px(),lep2.Py(),lep2.Pz(),lep2.E());
  m_truth_lep1ID=lep1.pdgId;
  m_truth_lep2ID=lep2.pdgId;

  if (lep1.Pt() < lep2.Pt()){
    std::cout << "WARNING::Truth leptons weren't ordered!" << std::endl;
    m_truth_lep1.SetPxPyPzE(lep2.Px(),lep2.Py(),lep2.Pz(),lep2.E());
    m_truth_lep2.SetPxPyPzE(lep1.Px(),lep1.Py(),lep1.Pz(),lep1.E());
    m_truth_lep1ID=lep2.pdgId;
    m_truth_lep2ID=lep1.pdgId;
  }

  // At least 2 leptons
  m_cutFlow_weighted->Fill(3, m_initialW);
  m_cutFlow_nominal->Fill(3);

  TLorentzVector ll=m_truth_lep1+m_truth_lep2;
  // Born candidates?
  m_cutFlow_weighted->Fill(7, m_initialW);
  m_cutFlow_nominal->Fill(7);

  // Dilepton selection
  // if ( lep1.pdgId+lep2.pdgId != 0 || ll.M() < 50.0 || ll.M() > 160.0 || Nlep!=2 )
  if ( lep1.pdgId+lep2.pdgId != 0 || ll.M() < 71.0 || ll.M() > 111.0 || Nlep!=2 )
    return EL::StatusCode::SUCCESS;

  // Z candidate
  m_cutFlow_weighted->Fill(4, m_initialW);
  m_cutFlow_nominal->Fill(4);


  TLVs jets = m_truthPtcls.jets;

  for (auto jet : jets){
    if (jet.Pt()>60){ // Jets were transfored to GeV in truth info
      TLorentzVector Jet;
      Jet.SetPxPyPzE(jet.Px(),jet.Py(),jet.Pz(),jet.E());
      m_truth_loose_jets.push_back(Jet);
      if (jet.Pt()>100)
        m_truth_jets.push_back(Jet);
    }
  }

  m_NTruthJets = m_truth_jets.size();
  if (m_NTruthJets > 1)
    if (m_truth_jets[0].Pt() < m_truth_jets[1].Pt()) fatal("Truth jets not ordered!");

  if (m_NTruthJets > 0){
    // At least 1 Jet
    m_cutFlow_weighted->Fill(5, m_initialW);
    m_cutFlow_nominal->Fill(5);
    // Born candidatres??
    m_cutFlow_weighted->Fill(8, m_initialW);
    m_cutFlow_nominal->Fill(8);
    // More than 1 jet
    if (m_NTruthJets>1){
      m_cutFlow_weighted->Fill(6, m_initialW);
      m_cutFlow_nominal->Fill(6);
      // Born candidatres??
      m_cutFlow_weighted->Fill(9, m_initialW);
      m_cutFlow_nominal->Fill(9);
    }
  }
  isTruth = true;
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsExtraSysts::FillTruthHistos() {

  if (!isTruth) return EL::StatusCode::SUCCESS;

  tch = 3;
  if (m_truth_lep1ID * m_truth_lep2ID >= 0) return EL::StatusCode::SUCCESS;
  if(std::abs(m_truth_lep1ID)==11 && std::abs(m_truth_lep2ID)==11) {
    tch = 0; // electron
  }else if (std::abs(m_truth_lep1ID)==13 && std::abs(m_truth_lep2ID)==13) {
    tch = 1; // muon
  }else if ((std::abs(m_truth_lep1ID)==11 && std::abs(m_truth_lep2ID)==13) || (std::abs(m_truth_lep1ID)==13 && std::abs(m_truth_lep2ID)==11)) {
    tch = 2; // e-mu
  }else{
    fatal("Warning: unexpected truth lepton combination!!");
  }

  bool truth_isGoodZ = false;
  m_isSelectedTruth = false;
  m_truth_HT=0;
  m_truth_dijet.SetPxPyPzE(0.,0.,0.,0.);
  m_truth_clJet.SetPxPyPzE(0.,0.,0.,0.);
  m_truthZ = m_truth_lep1 + m_truth_lep2;
  m_truth_minDR = 0.;


  truth_isGoodZ    = tch<2 && m_truth_lep1.Pt()>25. &&  m_truth_lep2.Pt()>25. && fabs(m_truth_lep1.Eta())<2.47 &&  fabs(m_truth_lep2.Eta())<2.47;
  m_isSelectedTruth = truth_isGoodZ && 71. < m_truthZ.M() &&  m_truthZ.M() < 111.;

  if (!m_isSelectedTruth) {
    m_truthZ.SetPxPyPzE(0.,0.,0.,0.);
    m_truth_lep1.SetPxPyPzE(0.,0.,0.,0.);
    m_truth_lep2.SetPxPyPzE(0.,0.,0.,0.);
    m_truth_lep1ID=0;
    m_truth_lep2ID=0;
    m_truth_loose_jets.clear();
    m_truth_jets.clear();
    m_NTruthJets=0;
    return EL::StatusCode::SUCCESS;
  }
  m_NTruthJets = m_truth_jets.size();
  m_truth_minDR = 99.;

  // Normal jets
  if (m_NTruthJets){ //  100pT Jet
    m_truth_HT = m_truth_lep1.Pt() + m_truth_lep2.Pt();
    if (m_NTruthJets>1){
      m_truth_dijet = m_truth_jets[0]+m_truth_jets[1];
    }
    for (int i=0; i<m_NTruthJets;i++){
      m_truth_HT += m_truth_jets[i].Pt();
      double minDR = DRrap(m_truth_jets[i],m_truthZ);
      if (minDR < m_truth_minDR){
        m_truth_minDR = minDR;
        m_truth_clJet = m_truth_jets[i];
      }
    }

    // Lead jet pT > 500 GeV
    if (m_truth_jets[0].Pt() >= 500.){

      // minDR
      if (m_truth_minDR==99) m_truth_minDR=0.;
      float fzjbin1 = 0.,fzjbin2=0.;
      float fzjbint=0.;
      for (int i=0;i<15;i++){
        if (m_truthZ.Pt()/m_truth_clJet.Pt()> zjbins[i] && m_truthZ.Pt()/m_truth_clJet.Pt()< zjbins[i+1]) fzjbin1 = float(i);
        for (int j=0;j<3;j++){
          if (m_truth_clJet.Pt()> jbins[j] && m_truth_clJet.Pt()< jbins[j+1]) fzjbin2 = float(j);
        }
      }
      fzjbint = fzjbin1*3.+fzjbin2;
      m_truth_fzjbin = fzjbint;
    }
  }

  bool doSherpa = true;
  if (doSherpa) // Sherpa signal only!
  for (int sys=1; sys<nsys+1; sys++){
    double newWeight = vector_of_weights[sys-1] * m_pileupW;

    // Loose Jet pT : 60 GeV and Higher.
    if (m_truth_loose_jets.size()>0)
      vhistt[1+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_truth_loose_jets[0].Pt(), newWeight);

    // Normal jets
    if (m_NTruthJets){ // 100pT Jet
      vhistt[0+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_truthZ.Pt(), newWeight); // Z pT
      vhistt[12+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_truth_lep1.Pt(), newWeight); // leading lepton
      vhistt[13+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_truth_lep2.Pt(), newWeight); // 2nd lepton
      if (m_NTruthJets>1){
        vhistt[3+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_truth_dijet.M(), newWeight); // m_jj
      }
      for (int i=0; i<m_NTruthJets;i++){
        vhistt[8+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(float(i),newWeight); //incl jet multiplicity
      }

      vhistt[2+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_truth_HT, newWeight);
      vhistt[9+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_NTruthJets,newWeight); //excl jet multiplicity
      // Lead jet pT > 500 GeV
      if (m_truth_jets[0].Pt() >= 500.){
        vhistt[11+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_NTruthJets,newWeight);//excl jet multiplicity
        for (int i=0; i<m_NTruthJets; i++)
          vhistt[10+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(float(i),newWeight); //incl jet multiplicity

        // minDR
        vhistt[4+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_truth_minDR,newWeight);
        vhistt[5+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_truth_fzjbin,newWeight);
        if(m_truth_minDR < 1.4) vhistt[6+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_truth_fzjbin,newWeight);
        else if(m_truth_minDR>2.0) vhistt[7+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_truth_fzjbin,newWeight);
        else vhistt[14+(tch*nunf)  + 2*nunf*(sys-1)]->Fill(m_truth_fzjbin,newWeight);
      }
    }
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsExtraSysts::setupJob(EL::Job& job) {
  job.useXAOD();
  ANA_CHECK_SET_TYPE(EL::StatusCode);
  ANA_CHECK(xAOD::Init());
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsExtraSysts::histInitialize() {

  // Control Plots
  // Define Control plots;
  std::string histvec[npl] = {std::string("Avmu"),std::string("ZMassX"),std::string("ZJpt"),        // 0 - 2
  std::string("ZPt"),std::string("Njets"),std::string("Ptj0"),                                 // 3 - 5
  std::string("HT1j"), std::string("ZPt1j"),std::string("Mjj"),std::string("DRZj"),                 // 6 - 19
  std::string("ZMass1j"),std::string("ZJptlDR"),std::string("ZJpthDR"),                        // 10 - 12
  std::string("MjjlDR"),std::string("MjjhDR"),std::string("NjexlDR"),                          // 13 - 15
  std::string("NjexhDR"),std::string("Njex"),std::string("Ptjc"), std::string("Ptj1"),              // 16 - 19
  std::string("DPjjlDR"), std::string("DPjjhDR"),                                         // 20 - 21
  std::string("NJetsInclu"),std::string("NJetsInclu500"),                                 // 22 - 23
  std::string("NJetsExclu"),std::string("NJetsExclu500"),                                 // 24 - 25
  std::string("ZJpt_l1.0"),std::string("ZJpt_h1.0"),                                      // 26 - 27
  std::string("ZJpt_l1.2"),std::string("ZJpt_h1.2"),                                      // 28 - 29
  std::string("ZJpt_l1.4"),std::string("ZJpt_h1.4"),                                      // 30 - 31
  std::string("ZMass0-250"),std::string("ZMass250-500"),std::string("ZMass500"),               // 32 - 34
  std::string("Lep1PT"),std::string("Lep2PT"), // 35 -36
  std::string("ZMass500jet"),std::string("ZMass2j"), std::string("ZMass3j"),  // 37-38-39
  std::string("ZJptmedDR")}; // 40
  int nbin[npl]    = {80,  75, 60, 200, 11, 150, 200, 200, 200, 80, 75,  60, 60,
     200, 200, 11, 11,  11, 75, 75, 32,  32, 11, 11, 11, 11, 60, 60, 60, 60, 60,
     60, 75, 75, 75, 100, 100,75,75,75,60};
  float dlow[npl]  = {-0.25, 50.,  0., 0., -0.5, 60., 0.,  0. ,  0., 0., 50. ,
     0.,  0. , 0.,  0. , -0.5, -0.5, -0.5, 30., 30., 0., 0., -0.5, -0.5, -0.5,
     -0.5, 0, 0, 0, 0, 0, 0, 50., 50., 50., 25., 25.,50.,50.,50.,0.};
  float dhigh[npl] = {79.75, 200., 3., 2000., 10.5,  1560., 4000., 2000., 4000., 4.,
     200., 3.,  3., 4000.,4000.,10.5, 10.5, 10.5, 1530.,1530., 3.2, 3.2, 10.5, 10.5,
     10.5, 10.5, 3., 3., 3., 3., 3., 3., 200., 200., 200., 2025, 2025.,200.,200.,200.,3.};
  for (Int_t q=0; q<nsys; q++) {
    std::string sysString = std::to_string(q);
    for (int j=0;j<npl;j++){
      for (int k=0;k<3;k++){
        // int i=j+(k*npl);
        int i=j+(k*npl)+q*3*npl;
        std::string chan = std::string("hee");
        if(k==1) chan = std::string("hmm");
        if(k==2) chan = std::string("hem");
        TString hname = chan+histvec[j]+sysString;
        // TString hname = chan+histvec[j]+"0";
        vhist[i]  =  new TH1F(hname,hname, nbin[j], dlow[j], dhigh[j]);
        vhist[i]->Sumw2();
        wk()->addOutput(vhist[i]);
      }
    }
  }

  // Unfolding & Response matrices.
  // Bin definitions
  int   unbin[nunf]   =  {100,    150,    200,   200,   80,  45,    45,    45,   11,   11,   11,   11 , 100,  100, 45    };
  float udlow[nunf]   =  {0.,    60.,    0.,    0.,    0.,  0.5,   0.5,   0.5,  -0.5, -0.5, -0.5, -0.5, 25,  25, 0.5 };
  float udhigh[nunf]  =  {2000., 1560.,  4000., 4000., 4.,  45.5,  45.5,  45.5, 10.5, 10.5, 10.5, 10.5, 2025,2025, 45.5   };
  // matrixes:
  std::string histvecm[nunf] = {std::string("respZpt1j"),std::string("respPtj0"),std::string("respHT1j"), std::string("respMjj"),
     std::string("respDRZj"), std::string("respZJpt"), std::string("respZJptlDR"),std::string("respZJpthDR"),
     std::string("respNJetsInclu"),std::string("respNJetsExclu"),std::string("respNJetsInclu500"),std::string("respNJetsExclu500"),
     std::string("respLep1Pt"),std::string("respLep2Pt"), std::string("respZJptmedDR")};
  std::string histvecm2[nunf] = {std::string("resp2Zpt1j"),std::string("resp2Ptj0"),std::string("resp2HT1j"), std::string("resp2Mjj"),
     std::string("resp2DRZj"), std::string("resp2ZJpt"), std::string("resp2ZJptlDR"),std::string("resp2ZJpthDR"),
     std::string("resp2NJetsInclu"),std::string("resp2NJetsExclu"),std::string("resp2NJetsInclu500"),std::string("resp2NJetsExclu500"),
     std::string("resp2Lep1Pt"),std::string("resp2Lep2Pt"), std::string("resp2ZJptmedDR")};
  // reco
  std::string histvecr[nunf] = {std::string("measZpt1j"),std::string("measPtj0"),std::string("measHT1j"), std::string("measMjj"),
     std::string("measDRZj"), std::string("measZJpt"), std::string("measZJptlDR"),std::string("measZJpthDR"),
     std::string("measNJetsInclu"),std::string("measNJetsExclu"),std::string("measNJetsInclu500"),std::string("measNJetsExclu500"),
     std::string("measLep1Pt"),std::string("measLep2Pt"), std::string("measZJptmedDR")};
  std::string histvecr2[nunf] = {std::string("meas2Zpt1j"),std::string("meas2Ptj0"),std::string("meas2HT1j"), std::string("meas2Mjj"),
     std::string("meas2DRZj"), std::string("meas2ZJpt"), std::string("meas2ZJptlDR"),std::string("meas2ZJpthDR"),
     std::string("meas2NJetsInclu"),std::string("meas2NJetsExclu"),std::string("meas2NJetsInclu500"),std::string("meas2NJetsExclu500"),
     std::string("meas2Lep1Pt"),std::string("meas2Lep2Pt"), std::string("meas2ZJptmedDR")};
  std::string histvecd[nunf] = {std::string("dataZpt1j"),std::string("dataPtj0"),std::string("dataHT1j"), std::string("dataMjj"),
     std::string("dataDRZj"), std::string("dataZJpt"), std::string("dataZJptlDR"),std::string("dataZJpthDR"),
     std::string("dataNJetsInclu"),std::string("dataNJetsExclu"),std::string("dataNJetsInclu500"),std::string("dataNJetsExclu500"),
     std::string("dataLep1Pt"),std::string("dataLep2Pt"), std::string("dataZJptmedDR")};
  //truth
  std::string histvect[nunf] = {std::string("trueZpt1j"),std::string("truePtj0"),std::string("trueHT1j"), std::string("trueMjj"),
     std::string("trueDRZj"), std::string("trueZJpt"), std::string("trueZJptlDR"),std::string("trueZJpthDR"),
     std::string("trueNJetsInclu"),std::string("trueNJetsExclu"),std::string("trueNJetsInclu500"),std::string("trueNJetsExclu500"),
     std::string("trueLep1Pt"),std::string("trueLep2Pt"), std::string("trueZJptmedDR")};
  std::string histvect2[nunf] = {std::string("true2Zpt1j"),std::string("true2Ptj0"),std::string("true2HT1j"), std::string("true2Mjj"),
     std::string("true2DRZj"), std::string("true2ZJpt"), std::string("true2ZJptlDR"),std::string("true2ZJpthDR"),
     std::string("true2NJetsInclu"),std::string("true2NJetsExclu"),std::string("true2NJetsInclu500"),std::string("true2NJetsExclu500"),
     std::string("true2Lep1Pt"), std::string("true2Lep2Pt"), std::string("true2ZJptmedDR")};

  int channel = m_config.getInt("Channel",-1);
  if (channel == -1) fatal("Channel not properly defined for sample!");

  // Define truth.
  std::string hname;
  for (Int_t q=0; q<nsys; q++) {
    std::string sysString = std::to_string(q);
    for (int j=0;j<nunf;j++){
       for (int k=0;k<2;k++){
          int i=j+(k*nunf)+q*2*nunf;
          std::string chan = std::string("hee");
          if(k==1) chan = std::string("hmm");
          if(channel!=3) {
             hname = chan+histvect[j]+sysString;
          }else if (channel==3){
             hname = chan+histvect2[j]+sysString;
          }
          vhistt[i]  =  new TH1F(hname.data(),hname.data(), unbin[j], udlow[j], udhigh[j]);
          vhistt[i]->Sumw2();
          wk()->addOutput(vhistt[i]);
        }
     }
   }

   // Define Unfolding matrix + response histo
   for (Int_t q=0; q<nsys; q++) {
     std::string sysString = std::to_string(q);
     // Unfolding matrices: 1 per systematic
     for (int j=0;j<nunf;j++){
       for (int k=0;k<2;k++){
         int i=j+(k*nunf)+q*2*nunf;
         std::string chan = std::string("hee");
         if(k==1) chan = std::string("hmm");
         TString hname;
         // Response
         if(channel!=3) { // Not MG
           hname = chan+histvecm[j]+sysString;
         }else if (channel==3){ // Madgraph signal
           hname = chan+histvecm2[j]+sysString;
         }
        vhistm[i]  =  new TH2F(hname,hname, unbin[j], udlow[j], udhigh[j], unbin[j], udlow[j], udhigh[j]);
        vhistm[i]->Sumw2();
        wk()->addOutput(vhistm[i]);

        // Measured
        if(channel==1) { // Data
           hname = chan+histvecd[j]+sysString;
        }else if (channel!=3){ // Not MG = Sherpa
           hname = chan+histvecr[j]+sysString;
        }else if (channel==3){ // MG
           hname = chan+histvecr2[j]+sysString;
        }
        vhistr[i]  =  new TH1F(hname,hname, unbin[j], udlow[j], udhigh[j]);
        vhistr[i]->Sumw2();
        wk()->addOutput(vhistr[i]);
      }
    }
  }

  // CutFlow initialization
  m_cutFlow_weighted = new TH1F("CutFlow_weighted", "CutFlow_weighted", 40, -0.5, 39.5);
  m_cutFlow_nominal = new TH1F("CutFlow_nominal", "CutFlow_nominal", 40, -0.5, 39.5);
  int bin=0;
  for (TString lbl:{"T 2 lep, pT, eta","T DR(l,jets)", "T Exactly 2 lep","T 2e or 2mu","T M(ll)","T Z+jets","T Z+2jets","Born 2 lep", "Born M(ll)", "Born Z+Jets","All Evts","GRL","PriVtx exists","Core Flags", "Pass HLT", "2elects, pT, eta", "Elec ID", "Elec Isolation", "2muons, pT, eta", "Muon ID", "Muon Isolation", "2 leptons", "Lep-jet OR", "Exactly 2 lep", "Exactly 2 lep", "M(ll), OS", "Z+Jets", "Z+2Jets", "Z+b", "Z+2b"}) {
    m_cutFlow_weighted->GetXaxis() -> SetBinLabel(++bin,lbl);
    m_cutFlow_nominal->GetXaxis()  -> SetBinLabel(  bin,lbl);
  }

  m_sum_of_weights = new TH1F("sum_of_weights", "sum_of_weights", 6, -0.5, 5.5);
  m_sum_of_weights->GetXaxis()->SetBinLabel(1, "DxAOD");
  m_sum_of_weights->GetXaxis()->SetBinLabel(2, "DxAOD squared");
  m_sum_of_weights->GetXaxis()->SetBinLabel(3, "DxAOD events");
  m_sum_of_weights->GetXaxis()->SetBinLabel(4, "xAOD");
  m_sum_of_weights->GetXaxis()->SetBinLabel(5, "xAOD squared");
  m_sum_of_weights->GetXaxis()->SetBinLabel(6, "xAOD events");

  hsumwgh = new TH1F("hsumwgh", "hsumwgh", 3,0.5,3.5);

  wk()->addOutput(m_cutFlow_weighted);
  wk()->addOutput(m_cutFlow_nominal);
  wk()->addOutput(m_sum_of_weights);
  wk()->addOutput(hsumwgh);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsExtraSysts::InitializeOR() {
  // m_flags.boostedLeptons = true;
  m_flags.boostedLeptons = false;
  m_flags.doElectrons = true; // by default
  m_flags.doMuons = true; // by defauly
  m_flags.doJets = true; // by default
  m_flags.doTaus = false; // On by default. Should keep?
  m_flags.doPhotons = false; // On by default. Let's see how I deal with this.
  m_flags.doMuPFJetOR = true;
  ANA_CHECK( ORUtils::recommendedTools(m_flags, m_toolBox) );
  ANA_CHECK( m_toolBox.initialize() );

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsExtraSysts::InitializeEventWeights() {
  int channel = m_config.getInt("Channel",-1);
  // std::cout << "ALEX CHANNEL = " << channel << std::endl;
  m_weightTool.setTypeAndName("PMGTools::PMGTruthWeightTool/PMGTruthWeightTool");
  ATH_CHECK(m_weightTool.retrieve());

  if (channel == 2){
    // The 100 event weights of our PDF set
    // Electron channel does not have the 100 variations, only nominal
    // Electron channel also does not have the alpha_s variation
    // Electron channel only has nominal + 2 alternate PDF + 6
    for (int i=0; i<10; i++)
      vector_name_of_weights.push_back("MUR1_MUF1_PDF26100"+std::to_string(i)); // 0 to 9
    for (int i=10; i<100; i++)
      vector_name_of_weights.push_back("MUR1_MUF1_PDF2610"+std::to_string(i)); // 10 to 99
    vector_name_of_weights.push_back("MUR1_MUF1_PDF261100"); // 100
    vector_name_of_weights.push_back("MUR1_MUF1_PDF13000"); // Alternate PDF #1 101
    vector_name_of_weights.push_back("MUR1_MUF1_PDF25300"); // Alternate PDF #2 102
    vector_name_of_weights.push_back("MUR1_MUF1_PDF270000"); // alpha_s up 103
    vector_name_of_weights.push_back("MUR1_MUF1_PDF269000"); // alpha_s down 104
    vector_name_of_weights.push_back("MUR0.5_MUF0.5_PDF261000"); // scale var1 105
    vector_name_of_weights.push_back("MUR0.5_MUF1_PDF261000"); // scale var2 106
    vector_name_of_weights.push_back("MUR1_MUF0.5_PDF261000"); // scale var3 107
    vector_name_of_weights.push_back("MUR1_MUF2_PDF261000"); // scale var4 108
    vector_name_of_weights.push_back("MUR2_MUF1_PDF261000"); // scale var5 109
    vector_name_of_weights.push_back("MUR2_MUF2_PDF261000"); // scale var6 110
    for (int i=0; i<vector_name_of_weights.size(); i++){
      vector_of_weights.push_back(0.);
      vector_sum_of_weights.push_back(0.);
    }
  }
  else if (channel ==4){ // ttbar
    vector_name_of_weights.push_back(" nominal ");
    // The 100 event weights of our PDF set
    for (int i=1; i<10; i++)
      vector_name_of_weights.push_back(" PDF set = 26000"+std::to_string(i)+" ");
    for (int i=10; i<100; i++)
      vector_name_of_weights.push_back(" PDF set = 2600"+std::to_string(i)+" ");
    vector_name_of_weights.push_back(" PDF set = 260100 "); // 100
    vector_name_of_weights.push_back(" PDF set = 13165 "); // Alternate PDF #1 101
    vector_name_of_weights.push_back(" PDF set = 25200 "); // Alternate PDF #2 102
    vector_name_of_weights.push_back(" PDF set = 265000 "); // alpha_s up 103
    vector_name_of_weights.push_back(" PDF set = 266000 "); // alpha_s down 104
    vector_name_of_weights.push_back(" muR = 2.0, muF = 2.0 "); // ISR down 410472 value weight #1 105
    vector_name_of_weights.push_back("Var3cDown"); // ISR down 410472 value weight #2 106
    vector_name_of_weights.push_back(" muR = 0.5, muF = 0.5 "); // ISR up 410482 value weight #1 107
    vector_name_of_weights.push_back("Var3cUp"); // ISR up 410482 value weight #2 108
    vector_name_of_weights.push_back("isr:muRfac=1.0_fsr:muRfac=0.5"); // FSR down 410472 109
    vector_name_of_weights.push_back("isr:muRfac=1.0_fsr:muRfac=2.0"); // FSR up 410472 110


    for (int i=0; i<vector_name_of_weights.size(); i++){
      vector_of_weights.push_back(0.);
      vector_sum_of_weights.push_back(0.);
    }
  }
  else fatal("Not sherpa or ttbar for extra systematics!");

  // if (m_weightTool->getWeightNames().size()>nWeights) fatal("More than 300 Truth weights?!");
  // std::cout << "ALEXANDRE WEIGHT NAMES!" << std::endl;
  // for (auto weight : m_weightTool->getWeightNames()){
    // TString newname = weight;
    // newname.ReplaceAll(" ","");
    // std::cout << weight << std::endl;
  //   vector_name_of_weights.push_back(weight);
  //   vector_sum_of_weights.push_back(0.);
  //   vector_of_weights.push_back(0.);
  // }
  // fatal("lol");
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZJetsExtraSysts::finalize() {
  hsumwgh->SetBinContent(1,m_sum_of_weights->GetBinContent(4));
  int channel = m_config.getInt("Channel",-1);
  if (channel == 4){ // ttbar only
    hsumwgh->SetBinContent(2,vector_sum_of_weights[105]*vector_sum_of_weights[106]/m_sum_of_weights->GetBinContent(4)); // ISR down Use this for 410472
    hsumwgh->SetBinContent(3,vector_sum_of_weights[107]*vector_sum_of_weights[108]/m_sum_of_weights->GetBinContent(4)); // ISR up Use this for 410482
  }
  delete m_finalMuons;
  delete m_finalElectrons;
  m_jets.clear();
  m_lepton1.Clear();
  m_lepton2.Clear();

  ANA_CHECK(m_eventTools->FinalizeTools());
  ANA_CHECK(m_jetTools->FinalizeTools()  );
  ANA_CHECK(m_muTools->FinalizeTools()   );
  ANA_CHECK(m_elTools->FinalizeTools()   );

  delete m_eventTools;
  delete m_jetTools;
  delete m_muTools;
  delete m_elTools;

  ANA_MSG_INFO ("Finalize: ");
  ANA_MSG_INFO ("-----------------------------------------------------");
  ANA_MSG_INFO ("Cutflow Reco");
  ANA_MSG_INFO ("All events:      " << m_cutFlow_nominal->GetBinContent(11));
  ANA_MSG_INFO ("Pass GRL:        " << m_cutFlow_nominal->GetBinContent(12));
  ANA_MSG_INFO ("Primary vertex:  " << m_cutFlow_nominal->GetBinContent(13));
  ANA_MSG_INFO ("Core flags:      " << m_cutFlow_nominal->GetBinContent(14));
  ANA_MSG_INFO ("Pass HLT         " << m_cutFlow_nominal->GetBinContent(15));
  ANA_MSG_INFO ("2 elecs, pT,eta: " << m_cutFlow_nominal->GetBinContent(16));
  ANA_MSG_INFO ("Elec ID:         " << m_cutFlow_nominal->GetBinContent(17));
  ANA_MSG_INFO ("Elec isolation:  " << m_cutFlow_nominal->GetBinContent(18));
  ANA_MSG_INFO ("2 muons, pT,eta: " << m_cutFlow_nominal->GetBinContent(19));
  ANA_MSG_INFO ("Muon ID:         " << m_cutFlow_nominal->GetBinContent(20));
  ANA_MSG_INFO ("Muons isolation: " << m_cutFlow_nominal->GetBinContent(21));
  ANA_MSG_INFO ("2 leptons        " << m_cutFlow_nominal->GetBinContent(22));
  ANA_MSG_INFO ("Lep-jet OR:      " << m_cutFlow_nominal->GetBinContent(23));
  ANA_MSG_INFO ("Exactly 2 lep:   " << m_cutFlow_nominal->GetBinContent(25));
  ANA_MSG_INFO ("Exactly 2 lep:   " << m_cutFlow_nominal->GetBinContent(26));
  ANA_MSG_INFO ("M(ll), OS        " << m_cutFlow_nominal->GetBinContent(27));
  ANA_MSG_INFO ("Z+jets           " << m_cutFlow_nominal->GetBinContent(28));
  ANA_MSG_INFO ("Z+2jets          " << m_cutFlow_nominal->GetBinContent(29));
  ANA_MSG_INFO ("----------------");
  ANA_MSG_INFO ("Cutflow Truth");
  ANA_MSG_INFO ("2e or 2mu:       " << m_cutFlow_weighted->GetBinContent(4));
  ANA_MSG_INFO ("M(ll):           " << m_cutFlow_weighted->GetBinContent(5));
  ANA_MSG_INFO ("Z+jets:          " << m_cutFlow_weighted->GetBinContent(6));
  ANA_MSG_INFO ("Z+2jets:         " << m_cutFlow_weighted->GetBinContent(7));

  return EL::StatusCode::SUCCESS;
}
