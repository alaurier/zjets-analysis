#include "zjets-analysis/ZJetsjetTools.h"

ZJetsjetTools::ZJetsjetTools(const char *name, bool isMC)
    : asg::AsgMessaging( name )
    , m_name(name)
    , m_isMC(isMC)
{}

StatusCode ZJetsjetTools::Initialize(Config &config){
  //Config parameters
  m_jetAlg          = "PFlow";
  m_doPFlow = true;
  if(m_doPFlow)
    m_jetCollectionName = "AntiKt4EMPFlowJets";
  else
    m_jetCollectionName = "AntiKt4EMTopoJets";
  m_pTCut           = config.getNum(m_name+"."+m_jetAlg+".Selection.pTCut");
  m_rapidityCut     = config.getNum(m_name+"."+m_jetAlg+".Selection.rapidityCut");
  m_dofJVT          = config.getBool(m_name+"."+m_jetAlg+".Selection.dofJVT");

  ANA_CHECK(InitializeJetTools(config));

  return StatusCode::SUCCESS;
}

StatusCode ZJetsjetTools::ApplySystematicVariation(const CP::SystematicSet &syst){

  ANA_CHECK(m_jetUncertainties->applySystematicVariation(syst));
  ANA_CHECK(m_jvtEffTool->applySystematicVariation(syst));
  //ANA_CHECK(m_fjvtEffTool->applySystematicVariation(syst));
  return StatusCode::SUCCESS;
}

StatusCode ZJetsjetTools::InitializeJetTools(Config &config){
  // Jet calibration tool
  m_jetCalibTool = new JetCalibrationTool("JetCalibrationTool");
  bool isData = m_isMC ? false : true;
  ANA_CHECK(m_jetCalibTool->setProperty("JetCollection", config.getStr(m_name+"."+m_jetAlg+".JetCalibrationTool.JetCollection").Data()));
  ANA_CHECK(m_jetCalibTool->setProperty("ConfigFile", config.getStr(m_name+"."+m_jetAlg+".JetCalibrationTool.ConfigFile").Data()));
  if(isData){
    ANA_CHECK(m_jetCalibTool->setProperty("CalibSequence", config.getStr(m_name+"."+m_jetAlg+".JetCalibrationTool.CalibSequence_Data").Data()));
  }
  else{
    ANA_CHECK(m_jetCalibTool->setProperty("CalibSequence", config.getStr(m_name+"."+m_jetAlg+".JetCalibrationTool.CalibSequence_MC").Data()));
  }
  ANA_CHECK(m_jetCalibTool->setProperty("CalibArea", config.getStr(m_name+"."+m_jetAlg+".JetCalibrationTool.CalibArea").Data()));
  ANA_CHECK(m_jetCalibTool->setProperty("IsData", isData));
  ANA_CHECK(m_jetCalibTool->initializeTool("JetCalibrationTool"));

  //Jet uncertainties tool
  m_jetUncertainties = new JetUncertaintiesTool("JetUncertaintiesTool");
  ANA_CHECK(m_jetUncertainties->setProperty("JetDefinition", config.getStr(m_name+"."+m_jetAlg+".JetUncertaintiesTool.JetDefinition").Data()));
  ANA_CHECK(m_jetUncertainties->setProperty("MCType", config.getStr(m_name+"."+m_jetAlg+".JetUncertaintiesTool.MCType").Data()));
  ANA_CHECK(m_jetUncertainties->setProperty("ConfigFile", config.getStr(m_name+"."+m_jetAlg+".JetUncertaintiesTool.ConfigFile").Data()));
  ANA_CHECK(m_jetUncertainties->setProperty("IsData", isData));
  ANA_CHECK(m_jetUncertainties->initialize());

  //fJVT tool
  m_fJvtTool.setTypeAndName("JetForwardJvtTool/fJvt");
  ANA_CHECK(m_fJvtTool.retrieve());

  //JVT efficiency tool
  m_jvtEffTool = new CP::JetJvtEfficiency("JetJvtEfficiency_JVT");
  //ANA_CHECK(m_jvtEffTool->setProperty("WorkingPoint", config.getStr(m_name+"."+m_jetAlg+".JetJvtEfficiency.WorkingPoint").Data()));
  ANA_CHECK(m_jvtEffTool->setProperty("SFFile", config.getStr(m_name+"."+m_jetAlg+".JetJvtEfficiency.SFFile").Data()));
  ANA_CHECK(m_jvtEffTool->initialize());

  //fJVT efficiency tool
  if(m_dofJVT){
    m_fjvtEffTool = new CP::JetJvtEfficiency("JetJvtEfficiency_fJVT");
    ANA_CHECK(m_fjvtEffTool->setProperty("SFFile", config.getStr(m_name+"."+m_jetAlg+".JetfJvtEfficiency.SFFile").Data()));
    ANA_CHECK(m_fjvtEffTool->setProperty("ScaleFactorDecorationName", config.getStr(m_name+"."+m_jetAlg+".JetfJvtEfficiency.ScaleFactorDecorationName").Data()));
    ANA_CHECK(m_fjvtEffTool->initialize());
  }

  return StatusCode::SUCCESS;
}

void ZJetsjetTools::GetJVTscaleFactors(float& jvtSF, float& fjvtSF){
  jvtSF = m_jvtSF;
  if(m_dofJVT)
    fjvtSF = m_fjvtSF;
  else
    fjvtSF = 1.;
}

StatusCode ZJetsjetTools::FinalizeTools(){
  delete m_jetCalibTool;
  delete m_jetUncertainties;
  delete m_jvtEffTool;
  if(m_dofJVT) delete m_fjvtEffTool;

  return StatusCode::SUCCESS;
}
