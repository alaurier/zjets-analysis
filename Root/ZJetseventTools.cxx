#include "zjets-analysis/ZJetseventTools.h"

ZJetseventTools::ZJetseventTools(const char *name, bool isMC)
    : asg::AsgMessaging( name )
    , m_name(name)
    , m_isMC(isMC)
{}

StatusCode ZJetseventTools::Initialize(Config &config){
  asg::AsgMetadataTool amdt("MetaDataTool");

  m_doBadBatman = config.getBool(m_name+".doBadBatman");

  const xAOD::FileMetaData *fmd = nullptr;
  //Check for meta data to find AMI tag, if not available default to mc16a
  if (!amdt.inputMetaStore()->contains<xAOD::FileMetaData>("FileMetaData"))
    m_mcType = "MC16a";
  else{
    ANA_CHECK(amdt.inputMetaStore()->retrieve(fmd, "FileMetaData"));
    std::string amiTag;
    fmd->value(xAOD::FileMetaData::amiTag, amiTag);
    if (TPRegexp("r7326").MatchB(amiTag)) { m_mcType = "MC15b"; }
    else if (TPRegexp("r7267").MatchB(amiTag)) { m_mcType = "MC15b"; }
    else if (TPRegexp("r7725").MatchB(amiTag)) { m_mcType = "MC15c"; }
    else if (TPRegexp("a818").MatchB(amiTag)) { m_mcType = "MC15c"; }
    else if (TPRegexp("r9364").MatchB(amiTag)) { m_mcType = "MC16a"; }
    else if (TPRegexp("r9781").MatchB(amiTag)) { m_mcType = "MC16c"; }
    else if (TPRegexp("r10201").MatchB(amiTag)) { m_mcType = "MC16d"; }
    else if (TPRegexp("r10724").MatchB(amiTag)) { m_mcType = "MC16e"; }
    else
      m_mcType = "MC16a";
  }

  // Trigger
  m_mu_triggers = config.getStrV("ZJetsmuonTools.Trigger.DiMuonTrigger");
  m_el_triggers = config.getStrV("ZJetselecTools.Trigger.DiElecTrigger");
  m_emu_triggers.push_back(config.getStr("ZJetseventTools.Trigger.SingleTrigger1"));
  m_emu_triggers.push_back(config.getStr("ZJetseventTools.Trigger.SingleTrigger2"));
  for (unsigned int i=0; i<m_mu_triggers.size(); i++) m_triggers.push_back(m_mu_triggers[i]);
  for (unsigned int i=0; i<m_el_triggers.size(); i++) m_triggers.push_back(m_el_triggers[i]);
  m_diMuonTriggerLeg1 = config.getStr("ZJetsmuonTools.Trigger.DiMuonTriggerLeg1");
  m_diMuonTriggerLeg2 = config.getStr("ZJetsmuonTools.Trigger.DiMuonTriggerLeg2");
  m_trigMatchMuondR = config.getNum("ZJetsmuonTools.Trigger.trigMatchMuondR");
  m_trigMatchElecdR = config.getNum("ZJetselecTools.Trigger.trigMatchElecdR");

  ANA_CHECK(InitializeTools(config));
  ANA_CHECK(InitializeTriggerTools());
  return StatusCode::SUCCESS;
}

int ZJetseventTools::getYear(int runNbr) {
  if (m_isMC) {
    if (m_mcType=="MC16e") return 2018;
    if (m_mcType=="MC16d") return 2017;
    return 2016; // 2015+2016
  }
  if (runNbr > 365000) fatal("Too large run number! Cant' get year");
  if (runNbr>348000) return 2018;
  if ( runNbr >= 325713 && runNbr <= 340453) return 2017;
  if ( runNbr >= 276262 && runNbr <= 311481) return 2016;
  fatal(Form("Don't know what year run %i is for",runNbr));
  return -1;
}

StatusCode ZJetseventTools::InitializeTools(Config &config){
  //GRL
  std::vector<std::string> vecGRL;
  for (auto grl : config.getStrV("ZJetseventTools.GRL")){
    printf("grl: %s\n grl path: %s\n", grl.Data(), gSystem->ExpandPathName(grl.Data()));
    vecGRL.push_back(PathResolverFindCalibFile(grl.Data()));
  }
  m_grlTool = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  ANA_CHECK(m_grlTool->setProperty("GoodRunsListVec", vecGRL));
  ANA_CHECK(m_grlTool->setProperty("PassThrough", false));
  ANA_CHECK(m_grlTool->initialize());

  //Pileup reweighting
  std::vector<std::string> confFiles, lcalcFiles;
  if(m_isMC){
    for (TString val : config.getStrV("ZJetseventTools.PRW.ConfigFiles" + m_mcType))
      confFiles.push_back(PathResolverFindDataFile(val.Data()));
    for (TString val : config.getStrV("ZJetseventTools.PRW.LumiCalcFiles" + m_mcType))
      lcalcFiles.push_back(PathResolverFindCalibFile(val.Data()));
  }
  else{
    for (TString val : config.getStrV("ZJetseventTools.PRW.LumiCalcFiles"))
      lcalcFiles.push_back(PathResolverFindCalibFile(val.Data()));
  }

  m_prwSF = config.getNum("ZJetseventTools.PRW.DataScaleFactor");

  m_PileupReweightingTool.setTypeAndName("CP::PileupReweightingTool/tool");
  ANA_CHECK(m_PileupReweightingTool.setProperty("ConfigFiles", confFiles));
  ANA_CHECK(m_PileupReweightingTool.setProperty("LumiCalcFiles", lcalcFiles));
  ANA_CHECK(m_PileupReweightingTool.setProperty("DataScaleFactor", m_prwSF));
  ANA_CHECK(m_PileupReweightingTool.setProperty("DataScaleFactorUP", config.getNum(m_name+".PRW.DataScaleFactorUP")));
  ANA_CHECK(m_PileupReweightingTool.setProperty("DataScaleFactorDOWN", config.getNum(m_name+".PRW.DataScaleFactorDOWN")));
  ANA_CHECK(m_PileupReweightingTool.retrieve());

  return StatusCode::SUCCESS;
}

StatusCode ZJetseventTools::InitializeTriggerTools(){
  // Initialize and configure trigger tools
  m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool");
  ToolHandle< TrigConf::ITrigConfigTool > configHandle( m_trigConfigTool );
  ANA_CHECK(configHandle->initialize());

  m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
  ANA_CHECK(m_trigDecisionTool->setProperty("ConfigTool", configHandle ));
  ANA_CHECK(m_trigDecisionTool->setProperty("TrigDecisionKey", "xTrigDecision"));
  ANA_CHECK(m_trigDecisionTool->initialize());

  m_trigMatching.setTypeAndName("Trig::MatchingTool/MyMatchingTool");
  ANA_CHECK(m_trigMatching.retrieve());

  return StatusCode::SUCCESS;
}

float ZJetseventTools::GetDataPRWsf(){
  return m_prwSF;
}

float ZJetseventTools::GetCorrectedMu(const xAOD::EventInfo* eventInfo){
  return m_PileupReweightingTool->getCorrectedAverageInteractionsPerCrossing( *eventInfo, true );
}

bool ZJetseventTools::PassRunLB(const xAOD::EventInfo* eventInfo){
  return m_grlTool->passRunLB(*eventInfo);
}

bool ZJetseventTools::PassEventCleaning(const xAOD::EventInfo* eventInfo){
  if(!eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad")){
    return false;
  }
  if(!m_isMC){
    if(eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error)
      return false;
    if(eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error)
      return false;
    if(eventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error)
      return false;
    if(eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18))
      return false;
    if(m_doBadBatman){
      static SG::AuxElement::ConstAccessor<char> isBadBatman("DFCommonJets_isBadBatman");
      if(isBadBatman(*eventInfo))
        return false;
    }
  }
  return true;
}

//void ZJetseventTools::removeJetsThatOverlapWithLeptons(std::vector<xAOD::Jet*> *jets, std::vector<xAOD::Electron*> *elecs, std::vector<xAOD::Muon*> *muons){
//  if(jets == nullptr)
//    ZJets::fatal("Can't perform OR without jets");
//  if(elecs != nullptr)
//    removeOverlap(jets, elecs, m_jet_DR_e);
//  if(muons != nullptr && muons->size() > 1)
//    for (auto jet = jets->rbegin(); jet != jets->rend(); ++jet) {
//      if(jetOverlapsMuon(*jet, muons->at(0), m_jet_DR_mu)) { jets->erase(jet.base() - 1); continue; }
//      if(jetOverlapsMuon(*jet, muons->at(1), m_jet_DR_mu)) jets->erase(jet.base() - 1);
//    }
//}

void ZJetseventTools::removeJetsThatOverlapWithLeptons(std::vector<xAOD::Jet*> *jets, std::vector<xAOD::Electron*> *elecs, std::vector<xAOD::Muon*> *muons){
  if(jets == nullptr)
    ZJets::fatal("Can't perform OR without jets");
  if(elecs != nullptr)
    removeOverlap(jets, elecs, m_jet_DR_e);
  if(muons != nullptr && muons->size()>0)
    for (auto jet = jets->rbegin(); jet != jets->rend(); ++jet) {
      if(jetOverlapsMuon(*jet, muons->at(0), m_jet_DR_mu)) { jets->erase(jet.base() - 1); continue; }
      if (muons->size()>1 && jetOverlapsMuon(*jet, muons->at(1), m_jet_DR_mu)) jets->erase(jet.base() - 1);
    }
}

bool ZJetseventTools::jetOverlapsMuon(xAOD::Jet* jet, xAOD::Muon* muon, double DRcut) {
  int Ntrack   = jet->auxdata<std::vector<int> >("NumTrkPt500")[0];
  double sumpT = jet->auxdata<std::vector<float> >("SumPtTrkPt500")[0];

  bool overlap = ( Ntrack<3 || (jet->pt()/muon->pt() < 2 && muon->pt()/sumpT>0.7) ) && ZJets::DRrap(jet, muon) < DRcut;

  return overlap;
}

void ZJetseventTools::removeLeptonsThatOverlapWithJets(std::vector<xAOD::Jet*> *jets, std::vector<xAOD::Electron*> *elecs, std::vector<xAOD::Muon*> *muons){
  if(elecs != nullptr)
    removeOverlap(elecs, jets, m_e_DR_jet);
  if(muons != nullptr)
    removeOverlap(muons, jets, m_mu_DR_jet);
}

StatusCode ZJetseventTools::FillSumOfWeights(xAOD::TEvent* event, TH1F* sum_of_weights){
  const xAOD::CutBookkeeperContainer* completeCBC = nullptr;
  ANA_CHECK(event->retrieveMetaInput(completeCBC, "CutBookkeepers"));

  int minCycle = 10000;
  for (auto cbk : *completeCBC) {
      if (!cbk->name().empty() && minCycle > cbk->cycle()) {
          minCycle = cbk->cycle();
      }
  }

  const xAOD::CutBookkeeper* allEventsCBK = nullptr;
  const xAOD::CutBookkeeper* DxAODEventsCBK = nullptr;

  bool foundDeriv = false;

  int maxCycle = -1;
  for (const auto& cbk : *completeCBC) {
      if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
          allEventsCBK = cbk;
          maxCycle = cbk->cycle();
      }
      if (cbk->name() == "STDM3Kernel" || cbk->name() == "STDM4Kernel") {
          DxAODEventsCBK = cbk;
          foundDeriv = true;
      }
  }

  uint64_t nEventsProcessed  = allEventsCBK->nAcceptedEvents();
  double SumOfWeights        = allEventsCBK->sumOfEventWeights();
  double SumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared();

  uint64_t nEventsDxAOD = 0;
  double SumOfWeightsDxAOD =0, SumOfWeightsSquaredDxAOD = 0;

  if (foundDeriv) {
    nEventsDxAOD           = DxAODEventsCBK->nAcceptedEvents();
    SumOfWeightsDxAOD        = DxAODEventsCBK->sumOfEventWeights();
    SumOfWeightsSquaredDxAOD = DxAODEventsCBK->sumOfEventWeightsSquared();
  }
  sum_of_weights->Fill(0., SumOfWeightsDxAOD);
  sum_of_weights->Fill(1., SumOfWeightsSquaredDxAOD);
  sum_of_weights->Fill(2., nEventsDxAOD);
  sum_of_weights->Fill(3., SumOfWeights);
  sum_of_weights->Fill(4., SumOfWeightsSquared);
  sum_of_weights->Fill(5., nEventsProcessed);

  return StatusCode::SUCCESS;
}

bool ZJetseventTools::PassTrigger(){
  bool pass_Trigger = false;

  //Loop over trigger names from config file and make a chain group, see Initialize()
  std::vector<const Trig::ChainGroup*> triggerChainGroups;
  for(auto trigger : m_triggers)
    triggerChainGroups.push_back(m_trigDecisionTool->getChainGroup(trigger.Data()));

  //If any of the triggers is passed, return true
  for(const auto triggerChainGroup : triggerChainGroups)
    if (triggerChainGroup->isPassed()){
      pass_Trigger = true;
      break;
    }
  return pass_Trigger;
}

bool ZJetseventTools::PassMuonTrigger(){
  bool pass_Trigger = false;

  //Loop over trigger names from config file and make a chain group, see Initialize()
  std::vector<const Trig::ChainGroup*> triggerChainGroups;
  for(auto trigger : m_mu_triggers)
    triggerChainGroups.push_back(m_trigDecisionTool->getChainGroup(trigger.Data()));

  //If any of the triggers is passed, return true
  for(const auto triggerChainGroup : triggerChainGroups)
    if (triggerChainGroup->isPassed()){
      pass_Trigger = true;
      break;
    }
  return pass_Trigger;
}

bool ZJetseventTools::PassElecTrigger(){
  bool pass_Trigger = false;

  //Loop over trigger names from config file and make a chain group, see Initialize()
  std::vector<const Trig::ChainGroup*> triggerChainGroups;
  for(auto trigger : m_el_triggers)
    triggerChainGroups.push_back(m_trigDecisionTool->getChainGroup(trigger.Data()));

  //If any of the triggers is passed, return true
  for(const auto triggerChainGroup : triggerChainGroups)
    if (triggerChainGroup->isPassed()){
      pass_Trigger = true;
      break;
    }
  return pass_Trigger;
}

bool ZJetseventTools::PassEMuTrigger(){
  bool pass_Trigger = false;

  //Loop over trigger names from config file and make a chain group, see Initialize()
  std::vector<const Trig::ChainGroup*> triggerChainGroups;
  for(auto trigger : m_emu_triggers)
    triggerChainGroups.push_back(m_trigDecisionTool->getChainGroup(trigger.Data()));

  //If any of the triggers is passed, return true
  for(const auto triggerChainGroup : triggerChainGroups)
    if (triggerChainGroup->isPassed()){
      pass_Trigger = true;
      break;
    }
  return pass_Trigger;
}


bool ZJetseventTools::TrigMatchMuon(xAOD::Muon &muon1, xAOD::Muon &muon2){
  bool matchTrigger = false;
  //Str trig = m_mu_triggers[0];
  //matchTrigger = ( m_trigMatching->match(muon1, trig.Data(), m_trigMatchMuondR) && m_trigMatching->match(muon2, trig.Data(), m_trigMatchMuondR) );
  bool matched = false;
  for (auto trig : m_mu_triggers){
    matched = ( m_trigMatching->match(muon1, trig.Data(), m_trigMatchMuondR) ||  m_trigMatching->match(muon2, trig.Data(), m_trigMatchMuondR) );
    if (matched == true) matchTrigger = true;
  }
  return matchTrigger;
}

bool ZJetseventTools::TrigMatchMuonEmu(xAOD::Muon &muon1, xAOD::Muon &muon2){
  bool matchTrigger = false;
  //Str trig = m_mu_triggers[0];
  //matchTrigger = ( m_trigMatching->match(muon1, trig.Data(), m_trigMatchMuondR) && m_trigMatching->match(muon2, trig.Data(), m_trigMatchMuondR) );
  bool matched = false;
  for (auto trig : m_emu_triggers){
    matched = ( m_trigMatching->match(muon1, trig.Data(), m_trigMatchMuondR) ||  m_trigMatching->match(muon2, trig.Data(), m_trigMatchMuondR) );
    if (matched == true) matchTrigger = true;
  }
  return matchTrigger;
}

bool ZJetseventTools::TrigMatchElectron(xAOD::Electron &elec1, xAOD::Electron &elec2){
  bool matchTrigger = false;
  //Str trig = m_el_triggers[0];
  //matchTrigger = ( m_trigMatching->match(elec1, trig.Data(), m_trigMatchElecdR) && m_trigMatching->match(elec2, trig.Data(), m_trigMatchElecdR) );
  bool matched = false;
  for (auto trig : m_el_triggers){
    matched = ( m_trigMatching->match(elec1, trig.Data(), m_trigMatchElecdR) && m_trigMatching->match(elec2, trig.Data(), m_trigMatchElecdR) );
    if (matched == true) matchTrigger = true;
  }
  return matchTrigger;
}

StatusCode ZJetseventTools::FinalizeTools(){
  delete m_grlTool;
  delete m_trigConfigTool;
  delete m_trigDecisionTool;
  return StatusCode::SUCCESS;
}
