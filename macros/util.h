#include <TH2.h>
#include "TFile.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TEnv.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TTree.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TError.h"
#include "atlasStyle/AtlasStyle.C"
#include "atlasStyle/AtlasUtils.C"
#include "atlasStyle/AtlasLabels.C"

typedef TString Str;
typedef std::vector<TString> StrV;
typedef std::map<TString, StrV> StrVMap;
typedef std::vector<TFile*> FileV;
typedef std::map<TString, FileV> FileVMap;
typedef std::vector<double> NumV;
typedef std::map<TString, NumV> NumVMap;
typedef std::vector<int> IntV;
typedef std::vector<TH1F*> HistV;
typedef std::map<TString, HistV*> HistVMap;
typedef std::map<TString, TH1F*> HistMap;
typedef std::vector<TH2F*> HistV2;
typedef std::map<TString, HistV2*> HistVMap2;
typedef std::map<TString, TH2F*> HistMap2;
typedef vector<TF1*> FuncV;

void fatal(Str msg) {
  std::cout << "FATAL: " << msg.Data() << std::endl;
  abort();
}
TH1F* rebin1DHisto(TH1F* input1DHisto, TString plot);
TH2F* rebin2DHisto(TH2F* input2DHisto, TString plot);


void setStyle();
TEnv *openSettingsFile(Str fileName);
TFile *openFile(Str fn);
TH1F *getHisto(TFile *f, Str hn);
TH1F *getHisto(Str fn, Str hn) {
  return getHisto(openFile(fn), hn);
}
TH2F *getHisto2(TFile *f, Str hn);
TH2F *getHisto2(Str fn, Str hn) {
  return getHisto2(openFile(fn), hn);
}
StrV vectorize(Str str, Str sep = " ");
NumV numberize(Str str, Str sep = " ");
IntV interize(Str str, Str sep = " ");
void drawText(double x, double y, Str txt, bool alignRight = 0, int col = kBlack, double tsize = 0.04, double angle =0.);
void drawLine(double x1, double y1, double x2, double y2, int col = kBlack, int ls = 1, int lw = 1);

void drawPlotInfo(double xTitle, double yTitle, TString plot, TString region, double lumi, double size=0.04, TString generator="", int iter=-1, bool highpT=false);

NumV* makeLinearBins(int nBins, float min, float max);
double getScaleFactorCF(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi);
double getScaleFactor(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi);


double getScaleFactor(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi){
  TObjArray *tx = sample.Tokenize("/");
  if((((TObjString *)(tx->Last()))->String()).Contains("data")){
    delete tx;
    return 1.0;
  }
  sample = (((TObjString *)(tx->Last()))->String()).ReplaceAll("_hist.root", "");
  delete tx;

  double sumW = getHisto(sampleFile, "hsumwgh")->GetBinContent(1);
  double sigma = xsecs->GetValue(sample + ".Xsec", 0.0);
  double k = xsecs->GetValue(sample + ".Kfactor", 0.0);
  double eff = xsecs->GetValue(sample + ".Filter", 0.0);
  if (sigma==0.0 || k==0.0 || eff ==0.0) fatal("Error in cross section file.");

  return sigma * k * eff * lumi / sumW;
}

TH1F *getHisto(TFile *f, Str hn) {
  TH1F *h = (TH1F*) f->Get(hn);
  if (h == NULL)
    fatal("Cannot access " + hn + " in " + f->GetName());
  return h;
}
TH2F *getHisto2(TFile *f, Str hn) {
  TH2F *h = (TH2F*) f->Get(hn);
  if (h == NULL)
    fatal("Cannot access " + hn + " in " + f->GetName());
  return h;
}

TEnv *openSettingsFile(Str fileName) {
  if (fileName == "")
    fatal("No config file name specified. Cannot open file!");
  TEnv *settings = new TEnv();
  int status = settings->ReadFile(fileName.Data(), EEnvLevel(0));
  if (status != 0)
    fatal(Form("Cannot read file %s", fileName.Data()));
  return settings;
}

StrV vectorize(Str str, Str sep) {
  StrV result;
  TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries() == 0)
    return result;
  TIter istr(strings);
  while (TObjString* os = (TObjString*) istr())
    if (os->GetString()[0] != '#')
      result.push_back(os->GetString());
    else
      break;
  return result;
}

NumV numberize(Str str, Str sep) {
  NumV result;
  TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries() == 0)
    return result;
  TIter istr(strings);
  while (TObjString* os = (TObjString*) istr())
    if (os->GetString()[0] != '#')
      result.push_back((os->GetString()).Atof());
    else
      break;
  return result;
}

IntV interize(Str str, Str sep) {
  IntV result;
  TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries() == 0)
    return result;
  TIter istr(strings);
  while (TObjString* os = (TObjString*) istr())
    if (os->GetString()[0] != '#')
      result.push_back((os->GetString()).Atoi());
    else
      break;
  return result;
}

TFile *openFile(Str fn) {
  TFile *f = TFile::Open(fn);
  if (f == NULL || f->IsZombie())
    fatal("Cannot open " + fn);
  return f;
}

void setStyle() {
  double tsize = 0.035;
  gStyle->SetTextSize(tsize);
  gStyle->SetLabelSize(tsize, "x");
  gStyle->SetTitleSize(tsize, "x");
  gStyle->SetLabelSize(tsize, "y");
  gStyle->SetTitleSize(tsize, "y");

  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.04);
  gStyle->SetPadBottomMargin(0.12);
  gStyle->SetPadTopMargin(0.05);
  TH1::SetDefaultSumw2();
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  gStyle->SetHistMinimumZero();
  gStyle->SetTitleOffset(1.4, "y");
  gStyle->SetTitleOffset(1.4, "x");
}

void drawText(double x, double y, Str txt, bool alignRight, int col, double tsize, double angle) {
  static TLatex *tex = new TLatex();
  if(alignRight)
    tex->SetTextAlign(31);
  tex->SetTextColor(col);
  tex->SetNDC();
  tex->SetTextSize(tsize);
  tex->SetTextFont(42);
  tex->SetTextAngle(angle);
  tex->DrawLatex(x, y, txt);
}

void drawLine(double x1, double y1, double x2, double y2, int col, int ls, int lw) {
  static TLine *line = new TLine();
  line->SetLineColor(col);
  line->SetLineStyle(ls);
  line->SetLineWidth(lw);
  line->DrawLine(x1, y1, x2, y2);
}

NumV* makeLinearBins(int nBins, float min, float max){
  NumV* bins = new NumV;
  float step = (max-min)/nBins;
  for(int i=0 ; i<=nBins; i++)
    bins->push_back(min+i*step);
  return bins;
}

void drawPlotInfo(double xTitle, double yTitle, TString plot, TString region, double lumi, double size, TString generator, int iter, bool highpt){
  ATLASLabel(xTitle, yTitle, "Internal");
  double deltaY=size*1.25;
  drawText(xTitle, yTitle-deltaY, Form("#sqrt{#it{s}} = 13 TeV, %.1f fb^{-1}", lumi / 1000),0,1,size);
  if (plot != "MllNoJet"){
    if (region == "hmm")
      drawText(xTitle, yTitle-2*deltaY, generator+"Z#rightarrow #mu#mu + #geq 1 jet",0,1,size);
    else if (region == "hee")
      drawText(xTitle, yTitle-2*deltaY, generator+"Z#rightarrow ee + #geq 1 jet",0,1,size);
    else if (region == "both")
      drawText(xTitle, yTitle-2*deltaY, generator+"Z\\rightarrow \\ell\\ell + \\geq 1 jet",0,1,size);
    else fatal("Not ee or mumu!");
  }
  else{
    if (region == "hmm")
      drawText(xTitle, yTitle-2*deltaY, generator+"Z#rightarrow #mu#mu + #geq 0 jet",0,1,size);
    else if (region == "hee")
      drawText(xTitle, yTitle-2*deltaY, generator+"Z#rightarrow ee + #geq 0 jet",0,1,size);
    else if (region == "both")
      drawText(xTitle, yTitle-2*deltaY, generator+"Z\\rightarrow \\ell\\ell + \\geq 1 jet",0,1,size);
    else fatal("Not ee or mumu!");
  }
  if (plot == "pTjet")
    drawText(xTitle, yTitle-3*deltaY, "p_{T}^{jet}#geq 60GeV, |y^{jet}| < 2.5",0,1,size);
  else
    drawText(xTitle, yTitle-3*deltaY, "p_{T}^{jet}#geq 100GeV, |y^{jet}| < 2.5",0,1,size);
  if (plot == "minDR" || plot.Contains("ZJpT") || plot == "NJets500" || plot == "Mll500Jet")
    drawText(xTitle, yTitle-4*deltaY, "Leading jet p_{T} #geq 500GeV",0,1,size);
  if (plot == "ZJpT") drawText(xTitle, yTitle-5*deltaY, "inclusive min#DeltaR(Z,j)",0,1,size);
  if (plot == "ZJpTlowDR") drawText(xTitle, yTitle-5*deltaY, "min#DeltaR(Z,j) #leq 1.4",0,1,size);
  if (plot == "ZJpTmedDR") drawText(xTitle, yTitle-5*deltaY, "1.4 #leq min#DeltaR(Z,j) #leq 2.0",0,1,size);
  if (plot == "ZJpThighDR") drawText(xTitle, yTitle-5*deltaY, "min#DeltaR(Z,j) #geq 2.0",0,1,size);
  if (iter > 0) drawText(xTitle, yTitle-6*deltaY, Form("Bayesian unfolding: %i iter.", iter),0,1,size);
  else if (iter == 0) drawText(xTitle, yTitle-6*deltaY, "Reco level: no unfold.",0,1,size);
  if (highpt) drawText(xTitle, yTitle-7*deltaY, "HighPt MuonWP Studies",0,1,size);
}

TH1F* rebin1DHisto(TH1F* input1DHisto, TString plot){
  Str config = "/afs/cern.ch/work/a/alaurier/private/ZJets/source/zjets-analysis/macros/Unfolding/ZJetsSystplots.data";
  TEnv *settings = openSettingsFile(config);

  int nBins;
  NumV bins;
  Str binType = settings->GetValue(plot + ".binType", " ");
  if (binType.Contains("regular")) {
    nBins = settings->GetValue(plot + ".nBins", -1);
    double min = settings->GetValue(plot + ".min", 0.0);
    double max = settings->GetValue(plot + ".max", 0.0);
    double step = (max-min)/nBins;
    for(int i=0 ; i<nBins+1; i++)
      bins.push_back(min+i*step);
  }
  else if (binType.Contains("variable")) {
    bins = numberize(settings->GetValue(plot + ".bins", " "));
    nBins = bins.size() - 1;
  }
  else{fatal("BinType is not regular or variable!");}

  if (plot.Contains("ZJpT")){ // This is the binWidth of each bins
    bins = numberize(settings->GetValue(plot + ".binWidth", " "));
    nBins = 3*bins.size(); // 2D binning : 3 regions
  }


  Double_t BinsVec[nBins+1]; // +1 since we need to define bin edges
  if (!plot.Contains("ZJpT")){
    for (int n=0; n<nBins+1;n++)
      BinsVec[n]=bins[n];
  }
  else {
    BinsVec[0] = 0.;
    double edge = 0.;
    for (int n=0; n<nBins/3;n++)
      for (int i=1; i<4; i++){
        edge = edge + bins[n];
        BinsVec[n*3+i] = edge;
        // std::cout << "Bin index = " << n*3 + i << std::endl;
      }
  }
  TH1F* rebinned1D = new TH1F("rebinned th1f" ,"rebinned th1f", nBins, BinsVec);

  // Rebin Sherpa Measured
  if (!plot.Contains("ZJpT")){
    for (int i=1; i<nBins+1; i++){
      double sumMeas=0.; // Reset Meas sums
      double sumError=0.;
      // Measured values!
      auto xAxis = input1DHisto->GetXaxis();
      for (int x=1; x<xAxis->GetNbins()+1; x++) // Loop through x Axis
        if ((xAxis->GetBinCenter(x) < rebinned1D->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinned1D->GetBinLowEdge(i))){
          sumMeas += input1DHisto->GetBinContent(x);
          sumError += std::pow(input1DHisto->GetBinError(x),2);
        }
      rebinned1D->SetBinContent(i, sumMeas);
      rebinned1D->SetBinError(i, std::sqrt(sumError));
      // rebinned1D->SetBinError(i, std::sqrt(sumMeas));
    }
  }
  else{
    NumV newBins = numberize(settings->GetValue(plot + ".bins", " "));
    auto xAxis = input1DHisto->GetXaxis();
    for (int k=1; k<4; k++){
      for (int i=1; i<newBins.size(); i++){ // for new binning in X
        double sumMeas=0.; // Reset meas sums
        double sumError=0.;
          for (int x=1; x<xAxis->GetNbins()/3+1;x++){
            double val = (xAxis->GetBinCenter(k+3*(x-1))+3-k)/30-.05;
            if ( (val > newBins[i-1]) && (val < newBins[i]) ){ // Value from original histo falls within new bounds
              sumMeas += input1DHisto->GetBinContent(k+3*(x-1));
              sumError += std::pow(input1DHisto->GetBinError(k+3*(x-1)),2);
            }
          }
        rebinned1D->SetBinContent(k+3*(i-1), sumMeas);
        // rebinned1D->SetBinError(k+3*(i-1), std::sqrt(sumMeas));
        rebinned1D->SetBinError(k+3*(i-1), std::sqrt(sumError));
      }
    }
  }
  return (TH1F*) rebinned1D;
}

TH2F* rebin2DHisto(TH2F* input2DHisto, TString plot){
  Str config = "/afs/cern.ch/work/a/alaurier/private/ZJets/source/zjets-analysis/macros/Unfolding/ZJetsSystplots.data";
  TEnv *settings = openSettingsFile(config);

  int nBins;
  NumV bins;
  Str binType = settings->GetValue(plot + ".binType", " ");
  if (binType.Contains("regular")) {
    nBins = settings->GetValue(plot + ".nBins", -1);
    double min = settings->GetValue(plot + ".min", 0.0);
    double max = settings->GetValue(plot + ".max", 0.0);
    double step = (max-min)/nBins;
    for(int i=0 ; i<nBins+1; i++)
      bins.push_back(min+i*step);
  }
  else if (binType.Contains("variable")) {
    bins = numberize(settings->GetValue(plot + ".bins", " "));
    nBins = bins.size() - 1;
  }
  else{fatal("BinType is not regular or variable!");}

  if (plot.Contains("ZJpT")){ // This is the binWidth of each bins
    bins = numberize(settings->GetValue(plot + ".binWidth", " "));
    nBins = 3*bins.size(); // 2D binning : 3 regions
  }


  Double_t BinsVec[nBins+1]; // +1 since we need to define bin edges
  if (!plot.Contains("ZJpT")){
    for (int n=0; n<nBins+1;n++)
      BinsVec[n]=bins[n];
  }
  else {
    BinsVec[0] = 0.;
    double edge = 0.;
    for (int n=0; n<nBins/3;n++)
      for (int i=1; i<4; i++){
        edge = edge + bins[n];
        BinsVec[n*3+i] = edge;
        // std::cout << "Bin index = " << n*3 + i << std::endl;
      }
  }
  TH2F* rebinned2D = new TH2F("rebinned th2f" ,"rebinned th2f", nBins, BinsVec, nBins, BinsVec);
  TH1F* rebinned1D = new TH1F("rebinned th1f" ,"rebinned th1f", nBins, BinsVec);

  if (!plot.Contains("ZJpT")){
    for (int i=1; i<nBins+1; i++){
      for (int j=1; j<nBins+1; j++){
        double sumResp=0.; // Reset response sum
        double sumError=0.;
        // Rebin Response matrix
        auto xAxis = input2DHisto->GetXaxis();
        auto yAxis = input2DHisto->GetYaxis();
        for (int x=1; x<xAxis->GetNbins()+1; x++){ // Loop through x Axis first
          if ((xAxis->GetBinCenter(x) < rebinned1D->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinned1D->GetBinLowEdge(i)))
            for (int y=1; y<yAxis->GetNbins()+1; y++) // Loop through y Axis
              if ((yAxis->GetBinCenter(y) < rebinned1D->GetBinLowEdge(j+1)) && (yAxis->GetBinCenter(y) > rebinned1D->GetBinLowEdge(j))){
                sumResp += input2DHisto->GetBinContent(x,y);
                sumError += std::pow(input2DHisto->GetBinError(x,y),2);
              }
        }
        rebinned2D->SetBinContent(i,j, sumResp);
        // rebinned2D->SetBinError(i,j, std::sqrt(sumResp));
        rebinned2D->SetBinError(i,j, std::sqrt(sumError));
      }
    }
  }
  else{ // ZJpT plots! Need to be careful in the rebinning of the 3 regions...
    // Hard coding a bunch of stuff here because 2D unfolding is annoying.
    for (int i=1; i<31; i++){
      for (int j=1; j<31; j++){
        double sumResp = 0.;
        double sumError = 0.;
        if ( (i<19) && (j<19) ) { // 1x1 bins
          sumResp += input2DHisto->GetBinContent(i, j);  // first 6 bins are unchanged
          sumError += std::pow(input2DHisto->GetBinError(i, j),2);  // first 6 bins are unchanged
        }
        else if ( (i<19) && (j<28) ){ // 1x2 bins
          int J = 0;
          if (j < 22) J = 0;
          else if (j < 25) J = 3;
          else if (j < 28) J = 6;
          sumResp += input2DHisto->GetBinContent(i, j+J);
          sumResp += input2DHisto->GetBinContent(i, j+J+3);
          sumError += std::pow(input2DHisto->GetBinError(i, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i, j+J+3),2);
        }
        else if ( (i<19) && (j<31) ){ // 1x3 bins
          int J = 9;
          sumResp += input2DHisto->GetBinContent(i, j+J);
          sumResp += input2DHisto->GetBinContent(i, j+J+3);
          sumResp += input2DHisto->GetBinContent(i, j+J+3+3);
          sumError += std::pow(input2DHisto->GetBinError(i, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i, j+J+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i, j+J+3+3),2);
        }
        else if ( (i<28) && (j<19) ) { // 2x1 bins
          int I = 0;
          if (i < 22) I = 0;
          else if (i < 25) I = 3;
          else if (i < 28) I = 6;
          sumResp += input2DHisto->GetBinContent(i+I, j);
          sumResp += input2DHisto->GetBinContent(i+I+3, j);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j),2);
        }
        else if ( (i<28) && (j<28) ){ // 2x2 bins
          int J = 0;
          if (j < 22) J = 0;
          else if (j < 25) J = 3;
          else if (j < 28) J = 6;
          int I = 0;
          if (i < 22) I = 0;
          else if (i < 25) I = 3;
          else if (i < 28) I = 6;
          sumResp += input2DHisto->GetBinContent(i+I, j+J);
          sumResp += input2DHisto->GetBinContent(i+I, j+J+3);
          sumResp += input2DHisto->GetBinContent(i+I+3, j+J);
          sumResp += input2DHisto->GetBinContent(i+I+3, j+J+3);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j+J+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j+J+3),2);
        }
        else if ( (i<28) && (j<31) ){ // 2x3 bins
          int I = 0;
          if (i < 22) I = 0;
          else if (i < 25) I = 3;
          else if (i < 28) I = 6;
          int J = 9;
          sumResp += input2DHisto->GetBinContent(i+I, j+J);
          sumResp += input2DHisto->GetBinContent(i+I, j+J+3);
          sumResp += input2DHisto->GetBinContent(i+I, j+J+3+3);
          sumResp += input2DHisto->GetBinContent(i+I+3, j+J);
          sumResp += input2DHisto->GetBinContent(i+I+3, j+J+3);
          sumResp += input2DHisto->GetBinContent(i+I+3, j+J+3+3);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j+J+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j+J+3+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j+J+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j+J+3+3),2);
        }
        else if ( (i<31) && (j<19) ){ // 3x1 bins
          int I = 9;
          sumResp += input2DHisto->GetBinContent(i+I, j);
          sumResp += input2DHisto->GetBinContent(i+I+3, j);
          sumResp += input2DHisto->GetBinContent(i+I+3+3, j);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3+3, j),2);
        }
        else if ( (i<31) && (j<28) ){ // 3x2 bins
          int I = 9;
          int J = 0;
          if (j < 22) J = 0;
          else if (j < 25) J = 3;
          else if (j < 28) J = 6;
          sumResp += input2DHisto->GetBinContent(i+I, j+J);
          sumResp += input2DHisto->GetBinContent(i+I+3, j+J);
          sumResp += input2DHisto->GetBinContent(i+I+3+3, j+J);
          sumResp += input2DHisto->GetBinContent(i+I, j+J+3);
          sumResp += input2DHisto->GetBinContent(i+I+3, j+J+3);
          sumResp += input2DHisto->GetBinContent(i+I+3+3, j+J+3);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3+3, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j+J+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j+J+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3+3, j+J+3),2);
        }
        else if ( (i<31) && (j<31) ){ // 3x3 bins
          int I = 9;
          int J = 9;
          sumResp += input2DHisto->GetBinContent(i+I, j+J);
          sumResp += input2DHisto->GetBinContent(i+I, j+J+3);
          sumResp += input2DHisto->GetBinContent(i+I, j+J+3+3);
          sumResp += input2DHisto->GetBinContent(i+I+3, j+J);
          sumResp += input2DHisto->GetBinContent(i+I+3, j+J+3);
          sumResp += input2DHisto->GetBinContent(i+I+3, j+J+3+3);
          sumResp += input2DHisto->GetBinContent(i+I+3+3, j+J);
          sumResp += input2DHisto->GetBinContent(i+I+3+3, j+J+3);
          sumResp += input2DHisto->GetBinContent(i+I+3+3, j+J+3+3);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j+J+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I, j+J+3+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j+J+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3, j+J+3+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3+3, j+J),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3+3, j+J+3),2);
          sumError += std::pow(input2DHisto->GetBinError(i+I+3+3, j+J+3+3),2);
        }
        else fatal("Need to fix ZJpT rebinning!");
        rebinned2D->SetBinContent(i,j, sumResp);
        // rebinned2D->SetBinError(i,j, std::sqrt(sumResp));
        rebinned2D->SetBinError(i,j, std::sqrt(sumError));
      }
    }
  } // else from !ZJpt
  return (TH2F*) rebinned2D;
}
