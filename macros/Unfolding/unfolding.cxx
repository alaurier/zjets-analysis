#include "../util.h"
#include "RooUnfoldResponse.h"
#include "RooUnfoldBayes.h"
#include "RooUnfoldSvd.h"
#include "RooUnfoldTUnfold.h"
#include "RooUnfoldIds.h"


void UnfoldingValidation(StrVMap sortedSampleFiles, TEnv* settings);
void doUnfolding(StrVMap sortedSampleFiles, TEnv* settings);
void printCrossSections(TEnv* settings);
HistV CalculateSherpaTheory(TString plot);
void doTest();
void doTest2();
void plotTheoryUncertainties();

void unfolding(){
  // Needed for unfolding
  gSystem->Load("/afs/cern.ch/work/a/alaurier/private/RooUnfold/RooUnfold/libRooUnfold.so");
  // Needed when passing filenames instead of files.
  TH1::AddDirectory(kFALSE);

  setStyle();
  TEnv *settings = openSettingsFile("ZJetsSystplots.data");

  // doTest2();
  // return;

  // Define unfolding method
  bool doValidation = (bool)settings->GetValue("UnfoldValidation", 0);
  bool FullUnfolding = (bool)settings->GetValue("FullUnfolding", 0);
  bool doCrossSections = (bool)settings->GetValue("CrossSections", 0);
  bool plotTheory = (bool)settings->GetValue("TheoryUncertainty", 0);

  if (doCrossSections) {printCrossSections(settings); return;}
  if (plotTheory) {plotTheoryUncertainties(); return;}

  // Fetch all the required samples
  // FileVMap sortedSampleFiles;
  StrVMap sortedSampleFiles;
  Str filePath = settings->GetValue("filePath", " ");
  StrV inputCategories = vectorize(settings->GetValue("inputCategories", " "));
  for (auto input : inputCategories) {
    StrV sampleList = vectorize(settings->GetValue(input.Data(), " "));
    for (auto sample : sampleList)
      sortedSampleFiles[input].push_back(filePath + sample + ".root");
      // sortedSampleFiles[input].push_back(openFile(filePath + sample + ".root"));
  }
  if (doValidation) UnfoldingValidation(sortedSampleFiles, settings);
  if (FullUnfolding) doUnfolding(sortedSampleFiles, settings);

  return;
}

void UnfoldingValidation(StrVMap sortedSampleFiles, TEnv* settings) {

  int iterations = settings->GetValue("NumberOfIterations",2); // Total number of systematics
  int start = settings->GetValue("StartK",1); // Total number of systematics
  int stop = settings->GetValue("StopK",4); // Total number of systematics
  int nSys = settings->GetValue("Systematics",1);
  bool HighPt = (bool)settings->GetValue("HighPt", 0);

  int debug = 0;
  // Get basic values needed
  double Totlumi = settings->GetValue("luminosity", 0.0);
  StrV plotList = vectorize(settings->GetValue("PlotList", " "));
  StrV channels = vectorize(settings->GetValue("channels", " "));
  StrV inputCategories = vectorize(settings->GetValue("inputCategories", " "));

  if (debug > 1){
    for (auto plot : plotList)
      std::cout << "alex plot = " << plot << std::endl;
    for (auto channel : channels)
      std::cout << "alex channel = " << channel << std::endl;
    for (auto input : inputCategories)
      std::cout << "alex input = " << input << std::endl;
  }

  // StrV inputLabels {"SherpaZJetsResp","SherpaZJetsTrue","MGZJetsResp","MGZJetsTrue"};
  // StrV inputCategories {"SherpaZJets","MGZJets"};
  StrV unfoldingInputs {"Sherpa", "MadGraph", "Background", "Data"};

  HistVMap sortedTruth;
  HistVMap sortedMeas;
  HistVMap2 sortedResp;

  HistVMap RebinnedTruth;
  HistVMap RebinnedMeas;
  HistVMap2 RebinnedResp;


  // start with muon channel for now
  for (auto region : channels){ // hee or hmm

    // Get plotting information ready
    TCanvas *can = new TCanvas("", "", 800, 800);

    for(auto plot : plotList){
      can->Clear();
      can->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      can->SetBottomMargin(0.12);
      can->SetRightMargin(0.04);

      Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot

      // Empty the maps of histograms and reinitialize them (for safety reasons)
      sortedTruth.clear(); // Reset map of histograms
      sortedMeas.clear(); // Reset map of histograms
      sortedResp.clear(); // Reset map of histograms
      RebinnedTruth.clear(); // Reset map of histograms
      RebinnedMeas.clear(); // Reset map of histograms
      RebinnedResp.clear(); // Reset map of histograms
      for (auto input : unfoldingInputs){ // Make new vectors in each map element (Response + truth x2)
        sortedTruth[input] = new HistV;
        sortedResp[input] = new HistV2;
        sortedMeas[input] = new HistV;
        RebinnedTruth[input] = new HistV;
        RebinnedResp[input] = new HistV2;
        RebinnedMeas[input] = new HistV;
      }

      // Read all the relevant histograms. For now, only use nominal!
      for(auto input : inputCategories){ // For Sherpa & MG
        if (input !="data"){
          for (auto sampleFileName : sortedSampleFiles[input]) { // Loop through SH or MG file
            TFile *sampleFile = TFile::Open(sampleFileName);
            // Get histograms
            TH1F* temp = new TH1F();
            TH2F* temp2 = new TH2F();
            if (input == "SherpaZJets"){ // MG has a different naming convention
              temp2 = getHisto2(sampleFile, region+"resp"+var+"0");
              sortedResp["Sherpa"]->push_back((TH2F*) temp2->Clone());
              temp = getHisto(sampleFile, region+"true"+var+"0");
              sortedTruth["Sherpa"]->push_back((TH1F*) temp->Clone());
              temp = getHisto(sampleFile, region+"meas"+var+"0");
              sortedMeas["Sherpa"]->push_back((TH1F*) temp->Clone());
              if (debug > 0) std::cout << "Sherpa integral of "+input+" = " << temp->Integral() << std::endl;
            }
            else if (input == "MGZJets"){
              TH1F* temp = new TH1F();
              TH2F* temp2 = new TH2F();
              temp2 = getHisto2(sampleFile, region+"resp2"+var+"0");
              sortedResp["MadGraph"]->push_back((TH2F*) temp2->Clone());
              temp = getHisto(sampleFile, region+"true2"+var+"0");
              sortedTruth["MadGraph"]->push_back((TH1F*) temp->Clone());
              temp = getHisto(sampleFile, region+"meas2"+var+"0");
              sortedMeas["MadGraph"]->push_back((TH1F*) temp->Clone());
              if (debug > 0) std::cout << "MadGraph integral of "+input+" = " << temp->Integral() << std::endl;
            }
            else {
              temp2 = getHisto2(sampleFile, region+"resp"+var+"0");
              sortedResp["Background"]->push_back((TH2F*) temp2->Clone());
              temp = getHisto(sampleFile, region+"true"+var+"0");
              sortedTruth["Background"]->push_back((TH1F*) temp->Clone());
              temp = getHisto(sampleFile, region+"meas"+var+"0");
              sortedMeas["Background"]->push_back((TH1F*) temp->Clone());
              if (debug > 0) std::cout << "Background integral of "+input+" = " << temp->Integral() << std::endl;
            }
            sampleFile->Close();
          }
        } // end of MC
        else { // data
          for (auto sampleFileName : sortedSampleFiles[input]) { // Loop data files
            TFile *sampleFile = TFile::Open(sampleFileName);
            // Get histograms
            TH1F* temp = new TH1F();
            temp = getHisto(sampleFile, region+"data"+var+"0");
            sortedMeas["Data"]->push_back((TH1F*) temp->Clone());
            if (debug > 0) std::cout << "Data integral of "+input+" = " << temp->Integral() << std::endl;
            sampleFile->Close();
          }
        } // end of data
      } // end of input categories

      // Start the rebinning procedure
      Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
      Str Ylabel = settings->GetValue(plot + ".Ylabel", " ");

      int nBins;
      NumV bins;
      Str binType = settings->GetValue(plot + ".binType", " ");
      if (binType.Contains("regular")) {
        nBins = settings->GetValue(plot + ".nBins", -1);
        double min = settings->GetValue(plot + ".min", 0.0);
        double max = settings->GetValue(plot + ".max", 0.0);
        double step = (max-min)/nBins;
        for(int i=0 ; i<nBins+1; i++)
          bins.push_back(min+i*step);
      }
      else if (binType.Contains("variable")) {
        bins = numberize(settings->GetValue(plot + ".bins", " "));
        nBins = bins.size() - 1;
      }
      else{fatal("BinType is not regular or variable!");}

      if (plot.Contains("ZJpT")){ // This is the binWidth of each bins
        bins = numberize(settings->GetValue(plot + ".binWidth", " "));
        nBins = 3*bins.size(); // 2D binning : 3 regions
      }


      Double_t BinsVec[nBins+1]; // +1 since we need to define bin edges
      if (!plot.Contains("ZJpT")){
        for (int n=0; n<nBins+1;n++)
          BinsVec[n]=bins[n];
      }
      else {
        BinsVec[0] = 0.;
        double edge = 0.;
        for (int n=0; n<nBins/3;n++)
          for (int i=1; i<4; i++){
            edge = edge + bins[n];
            BinsVec[n*3+i] = edge;
            // std::cout << "Bin index = " << n*3 + i << std::endl;
          }
      }

      TString hname1 = "Sherpa " + var;
      TString hname2 = "MadGraph " + var;
      TString hname3 = "Background " + var;
      TString hnamed = "Data " + var;

      TH1F* rebinnedSherpa = new TH1F(hname1 ,hname1, nBins, BinsVec);
      TH1F* rebinnedMG = new TH1F(hname2 ,hname2, nBins, BinsVec);
      TH1F* rebinnedBG = new TH1F(hname3 ,hname3, nBins, BinsVec);
      TH1F* rebinnedData = new TH1F(hnamed ,hnamed, nBins, BinsVec);
      TH2F* rebinnedSherpa2 = new TH2F(hname1+" response" ,hname1+" response", nBins, BinsVec, nBins, BinsVec);
      TH2F* rebinnedMG2 = new TH2F(hname2+" response" ,hname2+" response", nBins, BinsVec, nBins, BinsVec);
      TH2F* rebinnedBG2 = new TH2F(hname3+" response" ,hname3+" response", nBins, BinsVec, nBins, BinsVec);

      // Rebin Sherpa Response matrix
      if (!plot.Contains("ZJpT")){
        for (int i=1; i<nBins+1; i++){
          for (int j=1; j<nBins+1; j++){
            double sumResp=0.; // Reset response sum
            // Rebin Response matrix
            auto xAxis = sortedResp["Sherpa"]->at(0)->GetXaxis();
            auto yAxis = sortedResp["Sherpa"]->at(0)->GetYaxis();
            for (auto h : *(sortedResp["Sherpa"])){ // Get the Response matrix
              for (int x=1; x<xAxis->GetNbins()+1; x++){ // Loop through x Axis first
                if ((xAxis->GetBinCenter(x) < rebinnedSherpa->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinnedSherpa->GetBinLowEdge(i)))
                  for (int y=1; y<yAxis->GetNbins()+1; y++) // Loop through y Axis
                    if ((yAxis->GetBinCenter(y) < rebinnedSherpa->GetBinLowEdge(j+1)) && (yAxis->GetBinCenter(y) > rebinnedSherpa->GetBinLowEdge(j)))
                      sumResp += h->GetBinContent(x,y);
              }
            }
            rebinnedSherpa2->SetBinContent(i,j, sumResp);
            rebinnedSherpa2->SetBinError(i,j, std::sqrt(sumResp));
          }
        }
      }
      else{
        // Hard coding a bunch of stuff here because 2D unfolding is annoying.
        for (int i=1; i<31; i++){
          for (int j=1; j<31; j++){
            double sumResp =0.;
            for (auto h : *(sortedResp["Sherpa"])){ // Get response Matrix
              if ( (i<19) && (j<19) ) { // 1x1 bins
                sumResp += h->GetBinContent(i, j);  // first 6 bins are unchanged
              }
              else if ( (i<19) && (j<28) ){ // 1x2 bins
                int J = 0;
                if (j < 22) J = 0;
                else if (j < 25) J = 3;
                else if (j < 28) J = 6;
                sumResp += h->GetBinContent(i, j+J);
                sumResp += h->GetBinContent(i, j+J+3);
              }
              else if ( (i<19) && (j<31) ){ // 1x3 bins
                int J = 9;
                sumResp += h->GetBinContent(i, j+J);
                sumResp += h->GetBinContent(i, j+J+3);
                sumResp += h->GetBinContent(i, j+J+3+3);
              }
              else if ( (i<28) && (j<19) ) { // 2x1 bins
                int I = 0;
                if (i < 22) I = 0;
                else if (i < 25) I = 3;
                else if (i < 28) I = 6;
                sumResp += h->GetBinContent(i+I, j);
                sumResp += h->GetBinContent(i+I+3, j);
              }
              else if ( (i<28) && (j<28) ){ // 2x2 bins
                int J = 0;
                if (j < 22) J = 0;
                else if (j < 25) J = 3;
                else if (j < 28) J = 6;
                int I = 0;
                if (i < 22) I = 0;
                else if (i < 25) I = 3;
                else if (i < 28) I = 6;
                sumResp += h->GetBinContent(i+I, j+J);
                sumResp += h->GetBinContent(i+I, j+J+3);
                sumResp += h->GetBinContent(i+I+3, j+J);
                sumResp += h->GetBinContent(i+I+3, j+J+3);
              }
              else if ( (i<28) && (j<31) ){ // 2x3 bins
                int I = 0;
                if (i < 22) I = 0;
                else if (i < 25) I = 3;
                else if (i < 28) I = 6;
                int J = 9;
                sumResp += h->GetBinContent(i+I, j+J);
                sumResp += h->GetBinContent(i+I, j+J+3);
                sumResp += h->GetBinContent(i+I, j+J+3+3);
                sumResp += h->GetBinContent(i+I+3, j+J);
                sumResp += h->GetBinContent(i+I+3, j+J+3);
                sumResp += h->GetBinContent(i+I+3, j+J+3+3);
              }
              else if ( (i<31) && (j<19) ){ // 3x1 bins
                int I = 9;
                sumResp += h->GetBinContent(i+I, j);
                sumResp += h->GetBinContent(i+I+3, j);
                sumResp += h->GetBinContent(i+I+3+3, j);
              }
              else if ( (i<31) && (j<28) ){ // 3x2 bins
                int I = 9;
                int J = 0;
                if (j < 22) J = 0;
                else if (j < 25) J = 3;
                else if (j < 28) J = 6;
                sumResp += h->GetBinContent(i+I, j+J);
                sumResp += h->GetBinContent(i+I+3, j+J);
                sumResp += h->GetBinContent(i+I+3+3, j+J);
                sumResp += h->GetBinContent(i+I, j+J+3);
                sumResp += h->GetBinContent(i+I+3, j+J+3);
                sumResp += h->GetBinContent(i+I+3+3, j+J+3);
              }
              else if ( (i<31) && (j<31) ){ // 3x3 bins
                int I = 9;
                int J = 9;
                sumResp += h->GetBinContent(i+I, j+J);
                sumResp += h->GetBinContent(i+I, j+J+3);
                sumResp += h->GetBinContent(i+I, j+J+3+3);
                sumResp += h->GetBinContent(i+I+3, j+J);
                sumResp += h->GetBinContent(i+I+3, j+J+3);
                sumResp += h->GetBinContent(i+I+3, j+J+3+3);
                sumResp += h->GetBinContent(i+I+3+3, j+J);
                sumResp += h->GetBinContent(i+I+3+3, j+J+3);
                sumResp += h->GetBinContent(i+I+3+3, j+J+3+3);
              }
              else fatal("Need to fix ZJpT rebinning!");
            }
            rebinnedSherpa2->SetBinContent(i,j, sumResp);
            rebinnedSherpa2->SetBinError(i,j, std::sqrt(sumResp));
          }
        }
      }
      RebinnedResp["Sherpa"]->push_back((TH2F*) rebinnedSherpa2->Clone());

      // Rebin Sherpa Measured
      if (!plot.Contains("ZJpT")){
        for (int i=1; i<nBins+1; i++){
          double sumMeas=0.; // Reset Meas sums
          // Measured values!
          auto xAxis = sortedMeas["Sherpa"]->at(0)->GetXaxis();
          for (auto h : *(sortedMeas["Sherpa"])) // Get the Measured histo
            for (int x=1; x<xAxis->GetNbins()+1; x++) // Loop through x Axis
              if ((xAxis->GetBinCenter(x) < rebinnedSherpa->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinnedSherpa->GetBinLowEdge(i)))
                sumMeas += h->GetBinContent(x);
          rebinnedSherpa->SetBinContent(i, sumMeas);
          rebinnedSherpa->SetBinError(i, std::sqrt(sumMeas));
        }
      }
      else{
        NumV newBins = numberize(settings->GetValue(plot + ".bins", " "));
        auto xAxis = sortedMeas["Sherpa"]->at(0)->GetXaxis();
        for (int k=1; k<4; k++){
          for (int i=1; i<newBins.size(); i++){ // for new binning in X
            double sumMeas=0.; // Reset meas sums
            for (auto h : *(sortedMeas["Sherpa"])){ // Get the meas histo
              for (int x=1; x<xAxis->GetNbins()/3+1;x++){
                double val = (xAxis->GetBinCenter(k+3*(x-1))+3-k)/30-.05;
                if ( (val > newBins[i-1]) && (val < newBins[i]) ){ // Value from original histo falls within new bounds
                  sumMeas += h->GetBinContent(k+3*(x-1));
                }
              }
            }
            rebinnedSherpa->SetBinContent(k+3*(i-1), sumMeas);
            rebinnedSherpa->SetBinError(k+3*(i-1), std::sqrt(sumMeas));
          }
        }
      }
      RebinnedMeas["Sherpa"]->push_back((TH1F*) rebinnedSherpa->Clone());
      // Rebin Sherpa Truth
      if (!plot.Contains("ZJpT")){
        for (int i=1; i<nBins+1; i++){
          double sumTruth=0.; // Reset truth sums
          // Truth values!
          auto xAxis = sortedTruth["Sherpa"]->at(0)->GetXaxis();
          for (auto h : *(sortedTruth["Sherpa"])) // Get the Truth histo
            for (int x=1; x<xAxis->GetNbins()+1; x++) // Loop through x Axis
              if ((xAxis->GetBinCenter(x) < rebinnedSherpa->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinnedSherpa->GetBinLowEdge(i)))
                sumTruth += h->GetBinContent(x);
          rebinnedSherpa->SetBinContent(i, sumTruth);
          rebinnedSherpa->SetBinError(i, std::sqrt(sumTruth));
        }
      }
      else{
        NumV newBins = numberize(settings->GetValue(plot + ".bins", " "));
        auto xAxis = sortedTruth["Sherpa"]->at(0)->GetXaxis();
        for (int k=1; k<4; k++){
          for (int i=1; i<newBins.size(); i++){ // for new binning in X
            double sumTruth=0.; // Reset truth sums
            for (auto h : *(sortedTruth["Sherpa"])){ // Get the Truth histo
              for (int x=1; x<xAxis->GetNbins()/3+1;x++){
                double val = (xAxis->GetBinCenter(k+3*(x-1))+3-k)/30-.05;
                if ( (val > newBins[i-1]) && (val < newBins[i]) ){ // Value from original histo falls within new bounds
                  sumTruth += h->GetBinContent(k+3*(x-1));
                }
              }
            }
            rebinnedSherpa->SetBinContent(k+3*(i-1), sumTruth);
            rebinnedSherpa->SetBinError(k+3*(i-1), std::sqrt(sumTruth));
          }
        }
      }
      RebinnedTruth["Sherpa"]->push_back((TH1F*) rebinnedSherpa->Clone());

      // Rebin MadGraph Response matrix
      if (!plot.Contains("ZJpT")){
        for (int i=1; i<nBins+1; i++){
          for (int j=1; j<nBins+1; j++){
            double sumResp=0.; // Reset response sum
            // Rebin Response matrix
            auto xAxis = sortedResp["MadGraph"]->at(0)->GetXaxis();
            auto yAxis = sortedResp["MadGraph"]->at(0)->GetYaxis();
            for (auto h : *(sortedResp["MadGraph"])){ // Get the Response matrix
              for (int x=1; x<xAxis->GetNbins()+1; x++){ // Loop through x Axis first
                if ((xAxis->GetBinCenter(x) < rebinnedMG->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinnedMG->GetBinLowEdge(i)))
                  for (int y=1; y<yAxis->GetNbins()+1; y++) // Loop through y Axis
                    if ((yAxis->GetBinCenter(y) < rebinnedMG->GetBinLowEdge(j+1)) && (yAxis->GetBinCenter(y) > rebinnedMG->GetBinLowEdge(j)))
                      sumResp += h->GetBinContent(x,y);
              }
            }
            rebinnedMG2->SetBinContent(i,j, sumResp);
            rebinnedMG2->SetBinError(i,j, std::sqrt(sumResp));
          }
        }
      }
      else{
        // Hard coding a bunch of stuff here because 2D unfolding is annoying.
        for (int i=1; i<31; i++){
          for (int j=1; j<31; j++){
            double sumResp =0.;
            for (auto h : *(sortedResp["MadGraph"])){ // Get response Matrix
              if ( (i<19) && (j<19) ) { // 1x1 bins
                sumResp += h->GetBinContent(i, j);  // first 6 bins are unchanged
              }
              else if ( (i<19) && (j<28) ){ // 1x2 bins
                int J = 0;
                if (j < 22) J = 0;
                else if (j < 25) J = 3;
                else if (j < 28) J = 6;
                sumResp += h->GetBinContent(i, j+J);
                sumResp += h->GetBinContent(i, j+J+3);
              }
              else if ( (i<19) && (j<31) ){ // 1x3 bins
                int J = 9;
                sumResp += h->GetBinContent(i, j+J);
                sumResp += h->GetBinContent(i, j+J+3);
                sumResp += h->GetBinContent(i, j+J+3+3);
              }
              else if ( (i<28) && (j<19) ) { // 2x1 bins
                int I = 0;
                if (i < 22) I = 0;
                else if (i < 25) I = 3;
                else if (i < 28) I = 6;
                sumResp += h->GetBinContent(i+I, j);
                sumResp += h->GetBinContent(i+I+3, j);
              }
              else if ( (i<28) && (j<28) ){ // 2x2 bins
                int J = 0;
                if (j < 22) J = 0;
                else if (j < 25) J = 3;
                else if (j < 28) J = 6;
                int I = 0;
                if (i < 22) I = 0;
                else if (i < 25) I = 3;
                else if (i < 28) I = 6;
                sumResp += h->GetBinContent(i+I, j+J);
                sumResp += h->GetBinContent(i+I, j+J+3);
                sumResp += h->GetBinContent(i+I+3, j+J);
                sumResp += h->GetBinContent(i+I+3, j+J+3);
              }
              else if ( (i<28) && (j<31) ){ // 2x3 bins
                int I = 0;
                if (i < 22) I = 0;
                else if (i < 25) I = 3;
                else if (i < 28) I = 6;
                int J = 9;
                sumResp += h->GetBinContent(i+I, j+J);
                sumResp += h->GetBinContent(i+I, j+J+3);
                sumResp += h->GetBinContent(i+I, j+J+3+3);
                sumResp += h->GetBinContent(i+I+3, j+J);
                sumResp += h->GetBinContent(i+I+3, j+J+3);
                sumResp += h->GetBinContent(i+I+3, j+J+3+3);
              }
              else if ( (i<31) && (j<19) ){ // 3x1 bins
                int I = 9;
                sumResp += h->GetBinContent(i+I, j);
                sumResp += h->GetBinContent(i+I+3, j);
                sumResp += h->GetBinContent(i+I+3+3, j);
              }
              else if ( (i<31) && (j<28) ){ // 3x2 bins
                int I = 9;
                int J = 0;
                if (j < 22) J = 0;
                else if (j < 25) J = 3;
                else if (j < 28) J = 6;
                sumResp += h->GetBinContent(i+I, j+J);
                sumResp += h->GetBinContent(i+I+3, j+J);
                sumResp += h->GetBinContent(i+I+3+3, j+J);
                sumResp += h->GetBinContent(i+I, j+J+3);
                sumResp += h->GetBinContent(i+I+3, j+J+3);
                sumResp += h->GetBinContent(i+I+3+3, j+J+3);
              }
              else if ( (i<31) && (j<31) ){ // 3x3 bins
                int I = 9;
                int J = 9;
                sumResp += h->GetBinContent(i+I, j+J);
                sumResp += h->GetBinContent(i+I, j+J+3);
                sumResp += h->GetBinContent(i+I, j+J+3+3);
                sumResp += h->GetBinContent(i+I+3, j+J);
                sumResp += h->GetBinContent(i+I+3, j+J+3);
                sumResp += h->GetBinContent(i+I+3, j+J+3+3);
                sumResp += h->GetBinContent(i+I+3+3, j+J);
                sumResp += h->GetBinContent(i+I+3+3, j+J+3);
                sumResp += h->GetBinContent(i+I+3+3, j+J+3+3);
              }
              else fatal("Need to fix ZJpT rebinning!");
            }
            rebinnedMG2->SetBinContent(i,j, sumResp);
            rebinnedMG2->SetBinError(i,j, std::sqrt(sumResp));
          }
        }
      }
      RebinnedResp["MadGraph"]->push_back((TH2F*) rebinnedMG2->Clone());
      // Rebin MadGraph Measured
      if (!plot.Contains("ZJpT")){
        for (int i=1; i<nBins+1; i++){
          double sumMeas=0.; // Reset Meas sums
          // Measured values!
          auto xAxis = sortedMeas["MadGraph"]->at(0)->GetXaxis();
          for (auto h : *(sortedMeas["MadGraph"])) // Get the Measured histo
            for (int x=1; x<xAxis->GetNbins()+1; x++) // Loop through x Axis
              if ((xAxis->GetBinCenter(x) < rebinnedMG->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinnedMG->GetBinLowEdge(i)))
                sumMeas += h->GetBinContent(x);
          rebinnedMG->SetBinContent(i, sumMeas);
          rebinnedMG->SetBinError(i, std::sqrt(sumMeas));
        }
      }
      else{
        NumV newBins = numberize(settings->GetValue(plot + ".bins", " "));
        auto xAxis = sortedMeas["MadGraph"]->at(0)->GetXaxis();
        for (int k=1; k<4; k++){
          for (int i=1; i<newBins.size(); i++){ // for new binning in X
            double sumMeas=0.; // Reset meas sums
            for (auto h : *(sortedMeas["MadGraph"])){ // Get the meas histo
              for (int x=1; x<xAxis->GetNbins()/3+1;x++){
                double val = (xAxis->GetBinCenter(k+3*(x-1))+3-k)/30-.05;
                if ( (val > newBins[i-1]) && (val < newBins[i]) ){ // Value from original histo falls within new bounds
                  sumMeas += h->GetBinContent(k+3*(x-1));
                }
              }
            }
            rebinnedMG->SetBinContent(k+3*(i-1), sumMeas);
            rebinnedMG->SetBinError(k+3*(i-1), std::sqrt(sumMeas));
          }
        }
      }
      RebinnedMeas["MadGraph"]->push_back((TH1F*) rebinnedMG->Clone());
      // Rebin Madgraph Truth
      if (!plot.Contains("ZJpT")){
        for (int i=1; i<nBins+1; i++){
          double sumTruth=0.; // Reset truth sums
          // Truth values!
          auto xAxis = sortedTruth["MadGraph"]->at(0)->GetXaxis();
          for (auto h : *(sortedTruth["MadGraph"])) // Get the Truth histo
            for (int x=1; x<xAxis->GetNbins()+1; x++) // Loop through x Axis
              if ((xAxis->GetBinCenter(x) < rebinnedMG->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinnedMG->GetBinLowEdge(i)))
                sumTruth += h->GetBinContent(x);
          rebinnedMG->SetBinContent(i, sumTruth);
          rebinnedMG->SetBinError(i, std::sqrt(sumTruth));
        }
      }
      else{
        NumV newBins = numberize(settings->GetValue(plot + ".bins", " "));
        auto xAxis = sortedTruth["MadGraph"]->at(0)->GetXaxis();
        for (int k=1; k<4; k++){
          for (int i=1; i<newBins.size(); i++){ // for new binning in X
            double sumTruth=0.; // Reset truth sums
            for (auto h : *(sortedTruth["MadGraph"])){ // Get the Truth histo
              for (int x=1; x<xAxis->GetNbins()/3+1;x++){
                double val = (xAxis->GetBinCenter(k+3*(x-1))+3-k)/30-.05;
                if ( (val > newBins[i-1]) && (val < newBins[i]) ){ // Value from original histo falls within new bounds
                  sumTruth += h->GetBinContent(k+3*(x-1));
                }
              }
            }
            rebinnedMG->SetBinContent(k+3*(i-1), sumTruth);
            rebinnedMG->SetBinError(k+3*(i-1), std::sqrt(sumTruth));
          }
        }
      }
      RebinnedTruth["MadGraph"]->push_back((TH1F*) rebinnedMG->Clone());

      // Rebin Background Response matrix
      if (!plot.Contains("ZJpT")){
        for (int i=1; i<nBins+1; i++){
          for (int j=1; j<nBins+1; j++){
            double sumResp=0.; // Reset response sum
            // Rebin Response matrix
            auto xAxis = sortedResp["Background"]->at(0)->GetXaxis();
            auto yAxis = sortedResp["Background"]->at(0)->GetYaxis();
            for (auto h : *(sortedResp["Background"])){ // Get the Response matrix
              for (int x=1; x<xAxis->GetNbins()+1; x++){ // Loop through x Axis first
                if ((xAxis->GetBinCenter(x) < rebinnedBG->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinnedBG->GetBinLowEdge(i)))
                  for (int y=1; y<yAxis->GetNbins()+1; y++) // Loop through y Axis
                    if ((yAxis->GetBinCenter(y) < rebinnedBG->GetBinLowEdge(j+1)) && (yAxis->GetBinCenter(y) > rebinnedBG->GetBinLowEdge(j)))
                      sumResp += h->GetBinContent(x,y);
              }
            }
            rebinnedBG2->SetBinContent(i,j, sumResp);
            rebinnedBG2->SetBinError(i,j, std::sqrt(sumResp));
          }
        }
      }
      else{
        for (int i=1; i<nBins+1; i++){ // 15 bins
          for (int j=1; j<nBins+1; j++){ // 15 bins
            double sumResp=0.; // Reset response sum
            // Rebin Response matrix
            auto xAxis = sortedResp["Background"]->at(0)->GetXaxis();
            auto yAxis = sortedResp["Background"]->at(0)->GetYaxis();
            for (auto h : *(sortedResp["Background"]))
              for (int k=start; k<stop; k++)
                for (int x=1; x<xAxis->GetNbins()/3+1; x++) // Loop through x Axis first
                  if ((xAxis->GetBinCenter(k+3*(x-1))*0.1/3-0.05 < rebinnedBG->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(k+3*(x-1))*0.1/3-0.05 > rebinnedBG->GetBinLowEdge(i)))
                    for (int y=1; y<yAxis->GetNbins()/3+1; y++) // Loop through y Axis
                      if ((yAxis->GetBinCenter(k+3*(y-1))*0.1/3-0.05 < rebinnedBG->GetBinLowEdge(j+1)) && (yAxis->GetBinCenter(k+3*(y-1))*0.1/3-0.05 > rebinnedBG->GetBinLowEdge(j)))
                        sumResp = sumResp + h->GetBinContent(k+3*(x-1),k+3*(y-1));
            rebinnedBG2->SetBinContent(i,j, sumResp);
            rebinnedBG2->SetBinError(i,j, std::sqrt(sumResp));
          }
        }
      }
      RebinnedResp["Background"]->push_back((TH2F*) rebinnedBG2->Clone());
      // Rebin Background Measured
      if (!plot.Contains("ZJpT")){
        for (int i=1; i<nBins+1; i++){
          double sumMeas=0.; // Reset Meas sums
          // Measured values!
          auto xAxis = sortedMeas["Background"]->at(0)->GetXaxis();
          for (auto h : *(sortedMeas["Background"])) // Get the Measured histo
            for (int x=1; x<xAxis->GetNbins()+1; x++) // Loop through x Axis
              if ((xAxis->GetBinCenter(x) < rebinnedBG->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinnedBG->GetBinLowEdge(i)))
                sumMeas += h->GetBinContent(x);
          rebinnedBG->SetBinContent(i, sumMeas);
          rebinnedBG->SetBinError(i, std::sqrt(sumMeas));
        }
      }
      else{
        for (int i=1; i<nBins+1; i++){
          double sumMeas=0.; // Reset Meas sums
          // Measured values!
          auto xAxis = sortedMeas["Background"]->at(0)->GetXaxis();
          for (auto h : *(sortedMeas["Background"]))
            for (int k=start; k<stop; k++)
              for (int x=1; x<xAxis->GetNbins()/3+1; x++) // Loop through x Axis first
                if ((xAxis->GetBinCenter(k+3*(x-1))*0.1/3-0.05 < rebinnedBG->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(k+3*(x-1))*0.1/3-0.05 > rebinnedBG->GetBinLowEdge(i)))
                  sumMeas = sumMeas + h->GetBinContent(k+3*(x-1));
          rebinnedBG->SetBinContent(i, sumMeas);
          rebinnedBG->SetBinError(i, std::sqrt(sumMeas));
        }
      }
      RebinnedMeas["Background"]->push_back((TH1F*) rebinnedBG->Clone());
      // Rebin Background Truth
      if (!plot.Contains("ZJpT")){
        for (int i=1; i<nBins+1; i++){
          double sumTruth=0.; // Reset truth sums
          // Truth values!
          auto xAxis = sortedTruth["Background"]->at(0)->GetXaxis();
          for (auto h : *(sortedTruth["Background"])) // Get the Truth histo
            for (int x=1; x<xAxis->GetNbins()+1; x++) // Loop through x Axis
              if ((xAxis->GetBinCenter(x) < rebinnedBG->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinnedBG->GetBinLowEdge(i)))
                sumTruth += h->GetBinContent(x);
          rebinnedBG->SetBinContent(i, sumTruth);
          rebinnedBG->SetBinError(i, std::sqrt(sumTruth));
        }
      }
      else{
        for (int i=1; i<nBins+1; i++){
          double sumTruth=0.; // Reset truth sums
          // Truth values!
          auto xAxis = sortedTruth["Background"]->at(0)->GetXaxis();
          for (auto h : *(sortedTruth["Background"]))
            for (int k=start; k<stop; k++)
              for (int x=1; x<xAxis->GetNbins()/3+1; x++) // Loop through x Axis first
                if ((xAxis->GetBinCenter(k+3*(x-1))*0.1/3-0.05 < rebinnedBG->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(k+3*(x-1))*0.1/3-0.05 > rebinnedBG->GetBinLowEdge(i)))
                  sumTruth = sumTruth + h->GetBinContent(k+3*(x-1));
          rebinnedBG->SetBinContent(i, sumTruth);
          rebinnedBG->SetBinError(i, std::sqrt(sumTruth));
        }
      }
      RebinnedTruth["Background"]->push_back((TH1F*) rebinnedBG->Clone());

      // Rebin Data Measured
      if (!plot.Contains("ZJpT")){
        for (int i=1; i<nBins+1; i++){
          double sumMeas=0.; // Reset Meas sums
          // Measured values!
          auto xAxis = sortedMeas["Data"]->at(0)->GetXaxis();
          for (auto h : *(sortedMeas["Data"])) // Get the Measured histo
            for (int x=1; x<xAxis->GetNbins()+1; x++) // Loop through x Axis
              if ((xAxis->GetBinCenter(x) < rebinnedData->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinnedData->GetBinLowEdge(i)))
                sumMeas += h->GetBinContent(x);
          rebinnedData->SetBinContent(i, sumMeas);
          rebinnedData->SetBinError(i, std::sqrt(sumMeas));
        }
      }
      else{
        for (int i=1; i<nBins+1; i++){
          double sumMeas=0.; // Reset Meas sums
          // Measured values!
          auto xAxis = sortedMeas["Data"]->at(0)->GetXaxis();
          for (auto h : *(sortedMeas["Data"]))
            for (int k=start; k<stop; k++)
              for (int x=1; x<xAxis->GetNbins()/3+1; x++) // Loop through x Axis first
                if ((xAxis->GetBinCenter(k+3*(x-1))*0.1/3-0.05 < rebinnedData->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(k+3*(x-1))*0.1/3-0.05 > rebinnedData->GetBinLowEdge(i)))
                  sumMeas = sumMeas + h->GetBinContent(k+3*(x-1));
          rebinnedData->SetBinContent(i, sumMeas);
          rebinnedData->SetBinError(i, std::sqrt(sumMeas));
        }
      }
      RebinnedMeas["Data"]->push_back((TH1F*) rebinnedData->Clone());

      delete rebinnedSherpa;
      delete rebinnedMG;
      delete rebinnedBG;
      delete rebinnedSherpa2;
      delete rebinnedMG2;
      delete rebinnedBG2;

      if (debug > 0){
        for (auto input : unfoldingInputs){
          std::cout << "Integral of " << input << " " << plot << " = " << RebinnedMeas[input]->at(0)->Integral() << std::endl;
        }
      }

      // Make efficiency histograms
      hname1 = "Sherpa Z#rightarrow ll " + var + "reco efficiency";
      hname2 = "MadGraph Z#rightarrow ll " + var + "reco efficiency";
      TH1F* effhistSH = new TH1F(hname1 ,hname1, nBins, BinsVec);
      TH1F* effhistMG = new TH1F(hname2 ,hname2, nBins, BinsVec);
      effhistSH->SetTitle(";"+Xlabel+";"+"Reco Efficiency");
      effhistSH->SetMinimum(-0.05);
      effhistSH->SetLineColor(857);
      effhistSH->SetMarkerStyle(3);
      effhistMG->SetLineColor(2);
      effhistMG->SetMarkerStyle(25);
      double maxEff =1.0;
      // 1D ZJpT plots!
      if (plot.Contains("ZJpT")){
        Double_t BinsVec1D[11];
        BinsVec1D[0] = 0.;
        for (int i=1; i<7; i++)
          BinsVec1D[i] = BinsVec1D[i-1]+0.1;
        BinsVec1D[7]=0.8;
        BinsVec1D[8]=1.0;
        BinsVec1D[9]=1.2;
        BinsVec1D[10]=1.5;
        TH1F* effhistSH1D = new TH1F(hname1 ,hname1, 10, BinsVec1D);
        TH1F* effhistMG1D = new TH1F(hname2 ,hname2, 10, BinsVec1D);
        TH2F* mergedSherpaResponse = (TH2F*) RebinnedResp["Sherpa"]->at(0)->Clone();
        TH1F* mergedSherpaMeas = (TH1F*) RebinnedMeas["Sherpa"]->at(0)->Clone();
        TH1F* mergedSherpaTrue = (TH1F*) RebinnedTruth["Sherpa"]->at(0)->Clone();
        TH2F* mergedMGResponse = (TH2F*) RebinnedResp["MadGraph"]->at(0)->Clone();
        TH1F* mergedMGMeas = (TH1F*) RebinnedMeas["MadGraph"]->at(0)->Clone();
        TH1F* mergedMGTrue = (TH1F*) RebinnedTruth["MadGraph"]->at(0)->Clone();
        mergedSherpaResponse->Rebin2D(3,3);
        mergedSherpaMeas->Rebin(3);
        mergedSherpaTrue->Rebin(3);
        mergedMGResponse->Rebin2D(3,3);
        mergedMGMeas->Rebin(3);
        mergedMGTrue->Rebin(3);
        effhistSH1D->SetTitle(";"+Xlabel+";"+"Reco Efficiency");
        effhistSH1D->SetMinimum(-0.05);
        effhistSH1D->SetLineColor(857);
        effhistSH1D->SetMarkerStyle(3);
        effhistMG1D->SetLineColor(2);
        effhistMG1D->SetMarkerStyle(25);

        // Calculate 1D ZJpT efficiencies
        for (int i=1; i<11; i++){
          // Sherpa
          double sumResp=0., sumTrue=0.; // Reset sums
          // Passed Reco
          for (int j=1; j<11; j++)
            sumResp += mergedSherpaResponse->GetBinContent(j,i);   // Get the Reco && Truth

          // Pass Truth
          sumTrue += mergedSherpaTrue->GetBinContent(i);   // Get the Truth


          double eff=0.;
          if (sumTrue > 0) eff = sumResp/sumTrue;
          effhistSH1D->SetBinContent(i,eff);
          if (eff > 1) std::cout << "BUSTED EFFICIENCY IN " << plot << " : " << eff << std::endl;
          if (eff > maxEff) maxEff = eff;
          // effhistSH->SetBinError(i,0.000001);
          effhistSH1D->SetBinError(i,eff*(std::sqrt(1/sumResp + 1/sumTrue)));

          // MadGraph
          sumResp=0., sumTrue=0.; // Reset sums
          // Passed Reco
          for (int j=1; j<11; j++)
            sumResp += mergedMGResponse->GetBinContent(j,i);// Get the Reco && Truth

          // Pass Truth
          sumTrue += mergedMGTrue->GetBinContent(i); // Get the Truth

          eff=0.;
          if (sumTrue > 0) eff = sumResp/sumTrue;
          if (eff > 1) std::cout << "BUSTED EFFICIENCY IN " << plot << " : " << eff << std::endl;
          effhistMG1D->SetBinContent(i,eff);
          if (eff > maxEff) maxEff = eff;
          // effhistMG->SetBinError(i,0.000001);
          effhistMG1D->SetBinError(i,eff*(std::sqrt(1/sumResp + 1/sumTrue)));
        }
        if (maxEff > 1.0)
          effhistSH1D->SetMaximum(maxEff * 1.2);
        else
          effhistSH1D->SetMaximum(1.0);
        // Plot Efficiencies
        effhistSH1D->Draw("E");
        effhistMG1D->Draw("ESAME");
        // Normal plotting information
        // double xTitle=0.3, yTitle=0.55;
        double xTitle=0.15, yTitle=0.4;
        if (plot == "ZJpThighDR" && region == "hee") yTitle = 0.9;
        drawPlotInfo(xTitle,yTitle, plot, region, Totlumi);
        if (HighPt) { // Muon HighPt WP!
          drawText(xTitle, yTitle-6*0.04*1.25, "HighPt MuonWP Studies",0,1,0.04);
        }

        TLegend* leg = new TLegend(xTitle+.45, yTitle-.1, xTitle+0.55, yTitle-.215);
        if (plot == "ZJpThighDR" && region == "hee") leg = new TLegend(xTitle+.45, yTitle, xTitle+0.55, yTitle-.115);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.035);
        leg->SetFillStyle(0);

        leg->AddEntry(effhistSH1D,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pl");
        leg->AddEntry(effhistMG1D,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pl");
        leg->Draw();
        can->Print("1Defficiency"+region+plot+".png");

        // Make Acceptance histograms
        hname1 = "Sherpa Z#rightarrow ll " + var + "reco Acceptance";
        hname2 = "MadGraph Z#rightarrow ll " + var + "reco Acceptance";
        TH1F* AcchistSH1D = new TH1F(hname1 ,hname1, 10, BinsVec1D);
        TH1F* AcchistMG1D = new TH1F(hname2 ,hname2, 10, BinsVec1D);
        AcchistSH1D->SetTitle(";"+Xlabel+";"+"Fraction of Unmatched Events");
        AcchistSH1D->SetMinimum(-0.05);
        AcchistSH1D->SetMaximum(1.0);
        AcchistSH1D->SetLineColor(857);
        AcchistSH1D->SetMarkerStyle(3);
        AcchistMG1D->SetLineColor(2);
        AcchistMG1D->SetMarkerStyle(25);
        // Calculate Acceptance
        // How many reco events passed truth & reco over the total number reco
        // ie how many reco events actually passed truth as well!
        for (int i=1; i<11; i++){ // For each reco bin i
          // Sherpa
          double sumReco=0., sumResp=0.; // Reset sums
          // Passed Reco
          for (int j=1; j<11; j++)
            sumResp += mergedSherpaResponse->GetBinContent(i,j);
          // Total reco
          sumReco += mergedSherpaMeas->GetBinContent(i);

          double accept=0.;
          if (sumReco >0) accept = sumResp/sumReco;
          AcchistSH1D->SetBinContent(i,1-accept);
          AcchistSH1D->SetBinError(i,(1-accept)*(std::sqrt(1/sumResp + 1/sumReco)));

          // MadGraph
          sumReco=0., sumResp=0.; // Reset sums
          // Passed Reco
          for (int j=1; j<11; j++)
            sumResp += mergedMGResponse->GetBinContent(i,j);
          // Total reco
          sumReco += mergedMGMeas->GetBinContent(i);

          accept=0.;
          if (sumReco >0) accept = sumResp/sumReco;
          AcchistMG1D->SetBinContent(i,1-accept);
          AcchistMG1D->SetBinError(i,(1-accept)*(std::sqrt(1/sumResp + 1/sumReco)));
        } // End of acceptance bins
        // Plot acceptance
        AcchistSH1D->Draw("E");
        AcchistMG1D->Draw("ESAME");
        // Normal plotting information
        // double xTitle=0.3, yTitle=0.55;
        xTitle=0.15, yTitle=0.9;
        drawPlotInfo(xTitle,yTitle, plot, region, Totlumi);
        if (HighPt) { // Muon HighPt WP!
          drawText(xTitle, yTitle-6*0.04*1.25, "HighPt MuonWP Studies",0,1,0.04);
        }

        leg->Delete();
        leg = new TLegend(xTitle+.45, yTitle, xTitle+0.55, yTitle-.115);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.035);
        leg->SetFillStyle(0);

        leg->AddEntry(AcchistSH1D,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pl");
        leg->AddEntry(AcchistMG1D,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pl");
        leg->Draw();
        can->Print("1Dfakes"+region+plot+".png");

        // Make purity histograms
        hname1 = "Sherpa Z#rightarrow ll " + var + "reco purity";
        hname2 = "MadGraph Z#rightarrow ll " + var + "reco purity";
        TH1F* purityhistSH1D = new TH1F(hname1 ,hname1, 10, BinsVec1D);
        TH1F* purityhistMG1D = new TH1F(hname2 ,hname2, 10, BinsVec1D);
        purityhistSH1D->SetTitle(";"+Xlabel+";"+"Reco Purity");
        purityhistSH1D->SetMinimum(-0.05);
        purityhistSH1D->SetMaximum(1.05);
        purityhistSH1D->SetLineColor(857);
        purityhistSH1D->SetMarkerStyle(3);
        purityhistMG1D->SetLineColor(2);
        purityhistMG1D->SetMarkerStyle(25);

        // Calculate purity
        // Number of reco events in the same truth bin / total number of matched reco events in that bin
        // ie is there much exchange between bins
        // i.e how many reco events are in the correct truth bin.
        for (int i=1; i<11; i++){ // For each truth bin i
          // Sherpa
          double sumTrue=0., sumTotal=0.; // Reset sums
          // Passed Reco
          for (int j=1; j<11; j++){
            sumTotal += mergedSherpaResponse->GetBinContent(i,j); // Add all truth values
            if (i==j) // Is the event in the good truth bin
              sumTrue += mergedSherpaResponse->GetBinContent(i,j);
          }
          double purity=0.;
          if (sumTotal >0) purity = sumTrue/sumTotal;
          purityhistSH1D->SetBinContent(i,purity);
          purityhistSH1D->SetBinError(i,purity*(std::sqrt(1/sumTrue + 1/sumTotal)));

          // MadGraph
          sumTrue=0., sumTotal=0.; // Reset sums
          // Passed Reco
          for (int j=1; j<11; j++){
            sumTotal += mergedMGResponse->GetBinContent(i,j); // Add all truth values
            if (i==j) // Is the event in the good truth bin
              sumTrue += mergedMGResponse->GetBinContent(i,j);
          }
          purity=0.;
          if (sumTotal >0) purity = sumTrue/sumTotal;
          purityhistMG1D->SetBinContent(i,purity);
          purityhistMG1D->SetBinError(i,purity*(std::sqrt(1/sumTrue + 1/sumTotal)));
        } // End of efficiencies bins
        // Plot Efficiencies
        purityhistSH1D->Draw("E");
        purityhistMG1D->Draw("ESAME");
        // Normal plotting information
        xTitle=0.15, yTitle=0.4;
        drawPlotInfo(xTitle,yTitle, plot, region, Totlumi);
        if (HighPt) { // Muon HighPt WP!
          drawText(xTitle, yTitle-6*0.04*1.25, "HighPt MuonWP Studies",0,1,0.04);
        }

        leg->Delete();
        leg = new TLegend(xTitle+.45, yTitle-.1, xTitle+0.55, yTitle-.215);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.035);
        leg->SetFillStyle(0);
        leg->AddEntry(purityhistSH1D,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pl");
        leg->AddEntry(purityhistMG1D,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pl");

        leg->Draw();
        can->Print("1Dpurity"+region+plot+".png");

        // Response matrix
        RooUnfoldResponse response_Sherpa1D (mergedSherpaMeas,mergedSherpaTrue,mergedSherpaResponse);
        RooUnfoldResponse response_MadGraph1D (mergedMGMeas,mergedMGTrue,mergedMGResponse);
        // Sherpa Response matrix
        TString Resp_X = "Detector Level " + Xlabel;
        TString Resp_Y = "Particle Level " + Xlabel;
        auto* R1D = response_Sherpa1D.Hresponse();
        TH2F* result = new TH2F("1D response", "1D response", 10, BinsVec1D, 10, BinsVec1D);
        for (int i=1; i<11; i++)
          for (int j=1; j<11; j++)
            result->SetBinContent(i,j,R1D->GetBinContent(i,j));
        // auto* R = response_Sherpa.HresponseNoOverflow();
        // auto* R = Resp_Sherpa;
        result->SetTitle(";"+Resp_X+";"+Resp_Y);
        result->SetMinimum(1.);
        gPad->SetLogz(0);
        result->SetMaximum(result->GetMaximum()*1.1);
        can->SetRightMargin(0.18);
        result->SetStats(0);
        // Turn this on for adding bin content to reponse matrix
        // Alex!
        // gStyle->SetPaintTextFormat("3.f");
        // R->Draw("colz TEXT");
        // result->Draw("colz text");
        result->Draw("colz");
        drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.035,"Sherpa2.2 ");
        if (HighPt) { // Muon HighPt WP!
          drawText(0.15, 0.9-6*0.035*1.25, "HighPt MuonWP Studies",0,1,0.035);
        }
        // can->Print("1Dresponse_Sherpa"+region+plot+".png");

        R1D = response_MadGraph1D.Hresponse();
        for (int i=1; i<11; i++)
          for (int j=1; j<11; j++)
            result->SetBinContent(i,j,R1D->GetBinContent(i,j));
        // auto* R = response_Sherpa.HresponseNoOverflow();
        // auto* R = Resp_Sherpa;
        result->SetTitle(";"+Resp_X+";"+Resp_Y);
        result->SetMinimum(1.);
        gPad->SetLogz(0);
        result->SetMaximum(result->GetMaximum()*1.1);
        can->SetRightMargin(0.18);
        result->SetStats(0);
        // Turn this on for adding bin content to reponse matrix
        // Alex!
        // gStyle->SetPaintTextFormat("3.f");
        // result->Draw("colz TEXT");
        result->Draw("colz");
        drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.035,"MGPy8EG ");
        if (HighPt) { // Muon HighPt WP!
          drawText(0.15, 0.9-6*0.035*1.25, "HighPt MuonWP Studies",0,1,0.035);
        }
        // can->Print("1Dresponse_MadGraph"+region+plot+".png");



      }
      can->SetRightMargin(0.04); // Needed because I changed them for the response matrices


      // Calculate efficiencies
      // Number of events which passed both reco+truth / total truth
      // i.e how many good truth events were considered as good reco
      for (int i=1; i<nBins+1; i++){ // For each truth bin i
        // Sherpa
        double sumResp=0., sumTrue=0.; // Reset sums
        // Passed Reco
        for (auto h : *(RebinnedResp["Sherpa"])){ // Get the Reco && Truth
          for (int j=1; j<nBins+1; j++)
            sumResp += h->GetBinContent(j,i);
          }
        // Pass Truth
        for (auto h : *(RebinnedTruth["Sherpa"]))// Get the Truth
          sumTrue += h->GetBinContent(i);

        double eff=0.;
        if (sumTrue > 0) eff = sumResp/sumTrue;
        effhistSH->SetBinContent(i,eff);
        if (eff > 1) fatal("BUSTED EFFICIENCY IN " + plot);
        if (eff > maxEff) maxEff = eff;
        // effhistSH->SetBinError(i,0.000001);
        effhistSH->SetBinError(i,eff*(std::sqrt(1/sumResp + 1/sumTrue)));

        // MadGraph
        sumResp=0., sumTrue=0.; // Reset sums
        // Passed Reco
        for (auto h : *(RebinnedResp["MadGraph"])){ // Get the Reco && Truth
          for (int j=1; j<nBins+1; j++)
            sumResp += h->GetBinContent(j,i);
          }
        // Pass Truth
        for (auto h : *(RebinnedTruth["MadGraph"]))// Get the Truth
          sumTrue += h->GetBinContent(i);
        eff=0.;
        if (sumTrue > 0) eff = sumResp/sumTrue;
        if (eff > 1) std::cout << "BUSTED EFFICIENCY IN " << plot << " : " << eff << std::endl;
        effhistMG->SetBinContent(i,eff);
        if (eff > maxEff) maxEff = eff;
        // effhistMG->SetBinError(i,0.000001);
        effhistMG->SetBinError(i,eff*(std::sqrt(1/sumResp + 1/sumTrue)));

      } // End of efficiencies bins
      if (maxEff > 1.0)
        effhistSH->SetMaximum(maxEff * 1.2);
      else
        effhistSH->SetMaximum(1.0);
      // Plot Efficiencies
      effhistSH->Draw("E");
      effhistMG->Draw("ESAME");
      // Normal plotting information
      // double xTitle=0.3, yTitle=0.55;
      double xTitle=0.15, yTitle=0.4;
      if (plot == "ZJpThighDR" && region == "hee") yTitle = 0.9;
      drawPlotInfo(xTitle,yTitle, plot, region, Totlumi);
      if (HighPt) { // Muon HighPt WP!
        drawText(xTitle, yTitle-6*0.04*1.25, "HighPt MuonWP Studies",0,1,0.04);
      }

      TLegend* leg = new TLegend(xTitle+.45, yTitle-.1, xTitle+0.55, yTitle-.215);
      if (plot == "ZJpThighDR" && region == "hee") leg = new TLegend(xTitle+.45, yTitle, xTitle+0.55, yTitle-.115);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.035);
      leg->SetFillStyle(0);

      leg->AddEntry(effhistSH,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pl");
      leg->AddEntry(effhistMG,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pl");
      leg->Draw();

      can->Print("efficiency"+region+plot+".png");
      // End of efficiencies!

      // Make Acceptance histograms
      hname1 = "Sherpa Z#rightarrow ll " + var + "reco Acceptance";
      hname2 = "MadGraph Z#rightarrow ll " + var + "reco Acceptance";
      TH1F* AcchistSH = new TH1F(hname1 ,hname1, nBins, BinsVec);
      TH1F* AcchistMG = new TH1F(hname2 ,hname2, nBins, BinsVec);
      AcchistSH->SetTitle(";"+Xlabel+";"+"Fraction of Unmatched Events");
      AcchistSH->SetMinimum(-0.05);
      AcchistSH->SetMaximum(1.0);
      AcchistSH->SetLineColor(857);
      AcchistSH->SetMarkerStyle(3);
      AcchistMG->SetLineColor(2);
      AcchistMG->SetMarkerStyle(25);
      // Calculate Acceptance
      // How many reco events passed truth & reco over the total number reco
      // ie how many reco events actually passed truth as well!
      for (int i=1; i<nBins+1; i++){ // For each reco bin i
        // Sherpa
        double sumReco=0., sumResp=0.; // Reset sums
        // Passed Reco
        for (auto h : *(RebinnedResp["Sherpa"])){ // Get the Reco && Truth
          for (int j=1; j<nBins+1; j++)
            sumResp += h->GetBinContent(i,j);
        }
        // Total reco
        for (auto h : *(RebinnedMeas["Sherpa"]))// Get the Reco
          sumReco += h->GetBinContent(i);

        // std::cout << "alex Sherpa bin " << i << std::endl;
        // std::cout << "Passed Reco && truth: " << sumResp << std::endl;
        // std::cout << "Passed reco only: " << sumReco << std::endl;
        // std::cout << "acceptance rate = " << sumResp/sumReco << std::endl;
        // if (sumReco / sumResp < 1) fatal("ALEX");
        double accept=0.;
        if (sumReco >0) accept = sumResp/sumReco;
        AcchistSH->SetBinContent(i,1-accept);
        AcchistSH->SetBinError(i,(1-accept)*(std::sqrt(1/sumResp + 1/sumReco)));
        // AcchistSH->SetBinError(i,0.000001);

        // MadGraph
        sumReco=0., sumResp=0.; // Reset sums
        // Passed Reco
        for (auto h : *(RebinnedResp["MadGraph"])){ // Get the Reco && Truth
          for (int j=1; j<nBins+1; j++)
            sumResp += h->GetBinContent(i,j);
        }
        // Total reco
        for (auto h : *(RebinnedMeas["MadGraph"]))// Get the Reco
          sumReco += h->GetBinContent(i);

        accept=0.;
        if (sumReco >0) accept = sumResp/sumReco;
        AcchistMG->SetBinContent(i,1-accept);
        // AcchistMG->SetBinError(i,0.000001);
        AcchistMG->SetBinError(i,(1-accept)*(std::sqrt(1/sumResp + 1/sumReco)));
      } // End of acceptance bins
      // Plot acceptance
      AcchistSH->Draw("E");
      AcchistMG->Draw("ESAME");
      // Normal plotting information
      // double xTitle=0.3, yTitle=0.55;
      xTitle=0.15, yTitle=0.9;
      if (plot == "ZJpThighDR" && region == "hee") xTitle = 0.55;
      drawPlotInfo(xTitle,yTitle, plot, region, Totlumi);
      if (HighPt) { // Muon HighPt WP!
        drawText(xTitle, yTitle-6*0.04*1.25, "HighPt MuonWP Studies",0,1,0.04);
      }

      leg->Delete();
      leg = new TLegend(xTitle+.45, yTitle, xTitle+0.55, yTitle-.115);
      if (plot == "ZJpThighDR" && region == "hee") leg = new TLegend(0.15, yTitle,0.25, yTitle-.115);

      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.035);
      leg->SetFillStyle(0);

      leg->AddEntry(AcchistSH,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pl");
      leg->AddEntry(AcchistMG,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pl");


      leg->Draw();

      can->Print("fakes"+region+plot+".png");
      // delete effhistSH;
      // delete effhistMG;
      // End of acceptance!

      // Make purity histograms
      hname1 = "Sherpa Z#rightarrow ll " + var + "reco purity";
      hname2 = "MadGraph Z#rightarrow ll " + var + "reco purity";
      TH1F* purityhistSH = new TH1F(hname1 ,hname1, nBins, BinsVec);
      TH1F* purityhistMG = new TH1F(hname2 ,hname2, nBins, BinsVec);
      purityhistSH->SetTitle(";"+Xlabel+";"+"Reco Purity");
      purityhistSH->SetMinimum(-0.05);
      purityhistSH->SetMaximum(1.05);
      purityhistSH->SetLineColor(857);
      purityhistSH->SetMarkerStyle(3);
      purityhistMG->SetLineColor(2);
      purityhistMG->SetMarkerStyle(25);
      // Calculate purity
      // Number of reco events in the same truth bin / total number of matched reco events in that bin
      // ie is there much exchange between bins
      // i.e how many reco events are in the correct truth bin.
      for (int i=1; i<nBins+1; i++){ // For each truth bin i
        // Sherpa
        double sumTrue=0., sumTotal=0.; // Reset sums
        // Passed Reco
        for (auto h : *(RebinnedResp["Sherpa"])){ // Get the Reco && Truth
          for (int j=1; j<nBins+1; j++){
            sumTotal += h->GetBinContent(i,j); // Add all truth values
            if (i==j) // Is the event in the good truth bin
              sumTrue += h->GetBinContent(i,j);
          }
        }
        double purity=0.;
        if (sumTotal >0) purity = sumTrue/sumTotal;
        purityhistSH->SetBinContent(i,purity);
        // purityhistSH->SetBinError(i,0.000001);
        purityhistSH->SetBinError(i,purity*(std::sqrt(1/sumTrue + 1/sumTotal)));

        // MadGraph
        sumTrue=0., sumTotal=0.; // Reset sums
        // Passed Reco
        for (auto h : *(RebinnedResp["MadGraph"])){ // Get the Reco && Truth
          for (int j=1; j<nBins+1; j++){
            sumTotal += h->GetBinContent(i,j); // Add all truth values
            if (i==j) // Is the event in the good truth bin
              sumTrue += h->GetBinContent(i,j);
          }
        }
        purity=0.;
        if (sumTotal >0) purity = sumTrue/sumTotal;
        purityhistMG->SetBinContent(i,purity);
        // purityhistMG->SetBinError(i,0.000001);
        purityhistMG->SetBinError(i,purity*(std::sqrt(1/sumTrue + 1/sumTotal)));
      } // End of efficiencies bins
      // Plot Efficiencies
      purityhistSH->Draw("E");
      purityhistMG->Draw("ESAME");
      // Normal plotting information
      xTitle=0.15, yTitle=0.4;
      drawPlotInfo(xTitle,yTitle, plot, region, Totlumi);
      if (HighPt) { // Muon HighPt WP!
        drawText(xTitle, yTitle-6*0.04*1.25, "HighPt MuonWP Studies",0,1,0.04);
      }

      leg->Delete();
      leg = new TLegend(xTitle+.45, yTitle-.1, xTitle+0.55, yTitle-.215);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.035);
      leg->SetFillStyle(0);

      leg->AddEntry(purityhistSH,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pl");
      leg->AddEntry(purityhistMG,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pl");

      leg->Draw();

      can->Print("purity"+region+plot+".png");
      // End of efficiencies!

      // Need this for cross sections && closure
      TH1F* Meas_Sherpa = (TH1F*) RebinnedMeas["Sherpa"]->at(0)->Clone();
      TH1F* Meas_MadGraph = (TH1F*) RebinnedMeas["MadGraph"]->at(0)->Clone();
      TH1F* Truth_Sherpa = (TH1F*) RebinnedTruth["Sherpa"]->at(0)->Clone();
      TH1F* Truth_MadGraph = (TH1F*) RebinnedTruth["MadGraph"]->at(0)->Clone();
      TH2F* Resp_Sherpa = (TH2F*) RebinnedResp["Sherpa"]->at(0)->Clone();
      TH2F* Resp_MadGraph = (TH2F*) RebinnedResp["MadGraph"]->at(0)->Clone();
      // Prepare for plotting
      bool logY = true;
      double ratio = 0.36, margin = 0.02;
      TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
      TH1F* ratioPlot;
      TH1F* ratioPlot2;
      TH1F* ratioOne;


      // bool doUnfoldingClosure = (bool)settings->GetValue("doUnfoldingClosure", 0);
      bool doUnfoldingClosure = true;
      if (doUnfoldingClosure){
        // Start the unfolding closure tests!
        auto xAxis = Resp_Sherpa->GetXaxis();
        auto yAxis = Resp_Sherpa->GetYaxis();
        // continue;
        RooUnfoldResponse response_Sherpa (Meas_Sherpa,Truth_Sherpa,Resp_Sherpa);
        RooUnfoldResponse response_MadGraph (Meas_MadGraph,Truth_MadGraph,Resp_MadGraph);
        // Sherpa Response matrix
        TString Resp_X = "Detector Level " + Xlabel;
        TString Resp_Y = "Particle Level " + Xlabel;
        auto* R = response_Sherpa.Hresponse();
        // auto* R = response_Sherpa.HresponseNoOverflow();
        // auto* R = Resp_Sherpa;
        R->SetTitle(";"+Resp_X+";"+Resp_Y);
        if (plot=="minDR" || plot=="NJets500" || plot.Contains("ZJpT")){
        // if (plot=="NJets500" || plot.Contains("ZJpT")){
          R->SetMinimum(1.);
          gPad->SetLogz(0);
        }
        else{
          // R->SetMinimum(50.);
          R->SetMinimum(1.);
          // R->SetMaximum(1E7);
          gPad->SetLogz(1);
        }
        R->SetMaximum(R->GetMaximum()*1.1);
        can->SetRightMargin(0.18);
        R->SetStats(0);
        // Turn this on for adding bin content to reponse matrix
        // Alex!
        // gStyle->SetPaintTextFormat("2.g");
        // gStyle->SetPaintTextFormat("3.f");
        R->Draw("colz TEXT");
        R->Draw("colz");
        drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.035,"Sherpa2.2 ");
        if (HighPt) { // Muon HighPt WP!
          drawText(0.15, 0.9-6*0.035*1.25, "HighPt MuonWP Studies",0,1,0.035);
        }
        can->Print("response_Sherpa"+region+plot+".png");

        // R = response_MadGraph.HresponseNoOverflow();
        R = response_MadGraph.Hresponse();
        // R= Resp_MadGraph;
        R->SetTitle(";"+Resp_X+";"+Resp_Y);
        if (plot=="minDR" || plot=="NJets500" || plot.Contains("ZJpT")){
          R->SetMinimum(1.);
          gPad->SetLogz(0);
        }
        else{
          R->SetMinimum(1.);
          // R->SetMinimum(50.);
          // R->SetMaximum(1E7);
          gPad->SetLogz(1);
        }
        R->SetMaximum(R->GetMaximum()*1.1);
        can->SetRightMargin(0.18);
        R->SetStats(0);
        // gStyle->SetPaintTextFormat("3.f");
        // R->Draw("colz TEXT");
        R->Draw("colz");
        drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.035,"MGPy8EG ");
        if (HighPt) { // Muon HighPt WP!
          drawText(0.15, 0.9-6*0.035*1.25, "HighPt MuonWP Studies",0,1,0.035);
        }
        can->Print("response_MadGraph"+region+plot+".png");

        // Do unfolding and plots!
        // RooUnfoldBayes unfold (&response_Sherpa, Meas_Sherpa, iterations);
        // RooUnfoldSvd unfold (&response_Sherpa, Meas_Sherpa, kterm);
        // RooUnfoldBinByBin unfold (&response_Sherpa, Meas_Sherpa);
        // Unfolding by inverting the matrix
        RooUnfoldInvert unfold_Sherpa (&response_Sherpa, Meas_Sherpa);
        RooUnfoldInvert unfold_MadGraph (&response_MadGraph, Meas_MadGraph);
        auto* hReco_Sherpa = unfold_Sherpa.Hreco();
        auto* hReco_MadGraph = unfold_MadGraph.Hreco();
        // Iterations: Bayesian unfolding!
        RooUnfoldBayes unfold_bayesian_Sherpa (&response_Sherpa, Meas_Sherpa, iterations);
        RooUnfoldBayes unfold_bayesian_MadGraph (&response_MadGraph, Meas_MadGraph, iterations);
        auto* hReco_bayesian_Sherpa = unfold_bayesian_Sherpa.Hreco();
        auto* hReco_bayesian_MadGraph = unfold_bayesian_MadGraph.Hreco();
        // Svd
        // Prepare for plotting
        logY = true;
        ratio = 0.36, margin = 0.02;
        can->cd();
        gPad->SetBottomMargin(ratio);
        gPad->SetLogz(0);
        can->SetLogy(1);
        can->SetRightMargin(0.04);
        // if (plot=="NJets500" || plot.Contains("ZJpT")){
        // if (plot=="minDR" || plot=="NJets500" || plot.Contains("ZJpT")){
        if (plot=="minDR" || plot=="NJets500"){
          can->SetLogy(0);
          logY=false;
        }
        else{
          gPad->SetLogy(1);
        }
        // Start by defining the axis, and the min/max values
        hReco_bayesian_Sherpa->SetTitle(";"+Xlabel+";"+Ylabel);
        hReco_bayesian_Sherpa->GetXaxis()->SetTitleOffset(1.4);
        hReco_bayesian_Sherpa->GetYaxis()->SetLabelSize(0.03);
        hReco_bayesian_Sherpa->GetYaxis()->SetTitleOffset(1.4);
        hReco_bayesian_Sherpa->GetXaxis()->SetLabelSize(0);
        if(logY){
          hReco_bayesian_Sherpa->SetMaximum(hReco_bayesian_Sherpa->GetMaximum()*100);
          if(hReco_bayesian_Sherpa->GetMinimum() > 100)
            hReco_bayesian_Sherpa->SetMinimum(hReco_bayesian_Sherpa->GetMinimum()*0.05);
        }
        else
          hReco_bayesian_Sherpa->SetMaximum(hReco_bayesian_Sherpa->GetMaximum()*2.0);

        if (plot == "NJets") hReco_bayesian_Sherpa->SetMaximum(hReco_bayesian_Sherpa->GetMaximum()*10.0);
        if (plot == "ZJpTlowDR") hReco_bayesian_Sherpa->SetMaximum(hReco_bayesian_Sherpa->GetMaximum()*5.0);
        if (plot == "ZJpTlowDR" && region == "hmm") hReco_bayesian_Sherpa->SetMaximum(hReco_bayesian_Sherpa->GetMaximum()*5.0);
        if (plot == "pTZ") hReco_bayesian_Sherpa->SetMaximum(hReco_bayesian_Sherpa->GetMaximum()*5.0);
        if (plot == "pTjet") hReco_bayesian_Sherpa->SetMaximum(hReco_bayesian_Sherpa->GetMaximum()*5.0);

        // Define the Sherpa plots
        hReco_bayesian_Sherpa->SetLineWidth(0);
        hReco_bayesian_Sherpa->SetFillStyle(0);
        hReco_bayesian_Sherpa->SetLineColor(kRed);
        hReco_bayesian_Sherpa->SetMarkerStyle(25);
        hReco_bayesian_Sherpa->SetMarkerColor(kRed); // Red
        hReco_Sherpa->SetLineWidth(0);
        hReco_Sherpa->SetFillStyle(0);
        hReco_Sherpa->SetLineColor(kBlack);
        hReco_Sherpa->SetMarkerStyle(3);
        hReco_Sherpa->SetMarkerColor(kBlack); // Black
        Meas_Sherpa->SetLineColor(857); // Nice blue
        Truth_Sherpa->SetLineColor(417); // Nice green
        // Draw the plots!
        hReco_bayesian_Sherpa->Draw();
        hReco_Sherpa->Draw("same");
        Meas_Sherpa->Draw("same HIST");
        Truth_Sherpa->Draw("same HIST");
        // default is 0.3, 0.55
        drawPlotInfo(xTitle,0.9, plot, region, Totlumi, 0.035);
        if (HighPt) { // Muon HighPt WP!
          drawText(xTitle, 0.9-6*0.035*1.25, "HighPt MuonWP Studies",0,1,0.035);
        }

        leg->Delete();
        leg = new TLegend(0.5, 0.70, 0.65, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetFillStyle(0);
        leg->SetTextSize(0.03);

        leg->AddEntry(hReco_bayesian_Sherpa,"Bayesian Unfolded Sherpa2.2","pl");
        leg->AddEntry(hReco_Sherpa,"Inverted Unfolded Sherpa2.2","pl");
        leg->AddEntry(Meas_Sherpa,"Reco Sherpa2.2","pl");
        leg->AddEntry(Truth_Sherpa,"Truth Sherpa2.2","pl");
        leg->Draw();
        // Create a TPad for the ratio plot
        p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        p->SetTopMargin(1.0 - ratio);
        p->SetFillStyle(0);
        p->SetTicks();
        p->SetGridy();
        p->Draw();
        p->cd();
        // Make the ratio plot!
        ratioPlot = (TH1F*) hReco_bayesian_Sherpa->Clone();
        ratioPlot->Divide((TH1F*) Truth_Sherpa);
        ratioPlot->SetMarkerSize(1);
        ratioPlot->SetLineWidth(1);
        ratioPlot->SetFillStyle(0);
        ratioPlot->SetLineColor(kRed);
        ratioPlot->SetMarkerColor(kRed);
        ratioPlot->SetMarkerStyle(25);
        ratioPlot2 = (TH1F*) hReco_Sherpa->Clone();
        ratioPlot2->Divide((TH1F*) Truth_Sherpa);
        ratioPlot2->SetMarkerSize(1);
        ratioPlot2->SetLineWidth(1);
        ratioPlot2->SetFillStyle(0);
        ratioPlot2->SetLineColor(kBlack);
        ratioPlot2->SetMarkerColor(kBlack);
        ratioPlot2->SetMarkerStyle(3);

        // Define the ratio plot info
        ratioPlot->GetYaxis()->SetLabelSize(0.03);
        ratioPlot->GetYaxis()->SetTitle("Unfolded/Truth");
        ratioPlot->GetXaxis()->SetLabelSize(0.03);
        ratioPlot->GetXaxis()->SetTitleOffset(1.4);
        ratioPlot->GetYaxis()->SetTitleOffset(1.4);
        ratioPlot->GetYaxis()->SetRangeUser(0.95, 1.05);
        ratioPlot->Draw("HIST P");
        ratioPlot2->Draw("SAME HIST P");
        // Draw Black line centered at 1.
        ratioOne = (TH1F*) ratioPlot->Clone();
        for (int i=1; i<ratioPlot->GetNbinsX()+1;i++){
         ratioOne->SetBinContent(i,1);
         ratioOne->SetBinError(i,0);
        }
        ratioOne->SetLineColor(kBlack);
        ratioOne->Draw("same");

        can->Print("closure_Sherpa"+region+plot+".png");

        // MadGraph unfolding plots
        // Prepare for plotting
        logY = true;
        ratio = 0.36, margin = 0.02;
        can->cd();
        gPad->SetBottomMargin(ratio);
        gPad->SetLogz(0);
        can->SetLogy(1);
        can->SetRightMargin(0.04);
        // if (plot=="NJets500" || plot.Contains("ZJpT")){
        // if (plot=="minDR" || plot=="NJets500" || plot.Contains("ZJpT")){
        if (plot=="minDR" || plot=="NJets500"){
          can->SetLogy(0);
          logY=false;
        }
        else{
          gPad->SetLogy(1);
        }
        // Start by defining the axis, and the min/max values
        hReco_bayesian_MadGraph->SetTitle(";"+Xlabel+";"+Ylabel);
        hReco_bayesian_MadGraph->GetXaxis()->SetTitleOffset(1.4);
        hReco_bayesian_MadGraph->GetYaxis()->SetLabelSize(0.03);
        hReco_bayesian_MadGraph->GetYaxis()->SetTitleOffset(1.4);
        hReco_bayesian_MadGraph->GetXaxis()->SetLabelSize(0);
        if(logY){
          hReco_bayesian_MadGraph->SetMaximum(hReco_bayesian_MadGraph->GetMaximum()*100);
          if(hReco_bayesian_MadGraph->GetMinimum() > 100)
            hReco_bayesian_MadGraph->SetMinimum(hReco_bayesian_MadGraph->GetMinimum()*0.05);
        }
        else
          hReco_bayesian_MadGraph->SetMaximum(hReco_bayesian_MadGraph->GetMaximum()*2.0);
          if (plot == "NJets") hReco_bayesian_MadGraph->SetMaximum(hReco_bayesian_MadGraph->GetMaximum()*10.0);
          if (plot == "ZJpTlowDR") hReco_bayesian_MadGraph->SetMaximum(hReco_bayesian_MadGraph->GetMaximum()*5.0);
          if (plot == "pTZ") hReco_bayesian_MadGraph->SetMaximum(hReco_bayesian_MadGraph->GetMaximum()*5.0);
          if (plot == "pTjet") hReco_bayesian_MadGraph->SetMaximum(hReco_bayesian_MadGraph->GetMaximum()*5.0);

        // Define the MadGraph plots
        hReco_bayesian_MadGraph->SetLineWidth(0);
        hReco_bayesian_MadGraph->SetFillStyle(0);
        hReco_bayesian_MadGraph->SetLineColor(kRed);
        hReco_bayesian_MadGraph->SetMarkerStyle(25);
        hReco_bayesian_MadGraph->SetMarkerColor(kRed); // Red
        hReco_MadGraph->SetLineWidth(0);
        hReco_MadGraph->SetFillStyle(0);
        hReco_MadGraph->SetLineColor(kBlack);
        hReco_MadGraph->SetMarkerStyle(3);
        hReco_MadGraph->SetMarkerColor(kBlack); // Black
        Meas_MadGraph->SetLineColor(857); // Nice blue
        Truth_MadGraph->SetLineColor(417); // Nice green
        // Draw the plots!
        hReco_bayesian_MadGraph->Draw();
        hReco_MadGraph->Draw("same");
        Meas_MadGraph->Draw("same HIST");
        Truth_MadGraph->Draw("same HIST");
        // default is 0.3, 0.55
        drawPlotInfo(xTitle,0.9, plot, region, Totlumi, 0.035);
        if (HighPt) { // Muon HighPt WP!
          drawText(xTitle, 0.9-6*0.035*1.25, "HighPt MuonWP Studies",0,1,0.035);
        }

        leg->Delete();
        leg = new TLegend(0.5, 0.70, 0.65, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetFillStyle(0);
        leg->SetTextSize(0.030);

        leg->AddEntry(hReco_bayesian_MadGraph,"Bayesian Unfolded MGPy8EG","pl");
        leg->AddEntry(hReco_MadGraph,"Inverted Unfolded MGPy8EG","pl");
        leg->AddEntry(Meas_MadGraph,"Reco MGPy8EG","pl");
        leg->AddEntry(Truth_MadGraph,"Truth MGPy8EG","pl");
        leg->Draw();
        // Create a TPad for the ratio plot
        TPad *p2 = new TPad("p2_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p2->SetLogx(0);
        p2->Modified();
        p2->Update();
        p2->SetTopMargin(1.0 - ratio);
        p2->SetFillStyle(0);
        p2->SetTicks();
        p2->SetGridy();
        p2->Draw();
        p2->cd();
        // Make the ratio plot!
        ratioPlot = (TH1F*) hReco_bayesian_MadGraph->Clone();
        ratioPlot->Divide((TH1F*) Truth_MadGraph);
        ratioPlot->SetMarkerSize(1);
        ratioPlot->SetLineWidth(1);
        ratioPlot->SetFillStyle(0);
        ratioPlot->SetLineColor(kRed);
        ratioPlot->SetMarkerColor(kRed);
        ratioPlot->SetMarkerStyle(25);
        ratioPlot2 = (TH1F*) hReco_MadGraph->Clone();
        ratioPlot2->Divide((TH1F*) Truth_MadGraph);
        ratioPlot2->SetMarkerSize(1);
        ratioPlot2->SetLineWidth(1);
        ratioPlot2->SetFillStyle(0);
        ratioPlot2->SetLineColor(kBlack);
        ratioPlot2->SetMarkerColor(kBlack);
        ratioPlot2->SetMarkerStyle(3);

        // Define the ratio plot info
        ratioPlot->GetYaxis()->SetLabelSize(0.03);
        ratioPlot->GetYaxis()->SetTitle("Unfolded/Truth");
        ratioPlot->GetXaxis()->SetLabelSize(0.03);
        ratioPlot->GetXaxis()->SetTitleOffset(1.4);
        ratioPlot->GetYaxis()->SetTitleOffset(1.4);
        ratioPlot->GetYaxis()->SetRangeUser(0.95, 1.05);
        ratioPlot->Draw("HIST P");
        ratioPlot2->Draw("SAME HIST P");
        // Draw Black line centered at 1.
        ratioOne = (TH1F*) ratioPlot->Clone();
        for (int i=1; i<ratioPlot->GetNbinsX()+1;i++){
         ratioOne->SetBinContent(i,1);
         ratioOne->SetBinError(i,0);
        }
        ratioOne->SetLineColor(kBlack);
        ratioOne->Draw("same");

        can->Print("closure_MadGraph"+region+plot+".png");
      }

      // Draw CROSS SECTIONS! Results!!
      // dsigma/dX_i = 1 / (deltaX_i * Lumi * efficiency_i) * unfolded (acceptance*(data-bkg))
      bool doSingleCrossSection=false;
      if (doSingleCrossSection){
        TH1F* Pre_unfolded_data_sherpa = (TH1F*) RebinnedMeas["Data"]->at(0)->Clone();
        TH1F* Pre_unfolded_data_madgraph = (TH1F*) RebinnedMeas["Data"]->at(0)->Clone();
        TH1F* Pre_unfolded_sherpa = (TH1F*) RebinnedMeas["Sherpa"]->at(0)->Clone();
        TH1F* Pre_unfolded_madgraph = (TH1F*) RebinnedMeas["MadGraph"]->at(0)->Clone();
        TH1F* bg = (TH1F*) RebinnedMeas["Background"]->at(0)->Clone();

        Pre_unfolded_data_sherpa->Add(bg,-1);
        Pre_unfolded_data_madgraph->Add(bg,-1);
        // Define the different RooUnfold Objects
        RooUnfoldResponse response_data_sherpa (Meas_Sherpa,Truth_Sherpa,Resp_Sherpa);
        RooUnfoldResponse response_sherpa (Meas_Sherpa,Truth_Sherpa,Resp_Sherpa);
        RooUnfoldResponse response_data_madgraph (Meas_MadGraph,Truth_MadGraph,Resp_MadGraph);
        RooUnfoldResponse response_madgraph (Meas_MadGraph,Truth_MadGraph,Resp_MadGraph);

        double kterm = 2;
        // RooUnfoldSvd unfolding_data_sherpa (&response_data_sherpa, Pre_unfolded_data_sherpa, kterm);
        // RooUnfoldSvd unfolding_data_madgraph (&response_data_madgraph, Pre_unfolded_data_madgraph, kterm);
        // RooUnfoldSvd unfolding_sherpa (&response_sherpa, Pre_unfolded_sherpa, kterm);
        // RooUnfoldSvd unfolding_madgraph (&response_madgraph, Pre_unfolded_madgraph, kterm);

        RooUnfoldBayes unfolding_data_sherpa (&response_data_sherpa, Pre_unfolded_data_sherpa, iterations);
        RooUnfoldBayes unfolding_data_madgraph (&response_data_madgraph, Pre_unfolded_data_madgraph, iterations);
        RooUnfoldBayes unfolding_sherpa (&response_sherpa, Pre_unfolded_sherpa, iterations);
        RooUnfoldBayes unfolding_madgraph (&response_madgraph, Pre_unfolded_madgraph, iterations);
        // Bin-by-bin
        // RooUnfoldBinByBin unfolding_data_sherpa (&response_data_sherpa, Pre_unfolded_data_sherpa);
        // RooUnfoldBinByBin unfolding_data_madgraph (&response_data_madgraph, Pre_unfolded_data_madgraph);
        // RooUnfoldBinByBin unfolding_sherpa (&response_sherpa, Pre_unfolded_sherpa);
        // RooUnfoldBinByBin unfolding_madgraph (&response_madgraph, Pre_unfolded_madgraph);
        std::cout << "ALEX PLOTTING TABLE FOR DATA in plot " << plot << std::endl;
        unfolding_data_sherpa.PrintTable (cout, Truth_Sherpa);
        std::cout << "ALEX PLOTTING TABLE FOR SHERPA in plot " << plot << std::endl;
        unfolding_sherpa.PrintTable (cout, Truth_Sherpa);


        // Get the unfolded histograms
        auto* unfolded_data_sherpa = unfolding_data_sherpa.Hreco();
        auto* unfolded_data_madgraph = unfolding_data_madgraph.Hreco();
        auto* unfolded_sherpa = unfolding_sherpa.Hreco();
        auto* unfolded_madgraph = unfolding_madgraph.Hreco();
        // Make the cross section histograms
        hname1 = "Data diff. xs" + var;
        hname2 = "Data diff. xs 2" + var;
        hname3 = "Sherpa diff xs" + var;
        hnamed = "MadGraph diff xs" + var;
        TH1F* xs_data_sherpa = new TH1F(hname1 ,hname1, nBins, BinsVec);
        TH1F* xs_data_madgraph = new TH1F(hname2 ,hname2, nBins, BinsVec);
        TH1F* xs_sherpa = new TH1F(hname3 ,hname3, nBins, BinsVec);
        TH1F* xs_madgraph = new TH1F(hnamed ,hnamed, nBins, BinsVec);
        // Evaluate the cross sections!
        for (int i=1; i<nBins+1; i++){
          double binWidth = unfolded_data_sherpa->GetBinWidth(i);
          xs_data_sherpa->SetBinContent(i,unfolded_data_sherpa->GetBinContent(i)/(Totlumi*binWidth));
          xs_sherpa->SetBinContent(i,unfolded_sherpa->GetBinContent(i)/(Totlumi*binWidth));
          xs_data_madgraph->SetBinContent(i,unfolded_data_madgraph->GetBinContent(i)/(Totlumi*binWidth));
          xs_madgraph->SetBinContent(i,unfolded_madgraph->GetBinContent(i)/(Totlumi*binWidth));
        }
        // Plot the cross sections!
        logY = true;
        ratio = 0.36, margin = 0.02;
        can->cd();
        gPad->SetBottomMargin(ratio);
        gPad->SetLogz(0);
        can->SetLogy(1);
        can->SetRightMargin(0.04);
        if (plot=="minDR" || plot=="NJets500"){
          can->SetLogy(0);
          logY=false;
        }
        else{
          gPad->SetLogy(1);
        }
        // Start by defining the axis, and the min/max values
        Str xsLabel = settings->GetValue(plot + ".xs", " ");
        xs_sherpa->SetTitle(";"+Xlabel+";"+xsLabel);
        xs_sherpa->GetXaxis()->SetTitleOffset(1.4);
        xs_sherpa->GetYaxis()->SetLabelSize(0.03);
        xs_sherpa->GetYaxis()->SetTitleOffset(1.4);
        xs_sherpa->GetXaxis()->SetLabelSize(0);
        if(logY){
          xs_sherpa->SetMaximum(xs_sherpa->GetMaximum()*1000);
          if(xs_data_sherpa->GetMinimum() > 100)
            xs_sherpa->SetMinimum(xs_data_sherpa->GetMinimum()*0.05);
        }
        else
          xs_sherpa->SetMaximum(xs_sherpa->GetMaximum()*2.0);
        // Define all the plots
        xs_data_sherpa->SetLineWidth(1);
        // xs_data_sherpa->SetFillStyle(0);
        xs_data_sherpa->SetLineColor(1);
        // xs_data_sherpa->SetMarkerStyle(20);
        xs_data_sherpa->SetMarkerStyle(20);
        xs_sherpa->SetMarkerStyle(20);
        xs_sherpa->SetMarkerSize(0);
        xs_sherpa->SetLineColor(857);
        xs_madgraph->SetMarkerStyle(20);
        xs_madgraph->SetMarkerSize(0);
        xs_madgraph->SetLineColor(kRed);


        // Draw the plots!
        xs_sherpa->Draw();
        gStyle->SetErrorX(01);
        xs_data_sherpa->Draw("same p");
        gStyle->SetErrorX(0.0001);

        // xs_sherpa->Draw("same HIST");
        xs_madgraph->Draw("same HIST");
        // default is 0.3, 0.55
        drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.025);

        leg = new TLegend(0.65, 0.70, 0.80, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.020);
        leg->SetFillStyle(0);

        leg->AddEntry(xs_data_sherpa,"Bayesian Unfolded Data","pl");
        leg->AddEntry(xs_sherpa,"Bayesian Unfolded Sherpa2.2","pl");
        leg->AddEntry(xs_madgraph,"Bayesian Unfolded MGPy8EG","pl");
        leg->Draw();
        // xs_sherpa->Draw("same");
        // Create a TPad for the ratio plot
        p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        p->SetTopMargin(1.0 - ratio);
        p->SetFillStyle(0);
        p->SetTicks();
        p->SetGridy();
        p->Draw();
        p->cd();
        // Make the ratio plot!
        ratioPlot = (TH1F*) xs_sherpa->Clone();
        ratioPlot->Divide((TH1F*) xs_data_sherpa);
        ratioPlot->SetMarkerSize(1);
        ratioPlot->SetLineWidth(1);
        ratioPlot->SetFillStyle(0);
        ratioPlot->SetLineColor(857);
        ratioPlot->SetMarkerColor(857);
        ratioPlot->SetMarkerStyle(25);
        ratioPlot2 = (TH1F*) xs_madgraph->Clone();
        ratioPlot2->Divide((TH1F*) xs_data_madgraph);
        ratioPlot2->SetMarkerSize(1);
        ratioPlot2->SetLineWidth(1);
        ratioPlot2->SetFillStyle(0);
        ratioPlot2->SetLineColor(kRed);
        ratioPlot2->SetMarkerColor(kRed);
        ratioPlot2->SetMarkerStyle(3);

        // Define the ratio plot info
        gStyle->SetErrorX(0.5);
        ratioPlot->GetYaxis()->SetLabelSize(0.03);
        ratioPlot->GetYaxis()->SetTitle("Pred./Data");
        ratioPlot->GetXaxis()->SetLabelSize(0.03);
        ratioPlot->GetXaxis()->SetTitleOffset(1.4);
        ratioPlot->GetYaxis()->SetTitleOffset(1.4);
        ratioPlot->GetYaxis()->SetRangeUser(0.5, 2.0);
        // ratioPlot->GetYaxis()->SetRangeUser(0.3, 1.7);
        ratioPlot->Draw("P");
        ratioPlot2->Draw("SAME P");
        // Draw Black line centered at 1.
        ratioOne = (TH1F*) ratioPlot->Clone();
        for (int i=1; i<ratioPlot->GetNbinsX()+1;i++){
         ratioOne->SetBinContent(i,1);
         ratioOne->SetBinError(i,0);
        }
        ratioOne->SetLineColor(kBlack);
        ratioOne->Draw("same");

        can->Print("cross_section"+region+plot+".png");
      }



    } // end of plotList
    delete can;
  } // End of regions (ie mm or ee)





  return;
}

void doUnfolding(StrVMap sortedSampleFiles, TEnv* settings) {

  TFile *theory = new TFile("/afs/cern.ch/work/a/alaurier/private/ZJets/source/zjets-analysis/macros/ExtraSystematics/UnfoldedTheory_test.root");
  // TFile *theory = new TFile("/eos/user/a/alaurier/ZJets/FinalProduction/UnfoldedTheory.root");
  TFile *unfoldUncert = new TFile("/eos/user/a/alaurier/ZJets/FinalProduction/UnfoldingUncertainty.root");


  // Unfolding options
  // IntV Iterations = interize(settings->GetValue("NumberOfIterations", " ")); // Total number of systematics
  int iterations = settings->GetValue("NumberOfIterations",2); // Total number of systematics
  // int iterations = Iterations[0]; // Total number of systematics
  bool divideByBinWidth = (bool)settings->GetValue("divideBinWidth", 1);
  double lumiUncert = settings->GetValue("LumiUncert.",0.017);
  bool printUnfolding = false;
  bool doSimpleXS = false;
  bool saveXS = (bool)settings->GetValue("saveXS", 1);
  bool saveUncert = (bool)settings->GetValue("saveUncert", 1);
  bool doSherpa = (bool)settings->GetValue("doSherpa", 1);
  if (!saveXS && !saveUncert) divideByBinWidth = false;
  divideByBinWidth = (bool)settings->GetValue("divideBinWidth", 1);
  // Compare cross section ratios
  bool doRatios = (bool)settings->GetValue("doRatios", 0);
  // Combine cross sections and compare to 4 MC generators
  bool doCombine = (bool)settings->GetValue("doCombine", 0);
  // Compare rivet to analysis truth for nominal generators
  // If compareWithRivet is on, everything else is off!
  bool compareWithRivet = (bool)settings->GetValue("compareWithRivet", 0);
  // Get the fit for reweighting reco events.
  bool UnfoldingFit = (bool)settings->GetValue("UnfoldingFit", 0);



  int debug = 0;
  // Get basic values needed
  double Totlumi = settings->GetValue("luminosity", 0.0);
  StrV plotList = vectorize(settings->GetValue("PlotList", " "));
  StrV channels = vectorize(settings->GetValue("channels", " "));
  StrV inputCategories = vectorize(settings->GetValue("inputCategories", " "));
  int nSys = settings->GetValue("Systematics",1);
  bool HighPt = (bool)settings->GetValue("HighPt", 0);
  // Evaluation of error in error stack plots.
  bool doStackErrorQuadrature = (bool)settings->GetValue("QuadError", 0);
  bool doStackErrorMaximum = (bool)settings->GetValue("MaxError", 0);

  if (debug > 1){
    for (auto plot : plotList)
      std::cout << "alex plot = " << plot << std::endl;
    for (auto channel : channels)
      std::cout << "alex channel = " << channel << std::endl;
    for (auto input : inputCategories)
      std::cout << "alex input = " << input << std::endl;
  }

  // StrV inputLabels {"SherpaZJetsResp","SherpaZJetsTrue","MGZJetsResp","MGZJetsTrue"};
  // StrV inputCategories {"SherpaZJets","MGZJets"};
  StrV unfoldingInputs {"Sherpa", "MadGraph", "Background", "Data"};

  HistVMap sortedTruth;
  HistVMap sortedMeas;
  HistVMap2 sortedResp;

  HistVMap RebinnedTruth;
  HistVMap RebinnedMeas;
  HistVMap2 RebinnedResp;

  // Combining channels
  HistVMap data_down_elec;
  HistVMap data_up_elec;
  HistVMap data_down_muon;
  HistVMap data_up_muon;
  HistVMap data_elec;
  HistVMap data_muon;
  HistVMap Sherpa_elec;
  HistVMap MG_elec;
  HistVMap Sherpa_muon;
  HistVMap MG_muon;
  // Ratio plots
  HistVMap muonChannel;
  HistVMap elecChannel;
  HistVMap muonTruth;
  HistVMap muonTruthMG;
  HistVMap elecTruth;
  HistVMap elecTruthMG;
  for (auto plot : plotList){
    muonChannel[plot] = new HistV;
    elecChannel[plot] = new HistV;
    muonTruth[plot] = new HistV;
    muonTruthMG[plot] = new HistV;
    elecTruth[plot] = new HistV;
    elecTruthMG[plot] = new HistV;
    if (doCombine){
      data_down_elec[plot] = new HistV;
      data_up_elec[plot] = new HistV;
      data_down_muon[plot] = new HistV;
      data_up_muon[plot] = new HistV;
      data_elec[plot] = new HistV;
      data_muon[plot] = new HistV;
      Sherpa_elec[plot] = new HistV;
      MG_elec[plot] = new HistV;
      Sherpa_muon[plot] = new HistV;
      MG_muon[plot] = new HistV;
    }
  }

  HistVMap Unfolded;

  // start with muon channel for now
  for (auto region : channels){ // hee or hmm
    if (compareWithRivet) continue;
    // Get plotting information ready
    TCanvas *can = new TCanvas("", "", 800, 800);

    for(auto plot : plotList){

      can->Clear();
      can->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      can->SetBottomMargin(0.12);
      can->SetRightMargin(0.04);

      Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot

      // Empty the maps of histograms and reinitialize them (for safety reasons)
      sortedTruth.clear(); // Reset map of histograms
      sortedMeas.clear(); // Reset map of histograms
      sortedResp.clear(); // Reset map of histograms
      RebinnedTruth.clear(); // Reset map of histograms
      RebinnedMeas.clear(); // Reset map of histograms
      RebinnedResp.clear(); // Reset map of histograms
      Unfolded.clear(); // Reset map of histograms
      for (auto input : unfoldingInputs){ // Make new vectors in each map element (Response + truth x2)
        for (int sys=0; sys<nSys; sys++){
          TString inputSys = input+sys;
          if (sys==0){
            sortedTruth[inputSys] = new HistV;
            RebinnedTruth[inputSys] = new HistV;
          }
          sortedResp[inputSys] = new HistV2;
          sortedMeas[inputSys] = new HistV;
          RebinnedResp[inputSys] = new HistV2;
          RebinnedMeas[inputSys] = new HistV;
          Unfolded[inputSys] = new HistV;
        }
      }
      Unfolded["Data_MG"] = new HistV;

      // Read all the relevant histograms. For now, only use nominal!
      for(auto input : inputCategories){ // For Sherpa & MG
        if (input !="data"){
          for (auto sampleFileName : sortedSampleFiles[input]) { // Loop through SH or MG file
            TFile *sampleFile = TFile::Open(sampleFileName);
            for (int sys=0; sys<nSys; sys++){
              // Get histograms
              TH1F* temp = new TH1F();
              TH2F* temp2 = new TH2F();
              TString newlabel;
              if (input == "SherpaZJets"){ // MG has a different naming convention
                newlabel = "Sherpa"+std::to_string(sys);
                temp2 = getHisto2(sampleFile, region+"resp"+var+sys);
                sortedResp[newlabel]->push_back((TH2F*) temp2->Clone());
                if (sys==0){
                  temp = getHisto(sampleFile, region+"true"+var+sys);
                  sortedTruth[newlabel]->push_back((TH1F*) temp->Clone());
                }
                temp = getHisto(sampleFile, region+"meas"+var+sys);
                sortedMeas[newlabel]->push_back((TH1F*) temp->Clone());
                if (debug > 0) std::cout << "Sherpa integral of "+input+" = " << temp->Integral() << std::endl;
              }
              else if (input == "MGZJets"){
                TH1F* temp = new TH1F();
                TH2F* temp2 = new TH2F();
                newlabel = "MadGraph"+std::to_string(sys);
                temp2 = getHisto2(sampleFile, region+"resp2"+var+sys);
                sortedResp[newlabel]->push_back((TH2F*) temp2->Clone());
                if (sys==0){
                  temp = getHisto(sampleFile, region+"true2"+var+sys);
                  sortedTruth[newlabel]->push_back((TH1F*) temp->Clone());
                }
                temp = getHisto(sampleFile, region+"meas2"+var+sys);
                sortedMeas[newlabel]->push_back((TH1F*) temp->Clone());
                if (debug > 0) std::cout << "MadGraph integral of "+input+" = " << temp->Integral() << std::endl;
              }
              else {
                newlabel = "Background"+std::to_string(sys);
                temp2 = getHisto2(sampleFile, region+"resp"+var+sys);
                sortedResp[newlabel]->push_back((TH2F*) temp2->Clone());
                if (sys==0){
                  temp = getHisto(sampleFile, region+"true"+var+sys);
                  sortedTruth[newlabel]->push_back((TH1F*) temp->Clone());
                }
                temp = getHisto(sampleFile, region+"meas"+var+sys);
                sortedMeas[newlabel]->push_back((TH1F*) temp->Clone());
                if (debug > 0) std::cout << "Background integral of "+input+" = " << temp->Integral() << std::endl;
              }
            }
            sampleFile->Close();
          }
        } // end of MC
        else { // data
          for (auto sampleFileName : sortedSampleFiles[input]) { // Loop data files
            TFile *sampleFile = TFile::Open(sampleFileName);
            // Get histograms
            TH1F* temp = new TH1F();
            temp = getHisto(sampleFile, region+"data"+var+"0");
            sortedMeas["Data0"]->push_back((TH1F*) temp->Clone());
            if (debug > 0) std::cout << "Data integral of "+input+" = " << temp->Integral() << std::endl;
            sampleFile->Close();
          }
        } // end of data
      } // end of input categories

      // Start the rebinning procedure
      Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
      Str Ylabel = settings->GetValue(plot + ".Ylabel", " ");
      Str xsLabel = settings->GetValue(plot + ".xs", " ");


      int nBins;
      NumV bins;
      Str binType = settings->GetValue(plot + ".binType", " ");
      if (binType.Contains("regular")) {
        nBins = settings->GetValue(plot + ".nBins", -1);
        double min = settings->GetValue(plot + ".min", 0.0);
        double max = settings->GetValue(plot + ".max", 0.0);
        double step = (max-min)/nBins;
        for(int i=0 ; i<nBins+1; i++)
          bins.push_back(min+i*step);
      }
      else if (binType.Contains("variable")) {
        bins = numberize(settings->GetValue(plot + ".bins", " "));
        nBins = bins.size() - 1;
      }
      else{fatal("BinType is not regular or variable!");}


      if (plot.Contains("ZJpT")){ // This is the binWidth of each bins
        bins = numberize(settings->GetValue(plot + ".binWidth", " "));
        nBins = 3*bins.size(); // 2D binning : 3 regions
      }

      Double_t BinsVec[nBins+1]; // +1 since we need to define bin edges
      if (!plot.Contains("ZJpT")){
        for (int n=0; n<nBins+1;n++)
          BinsVec[n]=bins[n];
      }
      else {
        BinsVec[0] = 0.;
        double edge = 0.;
        for (int n=0; n<nBins/3;n++)
          for (int i=1; i<4; i++){
            edge = edge + bins[n];
            BinsVec[n*3+i] = edge;
            // std::cout << "Bin index = " << n*3 + i << std::endl;
          }
      }

      TString hname1 = "Sherpa " + var;
      TString hname2 = "MadGraph " + var;
      TString hname3 = "Background " + var;
      TString hnamed = "Data " + var;

      TH1F* rebinnedSherpa = new TH1F(hname1 ,hname1, nBins, BinsVec);
      TH1F* rebinnedMG = new TH1F(hname2 ,hname2, nBins, BinsVec);
      TH1F* rebinnedBG = new TH1F(hname3 ,hname3, nBins, BinsVec);
      TH1F* rebinnedData = new TH1F(hnamed ,hnamed, nBins, BinsVec);
      TH2F* rebinnedSherpa2 = new TH2F(hname1+" response" ,hname1+" response", nBins, BinsVec, nBins, BinsVec);
      TH2F* rebinnedMG2 = new TH2F(hname2+" response" ,hname2+" response", nBins, BinsVec, nBins, BinsVec);
      // TH2F* rebinnedSherpa2 = (TH2F*) sortedResp["Sherpa0"]->at(0)->Clone();


      // Rebin Sherpa Response matrix
      for (int sys=0; sys<nSys; sys++){
        TString newlabel = "Sherpa" + std::to_string(sys);
        rebinnedSherpa2 = (TH2F*) (rebin2DHisto((TH2F*) sortedResp[newlabel]->at(0)->Clone(), plot))->Clone();
        RebinnedResp[newlabel]->push_back((TH2F*) rebinnedSherpa2->Clone());
      }

      // Rebin Sherpa Measured
      for (int sys=0; sys<nSys; sys++){
        TString newlabel = "Sherpa" + std::to_string(sys);
        rebinnedSherpa = (TH1F*) (rebin1DHisto((TH1F*) sortedMeas[newlabel]->at(0)->Clone(), plot))->Clone();
        RebinnedMeas[newlabel]->push_back((TH1F*) rebinnedSherpa->Clone());

        // if (!plot.Contains("ZJpT")){
        //   for (int i=1; i<nBins+1; i++){
        //     double sumMeas=0.; // Reset Meas sums
        //     // Measured values!
        //     auto xAxis = sortedMeas[newlabel]->at(0)->GetXaxis();
        //     for (auto h : *(sortedMeas[newlabel])) // Get the Measured histo
        //       for (int x=1; x<xAxis->GetNbins()+1; x++) // Loop through x Axis
        //         if ((xAxis->GetBinCenter(x) < rebinnedSherpa->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(x) > rebinnedSherpa->GetBinLowEdge(i)))
        //           sumMeas += h->GetBinContent(x);
        //     rebinnedSherpa->SetBinContent(i, sumMeas);
        //     rebinnedSherpa->SetBinError(i, std::sqrt(sumMeas));
        //   }
        //   RebinnedMeas[newlabel]->push_back((TH1F*) rebinnedSherpa->Clone());
        // }
        // else{ // ZJpT plots! separate into 3 regions! No need to rebin right now...
        //   NumV newBins = numberize(settings->GetValue(plot + ".bins", " "));
        //   auto xAxis = sortedMeas[newlabel]->at(0)->GetXaxis();
        //   for (int k=1; k<4; k++){
        //     for (int i=1; i<newBins.size(); i++){ // for new binning in X
        //       double sumMeas=0.; // Reset meas sums
        //       for (auto h : *(sortedMeas[newlabel])){ // Get the meas histo
        //         for (int x=1; x<xAxis->GetNbins()/3+1;x++){
        //           double val = (xAxis->GetBinCenter(k+3*(x-1))+3-k)/30-.05;
        //           if ( (val > newBins[i-1]) && (val < newBins[i]) ){ // Value from original histo falls within new bounds
        //             sumMeas += h->GetBinContent(k+3*(x-1));
        //           }
        //         }
        //       }
        //       rebinnedSherpa->SetBinContent(k+3*(i-1), sumMeas);
        //       rebinnedSherpa->SetBinError(k+3*(i-1), std::sqrt(sumMeas));
        //     }
        //   }
        //   RebinnedMeas[newlabel]->push_back((TH1F*) rebinnedSherpa->Clone());
        // }
      }
      // Rebin Sherpa Truth
      rebinnedSherpa = (TH1F*) (rebin1DHisto((TH1F*) sortedTruth["Sherpa0"]->at(0)->Clone(), plot))->Clone();
      RebinnedTruth["Sherpa0"]->push_back((TH1F*) rebinnedSherpa->Clone());

      // Rebin MadGraph Response matrix
      for (int sys=0; sys<nSys; sys++){
        TString newlabel = "MadGraph" + std::to_string(sys);
        rebinnedMG2 = (TH2F*) (rebin2DHisto((TH2F*) sortedResp[newlabel]->at(0)->Clone(), plot))->Clone();
        RebinnedResp[newlabel]->push_back((TH2F*) rebinnedMG2->Clone());
      }
      // Rebin MadGraph Measured
      for (int sys=0; sys<nSys; sys++){
        TString newlabel = "MadGraph" + std::to_string(sys);
        rebinnedMG = (TH1F*) (rebin1DHisto((TH1F*) sortedMeas[newlabel]->at(0)->Clone(), plot))->Clone();
        RebinnedMeas[newlabel]->push_back((TH1F*) rebinnedMG->Clone());
      }
      // Rebin Madgraph Truth
      rebinnedMG = (TH1F*) (rebin1DHisto((TH1F*) sortedTruth["MadGraph0"]->at(0)->Clone(), plot))->Clone();
      RebinnedTruth["MadGraph0"]->push_back((TH1F*) rebinnedMG->Clone());

      // Rebin Background Measured
      for (int sys=0; sys<nSys; sys++){
        TString newlabel = "Background" + std::to_string(sys);
        TH1F* temp = (TH1F*) sortedMeas[newlabel]->at(0)->Clone();
        temp->Reset();
        for (auto h : *(sortedMeas[newlabel]))
          temp->Add(h);
        rebinnedBG = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
        RebinnedMeas[newlabel]->push_back((TH1F*) rebinnedBG->Clone());
      }
      // Rebin Background Truth

      // Rebin Data Measured
      rebinnedData = (TH1F*) (rebin1DHisto((TH1F*) sortedMeas["Data0"]->at(0)->Clone(), plot))->Clone();
      RebinnedMeas["Data0"]->push_back((TH1F*) rebinnedData->Clone());

      delete rebinnedData;
      delete rebinnedSherpa;
      delete rebinnedMG;
      delete rebinnedBG;
      delete rebinnedSherpa2;
      delete rebinnedMG2;

      if (debug > 0){
        for (auto input : unfoldingInputs){
          std::cout << "Integral of " << input << " " << plot << " = " << RebinnedMeas[input]->at(0)->Integral() << std::endl;
        }
      }

      if (iterations == 0){ // If iterations = 0, dont do unfolding, and instead do the syst plots using the Sherpa sample
        // Lots of repeating and bad practices in general. For some reason, If I dont create an empty plot for ZJPT, it breaks.
        // So for every histogram, I initialize as empty histos with correct binning, then clone the good ones if need be...
        // Get Systematic variations
        // Make plot of systematic ratios
        // This is a copy to fix ZJPt plots. Not good coding practice but it works...
        TH1F* SystematicStat =  new TH1F();
        if (plot.Contains("ZJpT")){
          bins = numberize(settings->GetValue(plot + ".bins", " "));
          nBins = bins.size() - 1;
          Double_t BinsVec[nBins+1]; // +1 since we need to define bin edges
          for (int n=0; n<nBins+1;n++)
            BinsVec[n]=bins[n];
          SystematicStat =  new TH1F("Summed Data" ,"Summed Data", nBins, BinsVec);
        }
        else SystematicStat = (TH1F*) RebinnedMeas["Sherpa0"]->at(0)->Clone();

        // TH1F* SystematicStat =  new TH1F("Summed Data" ,"Summed Data", nBins, BinsVec);
        // TH1F* SystematicStat =  new TH1F("Summed Data" ,"Summed Data", nBins, BinsVec);
        if (!plot.Contains("ZJpT"))
          SystematicStat = (TH1F*) RebinnedMeas["Sherpa0"]->at(0)->Clone();
        else{
          TH1F *temp = (TH1F*) RebinnedMeas["Sherpa0"]->at(0)->Clone();
          for (int i=1; i<nBins+1; i++)
            SystematicStat->SetBinContent(i, temp->GetBinContent(1+3*(i-1)) + temp->GetBinContent(2+3*(i-1)) + temp->GetBinContent(3+3*(i-1)));
        }

        TH1F* SystematicJet = (TH1F*) SystematicStat->Clone();
        TH1F* SystematicElec = (TH1F*) SystematicStat->Clone();
        TH1F* SystematicMuon = (TH1F*) SystematicStat->Clone();
        TH1F* SystematicPRW = (TH1F*) SystematicStat->Clone();
        TH1F* SystematicLumi = (TH1F*) SystematicStat->Clone();
        TH1F* SystematicUnfold = (TH1F*) SystematicStat->Clone();
        TH1F* TheoryUncertainty = (TH1F*) SystematicStat->Clone();
        TH1F* TheoryUncertainty_down = (TH1F*) SystematicStat->Clone();
        TH1F* TheoryUncertainty_up = (TH1F*) SystematicStat->Clone();
        for(int i=1; i<nBins+1; i++){
          SystematicStat->SetBinContent(i,0.);
          SystematicJet->SetBinContent(i,0.);
          SystematicElec->SetBinContent(i,0.);
          SystematicMuon->SetBinContent(i,0.);
          SystematicPRW->SetBinContent(i,0.);
          SystematicLumi->SetBinContent(i,0.);
          SystematicUnfold->SetBinContent(i,0.);
          TheoryUncertainty->SetBinContent(i,0.);
          TheoryUncertainty_down->SetBinContent(i,0.);
          TheoryUncertainty_up->SetBinContent(i,0.);
        }

        TH1F* nominal =  new TH1F("Summed Data" ,"Summed Data", nBins, BinsVec);
        if (!plot.Contains("ZJpT"))
          nominal = (TH1F*) RebinnedMeas["Sherpa0"]->at(0)->Clone();
        else {
          TH1F *temp = (TH1F*) RebinnedMeas["Sherpa0"]->at(0)->Clone();
          for (int i=1; i<nBins+1; i++)
            nominal->SetBinContent(i, temp->GetBinContent(1+3*(i-1)) + temp->GetBinContent(2+3*(i-1)) + temp->GetBinContent(3+3*(i-1)));

        //   nominal = (TH1F*) RebinnedMeas["Sherpa0ZJPT1"]->at(0)->Clone();
        //   TH1F* temp = (TH1F*) RebinnedMeas["Sherpa0ZJPT2"]->at(0)->Clone();
        //   nominal->Add(temp);
        //   temp = (TH1F*) RebinnedMeas["Sherpa0ZJPT3"]->at(0)->Clone();
        //   nominal->Add(temp);
        //   delete temp;
        }
        if (doStackErrorQuadrature){

          TH1F* variation = new TH1F();
          for (int sys=0; sys<nSys; sys++){
            if (!plot.Contains("ZJpT"))
              variation = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(sys)]->at(0)->Clone();
            // else{
              // variation = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(sys)+"ZJPT1"]->at(0)->Clone();
              // TH1F* temp = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(sys)+"ZJPT2"]->at(0)->Clone();
              // variation->Add(temp);
              // temp = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(sys)+"ZJPT3"]->at(0)->Clone();
              // variation->Add(temp);
              // delete temp;
            // }
            double var=0, val=0;
            if (sys==0 ){// Stat uncertainty
              for (int i=1; i<nBins+1; i++){
                var = nominal->GetBinError(i);
                // var = std::sqrt(nominal->GetBinContent(i));
                SystematicStat->SetBinContent(i,var);
              }
            }
            else if ( (nSys==31 && sys<=10) || (nSys==135 && sys<=16) || (nSys==131 && sys<=16) ){// Electron Systematics
              for (int i=1; i<nBins+1; i++){
                val = SystematicElec->GetBinContent(i);
                var = variation->GetBinContent(i)-nominal->GetBinContent(i);
                SystematicElec->SetBinContent(i,val+var*var);
              }
            }
            else if ( (nSys==31 && sys<=20) || (nSys==135 && sys<=102) || (nSys==131 && sys<=102) ){// Jet Systematics
              for (int i=1; i<nBins+1; i++){
                val = SystematicJet->GetBinContent(i);
                var = variation->GetBinContent(i)-nominal->GetBinContent(i);
                SystematicJet->SetBinContent(i,val+var*var);
              }
            }
            else if ( (nSys==31 && sys<=30) || (nSys==135 && sys<=132) || (nSys==131 && sys<=128) ){// Muon Systematics
              for (int i=1; i<nBins+1; i++){
                val = SystematicMuon->GetBinContent(i);
                var = variation->GetBinContent(i)-nominal->GetBinContent(i);
                SystematicMuon->SetBinContent(i,val+var*var);
              }
            }
            else if ( (nSys==135 && sys<=134) || (nSys==131 && sys<=130) ){// PRW SF Systematics
              for (int i=1; i<nBins+1; i++){
                val = SystematicPRW->GetBinContent(i);
                var = variation->GetBinContent(i)-nominal->GetBinContent(i);
                SystematicPRW->SetBinContent(i,val+var*var);
              }
            }
            else fatal("ALEX Systematics is buggy!!");
          }
        }
        else if (doStackErrorMaximum){
          for (int i=1; i<nBins+1; i++){
            // var = nominal->GetBinError(i);
            // var = std::sqrt(nominal->GetBinContent(i));
            SystematicStat->SetBinContent(i,std::sqrt(nominal->GetBinContent(i)));
          }

          TH1F* variation =  new TH1F("Summed Data" ,"Summed Data", nBins, BinsVec);
          TH1F *variation2 = new TH1F("Summed Data2" ,"Summed Data2", nBins, BinsVec);
          for (int sys=1; sys<(nSys+1)/2; sys++){
            if (!plot.Contains("ZJpT")) {
              variation = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(2*sys-1)]->at(0)->Clone();
              variation2 = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(2*sys)]->at(0)->Clone();
            }
            else {

              TH1F* temp = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(2*sys-1)]->at(0)->Clone();
              for (int i=1; i<nBins+1; i++)
                variation->SetBinContent(i, temp->GetBinContent(1+3*(i-1)) + temp->GetBinContent(2+3*(i-1)) + temp->GetBinContent(3+3*(i-1)));
              temp = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(2*sys)]->at(0)->Clone();
              for (int i=1; i<nBins+1; i++)
                variation2->SetBinContent(i, temp->GetBinContent(1+3*(i-1)) + temp->GetBinContent(2+3*(i-1)) + temp->GetBinContent(3+3*(i-1)));
            //   variation = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(2*sys-1)+"ZJPT1"]->at(0)->Clone();

            //   variation->Add(temp);
            //   temp = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(2*sys-1)+"ZJPT3"]->at(0)->Clone();
            //   variation->Add(temp);
            //   variation2 = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(2*sys)+"ZJPT1"]->at(0)->Clone();
            //   temp = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(2*sys)+"ZJPT2"]->at(0)->Clone();
            //   variation2->Add(temp);
            //   temp = (TH1F*) RebinnedMeas["Sherpa"+std::to_string(2*sys)+"ZJPT3"]->at(0)->Clone();
            //   variation2->Add(temp);
            //   delete temp;
            }
            double var=0, var1=0, var2=0, val=0;
            if ( (nSys==31 && sys<=5) || (nSys==135 && sys<=8) || (nSys==131 && sys<=8) ){// Electron Systematics
              for (int i=1; i<nBins+1; i++){
                val = SystematicElec->GetBinContent(i);
                var1 = std::pow(variation->GetBinContent(i)-nominal->GetBinContent(i),2);
                var2 = std::pow(variation2->GetBinContent(i)-nominal->GetBinContent(i),2);
                var = std::max(var1,var2);
                SystematicElec->SetBinContent(i,val+var); // var is already squared
              }
            }
            else if ( (nSys==31 && sys<=10) || (nSys==135 && sys<=51) || (nSys==131 && sys<=51) ){// Jet Systematics
              for (int i=1; i<nBins+1; i++){
                val = SystematicJet->GetBinContent(i);
                var1 = std::pow(variation->GetBinContent(i)-nominal->GetBinContent(i),2);
                var2 = std::pow(variation2->GetBinContent(i)-nominal->GetBinContent(i),2);
                var = std::max(var1,var2);
                SystematicJet->SetBinContent(i,val+var); // var is already squared
              }
            }
            else if ( (nSys==31 && sys<=15) || (nSys==135 && sys<=66) || (nSys==131 && sys<=64) ){// Muon Systematics
              for (int i=1; i<nBins+1; i++){
                val = SystematicMuon->GetBinContent(i);
                var1 = std::pow(variation->GetBinContent(i)-nominal->GetBinContent(i),2);
                var2 = std::pow(variation2->GetBinContent(i)-nominal->GetBinContent(i),2);
                var = std::max(var1,var2);
                SystematicMuon->SetBinContent(i,val+var); // var is already squared
              }
            }
            else if ( (nSys==135 && sys<=67) || (nSys==131 && sys<=65) ){// PRW SF Systematics
              for (int i=1; i<nBins+1; i++){
                val = SystematicPRW->GetBinContent(i);
                var1 = std::pow(variation->GetBinContent(i)-nominal->GetBinContent(i),2);
                var2 = std::pow(variation2->GetBinContent(i)-nominal->GetBinContent(i),2);
                var = std::max(var1,var2);
                SystematicPRW->SetBinContent(i,val+var); // var is already squared
              }
            }
            else fatal("ALEX Systematics is buggy!!");
          }
        }
        else ("This way of evaluating stack plot uncertainties not yet implemented!");
        // Each histogram now has sigma^2 as error. Need to get the sqrt! Except for the StatUncertainty which is already sigma
        for (int i=1; i<nBins+1;i++){
          double nom = nominal->GetBinContent(i);
          if (nom>0) {
            SystematicStat->SetBinContent(i,SystematicStat->GetBinContent(i)/nom);
            SystematicJet->SetBinContent(i,std::sqrt(SystematicJet->GetBinContent(i))/nom);
            SystematicElec->SetBinContent(i,std::sqrt(SystematicElec->GetBinContent(i))/nom);
            SystematicMuon->SetBinContent(i,std::sqrt(SystematicMuon->GetBinContent(i))/nom);
            SystematicPRW->SetBinContent(i,std::sqrt(SystematicPRW->GetBinContent(i))/nom);
            SystematicLumi->SetBinContent(i,lumiUncert);
            SystematicUnfold->SetBinContent(i,0.);
          }
          else {
            SystematicStat->SetBinContent(i,0);
            SystematicJet->SetBinContent(i,0);
            SystematicElec->SetBinContent(i,0);
            SystematicMuon->SetBinContent(i,0);
            SystematicPRW->SetBinContent(i,0);
            SystematicLumi->SetBinContent(i,0);
            SystematicUnfold->SetBinContent(i,0);
          }
        }
        // Do some plotting
        can->Clear();
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        can->SetBottomMargin(0.12);
        can->SetRightMargin(0.04);
        TString newYLabel = "Fractional Uncertainty";
        SystematicStat->SetTitle(";"+Xlabel+";"+newYLabel);
        SystematicStat->SetMinimum(0.);
        if (plot.Contains("NJets"))
          SystematicStat->SetMaximum(1.0);
        else if (plot.Contains("ZJpThighDR"))
          SystematicStat->SetMaximum(0.5);
        else SystematicStat->SetMaximum(0.3);

        // Define some colour schemes
        SystematicStat->SetLineColor(kGray); // Gray
        SystematicJet->SetLineColor(857); // Blue
        SystematicElec->SetLineColor(kRed+1); // Light blue
        SystematicMuon->SetLineColor(419); // nice green
        SystematicPRW->SetLineColor(kOrange+3); // Brown
        SystematicLumi->SetLineColor(kOrange-9); // Beige
        SystematicUnfold->SetLineColor(kOrange+7); // Orange

        // Create a Legend
        TLegend* leg = new TLegend(0.45, 0.70, 0.60, 0.93);
        // leg = new TLegend(0.80, 0.70, 0.95, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.030);
        leg->SetFillStyle(0);

        leg->AddEntry(SystematicLumi,"Luminosity","l");
        leg->AddEntry(SystematicPRW,"Pile-up","l");
        // leg->AddEntry(SystematicUnfold,"Unfolding","l");
        leg->AddEntry(SystematicMuon,"Muon systematics","l");
        leg->AddEntry(SystematicElec,"E/Gamma systematics","l");
        leg->AddEntry(SystematicJet,"Jet systematics","l");
        leg->AddEntry(SystematicStat,"Statistics","l");
        // Draw the plot!
        SystematicStat->Draw("hist");
        SystematicJet->Draw("same hist");
        SystematicElec->Draw("same hist");
        SystematicMuon->Draw("same hist");
        SystematicPRW->Draw("same hist");
        SystematicLumi->Draw("same hist");
        // SystematicUnfold->Draw("same hist");

        leg->Draw();
        drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.03, "", iterations);
        // can->Print("0iteration_ratios"+region+plot+".png");

        // Draw ratioStackPlot
        can->Clear();
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        can->SetBottomMargin(0.12);
        can->SetRightMargin(0.04);
        newYLabel = "Total Fractional Uncertainty";
        // Add the uncertainties in quadrature!
        for (int i=1; i<nBins+1; i++){
          double val = 0;
          val = std::pow(SystematicLumi->GetBinContent(i),2);
          val = val + std::pow(SystematicPRW->GetBinContent(i),2);
          SystematicPRW->SetBinContent(i,std::sqrt(val));
          // val = val + std::pow(SystematicUnfold->GetBinContent(i),2);
          // SystematicUnfold->SetBinContent(i,std::sqrt(val));
          val = val + std::pow(SystematicMuon->GetBinContent(i),2);
          SystematicMuon->SetBinContent(i,std::sqrt(val));
          val = val + std::pow(SystematicElec->GetBinContent(i),2);
          SystematicElec->SetBinContent(i,std::sqrt(val));
          val = val + std::pow(SystematicJet->GetBinContent(i),2);
          SystematicJet->SetBinContent(i,std::sqrt(val));
          val = val + std::pow(SystematicStat->GetBinContent(i),2);
          SystematicStat->SetBinContent(i,std::sqrt(val));
        }
        SystematicStat->SetTitle(";"+Xlabel+";"+newYLabel);
        if (plot.Contains("NJets"))
          SystematicStat->SetMaximum(1.0);
        else if (plot.Contains("ZJpThighDR"))
          SystematicStat->SetMaximum(0.5);
        else SystematicStat->SetMaximum(0.3);
        // Define some colour schemes
        SystematicStat->SetFillColor(kGray); // Gray
        SystematicJet->SetFillColor(857); // Blue
        SystematicElec->SetFillColor(kRed+1); // Light blue
        SystematicMuon->SetFillColor(419); // nice green
        SystematicPRW->SetFillColor(kOrange+3); // Brown
        SystematicLumi->SetFillColor(kOrange-9); // Beige
        SystematicUnfold->SetFillColor(kOrange+7); // Orange

        // Create a Legend
        leg = new TLegend(0.45, 0.70, 0.60, 0.93);
        // leg = new TLegend(0.80, 0.70, 0.95, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.030);
        leg->SetFillStyle(0);

        TString SystLabel = "#oplus Statistics";
        leg->AddEntry(SystematicStat,SystLabel,"f");
        SystLabel = "#oplus Jet";
        leg->AddEntry(SystematicJet,SystLabel,"f");
        SystLabel = "#oplus E/Gamma";
        leg->AddEntry(SystematicElec,SystLabel,"f");
        SystLabel = "#oplus Muon";
        leg->AddEntry(SystematicMuon,SystLabel,"f");
        // SystLabel = "#oplus Unfolding";
        // leg->AddEntry(SystematicUnfold,SystLabel,"f");
        SystLabel = "#oplus Pile-up";
        leg->AddEntry(SystematicPRW,SystLabel,"f");
        leg->AddEntry(SystematicLumi,"Luminosity","f");

        SystematicStat->SetLineColor(kBlack); // Gray fill
        SystematicJet->SetLineColor(kBlack); // Nice blue fill
        SystematicElec->SetLineColor(kBlack); // Nice blue
        SystematicMuon->SetLineColor(kBlack); // nice green
        SystematicPRW->SetLineColor(kBlack); // Orange
        SystematicLumi->SetLineColor(kBlack); // Orange
        SystematicUnfold->SetLineColor(kBlack); // Orange

        // Draw the plot!
        SystematicStat->Draw("hist");
        SystematicJet->Draw("same hist");
        SystematicElec->Draw("same hist");
        SystematicMuon->Draw("same hist");
        // SystematicUnfold->Draw("same hist");
        SystematicPRW->Draw("same hist");
        SystematicLumi->Draw("same hist");
        leg->Draw();
        drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.03, "", iterations,HighPt);
        gPad->RedrawAxis();
        gPad->RedrawAxis("G");
        can->Print("0iteration_ratioStackPlot"+region+plot+".png");

        continue; // skip the unfolding process.
      }

      // do the unfolding
      for (int sys=0; sys<nSys; sys++){
        if (!plot.Contains("ZJpT")){
          TString dataLabel = "Data" + std::to_string(sys);
          TString sherpaLabel = "Sherpa" + std::to_string(sys);
          TString madgraphLabel = "MadGraph" + std::to_string(sys);
          TString backgroundlabel = "Background" + std::to_string(sys);

          // Define the measured histograms
          TH1F* data = (TH1F*) RebinnedMeas["Data0"]->at(0)->Clone();
          TH1F* sherpa = (TH1F*) RebinnedMeas[sherpaLabel]->at(0)->Clone();
          TH1F* madgraph = (TH1F*) RebinnedMeas[madgraphLabel]->at(0)->Clone();
          TH1F* background = (TH1F*) RebinnedMeas[backgroundlabel]->at(0)->Clone();
          data->Add(background,-1); // Remove backgrounds

          // Get response matrices
          TH2F* sherpaR = (TH2F*) RebinnedResp[sherpaLabel]->at(0)->Clone();
          TH2F* madgraphR = (TH2F*) RebinnedResp[madgraphLabel]->at(0)->Clone();

          // Get truth histograms
          TH1F* sherpa_truth = (TH1F*) RebinnedTruth["Sherpa0"]->at(0)->Clone();
          TH1F* madgraph_truth = (TH1F*) RebinnedTruth["MadGraph0"]->at(0)->Clone();

          // Define response matrices
          RooUnfoldResponse response_sherpa (sherpa,sherpa_truth,sherpaR);
          RooUnfoldResponse response_madgraph (madgraph,madgraph_truth,madgraphR);
          // Bayesian unfolding!
          RooUnfoldBayes unfold_data (&response_sherpa, data, iterations);
          if (!doSherpa) RooUnfoldBayes unfold_data (&response_madgraph, data, iterations);
          RooUnfoldBayes unfold_sherpa (&response_sherpa, sherpa, iterations);
          RooUnfoldBayes unfold_madgraph (&response_madgraph, madgraph, iterations);

          if (printUnfolding){
            std::cout << "ALEX PLOTTING TABLE FOR DATA in plot " << plot << " and systematic " << sys << std::endl;
            unfold_data.PrintTable (cout, sherpa_truth);
          }
          // Get Data and push it to map
          auto* unfolded = unfold_data.Hreco();
          if (divideByBinWidth)
          for (int i=1; i<nBins+1; i++){
            unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
            unfolded->SetBinError(i,unfolded->GetBinError(i)/(unfolded->GetBinWidth(i)*Totlumi));
          }
          Unfolded[dataLabel]->push_back((TH1F*) unfolded->Clone());

          // Get Data with MG and push it to map
          if (sys==0){
            TH1F* data2 = (TH1F*) RebinnedMeas["Data0"]->at(0)->Clone();
            data2->Add(background,-1); // Remove backgrounds
            RooUnfoldBayes unfold_data2 (&response_madgraph, data2, iterations);
            auto* unfolded = unfold_data2.Hreco();
            if (divideByBinWidth)
            for (int i=1; i<nBins+1; i++){
              unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
              unfolded->SetBinError(i,unfolded->GetBinError(i)/(unfolded->GetBinWidth(i)*Totlumi));
            }
            Unfolded["Data_MG"]->push_back((TH1F*) unfolded->Clone());
          }

          // Get Sherpa and push it to map
          unfolded = unfold_sherpa.Hreco();
          if (divideByBinWidth)
          for (int i=1; i<nBins+1; i++){
            unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
            unfolded->SetBinError(i,unfolded->GetBinError(i)/(unfolded->GetBinWidth(i)*Totlumi));
          }
          Unfolded[sherpaLabel]->push_back((TH1F*) unfolded->Clone());

          // Get MadGraph and push it to map
          unfolded = unfold_madgraph.Hreco();
          if (divideByBinWidth)
          for (int i=1; i<nBins+1; i++){
            unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
            unfolded->SetBinError(i,unfolded->GetBinError(i)/(unfolded->GetBinWidth(i)*Totlumi));
          }
          // unfolded->Add(madgraph_truth,-1); std::cout << "unfolded madgraph - truth = " << unfolded->Integral() << std::endl;
          Unfolded[madgraphLabel]->push_back((TH1F*) unfolded->Clone());
        }
        else if (plot.Contains("ZJpT")){
          bins = numberize(settings->GetValue(plot + ".bins", " "));
          nBins = bins.size() - 1;
          Double_t BinsVec[nBins+1]; // +1 since we need to define bin edges
          for (int n=0; n<nBins+1;n++)
            BinsVec[n]=bins[n];

          TH1F* sumData =  new TH1F("Summed Data" ,"Summed Data", nBins, BinsVec);
          TH1F* sumData2 =  new TH1F("Summed Data2" ,"Summed Data2", nBins, BinsVec);
          TH1F* sumSherpa =  new TH1F("Summed Sherpa" ,"Summed Sherpa", nBins, BinsVec);
          TH1F* sumMadGraph =  new TH1F("Summed MadGraph" ,"Summed MadGraph", nBins, BinsVec);
          TString dataLabel = "Data" + std::to_string(sys);
          TString sherpaLabel = "Sherpa" + std::to_string(sys);
          TString madgraphLabel = "MadGraph" + std::to_string(sys);
          TString backgroundlabel = "Background" + std::to_string(sys);
          TString TruthSherpaLabel = "Sherpa0";
          TString TruthMGLabel = "MadGraph0";

          // Fetch objects
          TH1F* data = (TH1F*) RebinnedMeas["Data0"]->at(0)->Clone();
          TH1F* sherpa = (TH1F*) RebinnedMeas[sherpaLabel]->at(0)->Clone();
          TH1F* madgraph = (TH1F*) RebinnedMeas[madgraphLabel]->at(0)->Clone();
          TH1F* background = (TH1F*) RebinnedMeas[backgroundlabel]->at(0)->Clone();

          // Substrack background from data
          data->Add(background,-1); // Remove backgrounds

          // Get response matrices
          TH2F* sherpaR = (TH2F*) RebinnedResp[sherpaLabel]->at(0)->Clone();
          TH2F* madgraphR = (TH2F*) RebinnedResp[madgraphLabel]->at(0)->Clone();

          // Get truth histograms
          TH1F* sherpa_truth = (TH1F*) RebinnedTruth["Sherpa0"]->at(0)->Clone();
          TH1F* madgraph_truth = (TH1F*) RebinnedTruth["MadGraph0"]->at(0)->Clone();

          // Define response matrices
          RooUnfoldResponse response_sherpa (sherpa,sherpa_truth,sherpaR);
          // RooUnfoldResponse response_Sherpa (Meas_Sherpa,Truth_Sherpa,Resp_Sherpa);
          // RooUnfoldResponse response_MadGraph (Meas_MadGraph,Truth_MadGraph,Resp_MadGraph);
          RooUnfoldResponse response_madgraph (madgraph,madgraph_truth,madgraphR);

          // Produce 2D unfolding matrix for ZJpT plots
          // if (plot.Contains("ZJpT")){ // Get 2D response matrix for ZJpT
          //   // Sherpa Response matrix
          //   TString Resp_X = "Detector Level " + Xlabel;
          //   TString Resp_Y = "Particle Level " + Xlabel;
          //   auto* R = response_sherpa.Hresponse();
          //   R->SetTitle(";"+Resp_X+";"+Resp_Y);
          //   R->SetMinimum(1.);
          //   gPad->SetLogz(0);
          //   R->SetMaximum(R->GetMaximum()*1.1);
          //   can->SetRightMargin(0.18);
          //   R->SetStats(0);
          //   // Turn this on for adding bin content to reponse matrix
          //   // Alex!
          //   // gStyle->SetPaintTextFormat("2.g");
          //   // gStyle->SetPaintTextFormat("3.f");
          //   R->Draw("colz TEXT");
          //   R->Draw("colz");
          //   drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.035,"Sherpa2.2 ");
          //   if (HighPt) { // Muon HighPt WP!
          //     drawText(0.15, 0.9-6*0.035*1.25, "HighPt MuonWP Studies",0,1,0.035);
          //   }
          //   can->Print("response_Sherpa"+region+"2D"+plot+".png");
          // }

          // Bayesian unfolding!
          RooUnfoldBayes unfold_data (&response_sherpa, data, iterations);
          if (!doSherpa) RooUnfoldBayes unfold_data (&response_madgraph, data, iterations);
          RooUnfoldBayes unfold_sherpa (&response_sherpa, sherpa, iterations);
          RooUnfoldBayes unfold_madgraph (&response_madgraph, madgraph, iterations);

          if (printUnfolding){
            std::cout << "ALEX PLOTTING TABLE FOR DATA in plot " << plot << " and systematic " << sys << std::endl;
            unfold_data.PrintTable (cout, sherpa_truth);
          }

          // Get Data and push it to map
          auto* unfolded = unfold_data.Hreco();
          unfolded->Rebin(3);
          // ALEX DEBUG!!!
          for (int i=1; i<nBins+1; i++){
            // double sum = unfolded->GetBinContent(1+3*(i-1)) + unfolded->GetBinContent(2+3*(i-1)) + unfolded->GetBinContent(3+3*(i-1));
            // double error = std::pow(unfolded->GetBinError(1+3*(i-1)),2) + std::pow(unfolded->GetBinError(2+3*(i-1)),2) + std::pow(unfolded->GetBinError(3+3*(i-1)),2);
            // sumData->SetBinContent(i, sum);
            // sumData->SetBinError(i, std::sqrt(error));
            sumData->SetBinContent(i, unfolded->GetBinContent(i));
            sumData->SetBinError(i, unfolded->GetBinError(i));
            // std::cout << "GetBinError  " << i << " = " << sumData->GetBinError(i) << std::endl;
            // std::cout << "sqrt content " << i << " = " << std::sqrt(sumData->GetBinContent(i)) << std::endl;
          }
          if (divideByBinWidth)
          for (int i=1; i<nBins+1; i++){
            sumData->SetBinContent(i,sumData->GetBinContent(i)/(sumData->GetBinWidth(i)*Totlumi));
            sumData->SetBinError(i,sumData->GetBinError(i)/(sumData->GetBinWidth(i)*Totlumi));
          }
          Unfolded[dataLabel]->push_back((TH1F*) sumData->Clone());

          // Get Data with MG and push it to map
          if (sys==0){
            TH1F* data2 = (TH1F*) RebinnedMeas["Data0"]->at(0)->Clone();
            data2->Add(background,-1); // Remove backgrounds
            RooUnfoldBayes unfold_data2 (&response_madgraph, data2, iterations);
            auto* unfolded = unfold_data2.Hreco();
            for (int i=1; i<nBins+1; i++){
              double sum = unfolded->GetBinContent(1+3*(i-1)) + unfolded->GetBinContent(2+3*(i-1)) + unfolded->GetBinContent(3+3*(i-1));
              double error = std::pow(unfolded->GetBinError(1+3*(i-1)),2) + std::pow(unfolded->GetBinError(2+3*(i-1)),2) + std::pow(unfolded->GetBinError(3+3*(i-1)),2);
              sumData2->SetBinContent(i, sum);
              sumData2->SetBinError(i, std::sqrt(error));
            }
            if (divideByBinWidth)
            for (int i=1; i<nBins+1; i++){
              sumData2->SetBinContent(i,sumData2->GetBinContent(i)/(sumData2->GetBinWidth(i)*Totlumi));
              sumData2->SetBinError(i,sumData2->GetBinError(i)/(sumData2->GetBinWidth(i)*Totlumi));
            }
            Unfolded["Data_MG"]->push_back((TH1F*) sumData2->Clone());
          }

          // Get Sherpa and push it to map
          unfolded = unfold_sherpa.Hreco();
          unfolded->Rebin(3);
          for (int i=1; i<nBins+1; i++){
            // double sum = unfolded->GetBinContent(1+3*(i-1)) + unfolded->GetBinContent(2+3*(i-1)) + unfolded->GetBinContent(3+3*(i-1));
            // double error = std::pow(unfolded->GetBinError(1+3*(i-1)),2) + std::pow(unfolded->GetBinError(2+3*(i-1)),2) + std::pow(unfolded->GetBinError(3+3*(i-1)),2);
            // sumSherpa->SetBinContent(i, sum);
            // sumSherpa->SetBinError(i, std::sqrt(error));
            // sumSherpa->SetBinContent(i, unfolded->GetBinContent(i));
            // sumSherpa->SetBinError(i, std::sqrt(unfolded->GetBinContent(i)));
            sumSherpa->SetBinContent(i, unfolded->GetBinContent(i));
            sumSherpa->SetBinError(i, unfolded->GetBinError(i));
          }
          if (divideByBinWidth)
          for (int i=1; i<nBins+1; i++){
            sumSherpa->SetBinContent(i,sumSherpa->GetBinContent(i)/(sumSherpa->GetBinWidth(i)*Totlumi));
            sumSherpa->SetBinError(i,sumSherpa->GetBinError(i)/(sumSherpa->GetBinWidth(i)*Totlumi));
          }
          // unfolded->Add(sherpa_truth,-1); std::cout << "unfolded sherpa - truth = " << unfolded->Integral() << std::endl;
          Unfolded[sherpaLabel]->push_back((TH1F*) sumSherpa->Clone());

          // Get MadGraph and push it to map
          unfolded = unfold_madgraph.Hreco();
          unfolded->Rebin(3);
          for (int i=1; i<nBins+1; i++){
            // double sum = unfolded->GetBinContent(1+3*(i-1)) + unfolded->GetBinContent(2+3*(i-1)) + unfolded->GetBinContent(3+3*(i-1));
            // double error = std::pow(unfolded->GetBinError(1+3*(i-1)),2) + std::pow(unfolded->GetBinError(2+3*(i-1)),2) + std::pow(unfolded->GetBinError(3+3*(i-1)),2);
            // sumMadGraph->SetBinContent(i, sum);
            // sumMadGraph->SetBinError(i, std::sqrt(error));
            // sumMadGraph->SetBinContent(i, unfolded->GetBinContent(i));
            // sumMadGraph->SetBinError(i, std::sqrt(unfolded->GetBinContent(i)));
            sumMadGraph->SetBinContent(i, unfolded->GetBinContent(i));
            sumMadGraph->SetBinError(i, unfolded->GetBinError(i));
          }
          if (divideByBinWidth)
          for (int i=1; i<nBins+1; i++){
            sumMadGraph->SetBinContent(i,sumMadGraph->GetBinContent(i)/(sumMadGraph->GetBinWidth(i)*Totlumi));
            sumMadGraph->SetBinError(i,sumMadGraph->GetBinError(i)/(sumMadGraph->GetBinWidth(i)*Totlumi));
          }
          // unfolded->Add(madgraph_truth,-1); std::cout << "unfolded madgraph - truth = " << unfolded->Integral() << std::endl;
          Unfolded[madgraphLabel]->push_back((TH1F*) sumMadGraph->Clone());

          // for (int k=1; k<4; k++){
          //   // Define the measured histograms
          //   TH1F* data = (TH1F*) RebinnedMeas["Data0ZJPT"+std::to_string(k)]->at(0)->Clone();
          //   TH1F* sherpa = (TH1F*) RebinnedMeas[sherpaLabel+"ZJPT"+std::to_string(k)]->at(0)->Clone();
          //   TH1F* madgraph = (TH1F*) RebinnedMeas[madgraphLabel+"ZJPT"+std::to_string(k)]->at(0)->Clone();
          //   TH1F* background = (TH1F*) RebinnedMeas[backgroundlabel+"ZJPT"+std::to_string(k)]->at(0)->Clone();
          //   data->Add(background,-1); // Remove backgrounds
          //
          //   // Get response matrices
          //   TH2F* sherpaR = (TH2F*) RebinnedResp[sherpaLabel+"ZJPT"+std::to_string(k)]->at(0)->Clone();
          //   TH2F* madgraphR = (TH2F*) RebinnedResp[madgraphLabel+"ZJPT"+std::to_string(k)]->at(0)->Clone();
          //
          //   // Get truth histograms
          //   TH1F* sherpa_truth = (TH1F*) RebinnedTruth[TruthSherpaLabel+"ZJPT"+std::to_string(k)]->at(0)->Clone();
          //   TH1F* madgraph_truth = (TH1F*) RebinnedTruth[TruthMGLabel+"ZJPT"+std::to_string(k)]->at(0)->Clone();
          //
          //
          //   // Define response matrices
          //   RooUnfoldResponse response_sherpa (sherpa,sherpa_truth,sherpaR);
          //   RooUnfoldResponse response_madgraph (madgraph,madgraph_truth,madgraphR);
          //   // Bayesian unfolding!
          //   RooUnfoldBayes unfold_data (&response_sherpa, data, iterations);
          //   if (!doSherpa) RooUnfoldBayes unfold_data (&response_madgraph, data, iterations);
          //   RooUnfoldBayes unfold_sherpa (&response_sherpa, sherpa, iterations);
          //   RooUnfoldBayes unfold_madgraph (&response_madgraph, madgraph, iterations);
          //
          //   // std::cout << "ALEX PLOTTING TABLE FOR DATA in plot " << plot << " and systematic " << sys << std::endl;
          //   // unfold_data.PrintTable (cout, sherpa_truth);
          //   // Get Data and push it to map
          //   auto* unfolded = unfold_data.Hreco();
          //   // std::cout << "systematic " << sys << " has integral " << unfolded->Integral() << std::endl;
          //   if (divideByBinWidth)
          //   for (int i=1; i<nBins+1; i++){
          //     unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
          //     unfolded->SetBinError(i,unfolded->GetBinError(i)/(unfolded->GetBinWidth(i)*Totlumi));
          //   }
          //   sumData->Add((TH1F*) unfolded->Clone());
          //
          //   // Get Data with MG and push it to map
          //   if (sys==0){
          //     TH1F* data2 = (TH1F*) RebinnedMeas["Data0ZJPT"+std::to_string(k)]->at(0)->Clone();
          //     data2->Add(background,-1); // Remove backgrounds
          //     RooUnfoldBayes unfold_data2 (&response_madgraph, data2, iterations);
          //     auto* unfolded = unfold_data2.Hreco();
          //     if (divideByBinWidth)
          //     for (int i=1; i<nBins+1; i++){
          //       unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
          //       unfolded->SetBinError(i,unfolded->GetBinError(i)/(unfolded->GetBinWidth(i)*Totlumi));
          //     }
          //     sumData2->Add((TH1F*) unfolded->Clone());
          //     if (k==3) Unfolded["Data_MG"]->push_back((TH1F*) sumData2->Clone());
          //   }
          //
          //   // Get Sherpa and push it to map
          //   unfolded = unfold_sherpa.Hreco();
          //   if (divideByBinWidth)
          //   for (int i=1; i<nBins+1; i++){
          //     unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
          //     unfolded->SetBinError(i,unfolded->GetBinError(i)/(unfolded->GetBinWidth(i)*Totlumi));
          //   }
          //   // unfolded->Add(sherpa_truth,-1); std::cout << "unfolded sherpa - truth = " << unfolded->Integral() << std::endl;
          //   sumSherpa->Add((TH1F*) unfolded->Clone());
          //
          //   // Get MadGraph and push it to map
          //   unfolded = unfold_madgraph.Hreco();
          //   if (divideByBinWidth)
          //   for (int i=1; i<nBins+1; i++){
          //     unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
          //     unfolded->SetBinError(i,unfolded->GetBinError(i)/(unfolded->GetBinWidth(i)*Totlumi));
          //   }
          //   // unfolded->Add(madgraph_truth,-1); std::cout << "unfolded madgraph - truth = " << unfolded->Integral() << std::endl;
          //   sumMadGraph->Add((TH1F*) unfolded->Clone());
          // }
          // Unfolded[dataLabel]->push_back((TH1F*) sumData->Clone());
          // // std::cout << "label = " << dataLabel << ", " << sumData->Integral() << std::endl;
          // Unfolded[sherpaLabel]->push_back((TH1F*) sumSherpa->Clone());
          // Unfolded[madgraphLabel]->push_back((TH1F*) sumMadGraph->Clone());

        }
        else fatal("Broke the ZJpT check!");

      }

      TH1F* xs_data = (TH1F*) Unfolded["Data0"]->at(0)->Clone();
      TH1F* xs_sherpa = (TH1F*) Unfolded["Sherpa0"]->at(0)->Clone();
      TH1F* xs_madgraph = (TH1F*) Unfolded["MadGraph0"]->at(0)->Clone();
      xs_data->SetTitle(";"+Xlabel+";"+xsLabel);
      TLegend* leg = new TLegend();

      // Draw simple cross section plots for debugging
      if (doSimpleXS){
        // Do some plotting
        // Not log plots
        if (plot.Contains("ZJpT")){
          gPad->SetLogy(0);
          xs_data->SetMaximum(xs_madgraph->GetMaximum()*2.0);
          xs_data->SetMinimum(xs_data->GetMinimum()*.5);
        }
        else{
          gPad->SetLogy(1);
          if(xs_data->GetMinimum() > 100)
            xs_data->SetMinimum(xs_data->GetMinimum()*0.05);
          xs_data->SetMaximum(xs_madgraph->GetMaximum()*100);
        }

        // Define some colour schemes
        xs_data->SetLineColor(kBlack);
        xs_madgraph->SetLineColor(kRed);
        xs_madgraph->SetMarkerSize(0);
        xs_sherpa->SetLineColor(857); // Nice blue
        xs_sherpa->SetMarkerSize(0);
        // Create a Legend
        leg = new TLegend(0.65, 0.70, 0.80, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.020);
        leg->SetFillStyle(0);

        leg->AddEntry(xs_data,"Data","l");
        leg->AddEntry(xs_sherpa,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","l");
        leg->AddEntry(xs_madgraph,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","l");

        // Draw the plot!
        xs_data->Draw();
        xs_sherpa->Draw("same");
        xs_madgraph->Draw("same");
        leg->Draw();
        // Add the Plot infos!
        drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.025);
        if (saveXS)
          can->Print("cross_section"+region+plot+".png");
      }

      // Save cross sections into the individual channels to compare & add channels
      TString str1;
      if (doSherpa) str1 = "Sherpa";
      else str1 = "MadGraph";
      if (doRatios || UnfoldingFit)
      for (int sys=0; sys<nSys; sys++){
        if (region == "hmm") {
          muonChannel[plot]->push_back( (TH1F*) Unfolded["Data"+std::to_string(sys)]->at(0)->Clone() );
          muonTruth[plot]->push_back( (TH1F*)Unfolded[str1+std::to_string(sys)]->at(0)->Clone() );
        }
        else if (region == "hee"){
          elecChannel[plot]->push_back( (TH1F*) Unfolded["Data"+std::to_string(sys)]->at(0)->Clone() );
          elecTruth[plot]->push_back( (TH1F*) Unfolded[str1+std::to_string(sys)]->at(0)->Clone() );
        }
        else ("Wrong region somehow IN THE region LOOP.");
      }

      // Get Systematic variations
      // Make plot of systematic ratios
      TH1F* SystematicStat = (TH1F*) Unfolded["Data0"]->at(0)->Clone();
      TH1F* SystematicJet = (TH1F*) SystematicStat->Clone();
      TH1F* SystematicElec = (TH1F*) SystematicStat->Clone();
      TH1F* SystematicMuon = (TH1F*) SystematicStat->Clone();
      TH1F* SystematicPRW = (TH1F*) SystematicStat->Clone();
      TH1F* SystematicLumi = (TH1F*) SystematicStat->Clone();
      TH1F* SystematicUnfold = (TH1F*) SystematicStat->Clone();
      TH1F* TheoryUncertainty = (TH1F*) SystematicStat->Clone();
      TH1F* TheoryUncertainty_down = (TH1F*) SystematicStat->Clone();
      TH1F* TheoryUncertainty_up = (TH1F*) SystematicStat->Clone();
      TH1F* TheorySherpa = (TH1F*) SystematicStat->Clone();
      TH1F* Theoryttbar = (TH1F*) SystematicStat->Clone();

      for(int i=1; i<nBins+1; i++){
        SystematicStat->SetBinContent(i,0.);
        SystematicJet->SetBinContent(i,0.);
        SystematicElec->SetBinContent(i,0.);
        SystematicMuon->SetBinContent(i,0.);
        SystematicPRW->SetBinContent(i,0.);
        SystematicLumi->SetBinContent(i,0.);
        SystematicUnfold->SetBinContent(i,0);
        TheoryUncertainty->SetBinContent(i,0);
        TheoryUncertainty_down->SetBinContent(i,0);
        TheoryUncertainty_up->SetBinContent(i,0);
        TheorySherpa->SetBinContent(i,0);
        Theoryttbar->SetBinContent(i,0);
      }

      TH1F* nominal = (TH1F*) Unfolded["Data0"]->at(0)->Clone();
      if (doStackErrorQuadrature){
        TH1F* variation = new TH1F();
        for (int sys=0; sys<nSys; sys++){
          variation = (TH1F*) Unfolded["Data"+std::to_string(sys)]->at(0)->Clone();
          double var=0, val=0;
          if (sys==0 ){// Stat uncertainty
            for (int i=1; i<nBins+1; i++){
              var = nominal->GetBinError(i);
              // var = std::sqrt(nominal->GetBinContent(i));
              SystematicStat->SetBinContent(i,var);
            }
          }
          else if ( (nSys==31 && sys<=10) || (nSys==135 && sys<=16) || (nSys==131 && sys<=16) ){// Electron Systematics
            for (int i=1; i<nBins+1; i++){
              val = SystematicElec->GetBinContent(i);
              var = variation->GetBinContent(i)-nominal->GetBinContent(i);
              SystematicElec->SetBinContent(i,val+var*var);
            }
          }
          else if ( (nSys==31 && sys<=20) || (nSys==135 && sys<=102) || (nSys==131 && sys<=102) ){// Jet Systematics
            for (int i=1; i<nBins+1; i++){
              val = SystematicJet->GetBinContent(i);
              var = variation->GetBinContent(i)-nominal->GetBinContent(i);
              SystematicJet->SetBinContent(i,val+var*var);
            }
          }
          else if ( (nSys==31 && sys<=30) || (nSys==135 && sys<=132) || (nSys==131 && sys<=128) ){// Muon Systematics
            for (int i=1; i<nBins+1; i++){
              val = SystematicMuon->GetBinContent(i);
              var = variation->GetBinContent(i)-nominal->GetBinContent(i);
              SystematicMuon->SetBinContent(i,val+var*var);
            }
          }
          else if ( (nSys==135 && sys<=134) || (nSys==131 && sys<=130) ){// PRW SF Systematics
            for (int i=1; i<nBins+1; i++){
              val = SystematicPRW->GetBinContent(i);
              var = variation->GetBinContent(i)-nominal->GetBinContent(i);
              SystematicPRW->SetBinContent(i,val+var*var);
            }
          }
          else fatal("ALEX Quadrature Systematics are buggy!");
        }
      }
      else if (doStackErrorMaximum){
        TH1F* variation = new TH1F();
        TH1F* variation2 = new TH1F();
        // Stat uncertainty
        for (int i=1; i<nBins+1; i++){
          // var = nominal->GetBinError(i);
          // var = std::sqrt(nominal->GetBinContent(i));
          // SystematicStat->SetBinContent(i,var);
          SystematicStat->SetBinContent(i,nominal->GetBinError(i));
        }
        for (int sys=1; sys<(nSys+1)/2; sys++){
          variation = (TH1F*) Unfolded["Data"+std::to_string(2*sys-1)]->at(0)->Clone();
          variation2 = (TH1F*) Unfolded["Data"+std::to_string(2*sys)]->at(0)->Clone();
          double val = 0;
          double var=0, var1=0, var2=0;
          if ( (nSys==31 && sys<=5) || (nSys==135 && sys<=8) || (nSys==131 && sys<=8) ){// Electron Systematics
            for (int i=1; i<nBins+1; i++){
              val = SystematicElec->GetBinContent(i);
              var1 = std::pow(variation->GetBinContent(i)-nominal->GetBinContent(i),2);
              var2 = std::pow(variation2->GetBinContent(i)-nominal->GetBinContent(i),2);
              var = std::max(var1,var2);
              SystematicElec->SetBinContent(i,val+var); // var is already the squared value
            }
          }
          else if ( (nSys==31 && sys<=10) || (nSys==135 && sys<=51) || (nSys==131 && sys<=51) ){// Jet Systematics
            for (int i=1; i<nBins+1; i++){
              val = SystematicJet->GetBinContent(i);
              var1 = std::pow(variation->GetBinContent(i)-nominal->GetBinContent(i),2);
              var2 = std::pow(variation2->GetBinContent(i)-nominal->GetBinContent(i),2);
              var = std::max(var1,var2);
              SystematicJet->SetBinContent(i,val+var); // var is already the squared value
            }
          }
          else if ( (nSys==31 && sys<=15) || (nSys==135 && sys<=66) || (nSys==131 && sys<=64) ){// Muon Systematics
            for (int i=1; i<nBins+1; i++){
              val = SystematicMuon->GetBinContent(i);
              var1 = std::pow(variation->GetBinContent(i)-nominal->GetBinContent(i),2);
              var2 = std::pow(variation2->GetBinContent(i)-nominal->GetBinContent(i),2);
              var = std::max(var1,var2);
              SystematicMuon->SetBinContent(i,val+var); // var is already the squared value
            }
          }
          else if ( (nSys==135 && sys<=67) || (nSys==131 && sys<=65) ){// PRW SF Systematics
            for (int i=1; i<nBins+1; i++){
              val = SystematicPRW->GetBinContent(i);
              var1 = std::pow(variation->GetBinContent(i)-nominal->GetBinContent(i),2);
              var2 = std::pow(variation2->GetBinContent(i)-nominal->GetBinContent(i),2);
              var = std::max(var1,var2);
              SystematicPRW->SetBinContent(i,val+var); // var is already the squared value
            }
          }
          else fatal("ALEX Maximum Systematics are buggy!");
        }
      }
      else fatal("Not a correct stack error evaluation method");

      // Evaluate the Unfolding Uncertainty.
      TString unfoldingSource = settings->GetValue("UnfoldingUncertainty", " ");
      TH1F* fromUnfolding = getHisto(unfoldUncert, region+"unf"+var+"_"+iterations+unfoldingSource);
      for (int i=1; i<nBins+1; i++){
        // std::cout << "Unfolded data in bin " << i << " = " << nominal->GetBinContent(i) << std::endl;
        // std::cout << "Unfolded Sherpa in bin " << i << " = " << fromUnfolding->GetBinContent(i) << std::endl;
        double var = std::pow(fromUnfolding->GetBinContent(i) - nominal->GetBinContent(i),2);
        SystematicUnfold->SetBinContent(i,var);
      }

      // Evaluate the Theory uncertainties! They come in rebinned unfolded cross sections
      nominal = (TH1F*) Unfolded["Data0"]->at(0)->Clone();
      for (int sys=0; sys<11; sys++){ // 11 theory uncertainty pairs. 135-156
        // for (int sys=0; sys<4; sys++){ // 4 sherpa theory uncertainty pairs. 135-142
        // for (int sys=0; sys<7; sys++){ // 7 ttbar theory uncertainty pairs. 143-156
        int system = 135 + 2*sys;
        // if (sys !=3) continue;
        // if (sys < 4) continue;
        // int system = 143 + 2*sys;
        // int system = 150;
        TH1F* variation = getHisto(theory, region+var+system+"_"+std::to_string(iterations));
        system = system + 1;
        TH1F* variation2 = getHisto(theory, region+var+system+"_"+std::to_string(iterations));
        double val = 0;
        double var=0, var1=0, var2=0;
        for (int i=1; i<nBins+1; i++){
          double nom = nominal->GetBinContent(i);
          val = TheoryUncertainty->GetBinContent(i);
          var1 = std::pow(variation->GetBinContent(i)-nom,2);
          var2 = std::pow(variation2->GetBinContent(i)-nom,2);
          var = std::max(var1,var2);
          // std::cout << "ALEXANDRE VARIATION = " << var << std::endl;
          // std::cout << "ALEXANDRE NOMINAL   = " << nom << std::endl;
          // std::cout << "WHICH IS % wise = " << var / nom << std::endl;
          TheoryUncertainty->SetBinContent(i,val+var); // var is already the squared value
          // if (sys > 3) std::cout << plot << " bin " << i << " = " << std::sqrt(var)/nom << std::endl;
          if (sys < 4) TheorySherpa->SetBinContent(i, TheorySherpa->GetBinContent(i)+var);
          else if (sys < 11) Theoryttbar->SetBinContent(i, Theoryttbar->GetBinContent(i)+var);
        }
      }

      // Each histogram now has sigma^2 as error. Need to get the sqrt! Except for the StatUncertainty which is already sigma
      // TH1F* var_unfold = (TH1F*) Unfolded["Data_MG"]->at(0)->Clone();
      for (int i=1; i<nBins+1;i++){
        double nom = nominal->GetBinContent(i);
        // double var = var_unfold->GetBinContent(i);
        if (nom>0) {
          SystematicStat->SetBinContent(i,SystematicStat->GetBinContent(i)/nom);
          SystematicJet->SetBinContent(i,std::sqrt(SystematicJet->GetBinContent(i))/nom);
          SystematicElec->SetBinContent(i,std::sqrt(SystematicElec->GetBinContent(i))/nom);
          SystematicMuon->SetBinContent(i,std::sqrt(SystematicMuon->GetBinContent(i))/nom);
          SystematicPRW->SetBinContent(i,std::sqrt(SystematicPRW->GetBinContent(i))/nom);
          SystematicLumi->SetBinContent(i,lumiUncert);
          SystematicUnfold->SetBinContent(i,std::sqrt(SystematicUnfold->GetBinContent(i))/nom );
          TheoryUncertainty->SetBinContent(i, std::sqrt(TheoryUncertainty->GetBinContent(i))/nom);
          TheorySherpa->SetBinContent(i, std::sqrt(TheorySherpa->GetBinContent(i))/nom);
          Theoryttbar->SetBinContent(i, std::sqrt(Theoryttbar->GetBinContent(i))/nom);
        }
        else {
          SystematicStat->SetBinContent(i,0);
          SystematicJet->SetBinContent(i,0);
          SystematicElec->SetBinContent(i,0);
          SystematicMuon->SetBinContent(i,0);
          SystematicPRW->SetBinContent(i,0);
          SystematicLumi->SetBinContent(i,0);
          SystematicUnfold->SetBinContent(i,0);
          TheoryUncertainty->SetBinContent(i,0);
          TheorySherpa->SetBinContent(i,0);
          Theoryttbar->SetBinContent(i,0);
        }
      }
      // Do some plotting
      can->Clear();
      gPad->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      can->SetBottomMargin(0.12);
      can->SetRightMargin(0.04);
      TString newYLabel = "Fractional Uncertainty";
      SystematicStat->SetTitle(";"+Xlabel+";"+newYLabel);
      SystematicStat->SetMinimum(0.);
      if (plot.Contains("NJets"))
        SystematicStat->SetMaximum(1.0);
      else if (plot.Contains("ZJpThighDR"))
        SystematicStat->SetMaximum(0.5);
      else SystematicStat->SetMaximum(0.3);
      // Define some colour schemes
      SystematicStat->SetLineColor(kBlack); // Black
      TheoryUncertainty->SetLineColor(kMagenta+2); // Purple
      TheorySherpa->SetLineColor(kMagenta+2); // Purple
      Theoryttbar->SetLineColor(417); // Light green
      SystematicJet->SetLineColor(857); // Blue
      SystematicElec->SetLineColor(kRed+1); // Red
      SystematicMuon->SetLineColor(419); // nice green
      SystematicPRW->SetLineColor(kOrange+3); // Brown
      SystematicLumi->SetLineColor(kOrange-9); // Beige
      SystematicUnfold->SetLineColor(kOrange+7); // Orange
      // Make Lines larger
      SystematicStat->SetLineWidth(5); // Black
      TheoryUncertainty->SetLineWidth(5); // Purple
      TheorySherpa->SetLineWidth(5); // Purple
      Theoryttbar->SetLineWidth(5); // Purple
      SystematicJet->SetLineWidth(5); // Blue
      SystematicElec->SetLineWidth(5); // Red
      SystematicMuon->SetLineWidth(5); // nice green
      SystematicPRW->SetLineWidth(5); // Brown
      SystematicLumi->SetLineWidth(5); // Beige
      SystematicUnfold->SetLineWidth(5); // Orange

      // Create a Legend
      leg = new TLegend(0.45, 0.70, 0.60, 0.93);
      // leg = new TLegend(0.80, 0.70, 0.95, 0.93);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.030);
      leg->SetFillStyle(0);

      leg->AddEntry(SystematicStat,"Statistics","l");
      leg->AddEntry(TheorySherpa,"Theory Sherpa","l");
      leg->AddEntry(Theoryttbar,"Theory ttbar","l");
      // leg->AddEntry(TheoryUncertainty,"Theory Combined","l");
      leg->AddEntry(SystematicJet,"Jet systematics","l");
      leg->AddEntry(SystematicElec,"E/Gamma systematics","l");
      leg->AddEntry(SystematicMuon,"Muon systematics","l");
      leg->AddEntry(SystematicUnfold,"Unfolding","l");
      leg->AddEntry(SystematicPRW,"Pile-up","l");
      leg->AddEntry(SystematicLumi, "Luminosity", "l");

      // Draw the plot!
      SystematicStat->Draw("hist");
      SystematicJet->Draw("same hist");
      SystematicElec->Draw("same hist");
      SystematicMuon->Draw("same hist");
      SystematicUnfold->Draw("same hist");
      SystematicPRW->Draw("same hist");
      SystematicLumi->Draw("same hist");
      // TheoryUncertainty->Draw("same hist");
      TheorySherpa->Draw("same hist");
      Theoryttbar->Draw("same hist");
      leg->Draw();
      drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.03, "", iterations);
      if (saveUncert) can->Print(std::to_string(iterations)+"iteration_linearUncert"+region+plot+".png");

      // Draw ratioStackPlot
      can->Clear();
      gPad->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      can->SetBottomMargin(0.12);
      can->SetRightMargin(0.04);
      newYLabel = "Total Fractional Uncertainty";
      // Add the uncertainties in quadrature!
      for (int i=1; i<nBins+1; i++){
        double val = 0;
        val = std::pow(SystematicLumi->GetBinContent(i),2);
        val = val + std::pow(SystematicPRW->GetBinContent(i),2);
        SystematicPRW->SetBinContent(i,std::sqrt(val));
        val = val + std::pow(SystematicUnfold->GetBinContent(i),2);
        SystematicUnfold->SetBinContent(i,std::sqrt(val));
        val = val + std::pow(SystematicMuon->GetBinContent(i),2);
        SystematicMuon->SetBinContent(i,std::sqrt(val));
        val = val + std::pow(SystematicElec->GetBinContent(i),2);
        SystematicElec->SetBinContent(i,std::sqrt(val));
        val = val + std::pow(SystematicJet->GetBinContent(i),2);
        SystematicJet->SetBinContent(i,std::sqrt(val));
        val = val + std::pow(TheoryUncertainty->GetBinContent(i),2);
        TheoryUncertainty->SetBinContent(i,std::sqrt(val));
        val = val + std::pow(SystematicStat->GetBinContent(i),2);
        SystematicStat->SetBinContent(i,std::sqrt(val));
      }
      SystematicStat->SetTitle(";"+Xlabel+";"+newYLabel);
      SystematicStat->SetMinimum(0.);
      if (plot.Contains("NJets"))
        SystematicStat->SetMaximum(1.0);
      else if (plot.Contains("ZJpThighDR"))
        SystematicStat->SetMaximum(0.5);
      else SystematicStat->SetMaximum(0.3);
      // Define some colour schemes
      SystematicStat->SetFillColor(kGray); // Gray
      SystematicJet->SetFillColor(857); // Blue
      SystematicElec->SetFillColor(kRed+1); // Light blue
      SystematicMuon->SetFillColor(419); // nice green
      SystematicPRW->SetFillColor(kOrange+3); // Brown
      SystematicLumi->SetFillColor(kOrange-9); // Beige
      SystematicUnfold->SetFillColor(kOrange+7); // Orange
      TheoryUncertainty->SetFillColor(kMagenta+2); // Purple
      /// Line width
      SystematicStat->SetLineWidth(1); // Gray
      SystematicJet->SetLineWidth(1); // Blue
      SystematicElec->SetLineWidth(1); // Light blue
      SystematicMuon->SetLineWidth(1); // nice green
      SystematicPRW->SetLineWidth(1); // Brown
      SystematicLumi->SetLineWidth(1); // Beige
      SystematicUnfold->SetLineWidth(1); // Orange
      TheoryUncertainty->SetLineWidth(1); // Purple


      // Create a Legend
      leg = new TLegend(0.45, 0.70, 0.60, 0.93);
      // leg = new TLegend(0.80, 0.70, 0.95, 0.93);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.030);
      leg->SetFillStyle(0);

      TString SystLabel = "#oplus Statistics";
      leg->AddEntry(SystematicStat,SystLabel,"f");
      SystLabel = "#oplus Theory";
      leg->AddEntry(TheoryUncertainty,SystLabel,"f");
      SystLabel = "#oplus Jet";
      leg->AddEntry(SystematicJet,SystLabel,"f");
      SystLabel = "#oplus E/Gamma";
      leg->AddEntry(SystematicElec,SystLabel,"f");
      SystLabel = "#oplus Muon";
      leg->AddEntry(SystematicMuon,SystLabel,"f");
      SystLabel = "#oplus Unfolding";
      leg->AddEntry(SystematicUnfold,SystLabel,"f");
      SystLabel = "#oplus Pile-up";
      leg->AddEntry(SystematicPRW,SystLabel,"f");
      leg->AddEntry(SystematicLumi,"Luminosity","f");
      SystematicStat->SetLineColor(kBlack);
      SystematicJet->SetLineColor(kBlack);
      SystematicElec->SetLineColor(kBlack); // Nice blue
      SystematicMuon->SetLineColor(kBlack); // nice green
      SystematicPRW->SetLineColor(kBlack); // Orange
      SystematicLumi->SetLineColor(kBlack); // Orange
      SystematicUnfold->SetLineColor(kBlack); // Orange
      TheoryUncertainty->SetLineColor(kBlack); // Orange

      // Draw the plot!
      SystematicStat->Draw("hist");
      TheoryUncertainty->Draw("same hist");
      SystematicJet->Draw("same hist");
      SystematicElec->Draw("same hist");
      SystematicMuon->Draw("same hist");
      SystematicUnfold->Draw("same hist");
      SystematicPRW->Draw("same hist");
      SystematicLumi->Draw("same hist");
      leg->Draw();
      drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.03, "", iterations,HighPt);
      gPad->RedrawAxis();
      gPad->RedrawAxis("G");
      if (saveUncert) can->Print(std::to_string(iterations)+"iteration_ratioStackPlot"+region+plot+".png");

      // Full cross-sections + systematics + ratios plot!
      xs_data = (TH1F*) Unfolded["Data0"]->at(0)->Clone();
      TH1F* xs_data2 = (TH1F*) Unfolded["Data_MG"]->at(0)->Clone();
      TH1F* data_up = (TH1F*) Unfolded["Data0"]->at(0)->Clone();
      TH1F* data_down = (TH1F*) Unfolded["Data0"]->at(0)->Clone();
      xs_sherpa = (TH1F*) Unfolded["Sherpa0"]->at(0)->Clone();
      xs_madgraph = (TH1F*) Unfolded["MadGraph0"]->at(0)->Clone();
      TH1F* variation = new TH1F();
      TH1F* variation2 = new TH1F();

      std::vector<double> var_up, var_down, nom;
      for (int i=0; i<nBins; i++){
        var_up.push_back(0.);
        var_down.push_back(0.);
        nom.push_back(xs_data->GetBinContent(i+1));
      }
      // Start the error loops!
      for (int sys=1; sys<(nSys+1)/2; sys++){ // This assumes an odd number of total systematics! (nominal + even)
        variation = (TH1F*) Unfolded["Data"+std::to_string(2*sys-1)]->at(0)->Clone();
        variation2 = (TH1F*) Unfolded["Data"+std::to_string(2*sys)]->at(0)->Clone();
        for (int i=0; i<nBins; i++){
          double var = variation->GetBinContent(i+1) - nom[i];
          double var2 = variation2->GetBinContent(i+1) - nom[i];
          if ( (var > 0 ) && (var2 > 0) ){ // If both __1up and __1down are positive, get the max.
            double Var = std::max(var*var, var2*var2);
            var_up[i] = var_up[i] + Var;
          }
          else if ( (var < 0 ) && (var2 < 0) ){ // If both __1up and __1down are negative, get the max.
            double Var = std::max(var*var, var2*var2);
            var_down[i] = var_down[i] + Var;
          }
          else{
            if (var>=0) var_up[i] = var_up[i] + var*var;
            else var_down[i] = var_down[i] + var*var;
            if (var2>=0) var_up[i] = var_up[i] + var2*var2;
            else var_down[i] = var_down[i] + var2*var2;
          }
        }
      }

      // Evaluate up and down variations of theory.
      // Evaluate the Theory uncertainties! They come in rebinned unfolded cross sections
      for (int sys=0; sys<11; sys++){ // 11 theory uncertainty pairs. 135-156
        int system = 135 + 2*sys;
        TH1F* variation = getHisto(theory, region+var+system+"_"+std::to_string(iterations));
        system = system + 1;
        TH1F* variation2 = getHisto(theory, region+var+system+"_"+std::to_string(iterations));
        double var1=0, var2=0;
        for (int i=0; i<nBins; i++){
          var1 = variation->GetBinContent(i+1)-nom[i];
          var2 = variation2->GetBinContent(i+1)-nom[i];
          if ( (var1 > 0 ) && (var2 > 0) ){ // If both __1up and __1down are positive, get the max.
            double Var = std::max(var1*var1, var2*var2);
            var_up[i] = var_up[i] + Var;
          }
          else if ( (var1 < 0 ) && (var2 < 0) ){ // If both __1up and __1down are negative, get the max.
            double Var = std::max(var1*var1, var2*var2);
            var_down[i] = var_down[i] + Var;
          }
          else{
            if (var1>=0) var_up[i] = var_up[i] + var1*var1;
            else var_down[i] = var_down[i] + var1*var1;
            if (var2>=0) var_up[i] = var_up[i] + var2*var2;
            else var_down[i] = var_down[i] + var2*var2;
          }
        }
      }

      // Evaluate the up and down variations of unfolding Uncertainty.
      unfoldingSource = settings->GetValue("UnfoldingUncertainty", " ");
      fromUnfolding = getHisto(unfoldUncert, region+"unf"+var+"_"+iterations+unfoldingSource);
      for (int i=1; i<nBins+1; i++){
        // std::cout << "Unfolded data in bin " << i << " = " << nominal->GetBinContent(i) << std::endl;
        // std::cout << "Unfolded Sherpa in bin " << i << " = " << fromUnfolding->GetBinContent(i) << std::endl;
        double var = fromUnfolding->GetBinContent(i) - nom[i-1];
        if (var > 0) var_up[i-1] = var_up[i-1] + var*var;
        else if (var < 0) var_down[i-1] = var_down[i-1] + var*var;
      }


      // Add the final uncertainties
      for (int i=0; i<nBins; i++){
        // ADDING STAT AND LUMINOSITY UNCERTAINTY
        double stat = std::pow(xs_data->GetBinError(i+1),2);
        double lumi = std::pow(xs_data->GetBinContent(i+1)*lumiUncert,2);
        data_up->SetBinContent(i+1, nom[i] + std::sqrt(var_up[i] + stat + lumi) );
        data_down->SetBinContent(i+1, nom[i] - std::sqrt(var_down[i] + stat + lumi) );
        data_up->SetBinError(i+1, 0.);
        data_down->SetBinError(i+1, 0.);
        // std::cout << "Alex ratio = " << data_up->GetBinContent(i+1)/xs_data->GetBinContent(i+1) << std::endl;
        if (xs_data->GetBinContent(i+1) < 0){
          xs_data->SetBinContent(i+1,0.);
          data_up->SetBinContent(i+1,0.);
          data_down->SetBinContent(i+1,0.);
        }
      }

      // Start plotting
      can->Clear();
      double ratio = 0.36, margin = 0.02;
      can->cd();
      gPad->SetBottomMargin(ratio);
      gPad->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      // can->SetBottomMargin(0.12);
      can->SetRightMargin(0.04);
      // Do some plotting
      data_up->SetTitle(";"+Xlabel+";"+xsLabel);
      // Not log plots
      if (plot.Contains("ZJpT")){
        gPad->SetLogy(0);
        data_up->SetMaximum(xs_madgraph->GetMaximum()*2.0);
        data_up->SetMinimum(data_down->GetMinimum()*.5);
      }
      else{
        gPad->SetLogy(1);
        if(data_down->GetMinimum() > 100)
          data_up->SetMinimum(data_down->GetMinimum()*0.05);
        data_up->SetMaximum(xs_madgraph->GetMaximum()*100);
      }
      if (plot.Contains("NJets")) data_up->SetMaximum(data_up->GetMaximum()*10.); // Scale it up by 10 to help plotting
      if (plot.Contains("pTZ")) data_up->SetMaximum(data_up->GetMaximum()*5.); // Scale it up by 5 to help plotting
      data_up->SetFillColor(1);
      // SherpaUp->SetFillStyle(3345);
      // SherpaUp->SetFillStyle(3344);
      data_up->SetFillStyle(3005);
      data_up->SetMarkerColor(0);
      data_up->GetXaxis()->SetLabelSize(0.);
      data_down->SetFillColor(10);
      data_down->SetFillStyle(1001);
      data_up->SetLineColor(10);
      data_down->SetLineColor(10);
      xs_sherpa->SetMarkerStyle(25); // Square
      xs_sherpa->SetMarkerColor(814); // Dark Green
      xs_sherpa->SetLineColor(814); // Dark Green
      xs_madgraph->SetMarkerStyle(26); // triangle
      xs_madgraph->SetMarkerColor(807); // Orange
      xs_madgraph->SetLineColor(807); // Orange

      // xs_data->SetMarkerStyle(20);
      // xs_data->SetMarkerSize(0);
      xs_data->SetLineColor(kBlack);
      data_up->GetXaxis()->SetTitleOffset(1.4);
      data_up->GetYaxis()->SetLabelSize(0.03);
      data_up->GetYaxis()->SetTitleOffset(1.4);
      data_up->GetXaxis()->SetLabelSize(0);

      // Define legend
      // leg = new TLegend(0.65, 0.80, 0.80, 0.93);
      leg = new TLegend(0.65, 0.70, 0.80, 0.93);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.030);
      leg->SetFillStyle(0);

      leg->AddEntry(xs_data,"Data stat. unc","pe");
      leg->AddEntry(data_up,"Total unc.", "f");
      leg->AddEntry(xs_sherpa,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pl");
      leg->AddEntry(xs_madgraph,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pl");

      data_up->Draw("HIST");
      data_down->Draw("SAME HIST");
      xs_data->Draw("SAME");
      xs_sherpa->Draw("SAME");
      xs_madgraph->Draw("SAME");
      drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.03, "", -1, HighPt);
      leg->Draw();
      gPad->RedrawAxis();
      gPad->RedrawAxis("G");


      // Now add the ratio plot: first the pad
      TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
      p->SetLogx(0);
      p->Modified();
      p->Update();
      p->SetTopMargin(1.0 - ratio);
      p->SetFillStyle(0);
      p->SetTicks();
      p->SetGridy();
      p->Draw();
      p->cd();

      // Make the ratio plot!
      TH1F* ratio_up = (TH1F*) data_up->Clone();
      TH1F* ratio_down = (TH1F*) data_down->Clone();
      TH1F* ratio_sherpa = (TH1F*) xs_sherpa->Clone();
      TH1F* ratio_madgraph = (TH1F*) xs_madgraph->Clone();
      // Divide them
      ratio_up->Divide((TH1F*) xs_data);
      // for (int i=1; i<nBins+1; i++)
      //   std::cout << "Alex ratio plot= " << ratio_up->GetBinContent(i) << std::endl;
      ratio_down->Divide((TH1F*) xs_data);
      ratio_sherpa->Divide((TH1F*) xs_data);
      ratio_madgraph->Divide((TH1F*) xs_data);

      ratio_up->SetFillColor(1);
      ratio_up->SetFillStyle(3005);
      // ratioUp->SetMarkerColor(10);
      ratio_down->SetFillColor(10);
      ratio_down->SetFillStyle(1001);
      ratio_down->SetMarkerColor(10);

      ratio_up->SetLineColor(0);
      ratio_down->SetLineColor(0);
      ratio_sherpa->SetMarkerStyle(25); // Square
      ratio_sherpa->SetMarkerColor(814); // Dark Green
      ratio_sherpa->SetLineColor(814); // Dark Green
      ratio_madgraph->SetMarkerStyle(26); // triangle
      ratio_madgraph->SetMarkerColor(807); // Orange
      ratio_madgraph->SetLineColor(807); // Orange

      // Define the ratio plot info
      gStyle->SetErrorX(0.5);
      ratio_up->GetYaxis()->SetLabelSize(0.03);
      ratio_up->GetYaxis()->SetTitle("Pred./Data");
      ratio_up->GetXaxis()->SetLabelSize(0.03);
      ratio_up->GetXaxis()->SetTitleOffset(1.4);
      ratio_up->GetYaxis()->SetTitleOffset(1.4);
      ratio_up->GetYaxis()->SetRangeUser(0.5, 2.0);
      // ratioPlot->GetYaxis()->SetRangeUser(0.3, 1.7);
      // ratio_up->Draw("P");
      // ratio_up->Draw("SAME P");
      ratio_up->Draw("HIST");
      ratio_down->Draw("SAME HIST");
      ratio_sherpa->Draw("SAME");
      ratio_madgraph->Draw("SAME");
      // Draw Black line centered at 1.
      TH1F* ratioOne = (TH1F*) xs_data->Clone();
      for (int i=1; i<nBins+1;i++){
       ratioOne->SetBinContent(i,1);
       ratioOne->SetBinError(i,0);
      }
      ratioOne->SetLineColor(kBlack);
      ratioOne->SetMarkerSize(0.);
      ratioOne->Draw("SAME HIST");
      gPad->RedrawAxis();
      gPad->RedrawAxis("G");

      if (saveXS)
        can->Print("xs_"+region+plot+".png");

      // Save cross sections into the individual channels to add channels
      if (doCombine)
      for (int sys=0; sys<nSys; sys++){
        if (region == "hmm") {
          data_down_muon[plot]->push_back( (TH1F*) data_down->Clone() );
          data_up_muon[plot]->push_back( (TH1F*) data_up->Clone() );
          data_muon[plot]->push_back( (TH1F*) xs_data->Clone() );
          Sherpa_muon[plot]->push_back( (TH1F*) xs_sherpa->Clone() );
          MG_muon [plot]->push_back( (TH1F*) xs_madgraph->Clone() );
        }
        else if (region == "hee"){
          data_down_elec[plot]->push_back( (TH1F*) data_down->Clone() );
          data_up_elec[plot]->push_back( (TH1F*) data_up->Clone() );
          data_elec[plot]->push_back( (TH1F*)xs_data->Clone() );
          Sherpa_elec[plot]->push_back( (TH1F*) xs_sherpa->Clone() );
          MG_elec[plot]->push_back( (TH1F*) xs_madgraph->Clone() );
        }
        else ("Wrong region somehow IN THE region LOOP.");
      }


    } // end of plotList
    delete can;
  } // End of regions (ie mm or ee)

  // Now if we ran both channels, we compare the cross sections & add them!
  // Save cross sections into the individual channels to compare & add channels
  if (channels.size() == 2 && doRatios){
    double Totlumi = settings->GetValue("luminosity", 0.0);
    TCanvas *can = new TCanvas("", "", 800, 800);
    std::cout << "Alexandre starting to compare both lepton channels!" << std::endl;
    for (auto plot : plotList){
      // Fetch nominal cross sections
      TH1F* nominalElec = (TH1F*) elecChannel[plot]->at(0)->Clone();
      TH1F* nominalMuon = (TH1F*) muonChannel[plot]->at(0)->Clone();
      TH1F* truthElec = (TH1F*) elecTruth[plot]->at(0)->Clone();
      TH1F* truthMuon = (TH1F*) muonTruth[plot]->at(0)->Clone();

      // Make a copy of the histograms for the final up & down uncertainties
      TH1F* Elec_up = (TH1F*) nominalElec->Clone();
      TH1F* Elec_down = (TH1F*) nominalElec->Clone();
      TH1F* Muon_up = (TH1F*) nominalMuon->Clone();
      TH1F* Muon_down = (TH1F*) nominalMuon->Clone();

      // Needed definitions
      int nBins = nominalMuon->GetNbinsX();
      TH1F* variation = new TH1F();
      TH1F* variation2 = new TH1F();
      std::vector<double> elec_var_up, elec_var_down, muon_var_up, muon_var_down;
      for (int i=0; i<nBins; i++){ // Start the up/down variations with stat. error ^2 = N
        elec_var_up.push_back( nominalElec->GetBinContent(i+1) );
        elec_var_down.push_back( nominalElec->GetBinContent(i+1) );
        muon_var_up.push_back( nominalMuon->GetBinContent(i+1) );
        muon_var_down.push_back( nominalMuon->GetBinContent(i+1) );
      }

      for (int sys=1; sys<(nSys+1)/2; sys++){ // Loop through all pairs of systematics

        if ( (nSys==135 && sys<=8) || (nSys==131 && sys<=8) ){// Electron Systematics
          variation = (TH1F*) elecChannel[plot]->at(2*sys-1)->Clone();
          variation2 = (TH1F*) elecChannel[plot]->at(2*sys)->Clone();
          for (int i=0; i<nBins; i++){
            double val = nominalElec->GetBinContent(i+1);
            double var = variation->GetBinContent(i+1) - val;
            double var2 = variation2->GetBinContent(i+1) - val;
            if ( (var > 0 ) && (var2 > 0) ){ // If both __1up and __1down are positive, get the max.
              double Var = std::max(var*var, var2*var2);
              elec_var_up[i] = elec_var_up[i] + Var;
            }
            else if ( (var < 0 ) && (var2 < 0) ){
              double Var = std::max(var*var, var2*var2);
              elec_var_down[i] = elec_var_down[i] + Var;
            }
            else{
              if (var>=0) elec_var_up[i] = elec_var_up[i] + var*var;
              else elec_var_down[i] = elec_var_down[i] + var*var;
              if (var2>=0) elec_var_up[i] = elec_var_up[i] + var2*var2;
              else elec_var_down[i] = elec_var_down[i] + var2*var2;
            }
          }
        }
        else if ( (nSys==135 && sys<=51) || (nSys==131 && sys<=51) ){// Jet Systematics
          // Do nothing : Only compare using lepton systematics (since others are correlated)
        }
        else if ( (nSys==135 && sys<=66) || (nSys==131 && sys<=64) ){// Muon Systematics
          variation = (TH1F*) muonChannel[plot]->at(2*sys-1)->Clone();
          variation2 = (TH1F*) muonChannel[plot]->at(2*sys)->Clone();
          for (int i=0; i<nBins; i++){
            double val = nominalMuon->GetBinContent(i+1);
            double var = variation->GetBinContent(i+1) - val;
            double var2 = variation2->GetBinContent(i+1) - val;
            if ( (var > 0 ) && (var2 > 0) ){ // If both __1up and __1down are positive, get the max.
              double Var = std::max(var*var, var2*var2);
              muon_var_up[i] = muon_var_up[i] + Var;
            }
            else if ( (var < 0 ) && (var2 < 0) ){
              double Var = std::max(var*var, var2*var2);
              muon_var_down[i] = muon_var_down[i] + Var;
            }
            else{
              if (var>=0) muon_var_up[i] = muon_var_up[i] + var*var;
              else muon_var_down[i] = muon_var_down[i] + var*var;
              if (var2>=0) muon_var_up[i] = muon_var_up[i] + var2*var2;
              else muon_var_down[i] = muon_var_down[i] + var2*var2;
            }
          }
        }
        else if ( (nSys==135 && sys<=67) || (nSys==131 && sys<=65) ){// PRW SF Systematics
          // Do nothing : Only compare using lepton systematics (since others are correlated)
        }
        else fatal("ALEX Maximum Systematics are buggy!");
      } // End of full systematics loops

      // Evaluate the Up/down uncertainty histograms
      // This is the total uncertainty!
      for (int i=0; i<nBins; i++){
        Elec_up->SetBinContent(i+1, std::sqrt(elec_var_up[i]) );
        Elec_down->SetBinContent(i+1, std::sqrt(elec_var_down[i]) );
        Muon_up->SetBinContent(i+1, std::sqrt(muon_var_up[i]) );
        Muon_down->SetBinContent(i+1, std::sqrt(muon_var_down[i]) );
        // This is me trying something new
        nominalElec->SetBinError(i+1, std::sqrt(std::max(elec_var_up[i],elec_var_down[i])));
        nominalMuon->SetBinError(i+1, std::sqrt(std::max(muon_var_up[i],muon_var_down[i])));
        // truthElec->SetBinError(i+1, std::sqrt(truthElec->GetBinContent(i+1)));
        // truthMuon->SetBinError(i+1, std::sqrt(truthMuon->GetBinContent(i+1)));
      }
      // trying something new;
      TH1F* ratio = (TH1F*) nominalElec->Clone();
      ratio->Divide(nominalMuon);
      TH1F* ratio_truth = (TH1F*) truthElec->Clone();
      ratio_truth->Divide(truthMuon);
      TH1F* double_ratio = (TH1F*) ratio_truth->Clone();
      double_ratio->Divide(ratio);

      // Plot the ratio between both channels!
      can->Clear();
      gPad->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      can->SetBottomMargin(0.12);
      can->SetRightMargin(0.04);
      gStyle->SetErrorX(0.5);
      Str newYLabel = "Electron to Muon d#sigma/dX ratio";
      Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
      // Trying new stuff
      ratio->SetTitle(";"+Xlabel+";"+newYLabel);
      ratio->SetMaximum(1.5);
      ratio->SetMinimum(0.5);
      ratio->SetLineColor(10.);
      ratio->SetLineWidth(1.);
      // ratio->SetMarkerStyle(32); // inverted triangle
      ratio->SetMarkerColor(1); // brown
      ratio->SetLineColor(1); // brown

      ratio->Draw("E0");

      TH1F* ratioOne = (TH1F*) nominalElec->Clone();
      for (int i=1; i<nBins+1;i++){
       ratioOne->SetBinContent(i,1);
       ratioOne->SetBinError(i,1E-3);
      }
      ratioOne->SetMarkerColor(0.);
      ratioOne->SetMarkerSize(0.);
      ratioOne->SetLineWidth(3.);
      ratioOne->SetLineColor(kRed);
      ratioOne->Draw("same");

      TLegend* leg = new TLegend();
      leg = new TLegend(0.50, 0.15, 0.65, 0.28);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetFillStyle(0);
      leg->SetTextSize(0.035);

      leg->AddEntry(ratio,"data: Electron/Muon ratio","pl");
      // leg->AddEntry(ratio_up,"Stat. #oplus lepton uncertainty","f");
      leg->Draw();

      double xTitle=0.55, yTitle=0.90;
      drawPlotInfo(xTitle,yTitle, plot, "both", Totlumi);
      if (HighPt) { // Muon HighPt WP!
        drawText(xTitle, yTitle-6*0.04*1.25, "HighPt MuonWP Studies",0,1,0.04);
      }
      if (doSherpa){
        // drawText(xTitle, yTitle-6*0.04*1.25, "Sherpa2.2.1",0,1,0.04);
        can->Print("Sherpa_channel_ratio_"+plot+".png");
      }
      else{
        // drawText(xTitle, yTitle-6*0.04*1.25, "MGPy8",0,1,0.04);
        can->Print("MadGraph_channel_ratio_"+plot+".png");
      }

      // Do truth ratio plot
      can->Clear();
      gPad->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      can->SetBottomMargin(0.12);
      can->SetRightMargin(0.04);
      gStyle->SetErrorX(0.5);
      newYLabel = "Truth Electron to Muon d#sigma/dX ratio";
      // trying new stuff
      ratio_truth->SetTitle(";"+Xlabel+";"+newYLabel);
      ratio_truth->SetMaximum(1.5);
      ratio_truth->SetMinimum(0.5);
      ratio_truth->SetLineColor(10.);
      ratio_truth->SetLineWidth(1.);
      // ratio_truth->SetMarkerStyle(32); // inverted triangle
      ratio_truth->SetMarkerColor(1); // brown
      ratio_truth->SetLineColor(1); // brown

      ratio_truth->Draw("E0");

      ratioOne->Draw("same");

      leg = new TLegend(0.50, 0.15, 0.65, 0.28);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetFillStyle(0);
      leg->SetTextSize(0.035);

      leg->AddEntry(ratio,"Truth: Electron/Muon ratio","pl");
      // leg->AddEntry(ratio_up,"Stat. #oplus lepton uncertainty","f");
      leg->Draw();

      drawPlotInfo(xTitle,yTitle, plot, "both", Totlumi);
      if (HighPt) { // Muon HighPt WP!
        drawText(xTitle, yTitle-6*0.04*1.25, "HighPt MuonWP Studies",0,1,0.04);
      }
      if (doSherpa){
        // drawText(xTitle, yTitle-6*0.04*1.25, "Sherpa2.2.1",0,1,0.04);
        can->Print("Sherpa_truth_channel_ratio_"+plot+".png");
      }
      else{
        // drawText(xTitle, yTitle-6*0.04*1.25, "MGPy8",0,1,0.04);
        can->Print("MadGraph_truth_channel_ratio_"+plot+".png");
      }

      // Double ratio!
      can->Clear();
      gPad->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      can->SetBottomMargin(0.12);
      can->SetRightMargin(0.04);
      gStyle->SetErrorX(0.5);
      newYLabel = "Truth/Reco double ratio";
      // Trying new stuff
      double_ratio->SetTitle(";"+Xlabel+";"+newYLabel);
      double_ratio->SetMaximum(1.5);
      double_ratio->SetMinimum(0.5);
      double_ratio->SetLineColor(10.);
      double_ratio->SetLineWidth(1.);
      // double_ratio->SetMarkerStyle(32); // inverted triangle
      double_ratio->SetMarkerColor(1); // brown
      double_ratio->SetLineColor(1); // brown
      double_ratio->Draw("E0");
      ratioOne->Draw("same");

      leg = new TLegend(0.50, 0.15, 0.65, 0.28);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetFillStyle(0);
      leg->SetTextSize(0.035);

      leg->AddEntry(double_ratio,"Truth/Reco double ratio","pl");
      // leg->AddEntry(ratio_up,"Stat. #oplus lepton uncertainty","f");
      leg->Draw();

      drawPlotInfo(xTitle,yTitle, plot, "both", Totlumi);
      if (HighPt) { // Muon HighPt WP!
        drawText(xTitle, yTitle-6*0.04*1.25, "HighPt MuonWP Studies",0,1,0.04);
      }
      if (doSherpa){
        // drawText(xTitle, yTitle-6*0.04*1.25, "Sherpa2.2.1",0,1,0.04);
        can->Print("Sherpa_double_channel_ratio_"+plot+".png");
      }
      else{
        // drawText(xTitle, yTitle-6*0.04*1.25, "MGPy8",0,1,0.04);
        can->Print("MadGraph_double_channel_ratio_"+plot+".png");
      }


    } // end of plotlist
  } // end of if merge

  // If ran both channels, combine them and compare them to the 4 Generators in 2 pads!
  if (channels.size() == 2 && doCombine){
    std::cout << "Combining unfolded cross-sections of both channels!" << std::endl;
    double Totlumi = settings->GetValue("luminosity", 0.0);
    TCanvas *can = new TCanvas("", "", 800, 800);
    // Fetch data, data_up, data_down, Sherpa & MadGraph for both channels.
    // TFile *f1 = new TFile("/eos/user/a/alaurier/ZJets/FromRivet/Sherpa221_Zll.root");
    TFile *f1 = new TFile("/eos/user/a/alaurier/ZJets/FromRivet/Sherpa2210_Zll.root");
    TFile *f2 = new TFile("/eos/user/a/alaurier/ZJets/FromRivet/MGPy8EG_FxFx_Zll.root");

    for (auto plot : plotList){
      // We pull the theory variations.
      // TheoryRatios contains 8 rebinned histograms
      // up, down of 100PDF sets, choice of PDF, PDF alpha_S and scale uncertainty
      // It is the ratio of theory/nominal!, ie bin contents are around 1.
      HistV TheoryRatios = CalculateSherpaTheory(plot);
      int nBins = TheoryRatios[0]->GetNbinsX();
      std::vector<double> var_up, var_down;
      for (int i=0; i<nBins; i++){
        var_up.push_back(0.);
        var_down.push_back(0.);
      }
      double val1,val2,val;
      for (int j=0; j<4; j++){ // 4 pairs
        for (int i=1; i<nBins+1;i++){
          val1 = TheoryRatios[2*j]->GetBinContent(i)-1;
          val2 = TheoryRatios[2*j+1]->GetBinContent(i)-1;
          if (val1 > 0. && val2 > 0.){ // Both above nominal, get max
            val = std::max(val1,val2);
            var_up[i-1] = var_up[i-1] + val*val;
          }
          else if (val1 < 0. && val2 < 0.){ // Both below nominal, get min
            val = std::max(val1*val1,val2*val2);
            var_down[i-1] = var_down[i-1] + val; // Already squared
          }
          else if (val1 > 0. && val2 < 0.){ // one above, one below
            var_up[i-1] = var_up[i-1] + val1*val1;
            var_down[i-1] = var_down[i-1] + val2*val2;
          }
          else if (val1 < 0. && val2 > 0.){ // one above, one below
            var_down[i-1] = var_down[i-1] + val1*val1;
            var_up[i-1] = var_up[i-1] + val2*val2;
          }
        }
      }

      // Fetch nominal data cross sections
      TH1F* data_ee = (TH1F*) data_elec[plot]->at(0)->Clone(); // nominal electron
      TH1F* data_mm = (TH1F*) data_muon[plot]->at(0)->Clone(); // nominal muon
      TH1F* data = (TH1F*) data_elec[plot]->at(0)->Clone(); // nominal data
      data->Add(data_mm); // Addition of 2 channels is validated!

      // if (plot == "pTjet")
      //   std::cout << "140-180 pT jet = " << data->GetBinContent(4) << std::endl;
      // if (plot == "NJets")
      //   std::cout << "Total Fiducial cross section = " << data->Integral() << std::endl;
      // if (plot == "NJets500")
      //   std::cout << "Fiducial cross section, pT(jet) > 500GeV = " << data->Integral() << std::endl;
      // if (plot == "minDR")
      //   std::cout << "Fiducial cross section, minDR = " << data->Integral() << std::endl;
      // if (plot == "minDR")
      //   std::cout << "Fiducial cross section, minDR coll = " << data->Integral(1,7) << std::endl;
      // if (plot == "minDR")
      //   std::cout << "Fiducial cross section, minDR b2b= " << data->Integral(11,20) << std::endl;
      // if (plot == "ZJpT")
      //   std::cout << "Fiducial cross section, ZJPT = " << data->Integral() << std::endl;
      // if (plot == "ZJpTlowDR")
      //   std::cout << "Fiducial cross section, Collinear = " << data->Integral() << std::endl;
      // if (plot == "ZJpThighDR")
      //   std::cout << "Fiducial cross section, back-to-back = " << data->Integral() << std::endl;

      // Fetch Sherpa cross sections
      TH1F* Sherpa_ee = (TH1F*) Sherpa_elec[plot]->at(0)->Clone(); // nominal electron
      TH1F* Sherpa_mm = (TH1F*) Sherpa_muon[plot]->at(0)->Clone(); // nominal muon
      TH1F* Sherpa = (TH1F*) Sherpa_elec[plot]->at(0)->Clone(); // nominal Sherpa
      Sherpa->Add(Sherpa_mm);
      // Get up and down Sherpa theory
      TH1F* Sherpa_theory_up = (TH1F*) Sherpa->Clone();
      TH1F* Sherpa_theory_down = (TH1F*) Sherpa->Clone();
      for (int i=1;i<nBins+1;i++){
        Sherpa_theory_up->SetBinContent(i, Sherpa->GetBinContent(i)*(1+std::sqrt(var_up[i-1])));
        Sherpa_theory_down->SetBinContent(i, Sherpa->GetBinContent(i)*(1-std::sqrt(var_down[i-1])));
        Sherpa_theory_up->SetBinError(i, 0.);
        Sherpa_theory_down->SetBinError(i, 0.);
      }
      // Fetch Sherpa cross sections
      TH1F* MG_ee = (TH1F*) MG_elec[plot]->at(0)->Clone(); // nominal electron
      TH1F* MG_mm = (TH1F*) MG_muon[plot]->at(0)->Clone(); // nominal muon
      TH1F* MG = (TH1F*) MG_elec[plot]->at(0)->Clone(); // nominal MadGraph
      MG->Add(MG_mm);
      // Fetch Sherpa2.2.10
      TString histname;
      if (plot == "pTZ") histname = "ZpT_100GeVjet";
      else if (plot == "pTjet") histname = "j0pT";
      else if (plot == "NJets") histname = "NJets_excl";
      else if (plot == "minDR") histname = "DR";
      else if (plot == "NJets500") histname = "";
      else if (plot == "HT") histname = "HT";
      else if (plot == "Mjj") histname = "mjj";
      else if (plot == "ZJpT") histname = "ZJpT";
      else if (plot == "ZJpTlowDR") histname = "ZJpT_loDR";
      else if (plot == "ZJpTmedDR") histname = "";
      else if (plot == "ZJpThighDR") histname = "ZJpT_hiDR";
      else fatal("Unknown plot name for merger!");
      if (histname == ""){
        std::cout << "Skipping combination of " << plot << std::endl;
        continue;
      }
      TH1F* Sherpa10 = (TH1F*) f1->Get("ZJets_rivet/"+histname);
      TH1F* FxFx = (TH1F*) f2->Get("ZJets_rivet/"+histname);
      if (Sherpa10->GetNbinsX() != data->GetNbinsX()) fatal("Wrong binning for Sherpa plot: " + histname);
      if (FxFx->GetNbinsX() != data->GetNbinsX()) fatal("Wrong binning for FxFx plot: " + histname);
      // std::cout << "For plot " << plot << std::endl;
      // std::cout << "data integral = " << data->Integral() << std::endl;
      // std::cout << "Sh   integral = " << Sherpa->Integral() << std::endl;
      // std::cout << "Sh10 integral = " << Sherpa10->Integral() << std::endl;
      // std::cout << "MG   integral = " << MG->Integral() << std::endl;
      // std::cout << "FxFx integral = " << FxFx->Integral() << std::endl;

      // Get total uncertainty on data.
      TH1F* data_up = (TH1F*) data_up_elec[plot]->at(0)->Clone(); // nominal data
      TH1F* data_down = (TH1F*) data_down_elec[plot]->at(0)->Clone(); // nominal data
      // calculate new uncertainty
      // Start with up variation to data
      TH1F* data_ee_var = (TH1F*) data_up_elec[plot]->at(0)->Clone(); // up variation electron
      TH1F* data_mm_var = (TH1F*) data_up_muon[plot]->at(0)->Clone(); // up variation muon
      for (int i=1; i<data->GetNbinsX()+1;i++){ // Up variations
        if (divideByBinWidth){
          Sherpa10->SetBinContent(i, Sherpa10->GetBinContent(i)/(Sherpa10->GetBinWidth(i)));
          Sherpa10->SetBinError(i,Sherpa10->GetBinError(i)/(Sherpa10->GetBinWidth(i)));
          FxFx->SetBinContent(i, FxFx->GetBinContent(i)/(FxFx->GetBinWidth(i)));
          FxFx->SetBinError(i,FxFx->GetBinError(i)/(FxFx->GetBinWidth(i)));
        }
        double var_ee = data_ee_var->GetBinContent(i) - data_ee->GetBinContent(i);
        double var_mm = data_mm_var->GetBinContent(i) - data_mm->GetBinContent(i);
        double var = std::sqrt( var_ee*var_ee + var_mm*var_mm);
        data_up->SetBinContent(i, data->GetBinContent(i) + var);
      }
      // std::cout << "For plot " << plot << std::endl;
      // std::cout << "data integral = " << data->Integral() << std::endl;
      // std::cout << "Sh   integral = " << Sherpa->Integral() << std::endl;
      // std::cout << "Sh10 integral = " << Sherpa10->Integral() << std::endl;
      // std::cout << "MG   integral = " << MG->Integral() << std::endl;
      // std::cout << "FxFx integral = " << FxFx->Integral() << std::endl;
      // std::cout << " ===== " << std::endl;
      // std::cout << "Sherpa ee = " << Sherpa_ee->Integral() << ", mumu = " << Sherpa_mm->Integral() << std::endl;
      // std::cout << "MadGra ee = " << MG_ee->Integral() << ", mumu = " << MG_mm->Integral() << std::endl;

      // Next with down variation to data
      data_ee_var = (TH1F*) data_down_elec[plot]->at(0)->Clone(); // down variation electron
      data_mm_var = (TH1F*) data_down_muon[plot]->at(0)->Clone(); // down variation muon
      for (int i=1; i<data->GetNbinsX()+1;i++){ // Down variations
        double var_ee = data_ee_var->GetBinContent(i) - data_ee->GetBinContent(i);
        double var_mm = data_mm_var->GetBinContent(i) - data_mm->GetBinContent(i);
        double var = std::sqrt( var_ee*var_ee + var_mm*var_mm);
        data_down->SetBinContent(i, data->GetBinContent(i) - var);
      }
      // For plotting
      data_up->SetFillColor(1);
      data_up->SetFillStyle(3005);
      data_up->SetMarkerColor(0);
      data_up->GetXaxis()->SetLabelSize(0.);
      data_down->SetFillColor(10);
      data_down->SetFillStyle(1001);
      data_up->SetLineColor(10);
      data_down->SetLineColor(10);
      Sherpa->SetMarkerStyle(25); // Square
      Sherpa->SetMarkerColor(814); // Dark Green
      Sherpa->SetLineColor(814); // Dark Green
      /// sherpa Theory!
      Sherpa_theory_up->SetFillColor(820); // Light Green
      Sherpa_theory_down->SetFillColor(10); // overwrite white
      Sherpa_theory_up->SetLineColor(820); // Light Green
      Sherpa_theory_down->SetLineColor(10); // overwrite white
      MG->SetMarkerStyle(26); // triangle
      MG->SetMarkerColor(807); // Orange
      MG->SetLineColor(807); // Orange
      Sherpa10->SetMarkerStyle(24); // Circle
      Sherpa10->SetMarkerColor(857); // Nice Blue
      Sherpa10->SetLineColor(857); // Nice Blue
      FxFx->SetMarkerStyle(32); // inverted triangle
      FxFx->SetMarkerColor(kOrange+3); // brown
      FxFx->SetLineColor(kOrange+3); // brown

      // Start plotting
      can->Clear();

      if (!plot.Contains("ZJpT")){
        // double ratio = 0.36, margin = 0.02;
        double ratio = 0.4, margin = 0.02;
        can->cd();
        gPad->SetBottomMargin(0.45); // can use ratio: 60% of canvas is cross-section
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        if (!plot.Contains("ZJpT")){
          gPad->SetLogy(1);
          data_up->SetMaximum(data_up->GetMaximum()*10.);
        }
        else{
          data_up->SetMaximum(data_up->GetMaximum()*2.);
        }
        // can->SetBottomMargin(0.12);
        can->SetRightMargin(0.04);
        // Do some plotting
        Str xsLabel = settings->GetValue(plot + ".xs", " ");
        Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
        data_up->SetLineColor(kWhite);
        data_up->GetXaxis()->SetTitle("");
        data_up->GetYaxis()->SetTitle(xsLabel);
        // data_up->SetTitle(";"+Xlabel+";"+xsLabel);
        // data_up->GetXaxis()->SetTitleOffset(6.3);
        data_up->Draw("HIST");
        data_down->Draw("SAME HIST");
        // Sherpa_theory_up->Draw("SAME HIST");
        // Sherpa_theory_down->Draw("SAME HIST");
        data->Draw("SAME");
        // std::cout << "ALEX DATA INTEGRAL FOR PLOT " << plot << " IS " << data->Integral() << std::endl;
        // std::cout << "ALEX Sherpa INTEGRAL FOR PLOT " << plot << " IS " << Sherpa->Integral() << std::endl;
        Sherpa->Draw("SAME");
        MG->Draw("SAME");

        // Legend
        TLegend* leg = new TLegend(0.57, 0.70, 0.72, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.030);
        leg->SetFillStyle(0);

        leg->AddEntry(data,"Data stat. unc","pe");
        leg->AddEntry(data_up,"Total unc.", "f");
        Sherpa->SetFillColorAlpha(820, 0.1);
        leg->AddEntry(Sherpa,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pelf");
        leg->AddEntry(MG,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pelf");
        leg->AddEntry(Sherpa10,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2.10}","pelf");
        leg->AddEntry(FxFx,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8 FxFx}","pelf");
        leg->Draw("same");

        // Need to add this stuff before changing pads
        gPad->RedrawAxis();
        gPad->RedrawAxis("G");
        drawPlotInfo(0.15,0.9, plot, "both", Totlumi, 0.03, "", -1, HighPt);
        // This is a hack to include the proper position of the axis titles.
        drawText(0.06,0.13,"#bf{#it{Pred./Data}}",0, 1, 0.04, 90.);
        // drawText(0.85,0.015,"#bf{#it{"+Xlabel+"}}",0, 1, 0.032);


        // Now add the ratio plot: first the pad
        TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        // p->SetTopMargin(1.0 - ratio);
        p->SetTopMargin(0.55); // From the top
        p->SetBottomMargin(0.28); // From the bottom
        p->SetFillStyle(0);
        p->SetTicks();
        // p->SetGridy();
        p->Draw();
        p->cd();
        TH1F* ratio_up = (TH1F*) data_up->Clone();
        TH1F* ratio_down = (TH1F*) data_down->Clone();
        TH1F* ratio_Sherpa = (TH1F*) Sherpa->Clone();
        TH1F* ratio_Sherpa_up = (TH1F*) Sherpa_theory_up->Clone();
        TH1F* ratio_Sherpa_down = (TH1F*) Sherpa_theory_down->Clone();
        TH1F* ratio_MG = (TH1F*) MG->Clone();
        TH1F* ratio_Sherpa10 = (TH1F*) Sherpa10->Clone();
        TH1F* ratio_FxFx = (TH1F*) FxFx->Clone();

        TMultiGraph *mg = new TMultiGraph();

        double x[nBins];
        double y[nBins];
        double ex[nBins];
        double ey[nBins];
        double exl[nBins];
        double eyl[nBins];
        double exh[nBins];
        double eyh[nBins];
        double eyz[nBins];
        // Define Sherpa ratio plot
        for (int i=1;i<nBins+1;i++){
          x[i-1] = data->GetBinCenter(i);
          y[i-1] = Sherpa->GetBinContent(i)/data->GetBinContent(i);
          ex[i-1] = Sherpa->GetBinWidth(i)/2;
          ey[i-1] = (Sherpa->GetBinContent(i)/data->GetBinContent(i)) * std::sqrt(std::pow(Sherpa->GetBinError(i)/Sherpa->GetBinContent(i),2) + std::pow(data->GetBinError(i)/data->GetBinContent(i),2));
        }
        TGraphErrors* SherpaRatio = new TGraphErrors(nBins,x,y,ex,ey);
        if (plot.Contains("NJets")) SherpaRatio->GetXaxis()->SetRangeUser(0.5, 7.5);
        SherpaRatio->GetYaxis()->SetRangeUser(0.5, 2.0);
        SherpaRatio->GetXaxis()->SetLabelSize(0.);
        SherpaRatio->GetYaxis()->SetLabelSize(0.03);
        SherpaRatio->SetMarkerStyle(25); // Square
        SherpaRatio->SetMarkerColor(814); // Dark Green
        SherpaRatio->SetLineColor(814); // Dark Green
        mg->Add(SherpaRatio);

        // Define MG ratio plot
        for (int i=1;i<nBins+1;i++){
          x[i-1] = data->GetBinCenter(i);
          y[i-1] = MG->GetBinContent(i)/data->GetBinContent(i);
          ex[i-1] = MG->GetBinWidth(i)/2;
          ey[i-1] = (MG->GetBinContent(i)/data->GetBinContent(i)) * std::sqrt(std::pow(MG->GetBinError(i)/MG->GetBinContent(i),2) + std::pow(data->GetBinError(i)/data->GetBinContent(i),2));
        }
        TGraphErrors* MGRatio = new TGraphErrors(nBins,x,y,ex,ey);
        if (plot.Contains("NJets")) MGRatio->GetXaxis()->SetRangeUser(0.5, 7.5);
        MGRatio->GetYaxis()->SetRangeUser(0.5, 2.0);
        MGRatio->GetXaxis()->SetLabelSize(0.);
        MGRatio->GetYaxis()->SetLabelSize(0.03);
        MGRatio->SetMarkerStyle(26); // triangle
        MGRatio->SetMarkerColor(807); // Orange
        MGRatio->SetLineColor(807); // Orange
        mg->Add(MGRatio);

        // Define Sherpa Theory ratio plot
        for (int i=1;i<nBins+1;i++){
          x[i-1] = data->GetBinCenter(i);
          y[i-1] = Sherpa->GetBinContent(i)/data->GetBinContent(i);
          exl[i-1] = Sherpa->GetBinWidth(i)/2;
          exh[i-1] = Sherpa->GetBinWidth(i)/2;
          eyh[i-1] = Sherpa_theory_up->GetBinContent(i)/data->GetBinContent(i) - Sherpa->GetBinContent(i)/data->GetBinContent(i);
          eyl[i-1] = Sherpa->GetBinContent(i)/data->GetBinContent(i) - Sherpa_theory_down->GetBinContent(i)/data->GetBinContent(i);
        }
        TGraphAsymmErrors* SherpaTheoryRatio = new TGraphAsymmErrors(nBins,x,y,exl,exh,eyl,eyl);
        if (plot.Contains("NJets")) SherpaTheoryRatio->GetXaxis()->SetRangeUser(0.5, 7.5);
        SherpaTheoryRatio->GetYaxis()->SetRangeUser(0.5, 2.0);
        SherpaTheoryRatio->GetXaxis()->SetLabelSize(0.);
        SherpaTheoryRatio->GetYaxis()->SetLabelSize(0.03);
        SherpaTheoryRatio->SetMarkerSize(0); // Square
        SherpaTheoryRatio->SetLineWidth(0); // Dark Green
        SherpaTheoryRatio->SetFillColor(820); // Lighter Green
        SherpaTheoryRatio->SetFillColorAlpha(820, 0.2);
        mg->Add(SherpaTheoryRatio);

        // Define measured uncertainty ratio plot
        for (int i=1;i<nBins+1;i++){
          x[i-1] = data->GetBinCenter(i);
          y[i-1] = 1.0;
          exl[i-1] = Sherpa->GetBinWidth(i)/2;
          exh[i-1] = Sherpa->GetBinWidth(i)/2;
          eyh[i-1] = 1. - data_up->GetBinContent(i)/data->GetBinContent(i);
          eyl[i-1] = 1. - data_down->GetBinContent(i)/data->GetBinContent(i);
        }
        TGraphAsymmErrors* DataUncertRatio = new TGraphAsymmErrors(nBins,x,y,exl,exh,eyl,eyl);
        if (plot.Contains("NJets")) DataUncertRatio->GetXaxis()->SetRangeUser(0.5, 7.5);
        DataUncertRatio->GetYaxis()->SetRangeUser(0.5, 2.0);
        DataUncertRatio->GetXaxis()->SetLabelSize(0.);
        DataUncertRatio->GetYaxis()->SetLabelSize(0.03);
        DataUncertRatio->SetMarkerSize(0);
        DataUncertRatio->SetLineWidth(0);
        DataUncertRatio->SetFillColor(kBlack);
        DataUncertRatio->SetFillStyle(3005);
        mg->Add(DataUncertRatio);


        // Define Ratio == 1 ratio plot
        for (int i=1;i<nBins+1;i++){
          x[i-1] = data->GetBinCenter(i);
          y[i-1] = 1.0;
          ex[i-1] = MG->GetBinWidth(i)/2;
          ey[i-1] = 0.;
        }
        TGraphErrors* OneRatio = new TGraphErrors(nBins,x,y,ex,ey);
        if (plot.Contains("NJets")) OneRatio->GetXaxis()->SetRangeUser(0.5, 7.5);
        OneRatio->GetYaxis()->SetRangeUser(0.5, 2.0);
        OneRatio->GetXaxis()->SetLabelSize(0.);
        OneRatio->GetYaxis()->SetLabelSize(0.03);
        OneRatio->SetMarkerSize(0);
        OneRatio->SetMarkerColor(kBlack);
        OneRatio->SetLineColor(kBlack);
        OneRatio->SetLineWidth(2);
        mg->Add(OneRatio);



        mg->SetTitle("; ; ");
        if (plot.Contains("NJets")) mg->GetHistogram()->GetXaxis()->SetRangeUser(0.5, 7.5);
        mg->GetHistogram()->GetYaxis()->SetRangeUser(0.5, 2.0);
        mg->GetHistogram()->GetXaxis()->SetLabelSize(0.);
        mg->GetHistogram()->GetYaxis()->SetLabelSize(0.03);

        // mg->Draw("a2");
        // SherpaRatio->Draw("a2");
        // SherpaRatio->Draw("p");
        // MGRatio->Draw("a2");
        // MGRatio->Draw("p");
        mg->Draw("a2");
        mg->Draw("p");
        gPad->RedrawAxis();
        gPad->RedrawAxis("G");


        // Divide them
        ratio_up->Divide(data);
        ratio_down->Divide(data);
        ratio_Sherpa->Divide(data);
        ratio_Sherpa_up->Divide(data);
        ratio_Sherpa_down->Divide(data);
        ratio_MG->Divide(data);
        ratio_Sherpa10->Divide(data);
        ratio_FxFx->Divide(data);


        TH1F* ratioOne = (TH1F*) data->Clone();
        TH1F* ratioOne2 = (TH1F*) data->Clone();
        ratioOne->SetFillStyle(0);
        ratioOne2->SetFillStyle(0);
        for (int i=1; i<ratioOne->GetNbinsX()+1;i++){
          ratioOne->SetBinContent(i,1.);
          ratioOne->SetBinError(i,0);
          ratioOne2->SetBinContent(i,1.);
          ratioOne2->SetBinError(i,0);
          ratio_up->SetBinError(i, 0.);
          ratio_down->SetBinError(i, 0.);
        }
        ratioOne->SetLineColor(kBlack);
        ratioOne2->SetLineColor(kBlack);
        ratio_down->SetFillColor(10);
        ratio_down->SetFillStyle(1001);
        ratio_down->SetMarkerColor(10); // white marker
        ratio_down->SetLineColor(0); // white line
        ratio_Sherpa->SetFillStyle(0);
        ratio_MG->SetFillStyle(0);
        ratio_Sherpa10->SetFillStyle(0);
        ratio_FxFx->SetFillStyle(0);

        // gStyle->SetErrorX(0.5);
        // ratio_up->GetXaxis()->SetTitle("");
        // ratio_up->GetYaxis()->SetTitle("");
        // ratio_up->GetYaxis()->SetRangeUser(0.5, 2.0);
        // ratio_up->Draw("HIST");
        // ratio_down->Draw("SAME HIST");
        // // ratio_Sherpa_up->Draw("SAME HIST");
        // // ratio_Sherpa_down->Draw("SAME HIST");
        // ratioOne->Draw("SAME");
        // ratio_Sherpa->Draw("SAME");
        // ratio_MG->Draw("SAME");
        // gPad->RedrawAxis();
        // gPad->RedrawAxis("G");

        // Legend positions
        TLegend* leg2 = new TLegend();
        if (plot == "HT" || plot == "Mjj" || plot == "NJets" || plot == "pTZ"){ // Top of Pad
          leg = new TLegend(0.12, 0.405, 0.25, 0.445); // This is for top of pad
          leg2 = new TLegend(0.40, 0.405, 0.53, 0.445); // This is for top of pad
        }
        else if (plot.Contains("ZJpT") || plot=="minDR" || plot == "pTjet"){  // Bottom of Pad
          leg = new TLegend(0.12, 0.285, 0.25, 0.325); // This is for Bottom of pad
          leg2 = new TLegend(0.40, 0.285, 0.53, 0.325); // This is for Bottom of pad
        }
        else fatal("Need to figure out the proper Analysis ratio positions!");
        // Sherpa Legend
        leg->SetBorderSize(0);
        leg->SetTextSize(0.025);
        leg->SetFillStyle(0);
        leg->AddEntry(Sherpa,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pelf");
        leg->Draw("same");
        // MadGraph legend
        leg2->SetBorderSize(0);
        leg2->SetTextSize(0.025);
        leg2->SetFillStyle(0);
        leg2->AddEntry(MG,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pelf");
        leg2->Draw("same");

        // Now add the ratio plot: first the pad
        TPad *p2 = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p2->SetLogx(0);
        p2->Modified();
        p2->Update();
        p2->SetTopMargin(0.73); // From the top
        p2->SetBottomMargin(0.10); // From the Bottom
        p2->SetFillStyle(0);
        p2->SetTicks();
        // p2->SetGridy();
        p2->Draw();
        p2->cd();

        TH1F* ratio_up2 = (TH1F*) ratio_up->Clone();
        ratio_up2->GetYaxis()->SetRangeUser(0.5, 2.0);
        // if (plot.Contains("ZJpT"))
        //   ratio_up2->GetYaxis()->SetRangeUser(0.5, 5);
        ratio_up2->GetXaxis()->SetTitle(Xlabel);
        ratio_up2->GetXaxis()->SetLabelSize(0.04);
        ratio_up2->GetYaxis()->SetTitleSize(0.0);

        ratio_up2->Draw("HIST");
        ratio_down->Draw("HIST same");
        ratioOne2->Draw("same");
        ratio_Sherpa10->Draw("same");
        ratio_FxFx->Draw("same");
        gPad->RedrawAxis();
        gPad->RedrawAxis("G");
        // Legend positions
        if (plot == "HT" || plot == "Mjj" || plot == "NJets" || plot == "pTZ" || plot == "pTjet"){ // Top of Pad
          leg = new TLegend(0.12, 0.22, 0.25, 0.26);  // This is for the top of the pad
          leg2 = new TLegend(0.40, 0.22, 0.53, 0.26); // This is for the top of the pad
        }
        else if (plot.Contains("ZJpT")){  // Bottom of Pad
          leg = new TLegend(0.12, 0.1, 0.25, 0.14); // This is for the lower edge of the pad
          leg2 = new TLegend(0.40, 0.1, 0.53, 0.14); // This is for the lower edge of the pad
        }
        else if (plot == "minDR"){ //bottom of pad
          leg = new TLegend(0.12, 0.1, 0.25, 0.14); // This is for the lower edge of the pad
          leg2 = new TLegend(0.67, 0.22, 0.80, 0.26); // This is for the top edge of the pad
        }
        else fatal("Need to figure out the proper NNLO ratio positions!");
        // Legend Sherpa2.2.10
        leg->SetTextSize(0.025);
        leg->SetFillStyle(0);
        leg->SetBorderSize(0);
        leg->AddEntry(Sherpa10,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2.10}","plf");
        leg->Draw("same");
        // Legend FxFx
        // leg2->SetFillColor(0);
        leg2->SetBorderSize(0);
        leg2->SetTextSize(0.025);
        leg2->SetFillStyle(0);
        leg2->AddEntry(FxFx,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+PY8 FxFx}","plf");
        leg2->Draw("same");


        can->Print("total_xs_"+plot+".png");
      }
      else{
        // double ratio = 0.36, margin = 0.02;
        double ratio = 0.4, margin = 0.02;
        can->cd();
        gPad->SetBottomMargin(0.36);
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        if (!plot.Contains("ZJpT")){
          gPad->SetLogy(1);
          data_up->SetMaximum(data_up->GetMaximum()*10.);
        }
        else{
          data_up->SetMaximum(data_up->GetMaximum()*2.);
        }
        // can->SetBottomMargin(0.12);
        can->SetRightMargin(0.04);
        // Do some plotting
        Str xsLabel = settings->GetValue(plot + ".xs", " ");
        Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
        data_up->SetLineColor(kWhite);
        data_up->GetXaxis()->SetTitle("");
        data_up->GetYaxis()->SetTitle(xsLabel);
        // data_up->SetTitle(";"+Xlabel+";"+xsLabel);
        // data_up->GetXaxis()->SetTitleOffset(6.3);
        data_up->Draw("HIST");
        data_down->Draw("SAME HIST");
        data->Draw("SAME");
        // std::cout << "ALEX DATA INTEGRAL FOR PLOT " << plot << " IS " << data->Integral() << std::endl;
        // std::cout << "ALEX Sherpa INTEGRAL FOR PLOT " << plot << " IS " << Sherpa->Integral() << std::endl;
        Sherpa->Draw("SAME");
        MG->Draw("SAME");

        // Legend
        TLegend* leg = new TLegend(0.57, 0.70, 0.72, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.030);
        leg->SetFillStyle(0);

        leg->AddEntry(data,"Data stat. unc","pe");
        leg->AddEntry(data_up,"Total unc.", "f");
        Sherpa->SetFillColorAlpha(820, 0.1);
        leg->AddEntry(Sherpa,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pelf");
        leg->AddEntry(MG,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pelf");
        leg->Draw("same");

        // Need to add this stuff before changing pads
        gPad->RedrawAxis();
        gPad->RedrawAxis("G");
        drawPlotInfo(0.15,0.9, plot, "both", Totlumi, 0.03, "", -1, HighPt);
        // This is a hack to include the proper position of the axis titles.
        // drawText(0.06,0.13,"#bf{#it{Pred./Data}}",0, 1, 0.04, 90.);
        // drawText(0.85,0.015,"#bf{#it{"+Xlabel+"}}",0, 1, 0.032);


        // Now add the ratio plot: first the pad
        TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        p->SetTopMargin(1.0 - .36);
        p->SetFillStyle(0);
        p->SetTicks();
        // p->SetGridy();
        p->Draw();
        p->cd();
        TMultiGraph *mg = new TMultiGraph();

        double x[nBins];
        double y[nBins];
        double ex[nBins];
        double ey[nBins];
        double exl[nBins];
        double eyl[nBins];
        double exh[nBins];
        double eyh[nBins];
        double eyz[nBins];
        // Define Sherpa ratio plot
        for (int i=1;i<nBins+1;i++){
          x[i-1] = data->GetBinCenter(i);
          y[i-1] = Sherpa->GetBinContent(i)/data->GetBinContent(i);
          ex[i-1] = Sherpa->GetBinWidth(i)/2;
          ey[i-1] = (Sherpa->GetBinContent(i)/data->GetBinContent(i)) * std::sqrt(std::pow(Sherpa->GetBinError(i)/Sherpa->GetBinContent(i),2) + std::pow(data->GetBinError(i)/data->GetBinContent(i),2));
        }
        TGraphErrors* SherpaRatio = new TGraphErrors(nBins,x,y,ex,ey);
        if (plot.Contains("NJets")) SherpaRatio->GetXaxis()->SetRangeUser(0.5, 7.5);
        SherpaRatio->GetYaxis()->SetRangeUser(0.5, 2.0);
        SherpaRatio->GetXaxis()->SetLabelSize(0.);
        SherpaRatio->GetYaxis()->SetLabelSize(0.03);
        SherpaRatio->SetMarkerStyle(25); // Square
        SherpaRatio->SetMarkerColor(814); // Dark Green
        SherpaRatio->SetLineColor(814); // Dark Green
        mg->Add(SherpaRatio);

        // Define MG ratio plot
        for (int i=1;i<nBins+1;i++){
          x[i-1] = data->GetBinCenter(i);
          y[i-1] = MG->GetBinContent(i)/data->GetBinContent(i);
          ex[i-1] = MG->GetBinWidth(i)/2;
          ey[i-1] = (MG->GetBinContent(i)/data->GetBinContent(i)) * std::sqrt(std::pow(MG->GetBinError(i)/MG->GetBinContent(i),2) + std::pow(data->GetBinError(i)/data->GetBinContent(i),2));
        }
        TGraphErrors* MGRatio = new TGraphErrors(nBins,x,y,ex,ey);
        if (plot.Contains("NJets")) MGRatio->GetXaxis()->SetRangeUser(0.5, 7.5);
        MGRatio->GetYaxis()->SetRangeUser(0.5, 2.0);
        MGRatio->GetXaxis()->SetLabelSize(0.);
        MGRatio->GetYaxis()->SetLabelSize(0.03);
        MGRatio->SetMarkerStyle(26); // triangle
        MGRatio->SetMarkerColor(807); // Orange
        MGRatio->SetLineColor(807); // Orange
        mg->Add(MGRatio);

        // Define Sherpa Theory ratio plot
        for (int i=1;i<nBins+1;i++){
          x[i-1] = data->GetBinCenter(i);
          y[i-1] = Sherpa->GetBinContent(i)/data->GetBinContent(i);
          exl[i-1] = Sherpa->GetBinWidth(i)/2;
          exh[i-1] = Sherpa->GetBinWidth(i)/2;
          eyh[i-1] = Sherpa_theory_up->GetBinContent(i)/data->GetBinContent(i) - Sherpa->GetBinContent(i)/data->GetBinContent(i);
          eyl[i-1] = Sherpa->GetBinContent(i)/data->GetBinContent(i) - Sherpa_theory_down->GetBinContent(i)/data->GetBinContent(i);
        }
        TGraphAsymmErrors* SherpaTheoryRatio = new TGraphAsymmErrors(nBins,x,y,exl,exh,eyl,eyl);
        if (plot.Contains("NJets")) SherpaTheoryRatio->GetXaxis()->SetRangeUser(0.5, 7.5);
        SherpaTheoryRatio->GetYaxis()->SetRangeUser(0.5, 2.0);
        SherpaTheoryRatio->GetXaxis()->SetLabelSize(0.);
        SherpaTheoryRatio->GetYaxis()->SetLabelSize(0.03);
        SherpaTheoryRatio->SetMarkerSize(0); // Square
        SherpaTheoryRatio->SetLineWidth(0); // Dark Green
        SherpaTheoryRatio->SetFillColor(820); // Lighter Green
        SherpaTheoryRatio->SetFillColorAlpha(820, 0.2);
        mg->Add(SherpaTheoryRatio);

        // Define measured uncertainty ratio plot
        for (int i=1;i<nBins+1;i++){
          x[i-1] = data->GetBinCenter(i);
          y[i-1] = 1.0;
          exl[i-1] = Sherpa->GetBinWidth(i)/2;
          exh[i-1] = Sherpa->GetBinWidth(i)/2;
          eyh[i-1] = 1. - data_up->GetBinContent(i)/data->GetBinContent(i);
          eyl[i-1] = 1. - data_down->GetBinContent(i)/data->GetBinContent(i);
        }
        TGraphAsymmErrors* DataUncertRatio = new TGraphAsymmErrors(nBins,x,y,exl,exh,eyl,eyl);
        if (plot.Contains("NJets")) DataUncertRatio->GetXaxis()->SetRangeUser(0.5, 7.5);
        DataUncertRatio->GetYaxis()->SetRangeUser(0.5, 2.0);
        DataUncertRatio->GetXaxis()->SetLabelSize(0.);
        DataUncertRatio->GetYaxis()->SetLabelSize(0.03);
        DataUncertRatio->SetMarkerSize(0);
        DataUncertRatio->SetLineWidth(0);
        DataUncertRatio->SetFillColor(kBlack);
        DataUncertRatio->SetFillStyle(3005);
        mg->Add(DataUncertRatio);


        // Define Ratio == 1 ratio plot
        for (int i=1;i<nBins+1;i++){
          x[i-1] = data->GetBinCenter(i);
          y[i-1] = 1.0;
          ex[i-1] = MG->GetBinWidth(i)/2;
          ey[i-1] = 0.;
        }
        TGraphErrors* OneRatio = new TGraphErrors(nBins,x,y,ex,ey);
        if (plot.Contains("NJets")) OneRatio->GetXaxis()->SetRangeUser(0.5, 7.5);
        OneRatio->GetYaxis()->SetRangeUser(0.5, 2.0);
        OneRatio->GetXaxis()->SetLabelSize(0.);
        OneRatio->GetYaxis()->SetLabelSize(0.03);
        OneRatio->SetMarkerSize(0);
        OneRatio->SetMarkerColor(kBlack);
        OneRatio->SetLineColor(kBlack);
        OneRatio->SetLineWidth(2);
        mg->Add(OneRatio);


        mg->SetTitle(";"+Xlabel+";Pred./Data");
        mg->GetHistogram()->GetYaxis()->SetRangeUser(0.5, 2.0);
        mg->GetHistogram()->GetXaxis()->SetLabelSize(0.04);
        mg->GetHistogram()->GetYaxis()->SetLabelSize(0.03);

        // mg->Draw("a2");
        // SherpaRatio->Draw("a2");
        // SherpaRatio->Draw("p");
        // MGRatio->Draw("a2");
        // MGRatio->Draw("p");
        mg->Draw("a2");
        mg->Draw("p");
        gPad->RedrawAxis();
        gPad->RedrawAxis("G");

        // TH1F* ratio_up = (TH1F*) data_up->Clone();
        // ratio_up->GetXaxis()->SetTitle(Xlabel);
        // ratio_up->GetYaxis()->SetTitle("Pred./Data");
        // ratio_up->GetXaxis()->SetLabelSize(0.04);
        // TH1F* ratio_down = (TH1F*) data_down->Clone();
        // TH1F* ratio_Sherpa = (TH1F*) Sherpa->Clone();
        // TH1F* ratio_MG = (TH1F*) MG->Clone();
        // TH1F* ratio_Sherpa10 = (TH1F*) Sherpa10->Clone();
        // TH1F* ratio_FxFx = (TH1F*) FxFx->Clone();
        //
        // ratio_up->Divide(data);
        // ratio_down->Divide(data);
        // ratio_Sherpa->Divide(data);
        // ratio_MG->Divide(data);
        // ratio_Sherpa10->Divide(data);
        // ratio_FxFx->Divide(data);
        // TH1F* ratioOne = (TH1F*) data->Clone();
        // TH1F* ratioOne2 = (TH1F*) data->Clone();
        // ratioOne->SetFillStyle(0);
        // ratioOne2->SetFillStyle(0);
        // for (int i=1; i<ratioOne->GetNbinsX()+1;i++){
        //   ratioOne->SetBinContent(i,1.);
        //   ratioOne->SetBinError(i,0);
        //   ratioOne2->SetBinContent(i,1.);
        //   ratioOne2->SetBinError(i,0);
        //   ratio_up->SetBinError(i, 0.);
        //   ratio_down->SetBinError(i, 0.);
        // }
        // ratioOne->SetLineColor(kBlack);
        // ratioOne2->SetLineColor(kBlack);
        // ratio_down->SetFillColor(10);
        // ratio_down->SetFillStyle(1001);
        // ratio_down->SetMarkerColor(10); // white marker
        // ratio_down->SetLineColor(0); // white line
        // ratio_Sherpa->SetFillStyle(0);
        // ratio_MG->SetFillStyle(0);
        // ratio_Sherpa10->SetFillStyle(0);
        // ratio_FxFx->SetFillStyle(0);
        //
        // gStyle->SetErrorX(0.5);
        // ratio_up->GetYaxis()->SetRangeUser(0.5, 2.0);
        // ratio_up->Draw("HIST");
        // ratio_down->Draw("SAME HIST");
        // ratioOne->Draw("SAME");
        // ratio_Sherpa->Draw("SAME");
        // ratio_MG->Draw("SAME");
        // gPad->RedrawAxis();
        // gPad->RedrawAxis("G");

        // Legend positions
        TLegend* leg2 = new TLegend();
        if (plot == "HT" || plot == "Mjj" || plot == "NJets" || plot == "pTZ"){ // Top of Pad
          leg = new TLegend(0.12, 0.405, 0.25, 0.445); // This is for top of pad
          leg2 = new TLegend(0.40, 0.405, 0.53, 0.445); // This is for top of pad
        }
        else if (plot.Contains("ZJpT") || plot=="minDR" || plot == "pTjet"){  // Bottom of Pad
          leg = new TLegend(0.12, 0.285, 0.25, 0.325); // This is for Bottom of pad
          leg2 = new TLegend(0.40, 0.285, 0.53, 0.325); // This is for Bottom of pad
        }
        else fatal("Need to figure out the proper Analysis ratio positions!");
        // Sherpa Legend
        leg->SetBorderSize(0);
        leg->SetTextSize(0.025);
        leg->SetFillStyle(0);
        leg->AddEntry(Sherpa,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pelf");
        // leg->Draw("same");
        // MadGraph legend
        leg2->SetBorderSize(0);
        leg2->SetTextSize(0.025);
        leg2->SetFillStyle(0);
        leg2->AddEntry(MG,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pelf");
        // leg2->Draw("same");

        can->Print("total_xs_"+plot+".png");
      }
    }

  }

  // Compare analysis framework & rivet truths for nominal generators
  // Currently rivet produces ee+mm merged together
  if (channels.size() == 2 && compareWithRivet){

    TH1F* Sherpa =  new TH1F();
    TH1F* MadGraph = new TH1F();
    TH1F* SherpaRivet = new TH1F();
    TH1F* MGRivet = new TH1F();
    // Files for Sherpa2.2.1 & MadGraph LO
    TFile *f1 = new TFile("/eos/user/a/alaurier/ZJets/FromRivet/ManualOR/Sh221_Zll_ZJets_ManualOR_mc15_24_11.root");
    //TFile *f1 = new TFile("/eos/user/a/alaurier/ZJets/FromRivet/Corrected/Sh221_Zll.root");
    // TFile *f1 = new TFile("/eos/user/a/alaurier/ZJets/FromRivet/Sh221_Zll.root");
    TFile *f2 = new TFile("/eos/user/a/alaurier/ZJets/FromRivet/ManualOR/Sh221_Zll_ZJets_ManualOR_mc16_24_11.root");
    // TFile *f2 = new TFile("/eos/user/a/alaurier/ZJets/FromRivet/MGP8_LO_Zll.root");

    TFile *sherpa= new TFile("/eos/user/a/alaurier/ZJets/TruthJets/SherpaHigh.root");
    //TFile *sherpa= new TFile("/eos/user/a/alaurier/ZJets/FinalProduction/extraHistos.root");
    // TFile *sherpa= new TFile("/eos/user/a/alaurier/ZJets/FinalProduction/SherpaZJets.root");
    TFile *madgraph= new TFile("/eos/user/a/alaurier/ZJets/FinalProduction/MGZJets.root");

    TCanvas *can = new TCanvas("", "", 800, 800);
    std::cout << "Alexandre starting to rivet to analysis!" << std::endl;

    for (auto plot : plotList){
      Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
      // Fetch nominal cross sections
      Sherpa = getHisto(sherpa, "heetrue"+var+"0");
      TH1F* temp = getHisto(sherpa, "hmmtrue"+var+"0");
      Sherpa->Add(temp);

      if (plot == "minDR" || plot == "HighpTZ" || plot == "clJet" || plot.Contains("AllZJPT")){
        MadGraph = getHisto(sherpa, "heetrue"+var+"0");
        temp = getHisto(sherpa, "hmmtrue"+var+"0");
      }
      else{
        MadGraph = getHisto(madgraph, "heetrue2"+var+"0");
        temp = getHisto(madgraph, "hmmtrue2"+var+"0");
      }
      MadGraph->Add(temp);
      Sherpa = (TH1F*) (rebin1DHisto((TH1F*) Sherpa->Clone(), plot))->Clone();
      MadGraph = (TH1F*) (rebin1DHisto((TH1F*) MadGraph->Clone(), plot))->Clone();
      if (plot.Contains("ZJpT")){
        Sherpa->Rebin(3);
        MadGraph->Rebin(3);
      }

      TString histname;
      if (plot == "pTZ") histname = "ZpT_100GeVjet";
      else if (plot == "pTjet") histname = "j0pT";
      else if (plot == "NJets") histname = "NJets_excl";
      else if (plot == "minDR") histname = "DR";
      else if (plot == "NJets500") histname = "";
      else if (plot == "HT") histname = "HT";
      else if (plot == "Mjj") histname = "mjj";
      else if (plot == "ZJpT") histname = "ZJpT";
      else if (plot == "ZJpTlowDR") histname = "ZJpT_loDR";
      else if (plot == "ZJpTmedDR") histname = "";
      else if (plot == "ZJpThighDR") histname = "ZJpT_hiDR";
      else if (plot == "HighpTZ") histname = "ZpT_500GeVjet";
      else if (plot == "clJet") histname = "clJetpT";
      else if (plot == "AllZJPT") histname = "ZJpT";
      else if (plot == "AllZJPTlowDR") histname = "ZJpT_loDR";
      else if (plot == "AllZJPThighDR") histname = "ZJpT_hiDR";
      else fatal("Unknown plot name for merger!");
      if (histname == ""){
        std::cout << "Skipping combination of " << plot << std::endl;
        continue;
      }
      TH1F* SherpaRivet = (TH1F*) f1->Get("ZJets_rivet/"+histname);
      TH1F* MGRivet = (TH1F*) f2->Get("ZJets_rivet/"+histname);
      if (plot.Contains("ZJpT")){
        temp = (TH1F*) Sherpa->Clone();
        Sherpa = (TH1F*) SherpaRivet->Clone();
        for (int i=1; i<Sherpa->GetNbinsX()+1;i++){
          Sherpa->SetBinContent(i,temp->GetBinContent(i));
          Sherpa->SetBinError(i,temp->GetBinError(i));
        }
        temp = (TH1F*) MadGraph->Clone();
        MadGraph = (TH1F*) MGRivet->Clone();
        for (int i=1; i<MadGraph->GetNbinsX()+1;i++){
          MadGraph->SetBinContent(i,temp->GetBinContent(i));
          MadGraph->SetBinError(i,temp->GetBinError(i));
        }
      }
      // std::cout << std::sqrt(Sherpa->GetBinContent(5)) << " , " << Sherpa->GetBinError(5) << std::endl;
      if (SherpaRivet->GetNbinsX() != Sherpa->GetNbinsX()) fatal("Wrong binning for Sherpa plot: " + histname);
      if (MGRivet->GetNbinsX() != MadGraph->GetNbinsX()) fatal("Wrong binning for MadGraph plot: " + histname);

      // Need To divide by Bin width!
      for (int i=1; i<Sherpa->GetNbinsX()+1;i++){
        Sherpa->SetBinContent(i,Sherpa->GetBinContent(i)/(Totlumi*Sherpa->GetBinWidth(i)));
        Sherpa->SetBinError(i,Sherpa->GetBinError(i)/(Totlumi*Sherpa->GetBinWidth(i)));
        MadGraph->SetBinContent(i,MadGraph->GetBinContent(i)/(Totlumi*MadGraph->GetBinWidth(i)));
        MadGraph->SetBinError(i,MadGraph->GetBinError(i)/(Totlumi*MadGraph->GetBinWidth(i)));

        SherpaRivet->SetBinContent(i, SherpaRivet->GetBinContent(i)/(SherpaRivet->GetBinWidth(i)));
        SherpaRivet->SetBinError(i,SherpaRivet->GetBinError(i)/(SherpaRivet->GetBinWidth(i)));
        MGRivet->SetBinContent(i, MGRivet->GetBinContent(i)/(MGRivet->GetBinWidth(i)));
        MGRivet->SetBinError(i,MGRivet->GetBinError(i)/(MGRivet->GetBinWidth(i)));

        // std::cout << "Sherpa bin " << i << " = " << Sherpa->GetBinContent(i) << std::endl;
        // std::cout << "Rivet bin  " << i << " = " << SherpaRivet->GetBinContent(i) << std::endl;
      }
      MGRivet = (TH1F*) SherpaRivet->Clone();

      // Start plotting
      can->Clear();
      double ratio = 0.36, margin = 0.02;
      can->cd();
      // gPad->SetBottomMargin(ratio);
      gPad->SetBottomMargin(0.45);
      gPad->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      // can->SetBottomMargin(0.12);
      can->SetRightMargin(0.04);

      // Do some plotting
      Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
      Str xsLabel = settings->GetValue(plot + ".xs", " ");
      // Sherpa->SetTitle(";"+Xlabel+";"+xsLabel);
      Sherpa->GetXaxis()->SetTitle("");
      Sherpa->GetYaxis()->SetTitle(xsLabel);

      // Not log plots
      // if (plot.Contains("ZJpT")){
      if (plot.Contains("asdf")){
        gPad->SetLogy(0);
        Sherpa->SetMaximum(MadGraph->GetMaximum()*2.0);
        Sherpa->SetMinimum(0);
        // Sherpa->SetMinimum(Sherpa->GetMinimum()*.5);
      }
      else{
        gPad->SetLogy(1);
        if(Sherpa->GetMinimum() > 100)
          Sherpa->SetMinimum(Sherpa->GetMinimum()*0.05);
        Sherpa->SetMaximum(Sherpa->GetMaximum()*100);
      }
      if (plot.Contains("NJets")) Sherpa->SetMaximum(Sherpa->GetMaximum()*5.); // Scale it up by 5 to help plotting
      Sherpa->SetMarkerStyle(25); // Square
      Sherpa->SetMarkerColor(814); // Dark Green
      Sherpa->SetLineColor(814); // Dark Green
      SherpaRivet->SetMarkerStyle(3); // star that fits in square!
      SherpaRivet->SetMarkerColor(807); // Orange
      SherpaRivet->SetLineColor(807); // Orange
      MGRivet->SetMarkerStyle(24); // Circle
      MGRivet->SetMarkerColor(857); // Nice Blue
      MGRivet->SetLineColor(857); // Nice Blue
      MadGraph->SetMarkerStyle(32); // inverted triangle
      MadGraph->SetMarkerColor(kOrange+3); // brown
      MadGraph->SetLineColor(kOrange+3); // brown


      Sherpa->GetXaxis()->SetTitleOffset(1.4);
      Sherpa->GetYaxis()->SetLabelSize(0.03);
      Sherpa->GetYaxis()->SetTitleOffset(1.4);
      Sherpa->GetXaxis()->SetLabelSize(0);

      // Define legend
      // leg = new TLegend(0.65, 0.80, 0.80, 0.93);
      TLegend* leg = new TLegend(0.70, 0.70, 0.85, 0.93);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.020);
      leg->SetFillStyle(0);

      leg->AddEntry(Sherpa,"Sherpa2.2.1 Analysis","pl");
      leg->AddEntry(SherpaRivet,"mc15 campaign","pl");
      leg->AddEntry(MadGraph,"Sherpa2.2.1 Analysis","pl");
      leg->AddEntry(MGRivet,"mc16 campaign","pl");
      // leg->AddEntry(Sherpa,"Sherpa2.2.1 Analysis","pl");
      // leg->AddEntry(SherpaRivet,"Sherpa 2.2.1 Rivet","pl");
      // leg->AddEntry(MadGraph,"MGPy8 Analysis","pl");
      // leg->AddEntry(MGRivet,"MGPy8 Rivet","pl");

      Sherpa->Draw();
      SherpaRivet->Draw("SAME");
      MadGraph->Draw("SAME");
      MGRivet->Draw("SAME");
      if (plot == "minDR" || plot == "HighpTZ" || plot == "clJet")
        drawPlotInfo(0.15,0.9, "minDR", "both", Totlumi, 0.03, "", -1, HighPt);
      else if (plot=="AllZJPT")
        drawPlotInfo(0.15,0.9, "ZJpT", "both", Totlumi, 0.03, "", -1, HighPt);
      else if (plot=="AllZJPTlowDR")
        drawPlotInfo(0.15,0.9, "ZJpTlowDR", "both", Totlumi, 0.03, "", -1, HighPt);
      else if (plot=="AllZJPThighDR")
        drawPlotInfo(0.15,0.9, "ZJpThighDR", "both", Totlumi, 0.03, "", -1, HighPt);
      else
      drawPlotInfo(0.15,0.9, plot, "both", Totlumi, 0.03, "", -1, HighPt);
      leg->Draw();
      gPad->RedrawAxis();
      gPad->RedrawAxis("G");


      // Now add the ratio plot: first the pad
      TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
      p->SetLogx(0);
      p->Modified();
      p->Update();
      p->SetTopMargin(1.0 - ratio);
      p->SetTopMargin(0.55); // From the top
      p->SetBottomMargin(0.28); // From the bottom
      p->SetFillStyle(0);
      p->SetTicks();
      p->SetGridy();
      p->Draw();
      p->cd();

      // Make the ratio plot!
      TH1F* ratioOne = (TH1F*) Sherpa->Clone();
      TH1F* ratioSherpa = (TH1F*) SherpaRivet->Clone();
      TH1F* ratioMG = (TH1F*) MadGraph->Clone();
      TH1F* ratioMGRivet = (TH1F*) MGRivet->Clone();
      // Divide them
      ratioMG->Divide((TH1F*) MadGraph);
      ratioSherpa->Divide((TH1F*) Sherpa);
      ratioMGRivet->Divide((TH1F*) MadGraph);
      for (int i=1; i<Sherpa->GetNbinsX()+1;i++){
        ratioOne->SetBinContent(i,1.);
        ratioOne->SetBinError(i,0.);
      //     ratioSherpa->SetBinContent(i,Sherpa->GetBinContent(i)/SherpaRivet->GetBinContent(i));
      //   ratioSherpa->GetBinError(i,0.)
      // ratioMGRivet->Divide((TH1F*) Sherpa);
      }

      // Define the ratio plot info
      gStyle->SetErrorX(0.5);
      ratioOne->GetYaxis()->SetLabelSize(0.03);
      ratioOne->GetXaxis()->SetLabelSize(0);
      ratioOne->GetYaxis()->SetTitle("Truth Rivet/Analysis");
      ratioOne->GetXaxis()->SetTitleOffset(1.4);
      ratioOne->GetYaxis()->SetTitleOffset(1.4);
      ratioOne->GetYaxis()->SetRangeUser(0.8, 1.2);
      // ratioPlot->GetYaxis()->SetRangeUser(0.3, 1.7);
      // ratio_up->Draw("P");
      // ratio_up->Draw("SAME P");
      ratioOne->SetLineColor(kBlack);
      ratioOne->SetMarkerSize(0.001);
      ratioOne->Draw();
      ratioSherpa->Draw("SAME");

      leg = new TLegend(0.12, 0.405, 0.25, 0.445);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.020);
      leg->SetFillStyle(0);

      leg->AddEntry(SherpaRivet,"mc15 campaign","pl");
      // leg->AddEntry(SherpaRivet,"Sherpa2.2.1 truth ratio","pl");
      leg->Draw("SAME");
      gPad->RedrawAxis();
      gPad->RedrawAxis("G");

      p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
      p->SetLogx(0);
      p->Modified();
      p->Update();
      p->SetTopMargin(0.73); // From the top
      p->SetBottomMargin(0.10); // From the bottom
      p->SetFillStyle(0);
      p->SetTicks();
      p->SetGridy();
      p->Draw();
      p->cd();
      TH1F* ratioone = (TH1F*) ratioOne->Clone();
      ratioone->SetLineColor(kBlack);
      ratioone->SetMarkerSize(0.001);
      ratioone->GetYaxis()->SetLabelSize(0.03);
      ratioone->GetXaxis()->SetTitle(Xlabel);
      ratioone->GetYaxis()->SetTitle("");
      ratioone->GetXaxis()->SetLabelSize(0.04);
      ratioone->Draw();
      ratioMGRivet->Draw("SAME");

      leg = new TLegend(0.12, 0.22, 0.25, 0.26);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.020);
      leg->SetFillStyle(0);

      leg->AddEntry(MGRivet,"mc16 campaign","pl");
      // leg->AddEntry(MGRivet,"MGPy8 LO truth ratio","pl");
      leg->Draw("SAME");
      gPad->RedrawAxis();
      gPad->RedrawAxis("G");


      can->Print("RivetComparison"+plot+".png");


    } // end of plot
  } // end of compareWithRivet

  if (channels.size() == 2 && UnfoldingFit){
    TCanvas *can = new TCanvas("", "", 800, 800);
    std::cout << "Alexandre starting to create truth based fit!" << std::endl;
    for (auto plot : plotList){
      if (plot!="pTZ" && plot!="pTjet" && plot!="minDR") continue;
      // Fetch nominal cross sections
      TH1F* nominalElec = (TH1F*) elecChannel[plot]->at(0)->Clone();
      TH1F* nominalMuon = (TH1F*) muonChannel[plot]->at(0)->Clone();
      nominalElec->Add(nominalMuon);
      // nominalElec->Add(nominalMuon); // Full unfolded data;
      TH1F* truthElec = (TH1F*) elecTruth[plot]->at(0)->Clone();
      TH1F* truthMuon = (TH1F*) muonTruth[plot]->at(0)->Clone();
      truthElec->Add(truthMuon);
      // truthElec->Add(truthMuon); // Full Sherpa Truth;
      // Get the unfolded data / truth ratio
      nominalElec->Divide(truthElec);
      // nominalMuon->Divide(truthMuon);
      std::cout << "First bin value for electron = " << nominalElec->GetBinContent(1) << std::endl;
      // std::cout << "First bin value for muon = " << nominalMuon->GetBinContent(1) << std::endl;
      // for (int i=1; i<nominalElec->GetNbinsX()+1; i++)
      //   nominalElec->SetBinContent(i,1.);
      // Plotting things
      Str xsLabel = "Data/Pred.";
      Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
      nominalElec->SetTitle(";"+Xlabel+";"+xsLabel);
      // nominalMuon->SetTitle(";"+Xlabel+";"+xsLabel);
      double min=0., max=0.;
      if (plot == "pTZ"){
        min = 20.;
        max = 1400.;
        nominalElec->Fit("pol4","iR","",min,max);
        nominalElec->Draw();
        can->Print("forUnfolding"+plot+".png");
      }
      if (plot == "pTjet"){
        min = 60.;
        max = 1220.;
        nominalElec->Fit("pol5","iR","",min,max);
        nominalElec->Draw();
        can->Print("forUnfolding"+plot+".png");
      }
      if (plot == "minDR"){
        min = 0.;
        max = 4.;
        nominalElec->SetBinError(16,0); // ignore the minDR==pi bin
        nominalElec->Fit("pol5","iR","",min,max);
        nominalElec->Draw();
        can->Print("forUnfolding"+plot+".png");
      }


    }
  }
  theory->Close();

  return;
}

void printCrossSections(TEnv* settings){

  Str filePath = settings->GetValue("filePath", " ");

  TFile *DATA = new TFile(filePath+"data.root");
  TFile *SHERPA = new TFile(filePath+"SherpaZJets.root");
  TFile *TOPQUARK = new TFile(filePath+"TopQuark.root");
  TFile *DIBOSON = new TFile(filePath+"Diboson.root");
  TFile *WJETS = new TFile(filePath+"WJets.root");
  TFile *ZTT = new TFile(filePath+"Ztt.root");
  TFile *THEORY = new TFile(filePath+"UnfoldedTheory.root");
  TFile *UNFOLDING = new TFile(filePath+"UnfoldingUncertainty.root");


  int iterations = settings->GetValue("NumberOfIterations",2); // Total number of systematics
  double lumiUncert = settings->GetValue("LumiUncert.",0.017);
  double Totlumi = settings->GetValue("luminosity", 0.0);
  StrV plotList = vectorize(settings->GetValue("xsPlots", " "));
  StrV channels = vectorize(settings->GetValue("channels", " "));
  int nSys = settings->GetValue("Systematics",1);

  std::vector<double> electron_xs;
  std::vector<double> muon_xs;
  std::vector<double> combined_xs;
  std::vector<double> electron_theory;
  std::vector<double> muon_theory;
  std::vector<double> electron_theory_up;
  std::vector<double> muon_theory_up;
  std::vector<double> electron_theory_down;
  std::vector<double> muon_theory_down;

  std::vector<double> electron_scale_up;
  std::vector<double> electron_scale_down;
  std::vector<double> muon_scale_up;
  std::vector<double> muon_scale_down;



  std::vector<double> electron_stat;
  std::vector<double> muon_stat;
  std::vector<double> combined_stat;

  std::vector<double> electron_up;
  std::vector<double> muon_up;
  std::vector<double> combined_up;

  std::vector<double> electron_down;
  std::vector<double> muon_down;
  std::vector<double> combined_down;

  HistV unfoldedHistos;
  HistV TruthHistos;

  TH1F* temp = new TH1F();

  // Start with separate channels
  for (auto plot : plotList){
    Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
    for (auto chan : channels){
      // Reset unfolded histograms
      unfoldedHistos.clear();
      for (int sys=0; sys<nSys; sys++){

        // Get Response and rebin
        TH2F* RMatrix = getHisto2(SHERPA, chan+"resp"+var+sys);
        TH2F* RebinnedMatrix = (TH2F*) (rebin2DHisto((TH2F*) RMatrix->Clone(), plot))->Clone();
        // Get measured sherpa and rebin
        temp = getHisto(SHERPA,chan+"meas"+var+sys);
        TH1F* SherpaMeas = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
        // Get truth sherpa and rebin
        temp = getHisto(SHERPA,chan+"true"+var+"0");
        TH1F* SherpaTrue = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();

        // Define response matrix!
        RooUnfoldResponse Response (SherpaMeas,SherpaTrue,RebinnedMatrix);

        // get the data to unfold and rebin
        TH1F* data = getHisto(DATA,chan+"data"+var+sys);
        temp = getHisto(DIBOSON,chan+"meas"+var+sys);
        data->Add(temp,-1.);
        temp = getHisto(TOPQUARK,chan+"meas"+var+sys);
        data->Add(temp,-1.);
        temp = getHisto(WJETS,chan+"meas"+var+sys);
        data->Add(temp,-1.);
        temp = getHisto(ZTT,chan+"meas"+var+sys);
        data->Add(temp,-1.);
        TH1F* NewData = (TH1F*) (rebin1DHisto((TH1F*) data->Clone(), plot))->Clone();

        // Unfold the data!
        RooUnfoldBayes unfold_data (&Response, NewData, iterations);
        auto* unfolded = unfold_data.Hreco();

        TH1F* cross_section = (TH1F*) unfolded->Clone();
        // Scale by Lumi
        cross_section->Scale(1/Totlumi,"width");
        for (int i=1; i<cross_section->GetNbinsX()+1;i++)
          if (cross_section->GetBinContent(i) < 0) cross_section->SetBinContent(i,0.);

        // Save unfolded histogram to vector;
        unfoldedHistos.push_back(cross_section);

        // Define error for integrals;
        if (sys == 0){
          Double_t err = 0.;
          if (chan == "hee"){
            electron_xs.push_back(cross_section->IntegralAndError(1,cross_section->GetNbinsX(),err,"width"));
            electron_stat.push_back(err);
          } //hee
          if (chan == "hmm"){
            muon_xs.push_back(cross_section->IntegralAndError(1,cross_section->GetNbinsX(),err,"width"));
            muon_stat.push_back(err);
          } // hmm
          if (plot=="minDR"){
            if (chan == "hee"){
              electron_xs.push_back(cross_section->IntegralAndError(1,7,err,"width"));
              electron_stat.push_back(err);
              electron_xs.push_back(cross_section->IntegralAndError(11,20,err,"width"));
              electron_stat.push_back(err);
            } // hee
            if (chan == "hmm"){
              muon_xs.push_back(cross_section->IntegralAndError(1,7,err,"width"));
              muon_stat.push_back(err);
              muon_xs.push_back(cross_section->IntegralAndError(11,20,err,"width"));
              muon_stat.push_back(err);
            } // hmm
          } // minDR
        } // Nominal sys == 0

      } // Systematics

      // Calculate syst cross sections.
      if (unfoldedHistos.size() != nSys) fatal("Final unfolded histogram vector not correct size!");
      TH1F* nominal = (TH1F*) unfoldedHistos[0]->Clone();
      TH1F* variation1 = new TH1F();
      TH1F* variation2 = new TH1F();
      int nBins = nominal->GetNbinsX();

      std::vector<double> var_up, var_down;
      for (int i=0; i<nBins; i++){
        var_up.push_back(0.);
        var_down.push_back(0.);
      }
      if (var_up.size() !=nBins) fatal("Wrong vector size for var_up!");
      if (var_down.size() !=nBins) fatal("Wrong vector size for var_down!");
      double nom=0.,var1=0., var2=0., Var=0.;
      // Start the error loops!
      for (int sys=1; sys<(nSys+1)/2; sys++){ // This assumes an odd number of total systematics! (nominal + even)
        variation1 = (TH1F*) unfoldedHistos[2*sys-1]->Clone();
        variation2 = (TH1F*) unfoldedHistos[2*sys]->Clone();
        for (int i=1; i<nBins+1; i++){
          nom = nominal->GetBinContent(i);
          var1 = variation1->GetBinContent(i) - nom;
          var2 = variation2->GetBinContent(i) - nom;
          if ( (var1 > 0 ) && (var2 > 0) ){ // If both __1up and __1down are positive, get the max.
            Var = std::max(var1*var1, var2*var2);
            var_up[i-1] = var_up[i-1] + Var;
          }
          else if ( (var1 < 0 ) && (var2 < 0) ){ // If both __1up and __1down are negative, get the max.
            Var = std::max(var1*var1, var2*var2);
            var_down[i-1] = var_down[i-1] + Var;
          }
          else{
            if (var1>=0) var_up[i-1] = var_up[i-1] + var1*var1;
            else var_down[i-1] = var_down[i-1] + var1*var1;
            if (var2>=0) var_up[i-1] = var_up[i-1] + var2*var2;
            else var_down[i-1] = var_down[i-1] + var2*var2;
          }
        }
      }

      // Evaluate up and down variations of theory.
      // Evaluate the Theory uncertainties! They come in rebinned unfolded cross sections
      for (int sys=0; sys<11; sys++){ // 11 theory uncertainty pairs. 135-156
        int system = 135 + 2*sys;
        variation1 = getHisto(THEORY, chan+var+system+"_"+std::to_string(iterations));
        system = system + 1;
        variation2 = getHisto(THEORY, chan+var+system+"_"+std::to_string(iterations));
        for (int i=1; i<nBins+1; i++){
          var1 = variation1->GetBinContent(i)-nominal->GetBinContent(i);
          var2 = variation2->GetBinContent(i)-nominal->GetBinContent(i);
          if ( (var1 > 0 ) && (var2 > 0) ){ // If both __1up and __1down are positive, get the max.
            Var = std::max(var1*var1, var2*var2);
            var_up[i-1] = var_up[i-1] + Var;
          }
          else if ( (var1 < 0 ) && (var2 < 0) ){ // If both __1up and __1down are negative, get the max.
            Var = std::max(var1*var1, var2*var2);
            var_down[i-1] = var_down[i-1] + Var;
          }
          else{
            if (var1>=0) var_up[i-1] = var_up[i-1] + var1*var1;
            else var_down[i-1] = var_down[i-1] + var1*var1;
            if (var2>=0) var_up[i-1] = var_up[i-1] + var2*var2;
            else var_down[i-1] = var_down[i-1] + var2*var2;
          }
        }
      }
      // Evaluate the up and down variations of unfolding Uncertainty.
      TString unfoldingSource = settings->GetValue("UnfoldingUncertainty", " ");
      TH1F* fromUnfolding = getHisto(UNFOLDING, chan+"unf"+var+"_"+iterations+unfoldingSource);
      for (int i=1; i<nBins+1; i++){
        Var = fromUnfolding->GetBinContent(i) - nominal->GetBinContent(i);
        if (Var > 0) var_up[i-1] = var_up[i-1] + Var*Var;
        else if (var < 0) var_down[i-1] = var_down[i-1] + Var*Var;
      }

      // Add the final uncertainties
      double lumi=0.;
      TH1F* syst_up = (TH1F*) nominal->Clone();
      TH1F* syst_down = (TH1F*) nominal->Clone();
      for (int i=1; i<nBins+1; i++){
        // ADDING LUMINOSITY UNCERTAINTY
        lumi = std::pow(nominal->GetBinContent(i)*lumiUncert,2);
        syst_up->SetBinContent(i, nominal->GetBinContent(i) + std::sqrt(var_up[i-1] + lumi) );
        syst_down->SetBinContent(i, nominal->GetBinContent(i) - std::sqrt(var_down[i-1] + lumi) );
        syst_up->SetBinError(i, 0.);
        syst_down->SetBinError(i, 0.);
        if (plot =="NJets500" && chan == "hmm")
          for (int i=0; i<nBins;i++){
            syst_up->SetBinContent(nBins,0.);
            syst_down->SetBinContent(nBins,0.);
          }
      }

      // Save syst uncertainty
      if (chan == "hee"){
        electron_up.push_back(syst_up->Integral("width")-nominal->Integral("width"));
        electron_down.push_back(nominal->Integral("width")-syst_down->Integral("width"));
        if (plot=="minDR"){
          electron_up.push_back(syst_up->Integral(1,7,"width")-nominal->Integral(1,7,"width"));
          electron_up.push_back(syst_up->Integral(11,20,"width")-nominal->Integral(11,20,"width"));
          electron_down.push_back(nominal->Integral(1,7,"width")-syst_down->Integral(1,7,"width"));
          electron_down.push_back(nominal->Integral(11,20,"width")-syst_down->Integral(11,20,"width"));
        } //minDR
      } //hee
      if (chan == "hmm"){
        muon_up.push_back(syst_up->Integral("width")-nominal->Integral("width"));
        muon_down.push_back(nominal->Integral("width")-syst_down->Integral("width"));
        if (plot=="minDR"){
          muon_up.push_back(syst_up->Integral(1,7,"width")-nominal->Integral(1,7,"width"));
          muon_up.push_back(syst_up->Integral(11,20,"width")-nominal->Integral(11,20,"width"));
          muon_down.push_back(nominal->Integral(1,7,"width")-syst_down->Integral(1,7,"width"));
          muon_down.push_back(nominal->Integral(11,20,"width")-syst_down->Integral(11,20,"width"));

        } // minDR
      } // hmm

    } // Channel
  } // Plot

  // Truth theory predictions
  TFile *TRUTH = new TFile(filePath+"extra/SherpaZJets.root");
  for (auto plot : plotList){
    Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
    temp = getHisto(SHERPA,"hmmtrue"+var+"0");
    TH1F* nominalMuon = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
    nominalMuon->Scale(1/Totlumi,"width");
    temp = getHisto(SHERPA,"heetrue"+var+"0");
    TH1F* nominalElec = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
    nominalElec->Scale(1/Totlumi,"width");

    for (int i=1; i<nominalMuon->GetNbinsX()+1;i++){
      if (nominalElec->GetBinContent(i)<0.) nominalElec->SetBinContent(i,0.);
      if (nominalMuon->GetBinContent(i)<0.) nominalMuon->SetBinContent(i,0.);
    }

    // Reset unfolded histograms
    TruthHistos.clear();

    // This is the nominal from the truth file.
    // We use this as the reference agains the nominal truth of the original xs
    TH1F* newElec = getHisto(TRUTH,"heetrue"+var+"0");

    // 100 PDF variations
    int nBins = newElec->GetNbinsX();
    for (int i=1; i<nBins+1;i++)
      if (newElec->GetBinContent(i)<0.) newElec->SetBinContent(i,0.);
    std::vector<double> max, min;
    double val = 0.;
    for (int i=0; i<nBins; i++){
      max.push_back(0.);
      min.push_back(0.);
    }
    for (int sys=1; sys<101; sys++){ // 101 PDF variations
      temp = getHisto(TRUTH,"heetrue"+var+sys);
      for (int i=1; i<nBins+1; i++){
        if (temp->GetBinContent(i) < 0.) continue;
        val = temp->GetBinContent(i) - newElec->GetBinContent(i);
        if (val > 0.) max[i-1] = max[i-1] + val*val;
        else if (val < 0.) min[i-1] = min[i-1] + val*val;
      }
    }

    // 100 variations up
    temp = (TH1F*) newElec->Clone();
    for (int i=1; i<nBins+1;i++){
      temp->SetBinContent(i, newElec->GetBinContent(i)+std::sqrt(max[i-1])/99);
      if (temp->GetBinContent(i)<0.) temp->SetBinContent(i, 0.);
    }
    TruthHistos.push_back(temp);
    // 100 variations down
    for (int i=1; i<nBins+1;i++){
      temp->SetBinContent(i, newElec->GetBinContent(i)-std::sqrt(min[i-1])/99);
      if (temp->GetBinContent(i)<0.) temp->SetBinContent(i, 0.);
    }
    TruthHistos.push_back(temp);

    // Comparing against 2 different PDFS
    // PDF Set #1
    temp = getHisto(TRUTH,"heetrue"+var+"101");
    for (int i=1; i<nBins+1;i++)
      if (temp->GetBinContent(i)<0.) temp->SetBinContent(i, 0.);
    TruthHistos.push_back(temp);
    // PDF Set #2
    temp = getHisto(TRUTH,"heetrue"+var+"102");
    for (int i=1; i<nBins+1;i++)
      if (temp->GetBinContent(i)<0.) temp->SetBinContent(i, 0.);
    TruthHistos.push_back(temp);

    // PDF alpha_S
    TH1F* alphaS_high = getHisto(TRUTH,"heetrue"+var+"103");
    TH1F* alphaS_low = getHisto(TRUTH,"heetrue"+var+"104");
    alphaS_high->Add(alphaS_low,-1.);

    // PDF alpha_S up
    TH1F* copy = (TH1F*) newElec->Clone();
    for (int i=1; i<nBins+1; i++){
      copy->SetBinContent(i,newElec->GetBinContent(i)+0.5*alphaS_high->GetBinContent(i));
      if (copy->GetBinContent(i) < 0.) copy->SetBinContent(i,0.);
    }
    TruthHistos.push_back(copy);
    // PDF alpha_S down
    for (int i=1; i<nBins+1; i++){
      copy->SetBinContent(i,newElec->GetBinContent(i)-0.5*alphaS_high->GetBinContent(i));
      if (copy->GetBinContent(i) < 0.) copy->SetBinContent(i,0.);
    }
    TruthHistos.push_back(copy);

    // Scale
    // scale up get max values for envelope method
    TH1F* scale_up = getHisto(TRUTH,"heetrue"+var+"0");
    TH1F* scale_down = getHisto(TRUTH,"heetrue"+var+"0");
    for (int sys=105; sys<111; sys++){ // 6 scale variations
      temp = getHisto(TRUTH,"heetrue"+var+sys);
      for (int i=1; i<nBins+1; i++){
        if (temp->GetBinContent(i) > scale_up->GetBinContent(i)) scale_up->SetBinContent(i, temp->GetBinContent(i));
        if (temp->GetBinContent(i) < scale_down->GetBinContent(i)) scale_down->SetBinContent(i, temp->GetBinContent(i));
      }
    }
    TruthHistos.push_back(scale_up);
    TruthHistos.push_back(scale_down);
    if (TruthHistos.size() != 8) fatal("Incorrect number of TruthHistos!");
    TH1F* newElecRebinned = (TH1F*) (rebin1DHisto((TH1F*) newElec->Clone(), plot))->Clone();

    temp = (TH1F*) (rebin1DHisto((TH1F*) scale_up->Clone(), plot))->Clone();
    copy = (TH1F*) temp->Clone();
    for (int i=1; i<copy->GetNbinsX()+1; i++)
      if (newElecRebinned->GetBinContent(i) > 0)
        copy->SetBinContent(i, nominalElec->GetBinContent(i) * temp->GetBinContent(i) / newElecRebinned->GetBinContent(i));
      else copy->SetBinContent(i,0.);

    // Save scale theory values
    electron_scale_up.push_back(copy->Integral("width")-nominalElec->Integral("width"));
    if (plot=="minDR"){
      electron_scale_up.push_back(copy->Integral(1,7,"width")-nominalElec->Integral(1,7,"width"));
      electron_scale_up.push_back(copy->Integral(11,20,"width")-nominalElec->Integral(11,20,"width"));
    }
    copy = (TH1F*) temp->Clone();
    for (int i=1; i<temp->GetNbinsX()+1; i++)
      if (newElecRebinned->GetBinContent(i) > 0)
        copy->SetBinContent(i, nominalMuon->GetBinContent(i) * temp->GetBinContent(i) / newElecRebinned->GetBinContent(i));
      else copy->SetBinContent(i,0.);
    // Save scale theory values
    muon_scale_up.push_back(copy->Integral("width")-nominalMuon->Integral("width"));
    if (plot=="minDR"){
      muon_scale_up.push_back(copy->Integral(1,7,"width")-nominalMuon->Integral(1,7,"width"));
      muon_scale_up.push_back(copy->Integral(11,20,"width")-nominalMuon->Integral(11,20,"width"));
    }

    // Scale down
    temp = (TH1F*) (rebin1DHisto((TH1F*) scale_down->Clone(), plot))->Clone();
    copy = (TH1F*) temp->Clone();
    for (int i=1; i<temp->GetNbinsX()+1; i++)
      if (newElecRebinned->GetBinContent(i) > 0)
        copy->SetBinContent(i, nominalElec->GetBinContent(i) * temp->GetBinContent(i) / newElecRebinned->GetBinContent(i));
      else copy->SetBinContent(i,0.);

    // Save scale theory values
    electron_scale_down.push_back(nominalElec->Integral("width")-copy->Integral("width"));
    if (plot=="minDR"){
      electron_scale_down.push_back(nominalElec->Integral(1,7,"width")-copy->Integral(1,7,"width"));
      electron_scale_down.push_back(nominalElec->Integral(11,20,"width")-copy->Integral(11,20,"width"));
    }
    copy = (TH1F*) temp->Clone();
    for (int i=1; i<temp->GetNbinsX()+1; i++)
      if (newElecRebinned->GetBinContent(i) > 0)
        copy->SetBinContent(i, nominalMuon->GetBinContent(i) * temp->GetBinContent(i) / newElecRebinned->GetBinContent(i));
      else copy->SetBinContent(i,0.);
    // Save scale theory values
    muon_scale_down.push_back(nominalMuon->Integral("width")-copy->Integral("width"));
    if (plot=="minDR"){
      muon_scale_down.push_back(nominalMuon->Integral(1,7,"width")-copy->Integral(1,7,"width"));
      muon_scale_down.push_back(nominalMuon->Integral(11,20,"width")-copy->Integral(11,20,"width"));
    }



    // Evaluate total up & down contributions
    for (int i=0; i<nBins; i++){
      max[i] = 0.;
      min[i] = 0.;
    }
    TH1F* variation1;
    TH1F* variation2;
    double val1=0., val2=0.;
    for (int sys=0; sys<3;sys++){
      variation1 = (TH1F*) TruthHistos[2*sys]->Clone();
      variation2 = (TH1F*) TruthHistos[2*sys+1]->Clone();
      for (int i=1; i<nBins+1; i++){
        val1 = variation1->GetBinContent(i)-newElec->GetBinContent(i);
        val2 = variation2->GetBinContent(i)-newElec->GetBinContent(i);
        if (val1 > 0 && val2 > 0){ // both larger than 0, find max
          val = std::max(val1*val1, val2*val2);
          max[i-1] = max[i-1] + val;
        }
        else if (val1 < 0 && val2 < 0){ // both smaller than 0, find max
          val = std::max(val1*val1, val2*val2);
          min[i-1] = min[i-1] + val;
        }
        else{
          if (val1 > 0)
            max[i-1] = max[i-1] + val1*val1;
          if (val1 < 0)
            min[i-1] = min[i-1] + val1*val1;
          if (val2 > 0)
            max[i-1] = max[i-1] + val2*val2;
          if (val2 < 0)
            min[i-1] = min[i-1] + val2*val2;
        }
      }
    }

    // Save nominal theory values
    electron_theory.push_back(nominalElec->Integral("width"));
    muon_theory.push_back(nominalMuon->Integral("width"));
    if (plot=="minDR"){
      electron_theory.push_back(nominalElec->Integral(1,7,"width"));
      electron_theory.push_back(nominalElec->Integral(11,20,"width"));
      muon_theory.push_back(nominalMuon->Integral(1,7,"width"));
      muon_theory.push_back(nominalMuon->Integral(11,20,"width"));
    }

    // Find electron variation
    int nBinsNom = nominalElec->GetNbinsX();
    newElecRebinned = (TH1F*) (rebin1DHisto((TH1F*) newElec->Clone(), plot))->Clone();
    // Up variation for electrons;
    copy = (TH1F*) newElec->Clone();
    for (int i=1; i<nBins+1; i++)
      copy->SetBinContent(i, newElec->GetBinContent(i) + std::sqrt(max[i-1]));
    // Rebin the new histogram
    TH1F* newCopy = (TH1F*) (rebin1DHisto((TH1F*) copy->Clone(), plot))->Clone();

    temp = (TH1F*) nominalElec->Clone();
    for (int i=1; i<nBinsNom+1; i++){
      if (newElecRebinned->GetBinContent(i) > 0)
        temp->SetBinContent(i, nominalElec->GetBinContent(i)*newCopy->GetBinContent(i)/newElecRebinned->GetBinContent(i));
      else temp->SetBinContent(i, 0.);
    }

    // Save up theory variation
    electron_theory_up.push_back(temp->Integral("width")-nominalElec->Integral("width"));
    if (plot == "minDR"){
      electron_theory_up.push_back(temp->Integral(1,7,"width")-nominalElec->Integral(1,7,"width"));
      electron_theory_up.push_back(temp->Integral(11,20,"width")-nominalElec->Integral(11,20,"width"));
    }
    // Save up muon
    for (int i=1; i<nBinsNom+1; i++){
      if (newElecRebinned->GetBinContent(i) > 0)
        temp->SetBinContent(i, nominalMuon->GetBinContent(i)*newCopy->GetBinContent(i)/newElecRebinned->GetBinContent(i));
      else temp->SetBinContent(i, 0.);
    }
    muon_theory_up.push_back(temp->Integral("width")-nominalMuon->Integral("width"));
    if (plot == "minDR"){
      muon_theory_up.push_back(temp->Integral(1,7,"width")-nominalMuon->Integral(1,7,"width"));
      muon_theory_up.push_back(temp->Integral(11,20,"width")-nominalMuon->Integral(11,20,"width"));
    }

    // Down variation
    // Down variation for electrons;
    copy = (TH1F*) newElec->Clone();
    for (int i=1; i<nBins+1; i++)
      copy->SetBinContent(i, newElec->GetBinContent(i) - std::sqrt(min[i-1]));

    // Rebin the new histogram
    newCopy = (TH1F*) (rebin1DHisto((TH1F*) copy->Clone(), plot))->Clone();

    temp = (TH1F*) nominalElec->Clone();
    for (int i=1; i<nBinsNom+1; i++){
      if (newElecRebinned->GetBinContent(i) > 0)
        temp->SetBinContent(i, nominalElec->GetBinContent(i)*newCopy->GetBinContent(i)/newElecRebinned->GetBinContent(i));
      else temp->SetBinContent(i, 0.);
    }

    // Save up theory variation
    electron_theory_down.push_back(nominalElec->Integral("width")-temp->Integral("width"));
    if (plot == "minDR"){
      electron_theory_down.push_back(nominalElec->Integral(1,7,"width")-temp->Integral(1,7,"width"));
      electron_theory_down.push_back(nominalElec->Integral(11,20,"width")-temp->Integral(11,20,"width"));
    }
    // Save up muon
    for (int i=1; i<nBinsNom+1; i++){
      if (newElecRebinned->GetBinContent(i) > 0)
        temp->SetBinContent(i, nominalMuon->GetBinContent(i)*newCopy->GetBinContent(i)/newElecRebinned->GetBinContent(i));
      else temp->SetBinContent(i, 0.);
    }
    muon_theory_down.push_back(nominalMuon->Integral("width")-temp->Integral("width"));
    if (plot == "minDR"){
      muon_theory_down.push_back(nominalMuon->Integral(1,7,"width")-temp->Integral(1,7,"width"));
      muon_theory_down.push_back(nominalMuon->Integral(11,20,"width")-temp->Integral(11,20,"width"));
    }


  } // Theory Plot
  std::cout << electron_theory.size() << std::endl;
  if (electron_theory.size() != 7) fatal("WRONG NUMBER OF VALUES IN electron_theory!");
  if (electron_theory_up.size() != 7) fatal("WRONG NUMBER OF VALUES IN electron_theory_up!");
  if (electron_theory_down.size() != 7) fatal("WRONG NUMBER OF VALUES IN electron_theory_down!");
  if (muon_theory.size() != 7) fatal("WRONG NUMBER OF VALUES IN electron_theory!");
  if (muon_theory_up.size() != 7) fatal("WRONG NUMBER OF VALUES IN electron_theory_up!");
  if (muon_theory_down.size() != 7) fatal("WRONG NUMBER OF VALUES IN electron_theory_down!");


  std::cout << "=====================   START PRINTOUT   =====================" << std::endl;
  std::cout << "---------------   Total Fiducial cross section ---------------" << std::endl;
  std::cout << "In electron channel = " << electron_xs[0] << " +- " << electron_stat[0] << " (stat) "
            << " + " << electron_up[0] << " - " << electron_down[0] << " (syst) pb."<< std::endl;
  std::cout << "In muon     channel = " << muon_xs[0] << " +- " << muon_stat[0] << "(stat) "
            << " + " << muon_up[0] << " - " << muon_down[0] << " (syst) pb."<< std::endl;
  std::cout << "Theory elec channel = " << electron_theory[0] << " + " << electron_theory_up[0]
            << " - " << electron_theory_down[0] << " (syst) pb." << std::endl;
  std::cout << "Scale elec channel + " << electron_scale_up[0] << " - " << electron_scale_down[0] << std::endl;
  std::cout << "Theory muon channel = " << muon_theory[0] << " + " << muon_theory_up[0]
            << " - " << muon_theory_down[0] << " (syst) pb." << std::endl;
  std::cout << "Scale muon channel + " << muon_scale_up[0] << " - " << muon_scale_down[0] << std::endl;

  std::cout << "--------------   High pT Fiducial cross section --------------" << std::endl;
  std::cout << "In electron channel = " << 1000*electron_xs[1] << " +- " << 1000*electron_stat[1] << " (stat) "
            << " + " << 1000*electron_up[1] << " - " << 1000*electron_down[1] << " (syst) fb."<< std::endl;
  std::cout << "In muon     channel = " << 1000*muon_xs[1] << " +- " << 1000*muon_stat[1] << "(stat) "
            << " + " << 1000*muon_up[1] << " - " << 1000*muon_down[1] << " (syst) fb."<< std::endl;
  std::cout << "Theory elec channel = " << 1000*electron_theory[1] << " + " << 1000*electron_theory_up[1]
            << " - " << 1000*electron_theory_down[1] << " (syst) fb." << std::endl;
  std::cout << "Scale elec channel + " << 1000*electron_scale_up[1] << " - " << 1000*electron_scale_down[1] << std::endl;

  std::cout << "Theory muon channel = " << 1000*muon_theory[1] << " + " << 1000*muon_theory_up[1]
            << " - " << 1000*muon_theory_down[1] << " (syst) fb." << std::endl;
  std::cout << "Scale muon channel + " << 1000*muon_scale_up[1] << " - " << 1000*muon_scale_down[1] << std::endl;


  std::cout << "-------------   Collinear Fiducial cross section -------------" << std::endl;
  std::cout << "In electron channel = " << 1000*electron_xs[3] << " +- " << 1000*electron_stat[3] << " (stat) "
            << " + " << 1000*electron_up[3] << " - " << 1000*electron_down[3] << " (syst) fb."<< std::endl;
  std::cout << "Scale elec channel + " << 1000*electron_scale_up[3] << " - " << 1000*electron_scale_down[3] << std::endl;
  std::cout << "In muon     channel = " << 1000*muon_xs[3] << " +- " << 1000*muon_stat[3] << "(stat) "
            << " + " << 1000*muon_up[3] << " - " << 1000*muon_down[3] << " (syst) fb."<< std::endl;
  std::cout << "Theory elec channel = " << 1000*electron_theory[3] << " + " << 1000*electron_theory_up[3]
            << " - " << 1000*electron_theory_down[3] << " (syst) fb." << std::endl;
  std::cout << "Scale elec channel + " << 1000*electron_scale_up[3] << " - " << 1000*electron_scale_down[3] << std::endl;
  std::cout << "Theory muon channel = " << 1000*muon_theory[3] << " + " << 1000*muon_theory_up[3]
            << " - " << 1000*muon_theory_down[3] << " (syst) fb." << std::endl;
  std::cout << "Scale muon channel + " << 1000*muon_scale_up[3] << " - " << 1000*muon_scale_down[3] << std::endl;

  std::cout << "------------   Back-to-Back Fiducial cross section -----------" << std::endl;
  std::cout << "In electron channel = " << 1000*electron_xs[4] << " +- " << 1000*electron_stat[4] << " (stat) "
            << " + " << 1000*electron_up[4] << " - " << 1000*electron_down[4] << " (syst) fb."<< std::endl;
  std::cout << "In muon     channel = " << 1000*muon_xs[4] << " +- " << 1000*muon_stat[4] << "(stat) "
            << " + " << 1000*muon_up[4] << " - " << 1000*muon_down[4] << " (syst) fb."<< std::endl;
  std::cout << "Theory elec channel = " << 1000*electron_theory[4] << " + " << 1000*electron_theory_up[4]
            << " - " << 1000*electron_theory_down[4] << " (syst) fb." << std::endl;
  std::cout << "Theory muon channel = " << 1000*muon_theory[4] << " + " << 1000*muon_theory_up[4]
            << " - " << 1000*muon_theory_down[4] << " (syst) fb." << std::endl;
  std::cout << "Scale muon channel + " << 1000*muon_scale_up[4] << " - " << 1000*muon_scale_down[4] << std::endl;

  //
  //
  //
  // std::cout << "~~~~~~~~~~~~ VALIDATION IN HIGHPT ~~~~~~~~~~~~" << std::endl;
  // std::cout << "In electron channel = " << electron_xs[2] << " +- " << electron_stat[2] << " (stat) "
  //           << " + " << electron_up[2] << " - " << electron_down[2] << " (syst)"<< std::endl;
  // std::cout << "In muon     channel = " << muon_xs[2] << " +- " << muon_stat[2] << "(stat) "
  //           << " + " << muon_up[2] << " - " << muon_down[2] << " (syst)"<< std::endl;
  //
  // std::cout << "~~~~~~~~~~~~ VALIDATION IN COLLINEAR ~~~~~~~~~~~~" << std::endl;
  // std::cout << "In electron channel = " << electron_xs[5] << " +- " << electron_stat[5] << "(stat) " << std::endl;
  // std::cout << "In muon     channel = " << muon_xs[5] << " +- " << muon_stat[5] << "(stat) " << std::endl;
  // std::cout << "~~~~~~~~~~~~ VALIDATION IN BACK-TO-BACK ~~~~~~~~~~~~" << std::endl;
  // std::cout << "In electron channel = " << electron_xs[6] << " +- " << electron_stat[6] << "(stat) " << std::endl;
  // std::cout << "In muon     channel = " << muon_xs[6] << " +- " << muon_stat[6] << "(stat) " << std::endl;



  std::cout << "=====================   START PRINTOUT   =====================" << std::endl;
  std::cout << "---------------   Total Fiducial cross section ---------------" << std::endl;
  std::cout << "Total cross section = " << electron_xs[0]+muon_xs[0] << " +- "
            << std::sqrt(electron_stat[0]*electron_stat[0] + muon_stat[0]*muon_stat[0]) << " (stat) "
            << " + " << std::sqrt(electron_up[0]*electron_up[0]+ muon_up[0]*muon_up[0]) << " - "
            << std::sqrt(electron_down[0]*electron_down[0]+ muon_down[0]*muon_down[0]) << " (syst) pb."<< std::endl;

  std::cout << "Theory cross section = " << electron_theory[0] + muon_theory[0] << " + "
            << std::sqrt(electron_theory_up[0]*electron_theory_up[0]+ muon_theory_up[0]*muon_theory_up[0])
            << " - " << std::sqrt(electron_theory_down[0]*electron_theory_down[0]+ muon_theory_down[0]*muon_theory_down[0])
            << " (syst) pb." << std::endl;

  std::cout << "Total scale uncertainty + " << std::sqrt(electron_scale_up[0]*electron_scale_up[0] + muon_scale_up[0]*muon_scale_up[0])
            << " - " << std::sqrt(electron_scale_down[0]*electron_scale_down[0] + muon_scale_down[0]*muon_scale_down[0]) << std::endl;

  std::cout << "--------------   High pT Fiducial cross section --------------" << std::endl;
  std::cout << "Total cross section = " << 1000*(electron_xs[1]+muon_xs[1]) << " +- "
            << 1000*std::sqrt(electron_stat[1]*electron_stat[1] + muon_stat[1]*muon_stat[1]) << " (stat) "
            << " + " << 1000*std::sqrt(electron_up[1]*electron_up[1]+ muon_up[1]*muon_up[1]) << " - "
            << 1000*std::sqrt(electron_down[1]*electron_down[1]+ muon_down[1]*muon_down[1]) << " (syst) fb."<< std::endl;

  std::cout << "Theory cross section = " << 1000*(electron_theory[1] + muon_theory[1]) << " + "
            << 1000*std::sqrt(electron_theory_up[1]*electron_theory_up[1]+ muon_theory_up[1]*muon_theory_up[1])
            << " - " << 1000*std::sqrt(electron_theory_down[1]*electron_theory_down[1]+ muon_theory_down[1]*muon_theory_down[1])
            << " (syst) fb." << std::endl;

  std::cout << "Total scale uncertainty + " << 1000*std::sqrt(electron_scale_up[1]*electron_scale_up[1] + muon_scale_up[1]*muon_scale_up[1])
            << " - " << 1000*std::sqrt(electron_scale_down[1]*electron_scale_down[1] + muon_scale_down[1]*muon_scale_down[1]) << std::endl;

  std::cout << "--------------   High pT with minDR Fiducial cross section --------------" << std::endl;
  std::cout << "Total cross section = " << 1000*(electron_xs[2]+muon_xs[2]) << " +- "
            << 1000*std::sqrt(electron_stat[2]*electron_stat[2] + muon_stat[2]*muon_stat[2]) << " (stat) "
            << " + " << 1000*std::sqrt(electron_up[2]*electron_up[2]+ muon_up[2]*muon_up[2]) << " - "
            << 1000*std::sqrt(electron_down[2]*electron_down[2]+ muon_down[2]*muon_down[2]) << " (syst) fb."<< std::endl;

  std::cout << "Theory cross section = " << 1000*(electron_theory[2] + muon_theory[2]) << " + "
            << 1000*std::sqrt(electron_theory_up[2]*electron_theory_up[2]+ muon_theory_up[2]*muon_theory_up[2])
            << " - " << 1000*std::sqrt(electron_theory_down[2]*electron_theory_down[2]+ muon_theory_down[2]*muon_theory_down[2])
            << " (syst) fb." << std::endl;

  std::cout << "Total scale uncertainty + " << 1000*std::sqrt(electron_scale_up[2]*electron_scale_up[2] + muon_scale_up[2]*muon_scale_up[2])
            << " - " << 1000*std::sqrt(electron_scale_down[2]*electron_scale_down[2] + muon_scale_down[2]*muon_scale_down[2]) << std::endl;


  std::cout << "-------------   Collinear Fiducial cross section -------------" << std::endl;
  std::cout << "Total cross section = " << 1000*(electron_xs[3]+muon_xs[3]) << " +- "
            << 1000*std::sqrt(electron_stat[3]*electron_stat[3] + muon_stat[3]*muon_stat[3]) << " (stat) "
            << " + " << 1000*std::sqrt(electron_up[3]*electron_up[3]+ muon_up[3]*muon_up[3]) << " - "
            << 1000*std::sqrt(electron_down[3]*electron_down[3]+ muon_down[3]*muon_down[3]) << " (syst) fb."<< std::endl;

  std::cout << "Theory cross section = " << 1000*(electron_theory[3] + muon_theory[3]) << " + "
            << 1000*std::sqrt(electron_theory_up[3]*electron_theory_up[3]+ muon_theory_up[3]*muon_theory_up[3])
            << " - " << 1000*std::sqrt(electron_theory_down[3]*electron_theory_down[3]+ muon_theory_down[3]*muon_theory_down[3])
            << " (syst) fb." << std::endl;

  std::cout << "Total scale uncertainty + " << 1000*std::sqrt(electron_scale_up[3]*electron_scale_up[3] + muon_scale_up[3]*muon_scale_up[3])
            << " - " << 1000*std::sqrt(electron_scale_down[3]*electron_scale_down[3] + muon_scale_down[3]*muon_scale_down[3]) << std::endl;

  std::cout << "------------   Back-to-Back Fiducial cross section -----------" << std::endl;
  std::cout << "Total cross section = " << 1000*(electron_xs[4]+muon_xs[4]) << " +- "
            << 1000*std::sqrt(electron_stat[4]*electron_stat[4] + muon_stat[4]*muon_stat[4]) << " (stat) "
            << " + " << 1000*std::sqrt(electron_up[4]*electron_up[4]+ muon_up[4]*muon_up[4]) << " - "
            << 1000*std::sqrt(electron_down[4]*electron_down[4]+ muon_down[4]*muon_down[4]) << " (syst) fb."<< std::endl;

  std::cout << "Theory cross section = " << 1000*(electron_theory[4] + muon_theory[4]) << " + "
            << 1000*std::sqrt(electron_theory_up[4]*electron_theory_up[4]+ muon_theory_up[4]*muon_theory_up[4])
            << " - " << 1000*std::sqrt(electron_theory_down[4]*electron_theory_down[4]+ muon_theory_down[4]*muon_theory_down[4])
            << " (syst) fb." << std::endl;

  std::cout << "Total scale uncertainty + " << 1000*std::sqrt(electron_scale_up[4]*electron_scale_up[4] + muon_scale_up[4]*muon_scale_up[4])
            << " - " << 1000*std::sqrt(electron_scale_down[4]*electron_scale_down[4] + muon_scale_down[4]*muon_scale_down[4]) << std::endl;



  DATA->Close();
  SHERPA->Close();
  DIBOSON->Close();
  TOPQUARK->Close();
  ZTT->Close();
  WJETS->Close();
  THEORY->Close();
  return;
}

void doTest(){
  TCanvas *can = new TCanvas("", "", 800, 800);
  TFile *RemoveAll = openFile("/eos/user/a/alaurier/ZJets/FinalProduction/incom/SherpaZJets2018.root");
  TFile *RemoveElectrons = openFile("/eos/user/a/alaurier/ZJets/test/SherpaZJets2018.root");
  StrV channels;
  channels.push_back("hee");
  channels.push_back("hmm");
  StrV plots;
  plots.push_back("ZJpt");
  plots.push_back("ZJptlDR");
  plots.push_back("ZJpthDR");
  Double_t bins[11];
  for (int i=0; i<7; i++)
    bins[i] = (0.1*i);
  bins[7] = (0.8);
  bins[8] = (1.0);
  bins[9] = (1.2);
  bins[10] = (1.5);
  TH1F* neworig = new TH1F("new sel", "new sel", 10, bins);
  TH1F* newtest = new TH1F("new test", "new test", 10, bins);
  for (auto chan : channels){
    for (auto plot : plots) {
      TH1F* orig = getHisto(RemoveAll, chan+"true"+plot+"0");
      TH1F* test = getHisto(RemoveElectrons, chan+"true"+plot+"0");
      orig = (TH1F*) (rebin1DHisto((TH1F*) orig->Clone(), "ZJpT"))->Clone();
      test = (TH1F*) (rebin1DHisto((TH1F*) test->Clone(), "ZJpT"))->Clone();
      orig->Rebin(3);
      test->Rebin(3);
      for (int i=1; i<11; i++){
        neworig->SetBinContent(i, orig->GetBinContent(i));
        neworig->SetBinError(i, orig->GetBinError(i));
        newtest->SetBinContent(i, test->GetBinContent(i));
        newtest->SetBinError(i, test->GetBinError(i));
      }
      orig = (TH1F*) neworig->Clone();
      test = (TH1F*) newtest->Clone();

      double ratio = 0.36, margin = 0.02;
      can->Clear();
      can->cd();
      gPad->SetBottomMargin(ratio);


      orig->SetMaximum(orig->GetMaximum()*1.5);
      orig->SetLineColor(kBlack);
      test->SetLineColor(kRed);
      orig->SetLineWidth(3);
      test->SetLineWidth(3);
      orig->GetXaxis()->SetTitle("");
      orig->GetXaxis()->SetLabelSize(0.);
      orig->GetYaxis()->SetTitle("Events");
      orig->Draw();
      test->Draw("same");

      TLegend* leg = new TLegend(0.70, 0.75, 0.85, 0.98);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.020);
      leg->SetFillStyle(0);

      leg->AddEntry(orig,"Current selection","pe");
      leg->AddEntry(test,"Remove only electron jets", "pe");
      leg->Draw();
      if (chan == "hee")
        drawText(0.3, 0.7, "Electron Channel",0,1,0.04);
      else if (chan == "hmm")
        drawText(0.3, 0.7, "Muon Channel",0,1,0.04);


      // Now add the ratio plot: first the pad
      TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
      p->SetLogx(0);
      p->Modified();
      p->Update();
      p->SetTopMargin(1.0 - ratio);
      p->SetFillStyle(0);
      p->SetTicks();
      p->SetGridy();
      p->Draw();
      p->cd();

      // Make the ratio plot!
      TH1F* Ratio = (TH1F*) orig->Clone();
      // Divide them
      Ratio->Divide((TH1F*) test);  // Define the ratio plot info
      gStyle->SetErrorX(0.5);
      Ratio->GetYaxis()->SetLabelSize(0.03);
      Ratio->GetYaxis()->SetTitle("Nominal/Test");
      Ratio->GetXaxis()->SetTitle("pT(Z)/pT(jet)");
      Ratio->GetXaxis()->SetLabelSize(0.03);
      Ratio->GetXaxis()->SetTitleOffset(1.4);
      Ratio->GetYaxis()->SetTitleOffset(1.4);
      Ratio->GetYaxis()->SetRangeUser(0.5, 2.0);
      // ratioPlot->GetYaxis()->SetRangeUser(0.3, 1.7);
      // ratio_up->Draw("P");
      // ratio_up->Draw("SAME P");
      Ratio->Draw("HIST");

      can->Print("test_"+chan+plot+".png");
    }
    TString plot = "DRZj";
    TH1F* orig = getHisto(RemoveAll, chan+"true"+plot+"0");
    TH1F* test = getHisto(RemoveElectrons, chan+"true"+plot+"0");

    double ratio = 0.36, margin = 0.02;
    can->Clear();
    can->cd();
    gPad->SetBottomMargin(ratio);


    orig->SetMaximum(orig->GetMaximum()*1.5);
    orig->SetLineColor(kBlack);
    test->SetLineColor(kRed);
    orig->SetLineWidth(3);
    test->SetLineWidth(3);
    orig->GetXaxis()->SetTitle("");
    orig->GetXaxis()->SetLabelSize(0.);
    orig->GetYaxis()->SetTitle("Events");
    orig->Draw();
    test->Draw("same");

    TLegend* leg = new TLegend(0.70, 0.75, 0.85, 0.98);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.020);
    leg->SetFillStyle(0);

    leg->AddEntry(orig,"Current selection","pe");
    leg->AddEntry(test,"Remove only electron jets", "pe");
    leg->Draw();
    if (chan == "hee")
      drawText(0.3, 0.7, "Electron Channel",0,1,0.04);
    else if (chan == "hmm")
      drawText(0.3, 0.7, "Muon Channel",0,1,0.04);


    // Now add the ratio plot: first the pad
    TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
    p->SetLogx(0);
    p->Modified();
    p->Update();
    p->SetTopMargin(1.0 - ratio);
    p->SetFillStyle(0);
    p->SetTicks();
    p->SetGridy();
    p->Draw();
    p->cd();

    // Make the ratio plot!
    TH1F* Ratio = (TH1F*) orig->Clone();
    // Divide them
    Ratio->Divide((TH1F*) test);  // Define the ratio plot info
    gStyle->SetErrorX(0.5);
    Ratio->GetYaxis()->SetLabelSize(0.03);
    Ratio->GetYaxis()->SetTitle("Nominal/Test");
    Ratio->GetXaxis()->SetTitle("minDR(Z,jet)");
    Ratio->GetXaxis()->SetLabelSize(0.03);
    Ratio->GetXaxis()->SetTitleOffset(1.4);
    Ratio->GetYaxis()->SetTitleOffset(1.4);
    Ratio->GetYaxis()->SetRangeUser(0.5, 2.0);
    // ratioPlot->GetYaxis()->SetRangeUser(0.3, 1.7);
    // ratio_up->Draw("P");
    // ratio_up->Draw("SAME P");
    Ratio->Draw("HIST");

    can->Print("test_"+chan+plot+".png");

  }


  return;

}
void doTest2(){
  TCanvas *can = new TCanvas("", "", 800, 800);
  TFile *TRUTH = new TFile("/eos/user/a/alaurier/ZJets/FinalProduction/extra/SherpaZJets.root");

  TH1F* temp = new TH1F();
  temp = getHisto(TRUTH,"heetrueDRZj0");
  TH1F* newElec = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), "minDR"))->Clone();
  int nBins = newElec->GetNbinsX();
  TGraphAsymmErrors* newtest = new TGraphAsymmErrors(newElec);
  double x[nBins];
  double y[nBins];
  double exl[nBins];
  double eyl[nBins];
  double exh[nBins];
  double eyh[nBins];
  for (int i=i;i<nBins+1;i++){
    x[i-1] = newElec->GetBinCenter(i);
    y[i-1] = newElec->GetBinContent(i);
    exl[i-1] = newElec->GetBinWidth(i)/2;
    exh[i-1] = newElec->GetBinWidth(i)/2;
    eyl[i-1] = newElec->GetBinContent(i)*0.8;
    eyh[i-1] = newElec->GetBinContent(i)*1.2;
  }
  can->Clear();
  can->cd();

  auto gae = new TGraphAsymmErrors(nBins, x, y, exl, exh, eyl, eyl);
  gae->SetMarkerStyle(25); // Square
  gae->SetMarkerColor(813); // Dark Green
  gae->SetLineColor(820); // Dark Green
  gae->SetFillColor(820); // Light Green
  gae->SetFillStyle(3001);
  gae->Draw("a2");
  gae->Draw("p");

  /// sherpa Theory!

  // newtest->Draw("B");
  can->Print("tgraph_test.png");
}

HistV CalculateSherpaTheory(TString plot){ // Truth theory predictions
  // Define important variables
  TEnv *settings = openSettingsFile("ZJetsSystplots.data");
  TFile *TRUTH = new TFile("/eos/user/a/alaurier/ZJets/FinalProduction/extra/SherpaZJets.root");
  Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
  HistV TruthHistos;

  // Define empty histograms
  TH1F* temp = new TH1F();
  TH1F* temp2 = new TH1F();
  // This is the nominal from the truth file.
  // We use this as the reference agains the nominal truth of the original xs
  temp = getHisto(TRUTH,"heetrue"+var+"0");
  TH1F* newElec = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
  if (plot.Contains("ZJpT")) newElec->Rebin(3);

  // 100 PDF variations
  int nBins = newElec->GetNbinsX();
  for (int i=1; i<nBins+1;i++)
    if (newElec->GetBinContent(i)<0.) newElec->SetBinContent(i,0.);
  std::vector<double> max, min;
  double val = 0.;
  for (int i=0; i<nBins; i++){
    max.push_back(0.);
    min.push_back(0.);
  }
  for (int sys=1; sys<101; sys++){ // 101 PDF variations
    temp2 = getHisto(TRUTH,"heetrue"+var+sys);
    temp = (TH1F*) (rebin1DHisto((TH1F*) temp2->Clone(), plot))->Clone();
    if (plot.Contains("ZJpT")) temp->Rebin(3);
    for (int i=1; i<nBins+1; i++){
      if (temp->GetBinContent(i) < 0.) continue;
      val = temp->GetBinContent(i) - newElec->GetBinContent(i);
      if (val > 0.) max[i-1] = max[i-1] + val*val;
      else if (val < 0.) min[i-1] = min[i-1] + val*val;
    }
  }

  // 100 variations up
  temp = (TH1F*) newElec->Clone();
  for (int i=1; i<nBins+1;i++){
    temp->SetBinContent(i, (newElec->GetBinContent(i)+std::sqrt(max[i-1])/99)/newElec->GetBinContent(i));
    if (temp->GetBinContent(i)<0.) temp->SetBinContent(i, 0.);
  }
  TruthHistos.push_back(temp);
  // 100 variations down
  for (int i=1; i<nBins+1;i++){
    temp->SetBinContent(i, (newElec->GetBinContent(i)-std::sqrt(min[i-1])/99)/newElec->GetBinContent(i));
    if (temp->GetBinContent(i)<0.) temp->SetBinContent(i, 0.);
  }
  TruthHistos.push_back(temp);

  // Comparing against 2 different PDFS
  // PDF Set #1
  temp2 = getHisto(TRUTH,"heetrue"+var+"101");
  temp = (TH1F*) (rebin1DHisto((TH1F*) temp2->Clone(), plot))->Clone();
  if (plot.Contains("ZJpT")) temp->Rebin(3);
  for (int i=1; i<nBins+1;i++){
    if (temp->GetBinContent(i)<0.) temp->SetBinContent(i, 0.);
    temp->SetBinContent(i, temp->GetBinContent(i)/newElec->GetBinContent(i));
  }
  TruthHistos.push_back(temp);
  // PDF Set #2
  temp2 = getHisto(TRUTH,"heetrue"+var+"102");
  temp = (TH1F*) (rebin1DHisto((TH1F*) temp2->Clone(), plot))->Clone();
  if (plot.Contains("ZJpT")) temp->Rebin(3);
  for (int i=1; i<nBins+1;i++){
    if (temp->GetBinContent(i)<0.) temp->SetBinContent(i, 0.);
    temp->SetBinContent(i, temp->GetBinContent(i)/newElec->GetBinContent(i));
  }
  TruthHistos.push_back(temp);

  // PDF alpha_S
  TH1F* alphaS_high = getHisto(TRUTH,"heetrue"+var+"103");
  temp = (TH1F*) (rebin1DHisto((TH1F*) alphaS_high->Clone(), plot))->Clone();
  alphaS_high = (TH1F*) temp->Clone();
  TH1F* alphaS_low = getHisto(TRUTH,"heetrue"+var+"104");
  temp = (TH1F*) (rebin1DHisto((TH1F*) alphaS_low->Clone(), plot))->Clone();
  alphaS_low = (TH1F*) temp->Clone();
  alphaS_high->Add(alphaS_low,-1.);
  if (plot.Contains("ZJpT")) alphaS_high->Rebin(3);


  // PDF alpha_S up
  TH1F* copy = (TH1F*) newElec->Clone();
  for (int i=1; i<nBins+1; i++){
    copy->SetBinContent(i,(newElec->GetBinContent(i)+0.5*alphaS_high->GetBinContent(i))/newElec->GetBinContent(i));
    if (copy->GetBinContent(i) < 0.) copy->SetBinContent(i,0.);
  }
  TruthHistos.push_back(copy);
  // PDF alpha_S down
  for (int i=1; i<nBins+1; i++){
    copy->SetBinContent(i,(newElec->GetBinContent(i)-0.5*alphaS_high->GetBinContent(i))/newElec->GetBinContent(i));
    if (copy->GetBinContent(i) < 0.) copy->SetBinContent(i,0.);
  }
  TruthHistos.push_back(copy);

  // Scale
  // scale up get max values for envelope method
  TH1F* scale_up = (TH1F*) newElec->Clone();
  TH1F* scale_down = (TH1F*) newElec->Clone();
  for (int sys=105; sys<111; sys++){ // 6 scale variations
    temp2 = getHisto(TRUTH,"heetrue"+var+sys);
    temp = (TH1F*) (rebin1DHisto((TH1F*) temp2->Clone(), plot))->Clone();
    if (plot.Contains("ZJpT")) temp->Rebin(3);
    for (int i=1; i<nBins+1; i++){
      if (temp->GetBinContent(i) > scale_up->GetBinContent(i)) scale_up->SetBinContent(i, temp->GetBinContent(i));
      if (temp->GetBinContent(i) < scale_down->GetBinContent(i)) scale_down->SetBinContent(i, temp->GetBinContent(i));
    }
  }
  for (int i=1; i<nBins+1; i++){
    scale_up->SetBinContent(i,scale_up->GetBinContent(i)/newElec->GetBinContent(i));
    scale_down->SetBinContent(i,scale_down->GetBinContent(i)/newElec->GetBinContent(i));
  }
  TruthHistos.push_back(scale_up);
  TruthHistos.push_back(scale_down);


  // std::cout << "for plot: " << plot << std::endl;
  // for (int i=1;i<nBins+1;i++){
  //   for (int j=0;j<8;j++)
  //     std::cout << "Relative variation in bin " << i << " for var " << j << " = " << TruthHistos[j]->GetBinContent(i) << std::endl;
  // }

  return TruthHistos;
}

void plotTheoryUncertainties(){
  TCanvas *can = new TCanvas("", "", 800, 800);
  TFile *Sherpa = openFile("/eos/user/a/alaurier/ZJets/FinalProduction/extra/SherpaZJets.root");
  TEnv *settings = openSettingsFile("ZJetsSystplots.data");
  StrV plotList = vectorize(settings->GetValue("PlotList", " "));
  HistV TheoryUnc;

  TH1F* PDF_up = new TH1F();
  TH1F* PDF_down = new TH1F();
  TH1F* scale1 = new TH1F();
  TH1F* scale2 = new TH1F();
  TH1F* scale3 = new TH1F();
  TH1F* scale4 = new TH1F();
  TH1F* scale5 = new TH1F();
  TH1F* scale6 = new TH1F();

  TH1F* temp = new TH1F();
  TH1F* nom = new TH1F();

  for (auto plot : plotList) {
    Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot

    // Get Non-Scale PDF uncertainties
    TheoryUnc = CalculateSherpaTheory(plot);
    int nBins = TheoryUnc[0]->GetNbinsX();
    std::vector<double> var_up, var_down;
    for (int i=0; i<nBins; i++){
      var_up.push_back(0.);
      var_down.push_back(0.);
    }
    double val1,val2,val;
    for (int j=0; j<3; j++){ // 3 PDF pairs
      for (int i=1; i<nBins+1;i++){
        val1 = TheoryUnc[2*j]->GetBinContent(i)-1;
        val2 = TheoryUnc[2*j+1]->GetBinContent(i)-1;
        if (val1 > 0. && val2 > 0.){ // Both above nominal, get max
          val = std::max(val1,val2);
          var_up[i-1] = var_up[i-1] + val*val;
        }
        else if (val1 < 0. && val2 < 0.){ // Both below nominal, get min
          val = std::max(val1*val1,val2*val2);
          var_down[i-1] = var_down[i-1] + val; // Already squared
        }
        else if (val1 > 0. && val2 < 0.){ // one above, one below
          var_up[i-1] = var_up[i-1] + val1*val1;
          var_down[i-1] = var_down[i-1] + val2*val2;
        }
        else if (val1 < 0. && val2 > 0.){ // one above, one below
          var_down[i-1] = var_down[i-1] + val1*val1;
          var_up[i-1] = var_up[i-1] + val2*val2;
        }
      }
    }

    PDF_up = (TH1F*) TheoryUnc[0]->Clone();
    PDF_down = (TH1F*) TheoryUnc[0]->Clone();
    for (int i=1; i<nBins+1; i++){
      PDF_up->SetBinContent(i, 1 + std::sqrt(var_up[i-1]));
      PDF_down->SetBinContent(i, 1 - std::sqrt(var_down[i-1]));
    }

    // Get Scale Uncertainties
    temp = getHisto(Sherpa, "heetrue"+var+"0");
    nom = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
    if (plot.Contains("ZJpT")) nom->Rebin(3);


    scale1 = getHisto(Sherpa, "heetrue"+var+"105");
    scale1 = (TH1F*) (rebin1DHisto((TH1F*) scale1->Clone(), plot))->Clone();
    if (plot.Contains("ZJpT")) scale1->Rebin(3);
    for (int i=1; i<nBins+1;i++){
      scale1->SetBinContent(i,scale1->GetBinContent(i)/nom->GetBinContent(i));
      scale1->SetBinError(i,0.);
    }

    scale2 = getHisto(Sherpa, "heetrue"+var+"106");
    scale2 = (TH1F*) (rebin1DHisto((TH1F*) scale2->Clone(), plot))->Clone();
    if (plot.Contains("ZJpT")) scale2->Rebin(3);
    for (int i=1; i<nBins+1;i++){
      scale2->SetBinContent(i,scale2->GetBinContent(i)/nom->GetBinContent(i));
      scale2->SetBinError(i,0.);
    }

    scale3 = getHisto(Sherpa, "heetrue"+var+"107");
    scale3 = (TH1F*) (rebin1DHisto((TH1F*) scale3->Clone(), plot))->Clone();
    if (plot.Contains("ZJpT")) scale3->Rebin(3);
    for (int i=1; i<nBins+1;i++){
      scale3->SetBinContent(i,scale3->GetBinContent(i)/nom->GetBinContent(i));
      scale3->SetBinError(i,0.);
    }

    scale4 = getHisto(Sherpa, "heetrue"+var+"108");
    scale4 = (TH1F*) (rebin1DHisto((TH1F*) scale4->Clone(), plot))->Clone();
    if (plot.Contains("ZJpT")) scale4->Rebin(3);
    for (int i=1; i<nBins+1;i++){
      scale4->SetBinContent(i,scale4->GetBinContent(i)/nom->GetBinContent(i));
      scale4->SetBinError(i,0.);
    }

    scale5 = getHisto(Sherpa, "heetrue"+var+"109");
    scale5 = (TH1F*) (rebin1DHisto((TH1F*) scale5->Clone(), plot))->Clone();
    if (plot.Contains("ZJpT")) scale5->Rebin(3);
    for (int i=1; i<nBins+1;i++){
      scale5->SetBinContent(i,scale5->GetBinContent(i)/nom->GetBinContent(i));
      scale5->SetBinError(i,0.);
    }

    scale6 = getHisto(Sherpa, "heetrue"+var+"110");
    scale6 = (TH1F*) (rebin1DHisto((TH1F*) scale6->Clone(), plot))->Clone();
    if (plot.Contains("ZJpT")) scale6->Rebin(3);
    for (int i=1; i<nBins+1;i++){
      scale6->SetBinContent(i,scale6->GetBinContent(i)/nom->GetBinContent(i));
      scale6->SetBinError(i,0.);
    }


    double ratio = 0.36, margin = 0.02;
    can->Clear();
    can->cd();
    // gPad->SetBottomMargin(ratio);

    PDF_up->GetYaxis()->SetRangeUser(.5, 2.5);
    Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
    PDF_up->GetXaxis()->SetTitle(Xlabel);
    PDF_up->GetYaxis()->SetTitle("Relative Unc.");


    PDF_up->SetFillColor(1);
    PDF_up->SetFillStyle(3005);
    PDF_up->SetMarkerColor(0);
    PDF_down->SetFillColor(10);
    PDF_down->SetFillStyle(1001);
    PDF_up->SetLineColor(10);
    PDF_down->SetLineColor(10);
    scale1->SetLineColor(kMagenta+2); // Purple
    scale2->SetLineColor(417); // Light green
    scale3->SetLineColor(857); // Blue
    scale4->SetLineColor(kRed+1); // Red
    scale5->SetLineColor(419); // nice green
    scale6->SetLineColor(kCyan); // Brown
    // Make Lines larger
    scale1->SetLineWidth(3);
    scale2->SetLineWidth(3);
    scale3->SetLineWidth(3);
    scale4->SetLineWidth(3);
    scale5->SetLineWidth(3);
    scale6->SetLineWidth(3);
    // Marker size
    scale1->SetMarkerSize(0.01);
    scale2->SetMarkerSize(0.01);
    scale3->SetMarkerSize(0.01);
    scale4->SetMarkerSize(0.01);
    scale5->SetMarkerSize(0.01);
    scale6->SetMarkerSize(0.01);


    PDF_up->Draw("HIST");
    PDF_down->Draw("SAME HIST");
    scale1->Draw("SAME HIST");
    scale2->Draw("SAME HIST");
    scale3->Draw("SAME HIST");
    scale4->Draw("SAME HIST");
    scale5->Draw("SAME HIST");
    scale6->Draw("SAME HIST");
    gPad->RedrawAxis();
    gPad->RedrawAxis("G");



    TLegend* leg = new TLegend(0.55, 0.65, 0.85, 0.88);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.035);
    leg->SetFillStyle(0);

    leg->AddEntry(PDF_up,"PDF Uncertainty","pef");
    leg->AddEntry(scale1,"#mu_R=0.5, #mu_F=0.5", "l");
    leg->AddEntry(scale2,"#mu_R=0.5, #mu_F=1.0", "l");
    leg->AddEntry(scale3,"#mu_R=1.0, #mu_F=0.5", "l");
    leg->AddEntry(scale4,"#mu_R=1.0, #mu_F=2.0", "l");
    leg->AddEntry(scale5,"#mu_R=2.0, #mu_F=1.0", "l");
    leg->AddEntry(scale6,"#mu_R=2.0, #mu_F=2.0", "l");
    leg->Draw();
    double Totlumi = settings->GetValue("luminosity", 0.0);
    drawPlotInfo(0.15,0.9, plot, "both", Totlumi);


    can->Print("TheoryUncert_"+plot+".png");
  }

  return;
}
