#include "../util.h"
#include "RooUnfoldResponse.h"
#include "RooUnfoldBayes.h"
#include "RooUnfoldSvd.h"
#include "RooUnfoldTUnfold.h"
#include "RooUnfoldIds.h"

void copyNominal(TFile* file, TEnv* settings);
void copyNominalMG(TFile* file, TEnv* settings);
void copyttbar(TFile* file, TEnv* settings);
void copySherpa(TFile* file, TEnv* settings);
void copyZtautau(TFile* file, TEnv* settings);
void doUnfoldingZpT(TEnv* settings);
void doUnfoldingJetpT(TEnv* settings);
void doUnfoldingHighpT(TEnv* settings);
void doUnfoldingminDR(TEnv* settings);
void test();

int iterations = 2;

int makeHistos(){
  // Needed for unfolding
  gSystem->Load("/afs/cern.ch/work/a/alaurier/private/RooUnfold/RooUnfold/libRooUnfold.so");

  setStyle();
  Str config = "ZJetsplots.data";

  std::cout << "config file: " << config.Data() << std::endl;
  TEnv *settings = openSettingsFile(config);

  Str filePath = settings->GetValue("filePath.nominal", " ");

  // test();
  // return 0;

  // doUnfoldingJetpT(settings);
  // doUnfoldingZpT(settings);
  // doUnfoldingHighpT(settings);
  // doUnfoldingminDR(settings);
  // return 0;

  // data
  // TFile *data = openFile(filePath + "data.root");
  // copyNominal(data, settings);
  // data->Close();

  // Sherpa
  TFile *Sherpa = openFile(filePath + "SherpaZJets.root");
  copySherpa(Sherpa, settings);
  Sherpa->Close();

  // MadGraph
  // TFile *MadGraph = openFile(filePath + "MGZJets.root");
  // copyNominalMG(MadGraph, settings);
  // MadGraph->Close();

  // Diboson
  // TFile *Diboson = openFile(filePath + "Diboson.root");
  // copyNominal(Diboson, settings);
  // Diboson->Close();

  // ttbar
  TFile *ttbar = openFile(filePath + "ttbar.root");
  copyttbar(ttbar, settings);
  ttbar->Close();

  // SingleTop
  // TFile *SingleTop = openFile(filePath + "SingleTop.root");
  // copyNominal(SingleTop, settings);
  // SingleTop->Close();
  //
  // Ztt
  // TFile *Ztt = openFile(filePath + "Ztt.root");
  // copyZtautau(Ztt, settings);
  // Ztt->Close();

  // WJets
  // TFile *WJets = openFile(filePath + "WJets.root");
  // copyNominal(WJets, settings);
  // WJets->Close();

  return 0;
}

void test() {
  TH1::AddDirectory(kFALSE);
  TCanvas *can = new TCanvas("", "", 800, 800);
  TString plot = "pTZ"; // For rebinning
  TString his = "hemZPt1j"; // The histogram we fetch

  TFile *file = openFile("/eos/user/a/alaurier/ZJets/FinalProduction/FullUncertainties/ttbar_expanded.root");

  TH1F* temp = getHisto(file, his+"0"); // Get nominal
  TH1F* nom = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
  // TH1F* nom = (TH1F*) temp->Clone();
  temp = getHisto(file, his+"149"); // Get Gen/Matching
  TH1F* GenMatch = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
  // TH1F* GenMatch = (TH1F*) temp->Clone();

  temp = getHisto(file, his+"151"); // Get Fragmentation/Hadronization
  TH1F* Frag = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
  // TH1F* Frag = (TH1F*) temp->Clone();


  nom->SetLineColor(814);
  nom->SetMarkerStyle(25); // Square
  nom->SetMarkerColor(814); // Dark Green
  nom->GetXaxis()->SetTitle(plot);
  nom->GetYaxis()->SetTitle("Events");



  GenMatch->SetLineColor(kRed);
  GenMatch->SetMarkerStyle(24); // Circle
  GenMatch->SetMarkerColor(kRed);

  Frag->SetLineColor(kBlue);
  Frag->SetMarkerStyle(26); // triangle
  Frag->SetMarkerColor(kBlue);

  TLegend* leg = new TLegend();
  leg = new TLegend(0.65, 0.80, 0.80, 0.93);
  leg->SetFillColor(0);
  leg->SetBorderSize(0);
  leg->SetFillStyle(0);
  leg->SetTextSize(0.035);

  leg->AddEntry(nom,"nominal ttbar","pl");
  leg->AddEntry(GenMatch,"Gen./Matching","pl");
  leg->AddEntry(Frag,"Fragment./Hadron.","pl");

  if (plot != "minDR") gPad->SetLogy(1);

  nom->Draw();
  GenMatch->Draw("same");
  Frag->Draw("same");
  leg->Draw("same");
  can->Print(his+".png");



  // alex->GetXaxis()
  // alex->Draw();
  //
  // can->Print("fig.png");


}

void copyttbar(TFile* file, TEnv* settings){
  double Totlumi = settings->GetValue("luminosity", 0.0);
  TH1::AddDirectory(kFALSE);
  Str filePath = settings->GetValue("filePath.extras", " ");
  TFile *Hadronization = openFile(filePath + "PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.root");
  TFile *Matching = openFile(filePath + "aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.root");
  TFile *extra410472 = openFile(filePath + "410472.root");
  TFile *ISR410472 = openFile(filePath + "ISR_410472.root");
  TFile *ISR410482 = openFile(filePath + "ISR_410482.root");
  StrV channels = vectorize(settings->GetValue("channels", " "));

  // Get Nominal files for unfolding
  Str nominalPath = settings->GetValue("filePath.nominal", " ");
  TFile *Data = openFile(nominalPath + "data.root");
  TFile *Sherpa = openFile(nominalPath + "SherpaZJets.root");
  TFile *SingleTop = openFile(nominalPath + "SingleTop.root");
  TFile *WJets = openFile(nominalPath + "WJets.root");
  TFile *Ztt = openFile(nominalPath + "Ztt.root");
  TFile *Diboson = openFile(nominalPath + "Diboson.root");

  std::cout << "Starting scaling of " << file->GetName() << std::endl;
  TFile hisfile("ttbar_expanded.root","RECREATE");
  TH1F* temp = new TH1F();
  TH1F* copy = new TH1F();
  StrV plotList = vectorize(settings->GetValue("Controls", " "));
  int nBins = getHisto(file, "heeZPt1j0")->GetNbinsX();
  // Copy all the control histos back in new file, and copy the ones that are unchanged.
  for (auto plot : plotList){
    for (auto chan : channels){
      for (int sys=0; sys<143; sys++){ // 0 - 134 : Experimental, 135-142 are Sherpa theory
        if (sys < 135)
          temp = getHisto(file, chan+plot+std::to_string(sys));
        else temp = getHisto(file, chan+plot+"0");
        // std::cout << "control = " << plot << std::endl;
        // std::cout << temp->Integral() << std::endl;
        copy = (TH1F*) temp->Clone(chan+plot+std::to_string(sys));
        copy->Write();
      }
      temp = getHisto(file, chan+plot+"0"); // Get nominal
      copy = (TH1F*) temp->Clone(chan+plot+"150"); // 2nd of pair: Hard Scatter Generation and Matching
      copy->Write();
      copy = (TH1F*) temp->Clone(chan+plot+"152"); // 2nd of pair: Fragmentation & Hadronization
      copy->Write();
    }
  }
  /// Start ttbar modelling theory uncertainties
  // Copy the Hard-Scatter Generation&Matching and Fragmentation&Hadronization!
  for (auto chan : channels){
    for (auto plot : plotList){
      temp = getHisto(Matching, chan+plot+"0"); // Get Hard Scatter Generation and Matching
      copy = (TH1F*) temp->Clone(chan+plot+"149"); // 1st of pair: Hard Scatter Generation and Matching
      copy->Write();
      temp = getHisto(Hadronization, chan+plot+"0"); // Get Fragmentation&Hadronization
      copy = (TH1F*) temp->Clone(chan+plot+"151"); // 1st of pair: Fragmentation & Hadronization
      copy->Write();
    }
  }
  // Copy the alpha_S (FSR) from the extra410472 sample
  for (auto chan : channels){
    for (auto plot : plotList){
      temp = getHisto(extra410472, chan+plot+"107"); // alpha_S FSR down
      copy = (TH1F*) temp->Clone(chan+plot+"155"); // 1st of pair
      // Renormalize to nominal
      TH1F* reference = getHisto(file, chan+plot+"0");
      TH1F* extraRef = getHisto(extra410472, chan+plot+"0");
      for (int i=1; i<nBins+1; i++)
        if (extraRef->GetBinContent(i) > 0.)
          copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
      copy->Write();
      temp = getHisto(extra410472, chan+plot+"108"); // alpha_S FSR up
      copy = (TH1F*) temp->Clone(chan+plot+"156"); // 2nd of pair
      // Renormalize to nominal
      for (int i=1; i<nBins+1; i++)
        if (extraRef->GetBinContent(i) > 0.)
          copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
      copy->Write();
    }
  }
  // Copy the Additionnal radiation (ISR) down from the ISR_410472 sample
  for (auto chan : channels){
    for (auto plot : plotList){
      temp = getHisto(ISR410472, chan+plot+"105"); // alpha_S FSR down
      copy = (TH1F*) temp->Clone(chan+plot+"153"); // 1st of pair
      copy->Write();
    }
  }
  // Copy the Additionnal radiation (ISR) up from the ISR_410482 sample
  for (auto chan : channels){
    for (auto plot : plotList){
      temp = getHisto(ISR410482, chan+plot+"106"); // alpha_S FSR up
      copy = (TH1F*) temp->Clone(chan+plot+"154"); // 1st of pair
      copy->Write();
    }
  }


  // Start ttbar PDF unceratinties 143-148
  // Copy Alternate PDF from the extra410472 sample
  for (auto chan : channels){
    for (auto plot : plotList){
      // Renormalize to nominal
      TH1F* reference = getHisto(file, chan+plot+"0");
      TH1F* extraRef = getHisto(extra410472, chan+plot+"0");
      temp = getHisto(extra410472, chan+plot+"101"); // alternate set 1
      copy = (TH1F*) temp->Clone(chan+plot+"145"); // 1st of pair
      for (int i=1; i<nBins+1; i++)
        if (extraRef->GetBinContent(i) > 0.)
          copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
      copy->Write();
      temp = getHisto(extra410472, chan+plot+"102"); // alternate set 2
      copy = (TH1F*) temp->Clone(chan+plot+"146"); // 2nd of pair
      for (int i=1; i<nBins+1; i++)
        if (extraRef->GetBinContent(i) > 0.)
          copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
      copy->Write();
    }
  }
  // Uncertainty from the variation of 100 sets
  for (auto chan : channels){
    for (auto plot : plotList){
      TH1F* reference = getHisto(file, chan+plot+"0");
      TH1F* extraRef = getHisto(extra410472, chan+plot+"0");
      TH1F* nominal = getHisto(extra410472, chan+plot+"0"); // nominal PDF
      std::vector<double> max, min;
      int nBins = nominal->GetNbinsX();
      for (int i=0; i<nBins; i++){
        max.push_back(0.);
        min.push_back(0.);
      }
      TH1F* var = new TH1F();
      for (int sys=1; sys<101; sys++){
        var = getHisto(extra410472, chan+plot+sys);
        var->Add(nominal,-1.);
        double val = 0.;
        for (int i=1; i<nBins+1; i++){
          val = var->GetBinContent(i);
          if (val < 0) min[i-1] = min[i-1] + val*val;
          else if (val > 0) max[i-1] = max[i-1] + val*val;
        }
      }
      copy = (TH1F*) nominal->Clone(chan+plot+"143"); // 1st of pair: max
      for (int i=1; i<nBins+1; i++)
        copy->SetBinContent(i,nominal->GetBinContent(i)+std::sqrt(max[i-1]/99));
      for (int i=1; i<nBins+1; i++)
        if (extraRef->GetBinContent(i) > 0.)
          copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
      copy->Write();

      copy = (TH1F*) nominal->Clone(chan+plot+"144"); // 2nd of pair: min
      for (int i=1; i<nBins+1; i++)
        copy->SetBinContent(i,nominal->GetBinContent(i)-std::sqrt(min[i-1]/99));
      for (int i=1; i<nBins+1; i++)
        if (extraRef->GetBinContent(i) > 0.)
          copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
      copy->Write();
    }
  }
  // Uncertainty from PDF alpha_S
  for (auto chan : channels){
    for (auto plot : plotList){
      TH1F* reference = getHisto(file, chan+plot+"0");
      TH1F* extraRef = getHisto(extra410472, chan+plot+"0");
      TH1F* nominal = getHisto(extra410472, chan+plot+"0"); // nominal PDF
      TH1F* alphaS_high = getHisto(extra410472, chan+plot+"103"); // alphaS high
      TH1F* alphaS_low = getHisto(extra410472, chan+plot+"104"); // alphaS low
      int nBins = nominal->GetNbinsX();
      alphaS_high->Add(alphaS_low,-1.);
      copy = (TH1F*) nominal->Clone(chan+plot+"147"); // 1st of pair: max
      for (int i=1; i<nBins+1; i++)
        copy->SetBinContent(i,nominal->GetBinContent(i)+0.5*alphaS_high->GetBinContent(i));
      for (int i=1; i<nBins+1; i++)
        if (extraRef->GetBinContent(i) > 0.)
          copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
      copy->Write();
      copy = (TH1F*) nominal->Clone(chan+plot+"148"); // 2nd of pair: min
      for (int i=1; i<nBins+1; i++)
        copy->SetBinContent(i,nominal->GetBinContent(i)-0.5*alphaS_high->GetBinContent(i));
      for (int i=1; i<nBins+1; i++)
        if (extraRef->GetBinContent(i) > 0.)
          copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
      copy->Write();
    }
  }
  hisfile.Close();


  //////////////////////////////////////////////////
  //// Now we begin the unfolded uncerainties! ////
  //////////////////////////////////////////////////
  TFile newfile("UnfoldedTheory_ttbar.root","RECREATE");
  StrV RespChan = vectorize(settings->GetValue("Channels", " "));
  StrV respList = vectorize(settings->GetValue("PlotList", " "));

  // Define maps to store the ttbar unfolded histograms.
  HistMap Unfolded;


  for (int iter=1; iter < 6; iter++)
  for (auto plot : respList){
    Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
    for (auto chan : RespChan){
      // Reset unfolded histograms.
      Unfolded.clear();
      for (int i=0; i<111; i++){
        TString label = std::to_string(i);
        Unfolded[label] = new TH1F();
      }
      // if(iter !=2) continue;

      // Get nominal data - SingleTop - WJets - Diboson - Ztt
      TH1F* nominalData = getHisto(Data, chan+"data"+var+"0");
      temp = getHisto(SingleTop, chan+"meas"+var+"0");
      nominalData->Add(temp,-1.);
      temp = getHisto(WJets, chan+"meas"+var+"0");
      nominalData->Add(temp,-1.);
      temp = getHisto(Diboson, chan+"meas"+var+"0");
      nominalData->Add(temp,-1.);
      temp = getHisto(Ztt, chan+"meas"+var+"0");
      nominalData->Add(temp,-1.);

      // Rebin data - BG (without ttbar!)
      TH1F* RebinnedData = (TH1F*) (rebin1DHisto((TH1F*) nominalData->Clone(), plot))->Clone();
      int nBins = RebinnedData->GetNbinsX();


      // Get Nominal sherpa response matrix.
      TH2F* RMatrix = getHisto2(Sherpa, chan+"resp"+var+"0");
      temp = getHisto(Sherpa,chan+"meas"+var+"0");
      TH1F* SherpaMeas = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      temp = getHisto(Sherpa,chan+"true"+var+"0");
      TH1F* SherpaTrue = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();

      // Rebin Sherpa Response matrix
      TH2F* RebinnedMatrix = (TH2F*) (rebin2DHisto((TH2F*) RMatrix->Clone(), plot))->Clone();

      // Define response matrix!
      RooUnfoldResponse Response (SherpaMeas,SherpaTrue,RebinnedMatrix);

      TH1F* newttbar = new TH1F();
      TH1F* data = new TH1F();

      // Calculate the nominal ttbar from nominal analysis sample!
      data = (TH1F*) RebinnedData->Clone();
      temp = getHisto(file, chan+"meas"+var+"0"); // nominal ttbar
      newttbar = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      data->Add(newttbar,-1);
      // Unfold!
      RooUnfoldBayes unfold_data (&Response, data, iter);
      // Get unfolded data and push it to map
      auto* unfolded = unfold_data.Hreco();
      for (int i=1; i<nBins+1; i++){
        unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
        unfolded->SetBinError(i,0.);
      }
      if (plot.Contains("ZJpT")) unfolded->Rebin(3);
      TH1F* nominalttbar = (TH1F*) unfolded->Clone();

      // More complicated PDF !
      for (int i=0; i<111; i++){
        TString label = std::to_string(i);
        // Get Nominal rebinned data.
        data = (TH1F*) RebinnedData->Clone();
        // Get ttbar measured, rebin it, and remove from nominal rebinned data
        temp = getHisto(extra410472, chan+"meas"+var+i);
        newttbar = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
        data->Add(newttbar,-1);

        // Unfold!
        unfold_data = RooUnfoldBayes(&Response, data, iter);
        // Get unfolded data and push it to map
        unfolded = unfold_data.Hreco();
        for (int i=1; i<nBins+1; i++){
          // unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
          unfolded->SetBinError(i,0.);
        }
        if (plot.Contains("ZJpT")) unfolded->Rebin(3);
        Unfolded[label] = ((TH1F*) unfolded->Clone());
      }

      TH1F* nominal = (TH1F*) ((TH1F*) Unfolded["0"])->Clone(); // nominal PDF

      /// 145-146 ttbar PDF from choice of PDF set (using 2 different sets)
      // Alternate PDF set #1 : Unfolded[101]
      temp = (TH1F*) ((TH1F*) Unfolded["101"])->Clone(chan+var+"145"+"_"+iter);
      // Renormalize to nominal
      for (int i=1; i<nBins+1; i++)
        if (nominal->GetBinContent(i) > 0)
          temp->SetBinContent(i,nominalttbar->GetBinContent(i)*temp->GetBinContent(i)/nominal->GetBinContent(i));

      // Renormalize to nominal on 2nd to last bin
      double val = temp->GetBinContent(nBins-1)/nominalttbar->GetBinContent(nBins-1);
      double before = temp->GetBinContent(nBins-2)/nominalttbar->GetBinContent(nBins-2);
      double after = temp->GetBinContent(nBins)/nominalttbar->GetBinContent(nBins);
      if (val<1. && (before > 1. && after > 1.)) temp->SetBinContent(nBins-1, 0.5*(before+after)*nominalttbar->GetBinContent(nBins-1));
      else if (val>1. && (before< 1. && after < 1.)) temp->SetBinContent(nBins-1, 0.5*(before+after)*nominalttbar->GetBinContent(nBins-1));

      temp->Write();

      // Alternate PDF set #2 : Unfolded[102]
      temp = (TH1F*) ((TH1F*) Unfolded["102"])->Clone(chan+var+"146"+"_"+iter);
      // Renormalize to nominal
      for (int i=1; i<nBins+1; i++)
        if (nominal->GetBinContent(i) > 0)
          temp->SetBinContent(i,nominalttbar->GetBinContent(i)*temp->GetBinContent(i)/nominal->GetBinContent(i));

      // Renormalize to nominal on 2nd to last bin
      val = temp->GetBinContent(nBins-1)/nominalttbar->GetBinContent(nBins-1);
      before = temp->GetBinContent(nBins-2)/nominalttbar->GetBinContent(nBins-2);
      after = temp->GetBinContent(nBins)/nominalttbar->GetBinContent(nBins);
      if (val<1. && (before > 1. && after > 1.)) temp->SetBinContent(nBins-1, 0.5*(before+after)*nominalttbar->GetBinContent(nBins-1));
      else if (val>1. && (before< 1. && after < 1.)) temp->SetBinContent(nBins-1, 0.5*(before+after)*nominalttbar->GetBinContent(nBins-1));

      temp->Write();


      // 143-144 ttbar PDF from 100 set variations: unfolded[1-100]
      nominal = (TH1F*) ((TH1F*) Unfolded["0"])->Clone(); // nominal PDF
      std::vector<double> max, min, mean;
      for (int i=0; i<nBins; i++){
        mean.push_back(0.);
        max.push_back(0.);
        min.push_back(0.);
      }
      TH1F* Var = new TH1F();
      // Calculate the mean
      for (int sys=1; sys<101; sys++){
        Var = (TH1F*) ((TH1F*) Unfolded[std::to_string(sys)])->Clone();
        for (int i=1; i<nBins+1; i++){
          mean[i-1] = mean[i-1] + Var->GetBinContent(i)/100.;
        }
      }
      // Evaluate the variation
      for (int sys=1; sys<101; sys++){
        Var = (TH1F*) ((TH1F*) Unfolded[std::to_string(sys)])->Clone();
        double val = 0.;
        for (int i=1; i<nBins+1; i++){
          val = Var->GetBinContent(i) - nominal->GetBinContent(i);
          if (std::abs(val) > 2.5*std::sqrt(nominal->GetBinContent(i))) continue;
          if (val < 0) min[i-1] = min[i-1] + val*val;
          else if (val > 0) max[i-1] = max[i-1] + val*val;
        }
      }
      copy = (TH1F*) nominal->Clone(chan+var+"143"+"_"+iter); // 1st of pair: max
      for (int i=1; i<nBins+1; i++){
        copy->SetBinContent(i,nominal->GetBinContent(i)+std::sqrt(max[i-1]/99));
        // std::cout << "up var of bin " << i << " = " << (nominal->GetBinContent(i)+std::sqrt(max[i-1]))/nominal->GetBinContent(i) << std::endl;
        // std::cout << "down var of bin " << i << " = " << (nominal->GetBinContent(i)-std::sqrt(min[i-1]))/nominal->GetBinContent(i) << std::endl;
      }
      // Renormalize to nominal
      for (int i=1; i<nBins+1; i++)
        if (nominal->GetBinContent(i) > 0)
          copy->SetBinContent(i,nominalttbar->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));
      copy->Write();


      copy = (TH1F*) nominal->Clone(chan+var+"144"+"_"+iter); // 2nd of pair: min
      for (int i=1; i<nBins+1; i++)
        copy->SetBinContent(i,nominal->GetBinContent(i)-std::sqrt(min[i-1]/99));
      // Renormalize to nominal
      for (int i=1; i<nBins+1; i++)
        if (nominal->GetBinContent(i) > 0)
          copy->SetBinContent(i,nominalttbar->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));
      copy->Write();

      // 147-148 ttbar PDF alpha_S : unfolded[103-104]
      nominal = (TH1F*) ((TH1F*) Unfolded["0"])->Clone(); // nominal PDF
      TH1F* alphaS_high = (TH1F*) ((TH1F*) Unfolded["103"])->Clone(); // alphaS high
      TH1F* alphaS_low = (TH1F*) ((TH1F*) Unfolded["104"])->Clone(); // alphaS low
      alphaS_high->Add(alphaS_low,-1.);
      copy = (TH1F*) nominal->Clone(chan+var+"147"+"_"+iter); // 1st of pair: max
      for (int i=1; i<nBins+1; i++)
        copy->SetBinContent(i,nominal->GetBinContent(i)+0.5*alphaS_high->GetBinContent(i));
      // Renormalize to nominal
      for (int i=1; i<nBins+1; i++)
        if (nominal->GetBinContent(i) > 0)
          copy->SetBinContent(i,nominalttbar->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));
      copy->Write();

      copy = (TH1F*) nominal->Clone(chan+var+"148"+"_"+iter); // 2nd of pair: min
      for (int i=1; i<nBins+1; i++)
        copy->SetBinContent(i,nominal->GetBinContent(i)-0.5*alphaS_high->GetBinContent(i));
      // Renormalize to nominal
      for (int i=1; i<nBins+1; i++)
        if (nominal->GetBinContent(i) > 0)
          copy->SetBinContent(i,nominalttbar->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));
      copy->Write();

      //// ttbar modelling!
      // 149-150 Hard Scatter Generation and Matching. Compare against 410465
      data = (TH1F*) RebinnedData->Clone();
      temp = getHisto(Matching, chan+"meas"+var+"0"); // 410465
      newttbar = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      data->Add(newttbar,-1);

      // Unfold!
      unfold_data = RooUnfoldBayes(&Response, data, iter);
      // Get unfolded data and push it to map
      unfolded = unfold_data.Hreco();
      for (int i=1; i<nBins+1; i++){
        unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
        unfolded->SetBinError(i,0.);
      }
      if (plot.Contains("ZJpT")) unfolded->Rebin(3);

      copy = (TH1F*) unfolded->Clone(chan+var+"149"+"_"+iter); // 1st of pair
      copy->Write();

      copy = (TH1F*) nominalttbar->Clone(chan+var+"150"+"_"+iter); // 2nd of pair is nominal
      copy->Write();

      // 151-152 Fragmentation & Hadronization. Compare against 410558
      data = (TH1F*) RebinnedData->Clone();
      temp = getHisto(Hadronization, chan+"meas"+var+"0"); // 410558
      newttbar = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      data->Add(newttbar,-1);

      // Unfold!
      unfold_data = RooUnfoldBayes(&Response, data, iter);
      // Get unfolded data and push it to map
      unfolded = unfold_data.Hreco();
      for (int i=1; i<nBins+1; i++){
        unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
        unfolded->SetBinError(i,0.);
      }
      if (plot.Contains("ZJpT")) unfolded->Rebin(3);
      copy = (TH1F*) unfolded->Clone(chan+var+"151"+"_"+iter); // 1st of pair
      copy->Write();

      copy = (TH1F*) nominalttbar->Clone(chan+var+"152"+"_"+iter); // 2nd of pair is nominal
      copy->Write();

      // 153-154 Additional Radiation (ISR)
      data = (TH1F*) RebinnedData->Clone();
      temp = getHisto(ISR410472, chan+"meas"+var+"105"); // ISR down
      newttbar = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      data->Add(newttbar,-1);

      // Unfold!
      unfold_data = RooUnfoldBayes(&Response, data, iter);
      // Get unfolded data and push it to map
      unfolded = unfold_data.Hreco();
      for (int i=1; i<nBins+1; i++){
        unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
        unfolded->SetBinError(i,0.);
      }
      if (plot.Contains("ZJpT")) unfolded->Rebin(3);
      copy = (TH1F*) unfolded->Clone(chan+var+"153"+"_"+iter); // 1st of pair
      copy->Write();

      // ISR down
      data = (TH1F*) RebinnedData->Clone();
      temp = getHisto(ISR410482, chan+"meas"+var+"106"); // ISR down
      newttbar = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      data->Add(newttbar,-1);

      // Unfold!
      unfold_data = RooUnfoldBayes(&Response, data, iter);
      // Get unfolded data and push it to map
      unfolded = unfold_data.Hreco();
      for (int i=1; i<nBins+1; i++){
        unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
        unfolded->SetBinError(i,0.);
      }
      if (plot.Contains("ZJpT")) unfolded->Rebin(3);
      copy = (TH1F*) unfolded->Clone(chan+var+"154"+"_"+iter); // 1st of pair
      copy->Write();

      // 155-156 alpha_S (FSR) : unfolded[107-108]
      copy = (TH1F*) ((TH1F*) Unfolded["107"])->Clone(chan+var+"155"+"_"+iter); // 1st of pair
      // Renormalize to nominal
      for (int i=1; i<nBins+1; i++)
        if (nominal->GetBinContent(i) > 0)
          copy->SetBinContent(i,nominalttbar->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));
      copy->Write();

      copy = (TH1F*) ((TH1F*) Unfolded["108"])->Clone(chan+var+"156"+"_"+iter); // 2nd of pair
      // Renormalize to nominal
      for (int i=1; i<nBins+1; i++)
        if (nominal->GetBinContent(i) > 0)
          copy->SetBinContent(i,nominalttbar->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));
      copy->Write();
    }
  }

  Hadronization->Close();
  Matching->Close();
  extra410472->Close();
  ISR410472->Close();
  ISR410482->Close();
  Data->Close();
  Sherpa->Close();
  SingleTop->Close();
  WJets->Close();
  Ztt->Close();
  Diboson->Close();
  newfile.Close();
  return;
}

void copySherpa(TFile* file, TEnv* settings){
  double Totlumi = settings->GetValue("luminosity", 0.0);
  TH1::AddDirectory(kFALSE);
  Str filePath = settings->GetValue("filePath.extras", " ");
  TFile *extraSherpa = openFile(filePath + "SherpaZJets.root");
  StrV channels = vectorize(settings->GetValue("channels", " "));
  StrV channels2 = vectorize(settings->GetValue("channelsFull", " "));


  std::cout << "Starting scaling of " << file->GetName() << std::endl;
  TFile hisfile("SherpaZJets_expanded.root","RECREATE");
  TH1F* temp = new TH1F();
  TH1F* copy = new TH1F();
  StrV plotList = vectorize(settings->GetValue("Controls", " "));
  // Copy all the control histos back in new file, and copy the ones that are unchanged.
  for (auto plot : plotList){
    for (auto chan : channels){
      for (int sys=0; sys<159; sys++){ // 0 - 134 : Experimental, 135-142 are Sherpa theory
        if (sys < 135)
          temp = getHisto(file, chan+plot+std::to_string(sys));
        else
          temp = getHisto(file, chan+plot+"0");
        copy = (TH1F*) temp->Clone(chan+plot+std::to_string(sys));
        copy->Write();
      }
    }
  } // end of plotList


  //////////////////////////////////////////////////
  //// Now we begin the unfolded uncerainties! ////
  //////////////////////////////////////////////////
  // Get Nominal files for unfolding
  Str nominalPath = settings->GetValue("filePath.nominal", " ");
  TFile *Data = openFile(nominalPath + "data.root");
  TFile *TopQuark = openFile(nominalPath + "TopQuark.root");
  TFile *WJets = openFile(nominalPath + "WJets.root");
  TFile *Ztt = openFile(nominalPath + "Ztt.root");
  TFile *Diboson = openFile(nominalPath + "Diboson.root");

  // Define output file
  TFile newfile("UnfoldedTheory_sherpa.root","RECREATE");
  StrV respList = vectorize(settings->GetValue("PlotList", " "));

  // Define maps to store the ttbar unfolded histograms.
  HistMap Unfolded;

  for (int iter=1; iter<6; iter++)
  for (auto plot : respList){
    Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
    TString chan = "hee";
    // Reset unfolded histograms.
    Unfolded.clear();
    for (int i=0; i<111; i++){
      TString label = std::to_string(i);
      Unfolded[label] = new TH1F();
    }
    // if (iter!=2) continue;

    // First we evaluate the muon nominal cross section!
    TH1F* nominalData = getHisto(Data, "hmmdata"+var+"0");
    temp = getHisto(TopQuark, "hmmmeas"+var+"0");
    nominalData->Add(temp,-1.);
    temp = getHisto(WJets, "hmmmeas"+var+"0");
    nominalData->Add(temp,-1.);
    temp = getHisto(Diboson, "hmmmeas"+var+"0");
    nominalData->Add(temp,-1.);
    temp = getHisto(Ztt, "hmmmeas"+var+"0");
    nominalData->Add(temp,-1.);

    // Rebin data - BG
    TH1F* RebinnedData = (TH1F*) (rebin1DHisto((TH1F*) nominalData->Clone(), plot))->Clone();
    int nBins = RebinnedData->GetNbinsX();

    // Get Nominal sherpa response matrix.
    TH2F* RMatrix = getHisto2(file, "hmmresp"+var+"0");
    temp = getHisto(file,"hmmmeas"+var+"0");
    TH1F* SherpaMeas = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
    temp = getHisto(file,"hmmtrue"+var+"0");
    TH1F* SherpaTrue = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
    TH2F* RebinnedMatrix = (TH2F*) (rebin2DHisto((TH2F*) RMatrix->Clone(), plot))->Clone();

    // Define response matrix!
    RooUnfoldResponse Response (SherpaMeas,SherpaTrue,RebinnedMatrix);

    // Unfold!
    RooUnfoldBayes unfold_data (&Response, RebinnedData, iter);
    // Get unfolded data and push it to map
    auto* unfolded = unfold_data.Hreco();
    TH1F* muonChannel = (TH1F*) SherpaMeas->Clone();
    TH1F* electronChannel = (TH1F*) SherpaMeas->Clone();
    for (int i=1; i<nBins+1; i++){
      muonChannel->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
      muonChannel->SetBinError(i,0.);
    }
    if (plot.Contains("ZJpT")) muonChannel->Rebin(3);

    /// NOW DO THE ELECTRON UNFOLDING PART!

    // Get nominal data - TopQuark - WJets - Diboson - Ztt
    nominalData = getHisto(Data, chan+"data"+var+"0");
    temp = getHisto(TopQuark, chan+"meas"+var+"0");
    nominalData->Add(temp,-1.);
    temp = getHisto(WJets, chan+"meas"+var+"0");
    nominalData->Add(temp,-1.);
    temp = getHisto(Diboson, chan+"meas"+var+"0");
    nominalData->Add(temp,-1.);
    temp = getHisto(Ztt, chan+"meas"+var+"0");
    nominalData->Add(temp,-1.);

    // Rebin data
    RebinnedData = (TH1F*) (rebin1DHisto((TH1F*) nominalData->Clone(), plot))->Clone();
    nBins = RebinnedData->GetNbinsX();

    // Get Nominal electron sherpa response matrix.
    RMatrix = getHisto2(file, "heeresp"+var+"0");
    RebinnedMatrix = (TH2F*) (rebin2DHisto((TH2F*) RMatrix->Clone(), plot))->Clone();
    temp = getHisto(file,"heemeas"+var+"0");
    SherpaMeas = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
    temp = getHisto(file,"heetrue"+var+"0");
    SherpaTrue = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();

    // Define response matrix!
    Response = RooUnfoldResponse(SherpaMeas,SherpaTrue,RebinnedMatrix);
    // Unfold!
    unfold_data = RooUnfoldBayes(&Response, RebinnedData, iter);
    // Get unfolded data and push it to map
    unfolded = unfold_data.Hreco();
    for (int i=1; i<nBins+1; i++){
      electronChannel->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
      electronChannel->SetBinError(i,0.);
    }
    if (plot.Contains("ZJpT")) electronChannel->Rebin(3);


    // We need to unfold with the measured & response matrix of each systematic!
    for (int sys=0; sys<111; sys++){
      TString label = std::to_string(sys);

      // Get Truth Sherpa Matrix
      temp = getHisto(extraSherpa,chan+"true"+var+sys);
      SherpaTrue = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();

      // Get Response matrix of systematic & rebin it.
      RMatrix = getHisto2(extraSherpa, chan+"resp"+var+sys);
      RebinnedMatrix = (TH2F*) (rebin2DHisto((TH2F*) RMatrix->Clone(), plot))->Clone();

      temp = getHisto(extraSherpa,chan+"meas"+var+sys);
      SherpaMeas = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();

      // Define response matrix!
      Response = RooUnfoldResponse(SherpaMeas,SherpaTrue,RebinnedMatrix);

      // Unfold!
      TH1F* newdata = (TH1F*) RebinnedData->Clone();
      unfold_data = RooUnfoldBayes(&Response, newdata, iter);
      // Get unfolded data and push it to map
      unfolded = unfold_data.Hreco();
      for (int i=1; i<nBins+1; i++){
        // unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
        unfolded->SetBinError(i,0.);
      }
      if (plot.Contains("ZJpT")) unfolded->Rebin(3);
      Unfolded[label] = ((TH1F*) unfolded->Clone());
    }

    // 135-136 Sherpa PDF from 100 set variations
    TH1F* nominal = (TH1F*) ((TH1F*) Unfolded["0"])->Clone(); // nominal PDF
    nBins = nominal->GetNbinsX();

    // for (int i=1; i<19; i++)
    // std::cout << "from nominal ratio = " << electronChannel->GetBinContent(i)*nominal->GetBinWidth(i)*Totlumi/nominal->GetBinContent(i) << std::endl;
    // std::cout << "from zero    = " << nominal->GetBinContent(18)/(nominal->GetBinWidth(18)*Totlumi) << std::endl;
    // abort();

    std::vector<double> max, min, mean;
    for (int i=0; i<nBins; i++){
      mean.push_back(0.);
      max.push_back(0.);
      min.push_back(0.);
    }
    TH1F* Var = new TH1F();
    // Evaluate the mean
    for (int sys=1; sys<101; sys++){
      Var = (TH1F*) ((TH1F*) Unfolded[std::to_string(sys)])->Clone();
      for (int i=1; i<nBins+1; i++){
        mean[i-1] = mean[i-1] + Var->GetBinContent(i)/100.;
      }
    }
    // for (int i=1; i<nBins+1; i++){
    //   std::cout << "Bin Content " << i << " mean = " << mean[i-1] << std::endl;
    //   std::cout << "Bin content " << i << " nomi = " << nominal->GetBinContent(i) << std::endl;
    // }
    // Find the variations!
    for (int sys=1; sys<101; sys++){
      Var = (TH1F*) ((TH1F*) Unfolded[std::to_string(sys)])->Clone();
      double val = 0.;
      for (int i=1; i<nBins+1; i++){
        val = Var->GetBinContent(i) - nominal->GetBinContent(i);
        if (std::abs(val) > 2.5*std::sqrt(nominal->GetBinContent(i))) continue;
        if (val < 0) min[i-1] = min[i-1] + val*val;
        else if (val > 0) max[i-1] = max[i-1] + val*val;
      }
    }
    copy = (TH1F*) nominal->Clone(chan+var+"135"+"_"+iter); // 1st of pair: max
    for (int i=1; i<nBins+1; i++)
      copy->SetBinContent(i,nominal->GetBinContent(i)+std::sqrt(max[i-1]/99));
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (nominal->GetBinContent(i)>0)
        copy->SetBinContent(i,electronChannel->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));
    copy->Write();

    TH1F* muoncopy = (TH1F*) muonChannel->Clone("hmm"+var+"135"+"_"+iter); // 1st of hmm pair: max
    double ratio = 1.;
    for (int i=1; i<nBins+1; i++){
      if (nominal->GetBinContent(i) > 0)
        ratio = copy->GetBinContent(i) / electronChannel->GetBinContent(i);
      else ratio = 0.;
      muoncopy->SetBinContent(i, muonChannel->GetBinContent(i) * ratio);
    }
    muoncopy->Write();

    copy = (TH1F*) nominal->Clone(chan+var+"136"+"_"+iter); // 2nd of pair: min
    for (int i=1; i<nBins+1; i++)
      copy->SetBinContent(i,nominal->GetBinContent(i)-std::sqrt(min[i-1]/99));
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (nominal->GetBinContent(i)>0)
        copy->SetBinContent(i,electronChannel->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));
    copy->Write();

    muoncopy = (TH1F*) muonChannel->Clone("hmm"+var+"136"+"_"+iter); // 1st of hmm pair: max
    ratio = 1.;
    for (int i=1; i<nBins+1; i++){
      if (nominal->GetBinContent(i) > 0)
        ratio = copy->GetBinContent(i) / electronChannel->GetBinContent(i);
      else ratio = 0.;
      muoncopy->SetBinContent(i, muonChannel->GetBinContent(i) * ratio);
    }
    muoncopy->Write();

    // 137-138 Sherpa PDF from choice of PDF set
    copy = (TH1F*) ((TH1F*) Unfolded["101"])->Clone(chan+var+"137"+"_"+iter); // nominal PDF
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (nominal->GetBinContent(i)>0)
        copy->SetBinContent(i,electronChannel->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));

    // Renormalize to nominal on 2nd to last bin
    double val = copy->GetBinContent(nBins-1)/electronChannel->GetBinContent(nBins-1);
    double before = copy->GetBinContent(nBins-2)/electronChannel->GetBinContent(nBins-2);
    double after = copy->GetBinContent(nBins)/electronChannel->GetBinContent(nBins);
    if (val<1. && (before > 1. && after > 1.)) copy->SetBinContent(nBins-1, 0.5*(before+after)*electronChannel->GetBinContent(nBins-1));
    else if (val>1. && (before< 1. && after < 1.)) copy->SetBinContent(nBins-1, 0.5*(before+after)*electronChannel->GetBinContent(nBins-1));

    copy->Write();

    muoncopy = (TH1F*) muonChannel->Clone("hmm"+var+"137"+"_"+iter); // 1st of hmm pair: max
    ratio = 1.;
    for (int i=1; i<nBins+1; i++){
      if (nominal->GetBinContent(i) > 0)
        ratio = copy->GetBinContent(i) / electronChannel->GetBinContent(i);
      else ratio = 0.;
      muoncopy->SetBinContent(i, muonChannel->GetBinContent(i) * ratio);
    }
    muoncopy->Write();

    copy = (TH1F*) ((TH1F*) Unfolded["102"])->Clone(chan+var+"138"+"_"+iter); // nominal PDF
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (nominal->GetBinContent(i)>0)
        copy->SetBinContent(i,electronChannel->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));

    // Renormalize to nominal on 2nd to last bin
    val = copy->GetBinContent(nBins-1)/electronChannel->GetBinContent(nBins-1);
    before = copy->GetBinContent(nBins-2)/electronChannel->GetBinContent(nBins-2);
    after = copy->GetBinContent(nBins)/electronChannel->GetBinContent(nBins);
    if (val<1. && (before > 1. && after > 1.)) copy->SetBinContent(nBins-1, 0.5*(before+after)*electronChannel->GetBinContent(nBins-1));
    else if (val>1. && (before< 1. && after < 1.)) copy->SetBinContent(nBins-1, 0.5*(before+after)*electronChannel->GetBinContent(nBins-1));

    copy->Write();

    muoncopy = (TH1F*) muonChannel->Clone("hmm"+var+"138"+"_"+iter); // 1st of hmm pair: max
    ratio = 1.;
    for (int i=1; i<nBins+1; i++){
      if (nominal->GetBinContent(i) > 0)
        ratio = copy->GetBinContent(i) / electronChannel->GetBinContent(i);
      else ratio = 0.;
      muoncopy->SetBinContent(i, muonChannel->GetBinContent(i) * ratio);
    }
    muoncopy->Write();

    // 139-140 Sherpa PDF alpha_S
    nominal = (TH1F*) ((TH1F*) Unfolded["0"])->Clone(); // nominal PDF
    TH1F* alphaS_high = (TH1F*) ((TH1F*) Unfolded["103"])->Clone(); // alphaS high
    TH1F* alphaS_low = (TH1F*) ((TH1F*) Unfolded["104"])->Clone(); // alphaS low
    alphaS_high->Add(alphaS_low,-1.);

    copy = (TH1F*) nominal->Clone(chan+var+"139"+"_"+iter); // 1st of pair: max
    for (int i=1; i<nBins+1; i++)
      copy->SetBinContent(i,nominal->GetBinContent(i)+0.5*alphaS_high->GetBinContent(i));
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (nominal->GetBinContent(i)>0)
        copy->SetBinContent(i,electronChannel->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));
    copy->Write();

    muoncopy = (TH1F*) muonChannel->Clone("hmm"+var+"139"+"_"+iter); // 1st of hmm pair: max
    ratio = 1.;
    for (int i=1; i<nBins+1; i++){
      if (nominal->GetBinContent(i) > 0)
        ratio = copy->GetBinContent(i) / electronChannel->GetBinContent(i);
      else ratio = 0.;
      muoncopy->SetBinContent(i, muonChannel->GetBinContent(i) * ratio);
    }
    muoncopy->Write();

    copy = (TH1F*) nominal->Clone(chan+var+"140"+"_"+iter); // 2nd of pair: min
    for (int i=1; i<nBins+1; i++)
      copy->SetBinContent(i,nominal->GetBinContent(i)-0.5*alphaS_high->GetBinContent(i));
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (nominal->GetBinContent(i)>0)
        copy->SetBinContent(i,electronChannel->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));
    copy->Write();

    muoncopy = (TH1F*) muonChannel->Clone("hmm"+var+"140"+"_"+iter); // 2nd of hmm pair: min
    ratio = 1.;
    for (int i=1; i<nBins+1; i++){
      if (nominal->GetBinContent(i) > 0)
        ratio = copy->GetBinContent(i) / electronChannel->GetBinContent(i);
      else ratio = 0.;
      muoncopy->SetBinContent(i, muonChannel->GetBinContent(i) * ratio);
    }
    muoncopy->Write();

    // 141-142 Sherpa Scale
    TH1F* up = (TH1F*) nominal->Clone();
    TH1F* down = (TH1F*) nominal->Clone();
    for (int sys=105; sys<111; sys++){
      temp = (TH1F*) ((TH1F*) Unfolded[std::to_string(sys)])->Clone();
      for (int i=1; i<nBins+1; i++){
        double diff = temp->GetBinContent(i) - nominal->GetBinContent(i);
        // if (std::abs(diff) > 2.5*std::sqrt(nominal->GetBinContent(i))) continue;
        if (up->GetBinContent(i) < temp->GetBinContent(i)) up->SetBinContent(i,temp->GetBinContent(i));
        if (down->GetBinContent(i) > temp->GetBinContent(i)) down->SetBinContent(i,temp->GetBinContent(i));
      }
    }

    copy = (TH1F*) up->Clone(chan+var+"141"+"_"+iter); // 1st of hee scale pair: max
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (nominal->GetBinContent(i)>0)
        copy->SetBinContent(i,electronChannel->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));

    // Renormalize to nominal on 2nd to last bin. Uncertainty smoothing
    if (plot == "pTZ" || plot == "HT"){
      val = copy->GetBinContent(nBins-1)/electronChannel->GetBinContent(nBins-1);
      before = copy->GetBinContent(nBins-2)/electronChannel->GetBinContent(nBins-2);
      after = copy->GetBinContent(nBins)/electronChannel->GetBinContent(nBins);
      copy->SetBinContent(nBins-1, 0.5*(before+after)*electronChannel->GetBinContent(nBins-1));
    }
    // val = copy->GetBinContent(nBins-1)/electronChannel->GetBinContent(nBins-1);
    // before = copy->GetBinContent(nBins-2)/electronChannel->GetBinContent(nBins-2);
    // after = copy->GetBinContent(nBins)/electronChannel->GetBinContent(nBins);
    // if (val<1. && (before > 1. && after > 1.)) copy->SetBinContent(nBins-1, 0.5*(before+after)*electronChannel->GetBinContent(nBins-1));
    // else if (val>1. && (before< 1. && after < 1.)) copy->SetBinContent(nBins-1, 0.5*(before+after)*electronChannel->GetBinContent(nBins-1));

    copy->Write();

    muoncopy = (TH1F*) muonChannel->Clone("hmm"+var+"141"+"_"+iter); // 1st of hee scale pair: max
    ratio = 1.;
    for (int i=1; i<nBins+1; i++){
      if (nominal->GetBinContent(i) > 0)
        ratio = copy->GetBinContent(i) / electronChannel->GetBinContent(i);
      else ratio = 0.;
      muoncopy->SetBinContent(i, muonChannel->GetBinContent(i) * ratio);
    }
    muoncopy->Write();

    copy = (TH1F*) down->Clone(chan+var+"142"+"_"+iter); // 2nd of hee scale pair: min
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (nominal->GetBinContent(i)>0)
        copy->SetBinContent(i,electronChannel->GetBinContent(i)*copy->GetBinContent(i)/nominal->GetBinContent(i));

    // Renormalize to nominal on 2nd to last bin. Uncertainty smoothing
    if (plot == "pTZ" || plot == "HT"){
      val = copy->GetBinContent(nBins-1)/electronChannel->GetBinContent(nBins-1);
      before = copy->GetBinContent(nBins-2)/electronChannel->GetBinContent(nBins-2);
      after = copy->GetBinContent(nBins)/electronChannel->GetBinContent(nBins);
      copy->SetBinContent(nBins-1, 0.5*(before+after)*electronChannel->GetBinContent(nBins-1));
    }
    // val = copy->GetBinContent(nBins-1)/electronChannel->GetBinContent(nBins-1);
    // before = copy->GetBinContent(nBins-2)/electronChannel->GetBinContent(nBins-2);
    // after = copy->GetBinContent(nBins)/electronChannel->GetBinContent(nBins);
    // if (val<1. && (before > 1. && after > 1.)) copy->SetBinContent(nBins-1, 0.5*(before+after)*electronChannel->GetBinContent(nBins-1));
    // else if (val>1. && (before< 1. && after < 1.)) copy->SetBinContent(nBins-1, 0.5*(before+after)*electronChannel->GetBinContent(nBins-1));

    copy->Write();

    muoncopy = (TH1F*) muonChannel->Clone("hmm"+var+"142"+"_"+iter); // 2nd of hee scale pair: min
    ratio = 1.;
    for (int i=1; i<nBins+1; i++){
      if (nominal->GetBinContent(i) > 0)
        ratio = copy->GetBinContent(i) / electronChannel->GetBinContent(i);
      else ratio = 0.;
      muoncopy->SetBinContent(i, muonChannel->GetBinContent(i) * ratio);
    }
    muoncopy->Write();
  }

  extraSherpa->Close();
  Data->Close();
  TopQuark->Close();
  WJets->Close();
  Ztt->Close();
  Diboson->Close();
  newfile.Close();


} // end of function

void copyNominal(TFile* file, TEnv* settings){
  TH1::AddDirectory(kFALSE);

  std::cout << "Starting scaling of " << file->GetName() << std::endl;
  TString newname = file->GetName();
  TString filename;
  if (newname.Contains("WJets")) filename = "WJets_expanded.root";
  else if (newname.Contains("Diboson")) filename = "Diboson_expanded.root";
  else if (newname.Contains("data")) filename = "data_expanded.root";
  else if (newname.Contains("SingleTop")) filename = "SingleTop_expanded.root";
  else fatal("Forgot a process case in naming :: copyNominal");
  TFile hisfile(filename,"RECREATE");
  TH1F* temp = new TH1F();
  TH1F* copy = new TH1F();
  StrV plotList = vectorize(settings->GetValue("Controls", " "));
  for (auto plot : plotList){
    for (int sys=0; sys<159; sys++){
      if (sys < 135)
        temp = getHisto(file, "hem"+plot+std::to_string(sys));
      else temp = getHisto(file, "hem"+plot+"0");
      // std::cout << "control = " << plot << std::endl;
      // std::cout << temp->Integral() << std::endl;
      copy = (TH1F*) temp->Clone("hem"+plot+std::to_string(sys));
      copy->Write();
      if (sys < 135)
        temp = getHisto(file, "hee"+plot+std::to_string(sys));
      else temp = getHisto(file, "hee"+plot+"0");
      // std::cout << "control = " << plot << std::endl;
      // std::cout << temp->Integral() << std::endl;
      copy = (TH1F*) temp->Clone("hee"+plot+std::to_string(sys));
      copy->Write();
      if (sys < 135)
        temp = getHisto(file, "hmm"+plot+std::to_string(sys));
      else temp = getHisto(file, "hmm"+plot+"0");
      // std::cout << "control = " << plot << std::endl;
      // std::cout << temp->Integral() << std::endl;
      copy = (TH1F*) temp->Clone("hmm"+plot+std::to_string(sys));
      copy->Write();
    }
  }
  StrV respList = vectorize(settings->GetValue("Response", " "));
  TH2F* temp2 = new TH2F();
  TH2F* copy2 = new TH2F();
  for (auto plot: respList){
    for (int sys=0; sys<159; sys++){
      if (sys==0){
        temp = getHisto(file, "heetrue"+plot+"0");
        temp->Write();
        temp = getHisto(file, "hmmtrue"+plot+"0");
        temp->Write();
      }
      if (sys < 135)
        temp2 = getHisto2(file, "heeresp"+plot+std::to_string(sys));
      else temp2 = getHisto2(file, "heeresp"+plot+"0");
      copy2 = (TH2F*) temp2->Clone("heeresp"+plot+std::to_string(sys));
      copy2->Write();
      if (sys < 135)
        temp2 = getHisto2(file, "hmmresp"+plot+std::to_string(sys));
      else temp2 = getHisto2(file, "hmmresp"+plot+"0");
      copy2 = (TH2F*) temp2->Clone("hmmresp"+plot+std::to_string(sys));
      copy2->Write();
    }
  }
  std::cout << "Finished Scaling file " << file->GetName() << std::endl;
  hisfile.Close();
  return;
}

void copyNominalMG(TFile* file, TEnv* settings){
  TH1::AddDirectory(kFALSE);

  std::cout << "Starting scaling of " << file->GetName() << std::endl;
  TFile hisfile("MGZJets_expanded.root","RECREATE");
  TH1F* temp = new TH1F();
  TH1F* copy = new TH1F();
  StrV plotList = vectorize(settings->GetValue("Controls", " "));
  for (auto plot : plotList){
    for (int sys=0; sys<159; sys++){
      if (sys < 135)
        temp = getHisto(file, "hem"+plot+std::to_string(sys));
      else temp = getHisto(file, "hem"+plot+"0");
      // std::cout << "control = " << plot << std::endl;
      // std::cout << temp->Integral() << std::endl;
      copy = (TH1F*) temp->Clone("hem"+plot+std::to_string(sys));
      copy->Write();
      if (sys < 135)
        temp = getHisto(file, "hee"+plot+std::to_string(sys));
      else temp = getHisto(file, "hee"+plot+"0");
      // std::cout << "control = " << plot << std::endl;
      // std::cout << temp->Integral() << std::endl;
      copy = (TH1F*) temp->Clone("hee"+plot+std::to_string(sys));
      copy->Write();
      if (sys < 135)
        temp = getHisto(file, "hmm"+plot+std::to_string(sys));
      else temp = getHisto(file, "hmm"+plot+"0");
      // std::cout << "control = " << plot << std::endl;
      // std::cout << temp->Integral() << std::endl;
      copy = (TH1F*) temp->Clone("hmm"+plot+std::to_string(sys));
      copy->Write();
    }
  }
  StrV respList = vectorize(settings->GetValue("Response", " "));
  TH2F* temp2 = new TH2F();
  TH2F* copy2 = new TH2F();
  for (auto plot: respList){
    for (int sys=0; sys<159; sys++){
      if (sys==0){
        temp = getHisto(file, "heetrue2"+plot+"0");
        temp->Write();
        temp = getHisto(file, "hmmtrue2"+plot+"0");
        temp->Write();
      }
      if (sys < 135)
        temp2 = getHisto2(file, "heeresp2"+plot+std::to_string(sys));
      else temp2 = getHisto2(file, "heeresp2"+plot+"0");
      copy2 = (TH2F*) temp2->Clone("heeresp2"+plot+std::to_string(sys));
      copy2->Write();
      if (sys < 135)
        temp2 = getHisto2(file, "hmmresp2"+plot+std::to_string(sys));
      else temp2 = getHisto2(file, "hmmresp2"+plot+"0");
      copy2 = (TH2F*) temp2->Clone("hmmresp2"+plot+std::to_string(sys));
      copy2->Write();
    }
  }
  std::cout << "Finished Scaling file " << file->GetName() << std::endl;
  hisfile.Close();
  return;
}

void doUnfoldingJetpT(TEnv* settings){
  double Totlumi = settings->GetValue("luminosity", 0.0);
  TH1::AddDirectory(kFALSE);

  std::cout << "Starting Jet pt Unfolding uncertainty!"<< std::endl;
  Double_t binsVec[11];
  binsVec[0]=0.0;
  binsVec[1]=0.1;
  binsVec[2]=0.2;
  binsVec[3]=0.3;
  binsVec[4]=0.4;
  binsVec[5]=0.5;
  binsVec[6]=0.6;
  binsVec[7]=0.8;
  binsVec[8]=1.0;
  binsVec[9]=1.2;
  binsVec[10]=1.5;
  TH1F* Temp = new TH1F("temp","temp",10, binsVec);


  //////////////////////////////////////////////////
  //// Now we begin the unfolded uncerainties! ////
  //////////////////////////////////////////////////
  // Get Nominal files for unfolding

  Str nominalPath = settings->GetValue("filePath.nominal", " ");
  TFile *Data = openFile(nominalPath + "data.root");
  TFile *Sherpa = openFile(nominalPath + "SherpaZJets.root");
  TFile *TopQuark = openFile(nominalPath + "TopQuark.root");
  TFile *WJets = openFile(nominalPath + "WJets.root");
  TFile *Ztt = openFile(nominalPath + "Ztt.root");
  TFile *Diboson = openFile(nominalPath + "Diboson.root");
  // Get truth reweighted sherpa
  TFile *extraSherpa = openFile(nominalPath + "unf/SherpaZJets.root");

  // Define output file
  TFile newfile("UnfoldingUncertainty_JetpT.root","RECREATE");

  StrV RespChan = vectorize(settings->GetValue("Channels", " "));
  StrV respList = vectorize(settings->GetValue("PlotList", " "));

  TH1F* temp = new TH1F();

  for (auto plot : respList){
    Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
    for (auto chan : RespChan){
      TCanvas *can = new TCanvas("", "", 800, 800);

      // Get Nominal sherpa response matrix.
      TH2F* RMatrix = getHisto2(Sherpa, chan+"resp"+var+"0");
      temp = getHisto(Sherpa,chan+"meas"+var+"0");
      TH1F* SherpaMeas = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      temp = getHisto(Sherpa,chan+"true"+var+"0");
      TH1F* SherpaTrue = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      // Rebin Sherpa Response matrix
      TH2F* RebinnedMatrix = (TH2F*) (rebin2DHisto((TH2F*) RMatrix->Clone(), plot))->Clone();

      // Define response matrix!
      RooUnfoldResponse Response (SherpaMeas,SherpaTrue,RebinnedMatrix);

      // Unfold truth-reighted sherpa!
      temp = getHisto(extraSherpa, chan+"meas"+var+"0"); // Reweighted sherpa
      TH1F* NewSherpa = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      int nBins = NewSherpa->GetNbinsX();

      // How does the unfolded sherpa compare to its truth.
      temp = getHisto(extraSherpa, chan+"true"+var+"0"); // Reweighted sherpa
      TH1F* truth = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      if (plot.Contains("ZJpT")) truth->Rebin(3);

      if (plot.Contains("ZJpT")) SherpaTrue->Rebin(3);
      for (int i=1; i<nBins+1; i++){
        if (plot.Contains("ZJpT"))
          SherpaTrue->SetBinContent(i,3*SherpaTrue->GetBinContent(i)/(SherpaTrue->GetBinWidth(i)*Totlumi));
        else SherpaTrue->SetBinContent(i,SherpaTrue->GetBinContent(i)/(SherpaTrue->GetBinWidth(i)*Totlumi));
        SherpaTrue->SetBinError(i,0.);
      }


      // get the data to unfold
      TH1F* data = getHisto(Data,chan+"data"+var+"0");
      temp = getHisto(Diboson,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(TopQuark,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(WJets,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(Ztt,chan+"meas"+var+"0");
      data->Add(temp,-1.);

      TH1F* NewData = (TH1F*) (rebin1DHisto((TH1F*) data->Clone(), plot))->Clone();

      for (int iter=1; iter<6; iter++){
        // if (iter !=2) continue;
        // Unfold!
        RooUnfoldBayes unfold_data (&Response, NewSherpa, iter);
        auto* unfolded = unfold_data.Hreco();
        if (plot.Contains("ZJpT")) unfolded->Rebin(3);
        TH1F* Uncertainty = (TH1F*) unfolded->Clone();

        // By how much does the unfolding affect "sherpa data"
        for (int i=1; i<nBins+1; i++)
          Uncertainty->SetBinContent(i, Uncertainty->GetBinContent(i) / truth->GetBinContent(i));

        // Unfold the data
        unfold_data = RooUnfoldBayes(&Response, NewData, iter);
        // Get unfolded data and push it to map
        unfolded = unfold_data.Hreco();
        for (int i=1; i<nBins+1; i++){
          unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
          unfolded->SetBinError(i,0.);
        }
        if (plot.Contains("ZJpT")) unfolded->Rebin(3);
        TH1F* unfoldedData = (TH1F*) unfolded->Clone();

        TH1F* copy = (TH1F*) unfolded->Clone(chan+"unf"+var+"_"+iter+"JetpT");
        for (int i=1; i<nBins+1; i++)
          copy->SetBinContent(i, copy->GetBinContent(i) * Uncertainty->GetBinContent(i));
        copy->Write();

        TH1F* truth_copy = (TH1F*) truth->Clone();
        for (int i=1; i<nBins+1; i++){
          if (plot.Contains("ZJpT"))
            truth_copy->SetBinContent(i,3*truth->GetBinContent(i)/(truth->GetBinWidth(i)*Totlumi));
          else truth_copy->SetBinContent(i,truth->GetBinContent(i)/(truth->GetBinWidth(i)*Totlumi));
          truth_copy->SetBinError(i,0.);
        }


        // Create plots comparing truth level reweighted vs nominal unfolded data.
        can->Clear();
        double ratio = 0.36, margin = 0.02;
        can->cd();
        gPad->SetBottomMargin(ratio);
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        can->SetRightMargin(0.04);
        // Do some plotting
        Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
        Str xsLabel = settings->GetValue(plot + ".xs", " ");

        unfoldedData->SetTitle(";"+Xlabel+";"+xsLabel);
        // Not log plots
        if (plot.Contains("ZJpT")){
          gPad->SetLogy(0);
          unfoldedData->SetMaximum(unfoldedData->GetMaximum()*2.0);
          unfoldedData->SetMinimum(unfoldedData->GetMinimum()*.5);
        }
        else{
          gPad->SetLogy(1);
          if(unfoldedData->GetMinimum() > 100)
            unfoldedData->SetMinimum(unfoldedData->GetMinimum()*0.05);
          unfoldedData->SetMaximum(unfoldedData->GetMaximum()*100);
        }
        if (plot.Contains("NJets")) unfoldedData->SetMaximum(unfoldedData->GetMaximum()*5.); // Scale it up by 5 to help plotting
        unfoldedData->GetXaxis()->SetLabelSize(0.);
        unfoldedData->SetLineColor(kBlack);
        truth_copy->SetMarkerStyle(24); // Circle
        truth_copy->SetMarkerColor(857); // Nice Blue
        truth_copy->SetLineColor(857); // Nice Blue
        unfoldedData->Draw();
        truth_copy->Draw("SAME");

        TLegend* leg = new TLegend(0.65, 0.70, 0.85, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.020);
        leg->SetFillStyle(0);

        leg->AddEntry(data,"Unfolded data","pe");
        leg->AddEntry(truth_copy,"Truth reweighed Sherpa2.2.1","pelf");
        leg->Draw("same");
        drawPlotInfo(0.15,0.9, plot, chan, Totlumi, 0.025);

        TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        p->SetTopMargin(1.0 - ratio);
        p->SetFillStyle(0);
        p->SetTicks();
        p->SetGridy();
        p->Draw();
        p->cd();

        TH1F* Ratio = (TH1F*) truth_copy->Clone();
        Ratio->Divide(unfoldedData);
        if (plot.Contains("ZJpT")){
          for (int i=1; i<11; i++){
            Temp->SetBinContent(i, Ratio->GetBinContent(i));
            Temp->SetBinError(i, Ratio->GetBinError(i));
          }
          Ratio = (TH1F*) Temp->Clone();
          Ratio->SetLineColor(857); // Nice Blue
        }
        gStyle->SetErrorX(0.5);
        Ratio->GetYaxis()->SetLabelSize(0.03);
        Ratio->GetYaxis()->SetTitle("Pred./Data");
        Ratio->GetXaxis()->SetTitle(Xlabel);
        Ratio->GetXaxis()->SetLabelSize(0.03);
        Ratio->GetXaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetRangeUser(0.7, 2.0);
        // Ratio->Draw("hist");
        Ratio->Draw("E");
        if (iter == 2) can->Print("unfolding_JetpT_Ratio_"+chan+plot+".png");

        // Create plots comparing truth level NOT REWEIGHED vs nominal unfolded data.
        can->Clear();
        can->cd();
        gPad->SetBottomMargin(ratio);
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        can->SetRightMargin(0.04);
        if (plot.Contains("ZJpT")){
          gPad->SetLogy(0);
        }
        else{
          gPad->SetLogy(1);
        }

        SherpaTrue->SetMarkerStyle(24); // Circle
        SherpaTrue->SetMarkerColor(857); // Nice Blue
        SherpaTrue->SetLineColor(857); // Nice Blue
        unfoldedData->Draw();
        SherpaTrue->Draw("SAME");

        leg = new TLegend(0.65, 0.70, 0.85, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.020);
        leg->SetFillStyle(0);

        leg->AddEntry(data,"Unfolded data","pe");
        leg->AddEntry(SherpaTrue,"Truth original Sherpa2.2.1","pelf");
        leg->Draw("same");
        drawPlotInfo(0.15,0.9, plot, chan, Totlumi, 0.025);

        p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        p->SetTopMargin(1.0 - ratio);
        p->SetFillStyle(0);
        p->SetTicks();
        p->SetGridy();
        p->Draw();
        p->cd();

        Ratio = (TH1F*) SherpaTrue->Clone();
        Ratio->Divide(unfoldedData);
        if (plot.Contains("ZJpT")){
          for (int i=1; i<11; i++){
            Temp->SetBinContent(i, Ratio->GetBinContent(i));
            Temp->SetBinError(i, Ratio->GetBinError(i));
          }
          Ratio = (TH1F*) Temp->Clone();
          Ratio->SetLineColor(857); // Nice Blue
        }

        gStyle->SetErrorX(0.5);
        Ratio->GetYaxis()->SetLabelSize(0.03);
        Ratio->GetYaxis()->SetTitle("Pred./Data");
        Ratio->GetXaxis()->SetTitle(Xlabel);
        Ratio->GetXaxis()->SetLabelSize(0.03);
        Ratio->GetXaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetRangeUser(0.7, 2.0);
        Ratio->Draw("hist");
        if (iter == 2) can->Print("unfolding_Ratio_"+chan+plot+".png");
      }
    }
  }

  extraSherpa->Close();
  Data->Close();
  Sherpa->Close();
  TopQuark->Close();
  WJets->Close();
  Ztt->Close();
  Diboson->Close();
  newfile.Close();
  return;
} // end of jet pt unfolding unceratinty

void doUnfoldingZpT(TEnv* settings){
  double Totlumi = settings->GetValue("luminosity", 0.0);
  TH1::AddDirectory(kFALSE);

  std::cout << "Starting Z pt Unfolding uncertainty!"<< std::endl;
  Double_t binsVec[11];
  binsVec[0]=0.0;
  binsVec[1]=0.1;
  binsVec[2]=0.2;
  binsVec[3]=0.3;
  binsVec[4]=0.4;
  binsVec[5]=0.5;
  binsVec[6]=0.6;
  binsVec[7]=0.8;
  binsVec[8]=1.0;
  binsVec[9]=1.2;
  binsVec[10]=1.5;
  TH1F* Temp = new TH1F("temp","temp",10, binsVec);


  //////////////////////////////////////////////////
  //// Now we begin the unfolded uncerainties! ////
  //////////////////////////////////////////////////
  // Get Nominal files for unfolding

  Str nominalPath = settings->GetValue("filePath.nominal", " ");
  TFile *Data = openFile(nominalPath + "data.root");
  TFile *Sherpa = openFile(nominalPath + "SherpaZJets.root");
  TFile *TopQuark = openFile(nominalPath + "TopQuark.root");
  TFile *WJets = openFile(nominalPath + "WJets.root");
  TFile *Ztt = openFile(nominalPath + "Ztt.root");
  TFile *Diboson = openFile(nominalPath + "Diboson.root");
  // Get truth reweighted sherpa
  TFile *extraSherpa = openFile(nominalPath + "unf/ZPT/SherpaZJets.root");

  // Define output file
  TFile newfile("UnfoldingUncertainty_ZpT.root","RECREATE");

  StrV RespChan = vectorize(settings->GetValue("Channels", " "));
  StrV respList = vectorize(settings->GetValue("PlotList", " "));

  TH1F* temp = new TH1F();

  for (auto plot : respList){
    Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
    for (auto chan : RespChan){
      TCanvas *can = new TCanvas("", "", 800, 800);

      // Get Nominal sherpa response matrix.
      TH2F* RMatrix = getHisto2(Sherpa, chan+"resp"+var+"0");
      temp = getHisto(Sherpa,chan+"meas"+var+"0");
      TH1F* SherpaMeas = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      temp = getHisto(Sherpa,chan+"true"+var+"0");
      TH1F* SherpaTrue = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      // Rebin Sherpa Response matrix
      TH2F* RebinnedMatrix = (TH2F*) (rebin2DHisto((TH2F*) RMatrix->Clone(), plot))->Clone();

      // Define response matrix!
      RooUnfoldResponse Response (SherpaMeas,SherpaTrue,RebinnedMatrix);

      // Unfold truth-reighted sherpa!
      temp = getHisto(extraSherpa, chan+"meas"+var+"0"); // Reweighted sherpa
      TH1F* NewSherpa = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      int nBins = NewSherpa->GetNbinsX();

      // How does the unfolded sherpa compare to its truth.
      temp = getHisto(extraSherpa, chan+"true"+var+"0"); // Reweighted sherpa
      TH1F* truth = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      if (plot.Contains("ZJpT")) truth->Rebin(3);

      if (plot.Contains("ZJpT")) SherpaTrue->Rebin(3);
      for (int i=1; i<nBins+1; i++){
        if (plot.Contains("ZJpT"))
          SherpaTrue->SetBinContent(i,3*SherpaTrue->GetBinContent(i)/(SherpaTrue->GetBinWidth(i)*Totlumi));
        else SherpaTrue->SetBinContent(i,SherpaTrue->GetBinContent(i)/(SherpaTrue->GetBinWidth(i)*Totlumi));
        SherpaTrue->SetBinError(i,0.);
      }


      // get the data to unfold
      TH1F* data = getHisto(Data,chan+"data"+var+"0");
      temp = getHisto(Diboson,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(TopQuark,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(WJets,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(Ztt,chan+"meas"+var+"0");
      data->Add(temp,-1.);

      TH1F* NewData = (TH1F*) (rebin1DHisto((TH1F*) data->Clone(), plot))->Clone();

      for (int iter=1; iter<6; iter++){
        // if (iter !=2) continue;
        // Unfold!
        RooUnfoldBayes unfold_data (&Response, NewSherpa, iter);
        auto* unfolded = unfold_data.Hreco();
        if (plot.Contains("ZJpT")) unfolded->Rebin(3);
        TH1F* Uncertainty = (TH1F*) unfolded->Clone();

        // By how much does the unfolding affect "sherpa data"
        for (int i=1; i<nBins+1; i++)
          Uncertainty->SetBinContent(i, Uncertainty->GetBinContent(i) / truth->GetBinContent(i));

        // Unfold the data
        unfold_data = RooUnfoldBayes(&Response, NewData, iter);
        // Get unfolded data and push it to map
        unfolded = unfold_data.Hreco();
        for (int i=1; i<nBins+1; i++){
          unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
          unfolded->SetBinError(i,0.);
        }
        if (plot.Contains("ZJpT")) unfolded->Rebin(3);
        TH1F* unfoldedData = (TH1F*) unfolded->Clone();

        TH1F* copy = (TH1F*) unfolded->Clone(chan+"unf"+var+"_"+iter+"ZpT");
        for (int i=1; i<nBins+1; i++)
          copy->SetBinContent(i, copy->GetBinContent(i) * Uncertainty->GetBinContent(i));
        copy->Write();

        TH1F* truth_copy = (TH1F*) truth->Clone();
        for (int i=1; i<nBins+1; i++){
          if (plot.Contains("ZJpT"))
            truth_copy->SetBinContent(i,3*truth->GetBinContent(i)/(truth->GetBinWidth(i)*Totlumi));
          else truth_copy->SetBinContent(i,truth->GetBinContent(i)/(truth->GetBinWidth(i)*Totlumi));
          truth_copy->SetBinError(i,0.);
        }


        // Create plots comparing truth level reweighted vs nominal unfolded data.
        can->Clear();
        double ratio = 0.36, margin = 0.02;
        can->cd();
        gPad->SetBottomMargin(ratio);
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        can->SetRightMargin(0.04);
        // Do some plotting
        Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
        Str xsLabel = settings->GetValue(plot + ".xs", " ");

        unfoldedData->SetTitle(";"+Xlabel+";"+xsLabel);
        // Not log plots
        if (plot.Contains("ZJpT")){
          gPad->SetLogy(0);
          unfoldedData->SetMaximum(unfoldedData->GetMaximum()*2.0);
          unfoldedData->SetMinimum(unfoldedData->GetMinimum()*.5);
        }
        else{
          gPad->SetLogy(1);
          if(unfoldedData->GetMinimum() > 100)
            unfoldedData->SetMinimum(unfoldedData->GetMinimum()*0.05);
          unfoldedData->SetMaximum(unfoldedData->GetMaximum()*100);
        }
        if (plot.Contains("NJets")) unfoldedData->SetMaximum(unfoldedData->GetMaximum()*5.); // Scale it up by 5 to help plotting
        unfoldedData->GetXaxis()->SetLabelSize(0.);
        unfoldedData->SetLineColor(kBlack);
        truth_copy->SetMarkerStyle(24); // Circle
        truth_copy->SetMarkerColor(857); // Nice Blue
        truth_copy->SetLineColor(857); // Nice Blue
        unfoldedData->Draw();
        truth_copy->Draw("SAME");

        TLegend* leg = new TLegend(0.65, 0.70, 0.85, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.020);
        leg->SetFillStyle(0);

        leg->AddEntry(data,"Unfolded data","pe");
        leg->AddEntry(truth_copy,"Truth reweighed Sherpa2.2.1","pelf");
        leg->Draw("same");
        drawPlotInfo(0.15,0.9, plot, chan, Totlumi, 0.025);

        TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        p->SetTopMargin(1.0 - ratio);
        p->SetFillStyle(0);
        p->SetTicks();
        p->SetGridy();
        p->Draw();
        p->cd();

        TH1F* Ratio = (TH1F*) truth_copy->Clone();
        Ratio->Divide(unfoldedData);
        if (plot.Contains("ZJpT")){
          for (int i=1; i<11; i++){
            Temp->SetBinContent(i, Ratio->GetBinContent(i));
            Temp->SetBinError(i, Ratio->GetBinError(i));
          }
          Ratio = (TH1F*) Temp->Clone();
          Ratio->SetLineColor(857); // Nice Blue
        }
        gStyle->SetErrorX(0.5);
        Ratio->GetYaxis()->SetLabelSize(0.03);
        Ratio->GetYaxis()->SetTitle("Pred./Data");
        Ratio->GetXaxis()->SetTitle(Xlabel);
        Ratio->GetXaxis()->SetLabelSize(0.03);
        Ratio->GetXaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetRangeUser(0.7, 2.0);
        // Ratio->Draw("hist");
        Ratio->Draw("E");
        if (iter == 2) can->Print("unfolding_ZpT_Ratio_"+chan+plot+".png");

        // Create plots comparing truth level NOT REWEIGHED vs nominal unfolded data.
        can->Clear();
        can->cd();
        gPad->SetBottomMargin(ratio);
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        can->SetRightMargin(0.04);
        if (plot.Contains("ZJpT")){
          gPad->SetLogy(0);
        }
        else{
          gPad->SetLogy(1);
        }

        SherpaTrue->SetMarkerStyle(24); // Circle
        SherpaTrue->SetMarkerColor(857); // Nice Blue
        SherpaTrue->SetLineColor(857); // Nice Blue
        unfoldedData->Draw();
        SherpaTrue->Draw("SAME");

        leg = new TLegend(0.65, 0.70, 0.85, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.020);
        leg->SetFillStyle(0);

        leg->AddEntry(data,"Unfolded data","pe");
        leg->AddEntry(SherpaTrue,"Truth original Sherpa2.2.1","pelf");
        leg->Draw("same");
        drawPlotInfo(0.15,0.9, plot, chan, Totlumi, 0.025);

        p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        p->SetTopMargin(1.0 - ratio);
        p->SetFillStyle(0);
        p->SetTicks();
        p->SetGridy();
        p->Draw();
        p->cd();

        Ratio = (TH1F*) SherpaTrue->Clone();
        Ratio->Divide(unfoldedData);
        if (plot.Contains("ZJpT")){
          for (int i=1; i<11; i++){
            Temp->SetBinContent(i, Ratio->GetBinContent(i));
            Temp->SetBinError(i, Ratio->GetBinError(i));
          }
          Ratio = (TH1F*) Temp->Clone();
          Ratio->SetLineColor(857); // Nice Blue
        }

        gStyle->SetErrorX(0.5);
        Ratio->GetYaxis()->SetLabelSize(0.03);
        Ratio->GetYaxis()->SetTitle("Pred./Data");
        Ratio->GetXaxis()->SetTitle(Xlabel);
        Ratio->GetXaxis()->SetLabelSize(0.03);
        Ratio->GetXaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetRangeUser(0.7, 2.0);
        Ratio->Draw("hist");
        if (iter == 2) can->Print("unfolding_Ratio_"+chan+plot+".png");
      }
    }
  }

  extraSherpa->Close();
  Data->Close();
  Sherpa->Close();
  TopQuark->Close();
  WJets->Close();
  Ztt->Close();
  Diboson->Close();
  newfile.Close();
  return;
} // end of z pt unfolding unceratinty

void copyZtautau(TFile* file, TEnv* settings){
  TH1::AddDirectory(kFALSE);
  Str filePath = settings->GetValue("filePath.extras", " ");
  TFile *extraZtt = openFile(filePath + "Ztt.root");
  StrV channels = vectorize(settings->GetValue("channels", " "));
  StrV channels2 = vectorize(settings->GetValue("channelsFull", " "));


  std::cout << "Starting scaling of " << file->GetName() << std::endl;
  TFile hisfile("Ztt_expanded.root","RECREATE");
  TH1F* temp = new TH1F();
  TH1F* copy = new TH1F();
  StrV plotList = vectorize(settings->GetValue("Controls", " "));
  // Copy all the control histos back in new file, and copy the ones that are unchanged.
  for (auto plot : plotList){
    for (auto chan : channels){
      if (chan == "hem"){
        for (int sys=0; sys<135; sys++){ // 0 - 134 : Experimental, 135-142 are Sherpa theory
          temp = getHisto(file, chan+plot+std::to_string(sys));
          copy = (TH1F*) temp->Clone(chan+plot+std::to_string(sys));
          copy->Write();
        }
        for (int sys=143; sys<159; sys++){ // 0 - 134 : Experimental, 135-142 are Sherpa theory
          temp = getHisto(file, chan+plot+"0");
          copy = (TH1F*) temp->Clone(chan+plot+std::to_string(sys));
          copy->Write();
        }
      }
      else{
        for (int sys=0; sys<159; sys++){ // 0 - 134 : Experimental, 135-142 are Sherpa theory
          if (sys < 135)
            temp = getHisto(file, chan+plot+std::to_string(sys));
          else
            temp = getHisto(file, chan+plot+"0");
          copy = (TH1F*) temp->Clone(chan+plot+std::to_string(sys));
          copy->Write();
        }
      }
    }
    TH1F* histo = new TH1F();
    TH1F* nominal = new TH1F();
    nominal = getHisto(extraZtt, "hem"+plot+"0");
    int nBins = nominal->GetNbinsX();

    TH1F* reference = getHisto(file, "hem"+plot+"0");
    TH1F* extraRef = getHisto(extraZtt, "hem"+plot+"0");


    /// Sherpa Scale systematics (141-142)
    TH1F* up = getHisto(extraZtt, "hem"+plot+"0");
    TH1F* down = getHisto(extraZtt, "hem"+plot+"0");
    // Start with the scale variations
    histo = getHisto(extraZtt, "hem"+plot+"105"); // Scale 1
    for (int i=1; i<nBins+1; i++){
      double val = histo->GetBinContent(i);
      if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
      if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
    }
    histo = getHisto(extraZtt, "hem"+plot+"106"); // Scale 2
    for (int i=1; i<nBins+1; i++){
      double val = histo->GetBinContent(i);
      if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
      if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
    }
    histo = getHisto(extraZtt, "hem"+plot+"107"); // Scale 3
    for (int i=1; i<nBins+1; i++){
      double val = histo->GetBinContent(i);
      if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
      if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
    }
    histo = getHisto(extraZtt, "hem"+plot+"108"); // Scale 4
    for (int i=1; i<nBins+1; i++){
      double val = histo->GetBinContent(i);
      if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
      if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
    }
    histo = getHisto(extraZtt, "hem"+plot+"109"); // Scale 5
    for (int i=1; i<nBins+1; i++){
      double val = histo->GetBinContent(i);
      if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
      if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
    }
    histo = getHisto(extraZtt, "hem"+plot+"110"); // Scale 6
    for (int i=1; i<nBins+1; i++){
      double val = histo->GetBinContent(i);
      if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
      if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
    }
    copy = (TH1F*) down->Clone("hem"+plot+"141");
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (extraRef->GetBinContent(i) > 0.)
        copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
    copy->Write();

    copy = (TH1F*) up->Clone("hem"+plot+"142");
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (extraRef->GetBinContent(i) > 0.)
        copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
    copy->Write();

    /// Sherpa alpha_S 139-140
    TH1F* alphaS_high = getHisto(extraZtt, "hem"+plot+"103"); // alphaS high
    TH1F* alphaS_low = getHisto(extraZtt, "hem"+plot+"104"); // alphaS low
    alphaS_high->Add(alphaS_low,-1.);
    copy = (TH1F*) nominal->Clone("hem"+plot+"139"); // 1st of pair: max
    for (int i=1; i<nBins+1; i++)
      copy->SetBinContent(i,nominal->GetBinContent(i)+0.5*alphaS_high->GetBinContent(i));
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (extraRef->GetBinContent(i) > 0.)
        copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
    copy->Write();

    copy = (TH1F*) nominal->Clone("hem"+plot+"140"); // 2nd of pair: min
    for (int i=1; i<nBins+1; i++)
      copy->SetBinContent(i,nominal->GetBinContent(i)-0.5*alphaS_high->GetBinContent(i));
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (extraRef->GetBinContent(i) > 0.)
        copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
    copy->Write();

    /// Sherpa Choice of PDF set 137-138
    // hem 137
    histo = getHisto(extraZtt, "hem"+plot+"101");
    copy = (TH1F*) histo->Clone("hem"+plot+"137");
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (extraRef->GetBinContent(i) > 0.)
        copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
    copy->Write();

    // 138
    // hem 138
    histo = getHisto(extraZtt, "hem"+plot+"102");
    copy = (TH1F*) histo->Clone("hem"+plot+"138");
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (extraRef->GetBinContent(i) > 0.)
        copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
    copy->Write();

    /// Sherpa PDF variance 135-136
    // Uncertainty from the variation of 100 sets
    TH1F* nnominal = getHisto(extraZtt, "hem"+plot+"0"); // nominal PDF
    std::vector<double> max, min;
    for (int i=0; i<nBins; i++){
      max.push_back(0.);
      min.push_back(0.);
    }
    TH1F* var = new TH1F();
    for (int sys=1; sys<101; sys++){
      var = getHisto(extraZtt, "hem"+plot+sys);
      var->Add(nnominal,-1.);
      double val = 0.;
      for (int i=1; i<nBins+1; i++){
        val = var->GetBinContent(i);
        if (val < 0) min[i-1] = min[i-1] + val*val;
        else if (val > 0) max[i-1] = max[i-1] + val*val;
      }
    }
    copy = (TH1F*) nominal->Clone("hem"+plot+"135"); // 1st of pair: max
    for (int i=1; i<nBins+1; i++)
      copy->SetBinContent(i,nominal->GetBinContent(i)+std::sqrt(max[i-1]/99));

    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (extraRef->GetBinContent(i) > 0.)
        copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
    copy->Write();

    // hem 136
    copy = (TH1F*) nominal->Clone("hem"+plot+"136"); // 2nd of pair: min
    for (int i=1; i<nBins+1; i++)
      copy->SetBinContent(i,nominal->GetBinContent(i)-std::sqrt(min[i-1]/99));
    // Renormalize to nominal
    for (int i=1; i<nBins+1; i++)
      if (extraRef->GetBinContent(i) > 0.)
        copy->SetBinContent(i,reference->GetBinContent(i)*copy->GetBinContent(i)/extraRef->GetBinContent(i));
    copy->Write();
  } // end of plotList

  extraZtt->Close();
  hisfile.Close();
} // end of function TauTau

void doUnfoldingminDR(TEnv* settings){
  double Totlumi = settings->GetValue("luminosity", 0.0);
  TH1::AddDirectory(kFALSE);

  std::cout << "Starting minDR Unfolding uncertainty!"<< std::endl;
  Double_t binsVec[11];
  binsVec[0]=0.0;
  binsVec[1]=0.1;
  binsVec[2]=0.2;
  binsVec[3]=0.3;
  binsVec[4]=0.4;
  binsVec[5]=0.5;
  binsVec[6]=0.6;
  binsVec[7]=0.8;
  binsVec[8]=1.0;
  binsVec[9]=1.2;
  binsVec[10]=1.5;
  TH1F* Temp = new TH1F("temp","temp",10, binsVec);


  //////////////////////////////////////////////////
  //// Now we begin the unfolded uncerainties! ////
  //////////////////////////////////////////////////
  // Get Nominal files for unfolding

  Str nominalPath = settings->GetValue("filePath.nominal", " ");
  TFile *Data = openFile(nominalPath + "data.root");
  TFile *Sherpa = openFile(nominalPath + "SherpaZJets.root");
  TFile *TopQuark = openFile(nominalPath + "TopQuark.root");
  TFile *WJets = openFile(nominalPath + "WJets.root");
  TFile *Ztt = openFile(nominalPath + "Ztt.root");
  TFile *Diboson = openFile(nominalPath + "Diboson.root");
  // Get truth reweighted sherpa
  TFile *extraSherpa = openFile(nominalPath + "unf/minDR/SherpaZJets.root");

  // Define output file
  TFile newfile("UnfoldingUncertainty_minDR.root","RECREATE");

  StrV RespChan = vectorize(settings->GetValue("Channels", " "));
  StrV respList = vectorize(settings->GetValue("PlotList", " "));

  TH1F* temp = new TH1F();

  for (auto plot : respList){
    Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
    for (auto chan : RespChan){
      TCanvas *can = new TCanvas("", "", 800, 800);

      // Get Nominal sherpa response matrix.
      TH2F* RMatrix = getHisto2(Sherpa, chan+"resp"+var+"0");
      temp = getHisto(Sherpa,chan+"meas"+var+"0");
      TH1F* SherpaMeas = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      temp = getHisto(Sherpa,chan+"true"+var+"0");
      TH1F* SherpaTrue = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      // Rebin Sherpa Response matrix
      TH2F* RebinnedMatrix = (TH2F*) (rebin2DHisto((TH2F*) RMatrix->Clone(), plot))->Clone();

      // Define response matrix!
      RooUnfoldResponse Response (SherpaMeas,SherpaTrue,RebinnedMatrix);

      // Unfold truth-reighted sherpa!
      temp = getHisto(extraSherpa, chan+"meas"+var+"0"); // Reweighted sherpa
      TH1F* NewSherpa = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      int nBins = NewSherpa->GetNbinsX();

      // How does the unfolded sherpa compare to its truth.
      temp = getHisto(extraSherpa, chan+"true"+var+"0"); // Reweighted sherpa
      TH1F* truth = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      if (plot.Contains("ZJpT")) truth->Rebin(3);

      if (plot.Contains("ZJpT")) SherpaTrue->Rebin(3);
      for (int i=1; i<nBins+1; i++){
        if (plot.Contains("ZJpT"))
          SherpaTrue->SetBinContent(i,3*SherpaTrue->GetBinContent(i)/(SherpaTrue->GetBinWidth(i)*Totlumi));
        else SherpaTrue->SetBinContent(i,SherpaTrue->GetBinContent(i)/(SherpaTrue->GetBinWidth(i)*Totlumi));
        SherpaTrue->SetBinError(i,0.);
      }


      // get the data to unfold
      TH1F* data = getHisto(Data,chan+"data"+var+"0");
      temp = getHisto(Diboson,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(TopQuark,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(WJets,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(Ztt,chan+"meas"+var+"0");
      data->Add(temp,-1.);

      TH1F* NewData = (TH1F*) (rebin1DHisto((TH1F*) data->Clone(), plot))->Clone();

      for (int iter=1; iter<6; iter++){
        // if (iter !=2) continue;
        // Unfold!
        RooUnfoldBayes unfold_data (&Response, NewSherpa, iter);
        auto* unfolded = unfold_data.Hreco();
        if (plot.Contains("ZJpT")) unfolded->Rebin(3);
        TH1F* Uncertainty = (TH1F*) unfolded->Clone();

        // By how much does the unfolding affect "sherpa data"
        for (int i=1; i<nBins+1; i++)
          Uncertainty->SetBinContent(i, Uncertainty->GetBinContent(i) / truth->GetBinContent(i));

        // Unfold the data
        unfold_data = RooUnfoldBayes(&Response, NewData, iter);
        // Get unfolded data and push it to map
        unfolded = unfold_data.Hreco();
        for (int i=1; i<nBins+1; i++){
          unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
          unfolded->SetBinError(i,0.);
        }
        if (plot.Contains("ZJpT")) unfolded->Rebin(3);
        TH1F* unfoldedData = (TH1F*) unfolded->Clone();

        TH1F* copy = (TH1F*) unfolded->Clone(chan+"unf"+var+"_"+iter+"minDR");
        for (int i=1; i<nBins+1; i++)
          copy->SetBinContent(i, copy->GetBinContent(i) * Uncertainty->GetBinContent(i));
        copy->Write();

        TH1F* truth_copy = (TH1F*) truth->Clone();
        for (int i=1; i<nBins+1; i++){
          if (plot.Contains("ZJpT"))
            truth_copy->SetBinContent(i,3*truth->GetBinContent(i)/(truth->GetBinWidth(i)*Totlumi));
          else truth_copy->SetBinContent(i,truth->GetBinContent(i)/(truth->GetBinWidth(i)*Totlumi));
          truth_copy->SetBinError(i,0.);
        }


        // Create plots comparing truth level reweighted vs nominal unfolded data.
        can->Clear();
        double ratio = 0.36, margin = 0.02;
        can->cd();
        gPad->SetBottomMargin(ratio);
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        can->SetRightMargin(0.04);
        // Do some plotting
        Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
        Str xsLabel = settings->GetValue(plot + ".xs", " ");

        unfoldedData->SetTitle(";"+Xlabel+";"+xsLabel);
        // Not log plots
        if (plot.Contains("ZJpT")){
          gPad->SetLogy(0);
          unfoldedData->SetMaximum(unfoldedData->GetMaximum()*2.0);
          unfoldedData->SetMinimum(unfoldedData->GetMinimum()*.5);
        }
        else{
          gPad->SetLogy(1);
          if(unfoldedData->GetMinimum() > 100)
            unfoldedData->SetMinimum(unfoldedData->GetMinimum()*0.05);
          unfoldedData->SetMaximum(unfoldedData->GetMaximum()*100);
        }
        if (plot.Contains("NJets")) unfoldedData->SetMaximum(unfoldedData->GetMaximum()*5.); // Scale it up by 5 to help plotting
        unfoldedData->GetXaxis()->SetLabelSize(0.);
        unfoldedData->SetLineColor(kBlack);
        truth_copy->SetMarkerStyle(24); // Circle
        truth_copy->SetMarkerColor(857); // Nice Blue
        truth_copy->SetLineColor(857); // Nice Blue
        unfoldedData->Draw();
        truth_copy->Draw("SAME");

        TLegend* leg = new TLegend(0.65, 0.70, 0.85, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.020);
        leg->SetFillStyle(0);

        leg->AddEntry(data,"Unfolded data","pe");
        leg->AddEntry(truth_copy,"Truth reweighed Sherpa2.2.1","pelf");
        leg->Draw("same");
        drawPlotInfo(0.15,0.9, plot, chan, Totlumi, 0.025);

        TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        p->SetTopMargin(1.0 - ratio);
        p->SetFillStyle(0);
        p->SetTicks();
        p->SetGridy();
        p->Draw();
        p->cd();

        TH1F* Ratio = (TH1F*) truth_copy->Clone();
        Ratio->Divide(unfoldedData);
        if (plot.Contains("ZJpT")){
          for (int i=1; i<11; i++){
            Temp->SetBinContent(i, Ratio->GetBinContent(i));
            Temp->SetBinError(i, Ratio->GetBinError(i));
          }
          Ratio = (TH1F*) Temp->Clone();
          Ratio->SetLineColor(857); // Nice Blue
        }
        gStyle->SetErrorX(0.5);
        Ratio->GetYaxis()->SetLabelSize(0.03);
        Ratio->GetYaxis()->SetTitle("Pred./Data");
        Ratio->GetXaxis()->SetTitle(Xlabel);
        Ratio->GetXaxis()->SetLabelSize(0.03);
        Ratio->GetXaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetRangeUser(0.7, 2.0);
        // Ratio->Draw("hist");
        Ratio->Draw("E");
        if (iter == 2) can->Print("unfolding_minDR_Ratio_"+chan+plot+".png");

        // Create plots comparing truth level NOT REWEIGHED vs nominal unfolded data.
        can->Clear();
        can->cd();
        gPad->SetBottomMargin(ratio);
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        can->SetRightMargin(0.04);
        if (plot.Contains("ZJpT")){
          gPad->SetLogy(0);
        }
        else{
          gPad->SetLogy(1);
        }

        SherpaTrue->SetMarkerStyle(24); // Circle
        SherpaTrue->SetMarkerColor(857); // Nice Blue
        SherpaTrue->SetLineColor(857); // Nice Blue
        unfoldedData->Draw();
        SherpaTrue->Draw("SAME");

        leg = new TLegend(0.65, 0.70, 0.85, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.020);
        leg->SetFillStyle(0);

        leg->AddEntry(data,"Unfolded data","pe");
        leg->AddEntry(SherpaTrue,"Truth original Sherpa2.2.1","pelf");
        leg->Draw("same");
        drawPlotInfo(0.15,0.9, plot, chan, Totlumi, 0.025);

        p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        p->SetTopMargin(1.0 - ratio);
        p->SetFillStyle(0);
        p->SetTicks();
        p->SetGridy();
        p->Draw();
        p->cd();

        Ratio = (TH1F*) SherpaTrue->Clone();
        Ratio->Divide(unfoldedData);
        if (plot.Contains("ZJpT")){
          for (int i=1; i<11; i++){
            Temp->SetBinContent(i, Ratio->GetBinContent(i));
            Temp->SetBinError(i, Ratio->GetBinError(i));
          }
          Ratio = (TH1F*) Temp->Clone();
          Ratio->SetLineColor(857); // Nice Blue
        }

        gStyle->SetErrorX(0.5);
        Ratio->GetYaxis()->SetLabelSize(0.03);
        Ratio->GetYaxis()->SetTitle("Pred./Data");
        Ratio->GetXaxis()->SetTitle(Xlabel);
        Ratio->GetXaxis()->SetLabelSize(0.03);
        Ratio->GetXaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetRangeUser(0.7, 2.0);
        Ratio->Draw("hist");
        if (iter == 2) can->Print("unfolding_Ratio_"+chan+plot+".png");
      }
    }
  }

  extraSherpa->Close();
  Data->Close();
  Sherpa->Close();
  TopQuark->Close();
  WJets->Close();
  Ztt->Close();
  Diboson->Close();
  newfile.Close();
  return;
} // end of minDR unfolding unceratinty

void doUnfoldingHighpT(TEnv* settings){
  double Totlumi = settings->GetValue("luminosity", 0.0);
  TH1::AddDirectory(kFALSE);

  std::cout << "Starting highPT Unfolding uncertainty!"<< std::endl;
  Double_t binsVec[11];
  binsVec[0]=0.0;
  binsVec[1]=0.1;
  binsVec[2]=0.2;
  binsVec[3]=0.3;
  binsVec[4]=0.4;
  binsVec[5]=0.5;
  binsVec[6]=0.6;
  binsVec[7]=0.8;
  binsVec[8]=1.0;
  binsVec[9]=1.2;
  binsVec[10]=1.5;
  TH1F* Temp = new TH1F("temp","temp",10, binsVec);


  //////////////////////////////////////////////////
  //// Now we begin the unfolded uncerainties! ////
  //////////////////////////////////////////////////
  // Get Nominal files for unfolding

  Str nominalPath = settings->GetValue("filePath.nominal", " ");
  TFile *Data = openFile(nominalPath + "data.root");
  TFile *Sherpa = openFile(nominalPath + "SherpaZJets.root");
  TFile *TopQuark = openFile(nominalPath + "TopQuark.root");
  TFile *WJets = openFile(nominalPath + "WJets.root");
  TFile *Ztt = openFile(nominalPath + "Ztt.root");
  TFile *Diboson = openFile(nominalPath + "Diboson.root");
  // Get truth reweighted sherpa
  TFile *extraSherpa = openFile(nominalPath + "unf/HighPT/SherpaZJets.root");

  // Define output file
  TFile newfile("UnfoldingUncertainty_highPT.root","RECREATE");

  StrV RespChan = vectorize(settings->GetValue("Channels", " "));
  StrV respList = vectorize(settings->GetValue("PlotList", " "));

  TH1F* temp = new TH1F();

  for (auto plot : respList){
    Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
    for (auto chan : RespChan){
      TCanvas *can = new TCanvas("", "", 800, 800);

      // Get Nominal sherpa response matrix.
      TH2F* RMatrix = getHisto2(Sherpa, chan+"resp"+var+"0");
      temp = getHisto(Sherpa,chan+"meas"+var+"0");
      TH1F* SherpaMeas = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      temp = getHisto(Sherpa,chan+"true"+var+"0");
      TH1F* SherpaTrue = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      // Rebin Sherpa Response matrix
      TH2F* RebinnedMatrix = (TH2F*) (rebin2DHisto((TH2F*) RMatrix->Clone(), plot))->Clone();

      // Define response matrix!
      RooUnfoldResponse Response (SherpaMeas,SherpaTrue,RebinnedMatrix);

      // Unfold truth-reighted sherpa!
      temp = getHisto(extraSherpa, chan+"meas"+var+"0"); // Reweighted sherpa
      TH1F* NewSherpa = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      int nBins = NewSherpa->GetNbinsX();

      // How does the unfolded sherpa compare to its truth.
      temp = getHisto(extraSherpa, chan+"true"+var+"0"); // Reweighted sherpa
      TH1F* truth = (TH1F*) (rebin1DHisto((TH1F*) temp->Clone(), plot))->Clone();
      if (plot.Contains("ZJpT")) truth->Rebin(3);

      if (plot.Contains("ZJpT")) SherpaTrue->Rebin(3);
      for (int i=1; i<nBins+1; i++){
        if (plot.Contains("ZJpT"))
          SherpaTrue->SetBinContent(i,3*SherpaTrue->GetBinContent(i)/(SherpaTrue->GetBinWidth(i)*Totlumi));
        else SherpaTrue->SetBinContent(i,SherpaTrue->GetBinContent(i)/(SherpaTrue->GetBinWidth(i)*Totlumi));
        SherpaTrue->SetBinError(i,0.);
      }


      // get the data to unfold
      TH1F* data = getHisto(Data,chan+"data"+var+"0");
      temp = getHisto(Diboson,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(TopQuark,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(WJets,chan+"meas"+var+"0");
      data->Add(temp,-1.);
      temp = getHisto(Ztt,chan+"meas"+var+"0");
      data->Add(temp,-1.);

      TH1F* NewData = (TH1F*) (rebin1DHisto((TH1F*) data->Clone(), plot))->Clone();

      for (int iter=1; iter<6; iter++){
        // if (iter !=2) continue;
        // Unfold!
        RooUnfoldBayes unfold_data (&Response, NewSherpa, iter);
        auto* unfolded = unfold_data.Hreco();
        if (plot.Contains("ZJpT")) unfolded->Rebin(3);
        TH1F* Uncertainty = (TH1F*) unfolded->Clone();

        // By how much does the unfolding affect "sherpa data"
        for (int i=1; i<nBins+1; i++)
          Uncertainty->SetBinContent(i, Uncertainty->GetBinContent(i) / truth->GetBinContent(i));

        // Unfold the data
        unfold_data = RooUnfoldBayes(&Response, NewData, iter);
        // Get unfolded data and push it to map
        unfolded = unfold_data.Hreco();
        for (int i=1; i<nBins+1; i++){
          unfolded->SetBinContent(i,unfolded->GetBinContent(i)/(unfolded->GetBinWidth(i)*Totlumi));
          unfolded->SetBinError(i,0.);
        }
        if (plot.Contains("ZJpT")) unfolded->Rebin(3);
        TH1F* unfoldedData = (TH1F*) unfolded->Clone();

        TH1F* copy = (TH1F*) unfolded->Clone(chan+"unf"+var+"_"+iter+"highPT");
        for (int i=1; i<nBins+1; i++)
          copy->SetBinContent(i, copy->GetBinContent(i) * Uncertainty->GetBinContent(i));
        copy->Write();

        TH1F* truth_copy = (TH1F*) truth->Clone();
        for (int i=1; i<nBins+1; i++){
          if (plot.Contains("ZJpT"))
            truth_copy->SetBinContent(i,3*truth->GetBinContent(i)/(truth->GetBinWidth(i)*Totlumi));
          else truth_copy->SetBinContent(i,truth->GetBinContent(i)/(truth->GetBinWidth(i)*Totlumi));
          truth_copy->SetBinError(i,0.);
        }


        // Create plots comparing truth level reweighted vs nominal unfolded data.
        can->Clear();
        double ratio = 0.36, margin = 0.02;
        can->cd();
        gPad->SetBottomMargin(ratio);
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        can->SetRightMargin(0.04);
        // Do some plotting
        Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
        Str xsLabel = settings->GetValue(plot + ".xs", " ");

        unfoldedData->SetTitle(";"+Xlabel+";"+xsLabel);
        // Not log plots
        if (plot.Contains("ZJpT")){
          gPad->SetLogy(0);
          unfoldedData->SetMaximum(unfoldedData->GetMaximum()*2.0);
          unfoldedData->SetMinimum(unfoldedData->GetMinimum()*.5);
        }
        else{
          gPad->SetLogy(1);
          if(unfoldedData->GetMinimum() > 100)
            unfoldedData->SetMinimum(unfoldedData->GetMinimum()*0.05);
          unfoldedData->SetMaximum(unfoldedData->GetMaximum()*100);
        }
        if (plot.Contains("NJets")) unfoldedData->SetMaximum(unfoldedData->GetMaximum()*5.); // Scale it up by 5 to help plotting
        unfoldedData->GetXaxis()->SetLabelSize(0.);
        unfoldedData->SetLineColor(kBlack);
        truth_copy->SetMarkerStyle(24); // Circle
        truth_copy->SetMarkerColor(857); // Nice Blue
        truth_copy->SetLineColor(857); // Nice Blue
        unfoldedData->Draw();
        truth_copy->Draw("SAME");

        TLegend* leg = new TLegend(0.65, 0.70, 0.85, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.020);
        leg->SetFillStyle(0);

        leg->AddEntry(data,"Unfolded data","pe");
        leg->AddEntry(truth_copy,"Truth reweighed Sherpa2.2.1","pelf");
        leg->Draw("same");
        drawPlotInfo(0.15,0.9, plot, chan, Totlumi, 0.025);

        TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        p->SetTopMargin(1.0 - ratio);
        p->SetFillStyle(0);
        p->SetTicks();
        p->SetGridy();
        p->Draw();
        p->cd();

        TH1F* Ratio = (TH1F*) truth_copy->Clone();
        Ratio->Divide(unfoldedData);
        if (plot.Contains("ZJpT")){
          for (int i=1; i<11; i++){
            Temp->SetBinContent(i, Ratio->GetBinContent(i));
            Temp->SetBinError(i, Ratio->GetBinError(i));
          }
          Ratio = (TH1F*) Temp->Clone();
          Ratio->SetLineColor(857); // Nice Blue
        }
        gStyle->SetErrorX(0.5);
        Ratio->GetYaxis()->SetLabelSize(0.03);
        Ratio->GetYaxis()->SetTitle("Pred./Data");
        Ratio->GetXaxis()->SetTitle(Xlabel);
        Ratio->GetXaxis()->SetLabelSize(0.03);
        Ratio->GetXaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetRangeUser(0.7, 2.0);
        // Ratio->Draw("hist");
        Ratio->Draw("E");
        if (iter == 2) can->Print("unfolding_HighPt_Ratio_"+chan+plot+".png");

        // Create plots comparing truth level NOT REWEIGHED vs nominal unfolded data.
        can->Clear();
        can->cd();
        gPad->SetBottomMargin(ratio);
        gPad->SetLogy(0);
        can->SetLogx(0);
        can->Modified();
        can->Update();
        gPad->SetTicks();
        can->SetRightMargin(0.04);
        if (plot.Contains("ZJpT")){
          gPad->SetLogy(0);
        }
        else{
          gPad->SetLogy(1);
        }

        SherpaTrue->SetMarkerStyle(24); // Circle
        SherpaTrue->SetMarkerColor(857); // Nice Blue
        SherpaTrue->SetLineColor(857); // Nice Blue
        unfoldedData->Draw();
        SherpaTrue->Draw("SAME");

        leg = new TLegend(0.65, 0.70, 0.85, 0.93);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.020);
        leg->SetFillStyle(0);

        leg->AddEntry(data,"Unfolded data","pe");
        leg->AddEntry(SherpaTrue,"Truth original Sherpa2.2.1","pelf");
        leg->Draw("same");
        drawPlotInfo(0.15,0.9, plot, chan, Totlumi, 0.025);

        p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
        p->SetLogx(0);
        p->Modified();
        p->Update();
        p->SetTopMargin(1.0 - ratio);
        p->SetFillStyle(0);
        p->SetTicks();
        p->SetGridy();
        p->Draw();
        p->cd();

        Ratio = (TH1F*) SherpaTrue->Clone();
        Ratio->Divide(unfoldedData);
        if (plot.Contains("ZJpT")){
          for (int i=1; i<11; i++){
            Temp->SetBinContent(i, Ratio->GetBinContent(i));
            Temp->SetBinError(i, Ratio->GetBinError(i));
          }
          Ratio = (TH1F*) Temp->Clone();
          Ratio->SetLineColor(857); // Nice Blue
        }

        gStyle->SetErrorX(0.5);
        Ratio->GetYaxis()->SetLabelSize(0.03);
        Ratio->GetYaxis()->SetTitle("Pred./Data");
        Ratio->GetXaxis()->SetTitle(Xlabel);
        Ratio->GetXaxis()->SetLabelSize(0.03);
        Ratio->GetXaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetTitleOffset(1.4);
        Ratio->GetYaxis()->SetRangeUser(0.7, 2.0);
        Ratio->Draw("hist");
        if (iter == 2) can->Print("unfolding_Ratio_"+chan+plot+".png");
      }
    }
  }

  extraSherpa->Close();
  Data->Close();
  Sherpa->Close();
  TopQuark->Close();
  WJets->Close();
  Ztt->Close();
  Diboson->Close();
  newfile.Close();
  return;
} // end of HighPT unfolding unceratinty

/// This here is only for temporary safe-keeping while Arran validates the new Sherpa e-mu region with ttbar
// TH1F* elec = new TH1F();
// TH1F* nominalElec = new TH1F();
// TH1F* nominalMuon = new TH1F();
// TH1F* nominalEmu = new TH1F();
// nominalElec = getHisto(file, "hee"+plot+"0");
// nominalMuon = getHisto(file, "hmm"+plot+"0");
// nominalEmu = getHisto(file, "hem"+plot+"0");
// int nBins = nominalElec->GetNbinsX();
//
// /// Sherpa Scale systematics (141-142)
// TH1F* up = getHisto(file, "hee"+plot+"0");
// TH1F* down = getHisto(file, "hee"+plot+"0");
// // Start with the scale variations
// elec = getHisto(extraSherpa, "hee"+plot+"105"); // Scale 1
// for (int i=1; i<nBins+1; i++){
//   double val = elec->GetBinContent(i);
//   if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
//   if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
// }
// elec = getHisto(extraSherpa, "hee"+plot+"106"); // Scale 2
// for (int i=1; i<nBins+1; i++){
//   double val = elec->GetBinContent(i);
//   if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
//   if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
// }
// elec = getHisto(extraSherpa, "hee"+plot+"107"); // Scale 3
// for (int i=1; i<nBins+1; i++){
//   double val = elec->GetBinContent(i);
//   if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
//   if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
// }
// elec = getHisto(extraSherpa, "hee"+plot+"108"); // Scale 4
// for (int i=1; i<nBins+1; i++){
//   double val = elec->GetBinContent(i);
//   if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
//   if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
// }
// elec = getHisto(extraSherpa, "hee"+plot+"109"); // Scale 5
// for (int i=1; i<nBins+1; i++){
//   double val = elec->GetBinContent(i);
//   if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
//   if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
// }
// elec = getHisto(extraSherpa, "hee"+plot+"110"); // Scale 6
// for (int i=1; i<nBins+1; i++){
//   double val = elec->GetBinContent(i);
//   if (val > up->GetBinContent(i)) up->SetBinContent(i, val);
//   if (val < down->GetBinContent(i)) down->SetBinContent(i, val);
// }
// copy = (TH1F*) down->Clone("hee"+plot+"141");
// copy->Write();
// copy = (TH1F*) up->Clone("hee"+plot+"142");
// copy->Write();
// // Muon channel hmm
// elec = getHisto(extraSherpa, "hee"+plot+"0");
// temp = getHisto(file, "hmm"+plot+"0");
// for (int i=1; i< temp->GetNbinsX()+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i, nominalMuon->GetBinContent(i) * down->GetBinContent(i)/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hmm"+plot+"141");
// copy->Write();
// for (int i=1; i< temp->GetNbinsX()+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i, nominalMuon->GetBinContent(i) * up->GetBinContent(i)/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hmm"+plot+"142");
// copy->Write();
// // E-mu channel hem
// temp = getHisto(file, "hem"+plot+"0");
// for (int i=1; i< temp->GetNbinsX()+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i, nominalEmu->GetBinContent(i) * down->GetBinContent(i)/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hem"+plot+"141");
// copy->Write();
// for (int i=1; i< temp->GetNbinsX()+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i, nominalEmu->GetBinContent(i) * up->GetBinContent(i)/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hem"+plot+"142");
// copy->Write();
//
//
// /// Sherpa alpha_S 139-140
// TH1F* alphaS_high = getHisto(extraSherpa, "hee"+plot+"103"); // alphaS high
// TH1F* alphaS_low = getHisto(extraSherpa, "hee"+plot+"104"); // alphaS low
// alphaS_high->Add(alphaS_low,-1.);
// copy = (TH1F*) nominalElec->Clone("hee"+plot+"139"); // 1st of pair: max
// for (int i=1; i<nBins+1; i++)
//   copy->SetBinContent(i,nominalElec->GetBinContent(i)+0.5*alphaS_high->GetBinContent(i));
// copy->Write();
// copy = (TH1F*) nominalElec->Clone("hee"+plot+"140"); // 2nd of pair: min
// for (int i=1; i<nBins+1; i++)
//   copy->SetBinContent(i,nominalElec->GetBinContent(i)-0.5*alphaS_high->GetBinContent(i));
// copy->Write();
//
// // Muon channel hmm
// temp = getHisto(file, "hmm"+plot+"0"); // nominal PDF
// copy = (TH1F*) temp->Clone("hmm"+plot+"139"); // 1st of pair: max
// for (int i=1; i<nBins+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) copy->SetBinContent(i, 0.);
//   else copy->SetBinContent(i,nominalMuon->GetBinContent(i)*(nominalElec->GetBinContent(i)+0.5*alphaS_high->GetBinContent(i))/nominalElec->GetBinContent(i));
// }
// copy->Write();
// temp = getHisto(file, "hmm"+plot+"0"); // nominal PDF
// copy = (TH1F*) temp->Clone("hmm"+plot+"140"); // 2nd of pair: min
// for (int i=1; i<nBins+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) copy->SetBinContent(i, 0.);
//   else copy->SetBinContent(i,nominalMuon->GetBinContent(i)*(nominalElec->GetBinContent(i)-0.5*alphaS_high->GetBinContent(i))/nominalElec->GetBinContent(i));
// }
// copy->Write();
//
// // E-mu channel hem
// temp = getHisto(file, "hem"+plot+"0"); // nominal PDF
// copy = (TH1F*) temp->Clone("hem"+plot+"139"); // 1st of pair: max
// for (int i=1; i<nBins+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) copy->SetBinContent(i, 0.);
//   else copy->SetBinContent(i,nominalEmu->GetBinContent(i)*(nominalElec->GetBinContent(i)+0.5*alphaS_high->GetBinContent(i))/nominalElec->GetBinContent(i));
// }
// copy->Write();
// temp = getHisto(file, "hem"+plot+"0"); // nominal PDF
// copy = (TH1F*) temp->Clone("hem"+plot+"140"); // 2nd of pair: min
// for (int i=1; i<nBins+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) copy->SetBinContent(i, 0.);
//   else copy->SetBinContent(i,nominalEmu->GetBinContent(i)*(nominalElec->GetBinContent(i)-0.5*alphaS_high->GetBinContent(i))/nominalElec->GetBinContent(i));
// }
// copy->Write();
//
//
// /// Sherpa Choice of PDF set 137-138
// // hee 137
// elec = getHisto(extraSherpa, "hee"+plot+"101");
// copy = (TH1F*) elec->Clone("hee"+plot+"137");
// copy->Write();
// // hmm 137
// temp = getHisto(file, "hmm"+plot+"0");
// for (int i=1; i<nBins+1; i++){
//   if(nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i, nominalMuon->GetBinContent(i) * elec->GetBinContent(i)/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hmm"+plot+"137");
// copy->Write();
// // hem 137
// temp = getHisto(file, "hem"+plot+"0");
// for (int i=1; i< temp->GetNbinsX()+1; i++){
//   if(nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i, nominalEmu->GetBinContent(i) * elec->GetBinContent(i)/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hem"+plot+"137");
// copy->Write();
// // 138
// // hee 138
// elec = getHisto(extraSherpa, "hee"+plot+"102");
// copy = (TH1F*) elec->Clone("hee"+plot+"138");
// copy->Write();
// // hmm 138
// temp = getHisto(file, "hmm"+plot+"0");
// for (int i=1; i< temp->GetNbinsX()+1; i++){
//   if(nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i, nominalMuon->GetBinContent(i) * elec->GetBinContent(i)/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hmm"+plot+"138");
// copy->Write();
// // hem 138
// temp = getHisto(file, "hem"+plot+"0");
// for (int i=1; i< temp->GetNbinsX()+1; i++){
//   if(nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i, nominalEmu->GetBinContent(i) * elec->GetBinContent(i)/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hem"+plot+"138");
// copy->Write();
//
//
// /// Sherpa PDF variance 135-136
// // Uncertainty from the variation of 100 sets
// TH1F* nominal = getHisto(extraSherpa, "hee"+plot+"0"); // nominal PDF
// std::vector<double> max, min;
// for (int i=0; i<nBins; i++){
//   max.push_back(0.);
//   min.push_back(0.);
// }
// TH1F* var = new TH1F();
// for (int sys=1; sys<101; sys++){
//   var = getHisto(extraSherpa, "hee"+plot+sys);
//   var->Add(nominal,-1.);
//   double val = 0.;
//   for (int i=1; i<nBins+1; i++){
//     val = var->GetBinContent(i);
//     if (val < 0) min[i-1] = min[i-1] + val*val;
//     else if (val > 0) max[i-1] = max[i-1] + val*val;
//   }
// }
// copy = (TH1F*) nominal->Clone("hee"+plot+"135"); // 1st of pair: max
// for (int i=1; i<nBins+1; i++)
//   copy->SetBinContent(i,nominal->GetBinContent(i)+std::sqrt(max[i-1]));
// copy->Write();
// // hmm 135
// temp = getHisto(file, "hmm"+plot+"0"); // nominal PDF
// for (int i=1; i<nBins+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i,nominalMuon->GetBinContent(i)*(nominal->GetBinContent(i)+std::sqrt(max[i-1]))/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hmm"+plot+"135"); // 1st of pair: max
// copy->Write();
// // hem 135
// temp = getHisto(file, "hem"+plot+"0"); // nominal PDF
// for (int i=1; i<nBins+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i,nominalEmu->GetBinContent(i)*(nominal->GetBinContent(i)+std::sqrt(max[i-1]))/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hem"+plot+"135"); // 1st of pair: max
// copy->Write();
//
// // hee 136
// copy = (TH1F*) nominal->Clone("hee"+plot+"136"); // 2nd of pair: min
// for (int i=1; i<nBins+1; i++)
//   copy->SetBinContent(i,nominal->GetBinContent(i)-std::sqrt(min[i-1]));
// copy->Write();
// // hmm 136
// temp = getHisto(file, "hmm"+plot+"0"); // nominal PDF
// for (int i=1; i<nBins+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i,nominalMuon->GetBinContent(i)*(nominal->GetBinContent(i)-std::sqrt(min[i-1]))/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hmm"+plot+"136"); // 2nd of pair: min
// copy->Write();
// // hem 136
// temp = getHisto(file, "hem"+plot+"0"); // nominal PDF
// for (int i=1; i<nBins+1; i++){
//   if (nominalElec->GetBinContent(i) < 0.1) temp->SetBinContent(i, 0.);
//   else temp->SetBinContent(i,nominalEmu->GetBinContent(i)*(nominal->GetBinContent(i)-std::sqrt(min[i-1]))/nominalElec->GetBinContent(i));
// }
// copy = (TH1F*) temp->Clone("hem"+plot+"136"); // 2nd of pair: min
// copy->Write();
