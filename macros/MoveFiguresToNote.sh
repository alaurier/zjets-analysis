# This script moves the .png files that are produced from the unfolding script all the way to the note's git project so I don't have to do it by hand!
# Lots of stuff here is done by hand, mostly because its more robust and I dont feel like thinking of a clever way of doing this.

export note=/afs/cern.ch/work/a/alaurier/private/SupportNote/ANA-STDM-2018-49-INT1

# Move theory uncertainty
mv TheoryUncert_*.png $note/fig/TheoryUncertainty/


# First we deal with the unfolded cross sections

# Total cross sections
# Main results: minDR, ZJpT, ZJpT collinear, ZJpT back-to-back, NJets, pTZ, pTjet
mv total_xs_minDR.png $note/fig/xs/total/
mv total_xs_ZJpTlowDR.png $note/fig/xs/total/
mv total_xs_ZJpThighDR.png $note/fig/xs/total/
mv total_xs_ZJpT.png $note/fig/xs/total/
mv total_xs_NJets.png $note/fig/xs/total/
mv total_xs_pTjet.png $note/fig/xs/total/
mv total_xs_pTZ.png $note/fig/xs/total/
# Extra results: HT, Mjj, NJets500
mv total_xs_*.png $note/fig/xs/total/extra/

# Electron cross sections
# Main results: minDR, ZJpT, ZJpT collinear, ZJpT back-to-back, NJets, pTZ, pTjet
mv xs_heeminDR.png $note/fig/xs/electron/
mv xs_heeZJpTlowDR.png $note/fig/xs/electron/
mv xs_heeZJpThighDR.png $note/fig/xs/electron/
mv xs_heeZJpT.png $note/fig/xs/electron/
mv xs_heeNJets.png $note/fig/xs/electron/
mv xs_heepTjet.png $note/fig/xs/electron/
mv xs_heepTZ.png $note/fig/xs/electron/
# Extra results: HT, Mjj, NJets500
mv xs_hee*.png $note/fig/xs/electron/extra/

# Muon cross sections
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv xs_hmmminDR.png $note/fig/xs/muon/
mv xs_hmmZJpTlowDR.png $note/fig/xs/muon/
mv xs_hmmZJpThighDR.png $note/fig/xs/muon/
mv xs_hmmZJpT.png $note/fig/xs/muon/
mv xs_hmmNJets.png $note/fig/xs/muon/
mv xs_hmmpTjet.png $note/fig/xs/muon/
mv xs_hmmpTZ.png $note/fig/xs/muon/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv xs_hmm*.png $note/fig/xs/muon/extra/


############################################
### Now we move the uncertainty stack plots.
# First we copy the nominal iterations to the correct folder
# Then we move all the iterations to their respective folders

# Electron uncertainties
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
cp 2iteration_ratioStackPlotheeminDR.png $note/fig/uncert/electron/
cp 2iteration_ratioStackPlotheeZJpTlowDR.png $note/fig/uncert/electron/
cp 2iteration_ratioStackPlotheeZJpThighDR.png $note/fig/uncert/electron/
cp 2iteration_ratioStackPlotheeZJpT.png $note/fig/uncert/electron/
cp 2iteration_ratioStackPlotheeNJets.png $note/fig/uncert/electron/
cp 2iteration_ratioStackPlotheepTjet.png $note/fig/uncert/electron/
cp 2iteration_ratioStackPlotheepTZ.png $note/fig/uncert/electron/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
cp 2iteration_ratioStackPlothee*.png $note/fig/uncert/electron/extra/

# Muon uncertainties
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
cp 2iteration_ratioStackPlothmmminDR.png $note/fig/uncert/muon/
cp 2iteration_ratioStackPlothmmZJpTlowDR.png $note/fig/uncert/muon/
cp 2iteration_ratioStackPlothmmZJpThighDR.png $note/fig/uncert/muon/
cp 2iteration_ratioStackPlothmmZJpT.png $note/fig/uncert/muon/
cp 2iteration_ratioStackPlothmmNJets.png $note/fig/uncert/muon/
cp 2iteration_ratioStackPlothmmpTjet.png $note/fig/uncert/muon/
cp 2iteration_ratioStackPlothmmpTZ.png $note/fig/uncert/muon/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
cp 2iteration_ratioStackPlothmm*.png $note/fig/uncert/muon/extra/

## Linear uncertainties
mv 2iteration_linearUncert* $note/fig/uncert/linear/

# Now we move all electron uncertainties
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
for s in {0..5}; do mv "$s"iteration_ratioStackPlotheeminDR.png $note/fig/uncert/"$s"iter/electron/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlotheeZJpTlowDR.png $note/fig/uncert/"$s"iter/electron/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlotheeZJpThighDR.png $note/fig/uncert/"$s"iter/electron/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlotheeZJpT.png $note/fig/uncert/"$s"iter/electron/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlotheeNJets.png $note/fig/uncert/"$s"iter/electron/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlotheepTjet.png $note/fig/uncert/"$s"iter/electron/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlotheepTZ.png $note/fig/uncert/"$s"iter/electron/; done
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
for s in {0..5}; do mv "$s"iteration_ratioStackPlothee*.png $note/fig/uncert/"$s"iter/electron/extra/; done

# Now we move all muon uncertainties
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
for s in {0..5}; do mv "$s"iteration_ratioStackPlothmmminDR.png $note/fig/uncert/"$s"iter/muon/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlothmmZJpTlowDR.png $note/fig/uncert/"$s"iter/muon/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlothmmZJpThighDR.png $note/fig/uncert/"$s"iter/muon/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlothmmZJpT.png $note/fig/uncert/"$s"iter/muon/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlothmmNJets.png $note/fig/uncert/"$s"iter/muon/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlothmmpTjet.png $note/fig/uncert/"$s"iter/muon/; done
for s in {0..5}; do mv "$s"iteration_ratioStackPlothmmpTZ.png $note/fig/uncert/"$s"iter/muon/; done
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
for s in {0..5}; do mv "$s"iteration_ratioStackPlothmm*.png $note/fig/uncert/"$s"iter/muon/extra/; done


############################################
### Now we move the cross section ratios

# Reco
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv Sherpa_channel_ratio_minDR.png $note/fig/ratios/Unfolded/
mv Sherpa_channel_ratio_ZJpTlowDR.png $note/fig/ratios/Unfolded/
mv Sherpa_channel_ratio_ZJpThighDR.png $note/fig/ratios/Unfolded/
mv Sherpa_channel_ratio_ZJpT.png $note/fig/ratios/Unfolded/
mv Sherpa_channel_ratio_NJets.png $note/fig/ratios/Unfolded/
mv Sherpa_channel_ratio_pTjet.png $note/fig/ratios/Unfolded/
mv Sherpa_channel_ratio_pTZ.png $note/fig/ratios/Unfolded/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv Sherpa_channel_ratio_*.png $note/fig/ratios/Unfolded/extra/

# Truth
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv Sherpa_truth_channel_ratio_minDR.png $note/fig/ratios/Truth/
mv Sherpa_truth_channel_ratio_ZJpTlowDR.png $note/fig/ratios/Truth/
mv Sherpa_truth_channel_ratio_ZJpThighDR.png $note/fig/ratios/Truth/
mv Sherpa_truth_channel_ratio_ZJpT.png $note/fig/ratios/Truth/
mv Sherpa_truth_channel_ratio_NJets.png $note/fig/ratios/Truth/
mv Sherpa_truth_channel_ratio_pTjet.png $note/fig/ratios/Truth/
mv Sherpa_truth_channel_ratio_pTZ.png $note/fig/ratios/Truth/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv Sherpa_truth_channel_ratio_*.png $note/fig/ratios/Truth/extra/

# Double
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv Sherpa_double_channel_ratio_minDR.png $note/fig/ratios/Double/
mv Sherpa_double_channel_ratio_ZJpTlowDR.png $note/fig/ratios/Double/
mv Sherpa_double_channel_ratio_ZJpThighDR.png $note/fig/ratios/Double/
mv Sherpa_double_channel_ratio_ZJpT.png $note/fig/ratios/Double/
mv Sherpa_double_channel_ratio_NJets.png $note/fig/ratios/Double/
mv Sherpa_double_channel_ratio_pTjet.png $note/fig/ratios/Double/
mv Sherpa_double_channel_ratio_pTZ.png $note/fig/ratios/Double/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv Sherpa_double_channel_ratio_*.png $note/fig/ratios/Double/extra/

############################################
### Now we move the Unfolding closure plots

# Electron
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv closure_SherpaheeminDR.png $note/fig/closure/electron/
mv closure_SherpaheeZJpTlowDR.png $note/fig/closure/electron/
mv closure_SherpaheeZJpThighDR.png $note/fig/closure/electron/
mv closure_SherpaheeZJpT.png $note/fig/closure/electron/
mv closure_SherpaheeNJets.png $note/fig/closure/electron/
mv closure_SherpaheepTjet.png $note/fig/closure/electron/
mv closure_SherpaheepTZ.png $note/fig/closure/electron/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv closure_Sherpahee*.png $note/fig/closure/electron/extra/
mv closure_MadGraphhee*.png $note/fig/closure/electron/MadGraph/

# Muon
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv closure_SherpahmmminDR.png $note/fig/closure/muon/
mv closure_SherpahmmZJpTlowDR.png $note/fig/closure/muon/
mv closure_SherpahmmZJpThighDR.png $note/fig/closure/muon/
mv closure_SherpahmmZJpT.png $note/fig/closure/muon/
mv closure_SherpahmmNJets.png $note/fig/closure/muon/
mv closure_SherpahmmpTjet.png $note/fig/closure/muon/
mv closure_SherpahmmpTZ.png $note/fig/closure/muon/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv closure_Sherpahmm*.png $note/fig/closure/muon/extra/
mv closure_MadGraphhmm*.png $note/fig/closure/muon/MadGraph/

############################################
### Now we move the Efficiency plots

# Electron
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv efficiencyheeminDR.png $note/fig/efficiencies/electron/
mv efficiencyheeZJpTlowDR.png $note/fig/efficiencies/electron/
mv efficiencyheeZJpThighDR.png $note/fig/efficiencies/electron/
mv efficiencyheeZJpT.png $note/fig/efficiencies/electron/
mv efficiencyheeNJets.png $note/fig/efficiencies/electron/
mv efficiencyheepTjet.png $note/fig/efficiencies/electron/
mv efficiencyheepTZ.png $note/fig/efficiencies/electron/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv efficiencyhee*.png $note/fig/efficiencies/electron/extra/
# mv 1D efficiencies of ZJpT plots
mv 1Defficiencyhee*.png $note/fig/efficiencies/electron/

# Muon
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv efficiencyhmmminDR.png $note/fig/efficiencies/muon/
mv efficiencyhmmZJpTlowDR.png $note/fig/efficiencies/muon/
mv efficiencyhmmZJpThighDR.png $note/fig/efficiencies/muon/
mv efficiencyhmmZJpT.png $note/fig/efficiencies/muon/
mv efficiencyhmmNJets.png $note/fig/efficiencies/muon/
mv efficiencyhmmpTjet.png $note/fig/efficiencies/muon/
mv efficiencyhmmpTZ.png $note/fig/efficiencies/muon/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv efficiencyhmm*.png $note/fig/efficiencies/muon/extra/
# mv 1D efficiencies of ZJpT plots
mv 1Defficiencyhmm*.png $note/fig/efficiencies/muon/


############################################
### Now we move the unmatched events (fakes) plots

# Electron
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv fakesheeminDR.png $note/fig/unmatched/electron/
mv fakesheeZJpTlowDR.png $note/fig/unmatched/electron/
mv fakesheeZJpThighDR.png $note/fig/unmatched/electron/
mv fakesheeZJpT.png $note/fig/unmatched/electron/
mv fakesheeNJets.png $note/fig/unmatched/electron/
mv fakesheepTjet.png $note/fig/unmatched/electron/
mv fakesheepTZ.png $note/fig/unmatched/electron/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv fakeshee*.png $note/fig/unmatched/electron/extra/
# mv 1D efficiencies of ZJpT plots
mv 1Dfakeshee*.png $note/fig/unmatched/electron/


# Muon
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv fakeshmmminDR.png $note/fig/unmatched/muon/
mv fakeshmmZJpTlowDR.png $note/fig/unmatched/muon/
mv fakeshmmZJpThighDR.png $note/fig/unmatched/muon/
mv fakeshmmZJpT.png $note/fig/unmatched/muon/
mv fakeshmmNJets.png $note/fig/unmatched/muon/
mv fakeshmmpTjet.png $note/fig/unmatched/muon/
mv fakeshmmpTZ.png $note/fig/unmatched/muon/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv fakeshmm*.png $note/fig/unmatched/muon/extra/
# mv 1D efficiencies of ZJpT plots
mv 1Dfakeshmm*.png $note/fig/unmatched/muon/


############################################
### Now we move the purity plots

# Electron
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv purityheeminDR.png $note/fig/purities/electron/
mv purityheeZJpTlowDR.png $note/fig/purities/electron/
mv purityheeZJpThighDR.png $note/fig/purities/electron/
mv purityheeZJpT.png $note/fig/purities/electron/
mv purityheeNJets.png $note/fig/purities/electron/
mv purityheepTjet.png $note/fig/purities/electron/
mv purityheepTZ.png $note/fig/purities/electron/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv purityhee*.png $note/fig/purities/electron/extra/
#
mv 1Dpurityhee*.png $note/fig/purities/electron/

# Muon
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv purityhmmminDR.png $note/fig/purities/muon/
mv purityhmmZJpTlowDR.png $note/fig/purities/muon/
mv purityhmmZJpThighDR.png $note/fig/purities/muon/
mv purityhmmZJpT.png $note/fig/purities/muon/
mv purityhmmNJets.png $note/fig/purities/muon/
mv purityhmmpTjet.png $note/fig/purities/muon/
mv purityhmmpTZ.png $note/fig/purities/muon/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv purityhmm*.png $note/fig/purities/muon/extra/
#
mv 1Dpurityhmm*.png $note/fig/purities/muon/

############################################
### Now we move the response matrices

# Electron
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv response_SherpaheeminDR.png $note/fig/response/electron/
mv response_Sherpahee*ZJpTlowDR.png $note/fig/response/electron/
mv response_Sherpahee*ZJpThighDR.png $note/fig/response/electron/
mv response_Sherpahee*ZJpT.png $note/fig/response/electron/
mv response_SherpaheeNJets.png $note/fig/response/electron/
mv response_SherpaheepTjet.png $note/fig/response/electron/
mv response_SherpaheepTZ.png $note/fig/response/electron/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv response_Sherpahee*.png $note/fig/response/electron/extra/
mv response_MadGraphhee*.png $note/fig/response/electron/MadGraph/
#
mv 1Dresponse_Sherpahee*.png $note/fig/response/electron/
mv 1Dresponse_MadGraphhee*.png $note/fig/response/electron/MadGraph/


# Muon
# Main results: minDR, ZJpT, ZJpT collinear, NJets, pTZ, pTjet
mv response_SherpahmmminDR.png $note/fig/response/muon/
mv response_Sherpahmm*ZJpTlowDR.png $note/fig/response/muon/
mv response_Sherpahmm*ZJpThighDR.png $note/fig/response/muon/
mv response_Sherpahmm*ZJpT.png $note/fig/response/muon/
mv response_SherpahmmNJets.png $note/fig/response/muon/
mv response_SherpahmmpTjet.png $note/fig/response/muon/
mv response_SherpahmmpTZ.png $note/fig/response/muon/
# Extra results: HT, Mjj, NJets500, ZJpT back-to-back
mv response_Sherpahmm*.png $note/fig/response/muon/extra/
mv response_MadGraphhmm*.png $note/fig/response/muon/MadGraph/
#
mv 1Dresponse_Sherpahmm*.png $note/fig/response/muon/
mv 1Dresponse_MadGraphhmm*.png $note/fig/response/muon/MadGraph/


############################################
### Now we move the Control Plots
mv yieldshee*.png $note/fig/ControlPlots/electron/
mv yieldshmm*.png $note/fig/ControlPlots/muon/

# Rivet comparison
# Main results: minDR, ZJpT, ZJpT collinear, ZJpT back-to-back, NJets, pTZ, pTjet
mv RivetComparison*.png $note/fig/RivetComparison/
