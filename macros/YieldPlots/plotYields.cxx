#include "../util.h"
#include "RooUnfoldResponse.h"
#include "RooUnfoldBayes.h"
#include "RooUnfoldSvd.h"
#include "RooUnfoldTUnfold.h"
#include "RooUnfoldIds.h"

void doPlots(StrVMap sortedSampleFiles, TEnv* settings);

void plotYields(){
  // Needed for unfolding
  gSystem->Load("/afs/cern.ch/work/a/alaurier/private/RooUnfold/RooUnfold/libRooUnfold.so");
  // Needed when passing filenames instead of files.
  TH1::AddDirectory(kFALSE);

  setStyle();
  TEnv *settings = openSettingsFile("ZJetsSystplots.data");

  // Define systematics to run over.
  int nSys = settings->GetValue("Systematics",1);

  // Fetch all the required samples
  // FileVMap sortedSampleFiles;
  StrVMap sortedSampleFiles;
  Str filePath = settings->GetValue("filePath", " ");
  StrV inputCategories = vectorize(settings->GetValue("inputCategories", " "));
  for (auto input : inputCategories) {
    StrV sampleList = vectorize(settings->GetValue(input.Data(), " "));
    for (auto sample : sampleList)
      sortedSampleFiles[input].push_back(filePath + sample + ".root");
      // sortedSampleFiles[input].push_back(openFile(filePath + sample + ".root"));
  }
  doPlots(sortedSampleFiles, settings);

  return;
}

void doPlots(StrVMap sortedSampleFiles, TEnv* settings) {

  // Unfolding options
  double lumiUncert = settings->GetValue("LumiUncert.",0.017);

  int debug = 0;
  // Get basic values needed
  double Totlumi = settings->GetValue("luminosity", 0.0);
  StrV plotList = vectorize(settings->GetValue("PlotList", " "));
  StrV channels = vectorize(settings->GetValue("channels", " "));
  StrV inputCategories = vectorize(settings->GetValue("inputCategories", " "));
  int nSys = settings->GetValue("Systematics", 1);

  if (debug > 1){
    for (auto plot : plotList)
      std::cout << "alex plot = " << plot << std::endl;
    for (auto channel : channels)
      std::cout << "alex channel = " << channel << std::endl;
    for (auto input : inputCategories)
      std::cout << "alex input = " << input << std::endl;
  }

  HistVMap SummedMeas;
  HistVMap RebinnedMeas;

  for (auto region : channels){ // hee or hmm

    // Get plotting information ready
    TCanvas *can = new TCanvas("", "", 800, 800);

    for(auto plot : plotList){
      can->Clear();
      can->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      can->SetBottomMargin(0.12);
      can->SetRightMargin(0.04);

      Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
      Str binType = settings->GetValue(plot + ".binType", " ");
      Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
      Str Ylabel = settings->GetValue(plot + ".Ylabel", " ");

      // For rebinning purposes.
      int nBins;
      NumV bins;
      if (binType.Contains("regular")) {
        nBins = settings->GetValue(plot + ".nBins", -1);
        double min = settings->GetValue(plot + ".min", 0.0);
        double max = settings->GetValue(plot + ".max", 0.0);
        double step = (max-min)/nBins;
        for(int i=0 ; i<nBins+1; i++)
           bins.push_back(min+i*step);
      }
      else if (binType.Contains("variable")) {
        bins = numberize(settings->GetValue(plot + ".bins", " "));
        nBins = bins.size() - 1;
      }
      else{fatal("BinType is not regular or variable!");}

      Double_t BinsVec[nBins+1];
      for (int n=0; n<nBins+1;n++)
        BinsVec[n]=bins[n];

      // Empty the maps of histograms and reinitialize them (for safety reasons)
      SummedMeas.clear(); // Reset map of histograms
      RebinnedMeas.clear(); // Reset map of histograms
      for (auto input : inputCategories){ // Make new vectors in each map element
        for (int sys=0; sys<nSys; sys++){
          TString inputSys = input+sys;
          RebinnedMeas[inputSys] = new HistV;
          SummedMeas[inputSys] = new HistV;
        }
      }

      for(auto input : inputCategories){ // Loop through all categories
        for (auto sampleFileName : sortedSampleFiles[input]) { // Loop through all files of category
          TFile *sampleFile = TFile::Open(sampleFileName);
          TH1F* temp = new TH1F();

          // Get histogram and rebin it.
          if (!input.Contains("data")){ // if not data
            for(int sys=0; sys<nSys; sys++){
              temp = getHisto(sampleFile, region+var+sys);
              temp = (TH1F*)temp->Rebin(nBins, temp->GetName(), BinsVec);
              temp->SetTitle(";"+Xlabel+";"+Ylabel);
              TString newinputLabel = input+sys;
              RebinnedMeas[newinputLabel]->push_back((TH1F*) temp->Clone());
            }
          }
          else{
            temp = getHisto(sampleFile, region+var+"0");
            temp = (TH1F*)temp->Rebin(nBins, temp->GetName(), BinsVec);
            temp->SetTitle(";"+Xlabel+";"+Ylabel);
            RebinnedMeas["data0"]->push_back((TH1F*) temp->Clone());
          }
          sampleFile->Close();
        }
      } // end of input categories

      // Add the histograms together!
      // this order : SherpaZJets/MGZJets Diboson Top Ztt WJets
      for (int sys=0; sys<nSys; sys++){
        TH1F* sh = (TH1F*) RebinnedMeas["SherpaZJets"+std::to_string(sys)]->at(0)->Clone();
        TH1F* mg = (TH1F*) RebinnedMeas["MGZJets"+std::to_string(sys)]->at(0)->Clone();
        TH1F* wjets = (TH1F*) RebinnedMeas["WJets"+std::to_string(sys)]->at(0)->Clone();
        TH1F* ztt = (TH1F*) RebinnedMeas["Ztt"+std::to_string(sys)]->at(0)->Clone();
        TH1F* top = (TH1F*) RebinnedMeas["Top"+std::to_string(sys)]->at(0)->Clone();
        TH1F* diboson = (TH1F*) RebinnedMeas["Diboson"+std::to_string(sys)]->at(0)->Clone();
        ztt->Add(wjets);
        top->Add(ztt);
        diboson->Add(top);
        sh->Add(diboson);
        mg->Add(diboson);
        SummedMeas["SherpaZJets"+std::to_string(sys)]->push_back((TH1F*) sh->Clone());
        SummedMeas["MGZJets"+std::to_string(sys)]->push_back((TH1F*) mg->Clone());
        SummedMeas["Diboson"+std::to_string(sys)]->push_back((TH1F*) diboson->Clone());
        SummedMeas["Top"+std::to_string(sys)]->push_back((TH1F*) top->Clone());
        SummedMeas["Ztt"+std::to_string(sys)]->push_back((TH1F*) ztt->Clone());
        SummedMeas["WJets"+std::to_string(sys)]->push_back((TH1F*) wjets->Clone());
      }
      // Get data
      TH1F* Data = (TH1F*) RebinnedMeas["data0"]->at(0)->Clone();
      TH1F* Sherpa = (TH1F*) SummedMeas["SherpaZJets0"]->at(0)->Clone();
      TH1F* MadGraph = (TH1F*) SummedMeas["MGZJets0"]->at(0)->Clone();
      TH1F* Diboson = (TH1F*) SummedMeas["Diboson0"]->at(0)->Clone();
      TH1F* Top = (TH1F*) SummedMeas["Top0"]->at(0)->Clone();
      TH1F* Ztt = (TH1F*) SummedMeas["Ztt0"]->at(0)->Clone();
      TH1F* WJets = (TH1F*) SummedMeas["WJets0"]->at(0)->Clone();

      // For the systematics evaluation!
      TH1F* Sherpa_up = (TH1F*) Sherpa->Clone();
      TH1F* Sherpa_down = (TH1F*) Sherpa->Clone();
      std::vector<double> var_up, var_down, nom;
      for (int i=0; i<nBins; i++){
        var_up.push_back(0.);
        var_down.push_back(0.);
        nom.push_back(Sherpa->GetBinContent(i+1));
      }

      // Start the error loops!
      TH1F* variation = new TH1F();
      TH1F* variation2 = new TH1F();
      for (int sys=1; sys<(nSys+1)/2; sys++){ // This assumes an odd number of total systematics! (nominal + even)
        variation = (TH1F*) SummedMeas["SherpaZJets"+std::to_string(2*sys-1)]->at(0)->Clone();
        variation2 = (TH1F*) SummedMeas["SherpaZJets"+std::to_string(2*sys)]->at(0)->Clone();
        for (int i=0; i<nBins; i++){
          double var = variation->GetBinContent(i+1) - nom[i];
          double var2 = variation2->GetBinContent(i+1) - nom[i];
          if ( (var > 0 ) && (var2 > 0) ){ // If both __1up and __1down are positive, get the max.
            double Var = std::max(var,var2);
            var_up[i] = var_up[i] + Var*Var;
          }
          else if ( (var < 0 ) && (var2 < 0) ){
            double Var = std::max( std::abs(var), std::abs(var2) );
            var_down[i] = var_down[i] + Var*Var;
          }
          else{
            if (var>=0) var_up[i] = var_up[i] + var*var;
            else var_down[i] = var_down[i] + var*var;
            if (var2>=0) var_up[i] = var_up[i] + var2*var2;
            else var_down[i] = var_down[i] + var2*var2;
          }
        }
      }

      for (int i=0; i<nBins; i++){
        // ADDING LUMINOSITY AND UNFOLDING UNCERTAINTY
        double stat = std::pow(Sherpa->GetBinError(i+1),2);
        // double stat = 0.;
        double lumi = std::pow(Sherpa->GetBinContent(i+1)*lumiUncert,2);
        Sherpa_up->SetBinContent(i+1, nom[i] + std::sqrt(var_up[i] + stat + lumi) );
        Sherpa_down->SetBinContent(i+1, nom[i] - std::sqrt(var_down[i] + stat + lumi ) );
        Sherpa_up->SetBinError(i+1, 0.);
        Sherpa_down->SetBinError(i+1, 0.);
        // Sherpa->SetBinError(i+1, std::sqrt(nom[i]));
        // Sherpa->SetBinError(i+1, std::sqrt(Sherpa->GetBinContent(i+1)));
        // MadGraph->SetBinError(i+1, std::sqrt(MadGraph->GetBinContent(i+1)));
      }

      // Start plotting
      can->Clear();
      double ratio = 0.36, margin = 0.02;
      can->cd();
      gPad->SetBottomMargin(ratio);
      gPad->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      // can->SetBottomMargin(0.12);
      can->SetRightMargin(0.04);
      // Do some plotting
      Sherpa_up->SetTitle(";"+Xlabel+";"+Ylabel);
      // Not log plots
      if (plot.Contains("ZJpT")){
        gPad->SetLogy(0);
        Sherpa_up->SetMaximum(MadGraph->GetMaximum()*1.8);
        Sherpa_up->SetMinimum(0);
      }
      else{
        gPad->SetLogy(1);
        if(Sherpa_up->GetMinimum() > 100)
          Sherpa_up->SetMinimum(Sherpa_up->GetMinimum()*0.05);
        Sherpa_up->SetMaximum(Sherpa_up->GetMaximum()*100);
        if (plot.Contains("minDR")){
          Sherpa_up->SetMinimum(1.);
          Sherpa_up->SetMaximum(MadGraph->GetMaximum()*280);
        }
      }
      if (plot.Contains("NJets")) Sherpa_up->SetMaximum(Sherpa_up->GetMaximum()*20.); // Scale it up by 5 to help plotting
      // Set up the hash fills
      Sherpa_up->SetFillColor(1);
      Sherpa_up->SetFillStyle(3005);
      Sherpa_up->SetMarkerColor(0);
      Sherpa_up->GetXaxis()->SetLabelSize(0.);
      Sherpa_down->SetFillColor(10);
      Sherpa_down->SetFillStyle(1001);
      Sherpa_up->SetLineColor(10);
      Sherpa_down->SetLineColor(10);
      // Set Data
      Data->SetLineWidth(1);
      Data->SetFillStyle(0);
      Data->SetLineColor(1);
      Data->SetMarkerStyle(20);
      // Set the different colour contributions
      Sherpa->SetLineColor(857);
      MadGraph->SetLineColor(807);
      Data->SetLineColor(kBlack);
      Diboson->SetFillColor(419);
      Top->SetFillColor(417);
      Ztt->SetFillColor(600);
      WJets->SetFillColor(591);
      // Get rid of markers!
      Sherpa->SetMarkerSize(0);
      MadGraph->SetMarkerSize(0);
      Diboson->SetMarkerSize(0);
      Top->SetMarkerSize(0);
      Ztt->SetMarkerSize(0);
      WJets->SetMarkerSize(0);
      // Give black outline to stack plots!
      Diboson->SetLineColor(kBlack);
      Top->SetLineColor(kBlack);
      WJets->SetLineColor(kBlack);
      Diboson->SetLineColor(kBlack);
      //
      Sherpa_up->GetXaxis()->SetTitleOffset(1.4);
      Sherpa_up->GetYaxis()->SetLabelSize(0.03);
      Sherpa_up->GetYaxis()->SetTitleOffset(1.4);
      Sherpa_up->GetXaxis()->SetLabelSize(0);

      // Define legend
      // leg = new TLegend(0.65, 0.80, 0.80, 0.93);
      TLegend* leg = new TLegend(0.65, 0.70, 0.80, 0.93);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.030);
      leg->SetFillStyle(0);

      leg->AddEntry(Data,"Data stat. unc","pe");
      leg->AddEntry(Sherpa,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pl");
      // leg->AddEntry(Sherpa_up,"MC uncertainty", "f");
      leg->AddEntry(Sherpa_up,"MC syst. uncertainty", "f");
      // leg->AddEntry(Sherpa_up,"MC stat. #oplus syst.", "f");
      leg->AddEntry(MadGraph,"Z#rightarrow ll, #scale[0.9]{MG5_aMC+P}#scale[0.8]{Y}#scale[0.9]{8}","pl");
      leg->AddEntry(Diboson,"Diboson","pef");
      leg->AddEntry(Top,"Top quark","pef");
      leg->AddEntry(Ztt,"Z #rightarrow #tau#tau","pef");
      leg->AddEntry(WJets,"W + jets","pef");

      Sherpa_up->Draw("HIST");
      Sherpa_down->Draw("SAME HIST");
      Data->Draw("SAME");
      Sherpa->Draw("SAME HIST");
      MadGraph->Draw("SAME HIST");
      Diboson->Draw("SAME HIST");
      Top->Draw("SAME HIST");
      Ztt->Draw("SAME HISt");
      WJets->Draw("SAME HIST");

      drawPlotInfo(0.15,0.9, plot, region, Totlumi, 0.030);
      leg->Draw();
      gPad->RedrawAxis();
      gPad->RedrawAxis("G");

      // Now add the ratio plot: first the pad
      TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
      p->SetLogx(0);
      p->Modified();
      p->Update();
      p->SetTopMargin(1.0 - ratio);
      p->SetFillStyle(0);
      p->SetTicks();
      p->SetGridy();
      p->Draw();
      p->cd();

      // Make the ratio plot!
      TH1F* ratio_up = (TH1F*) Sherpa_up->Clone();
      TH1F* ratio_down = (TH1F*) Sherpa_down->Clone();
      TH1F* ratio_sherpa = (TH1F*) Data->Clone();
      TH1F* ratio_madgraph = (TH1F*) Data->Clone();
      // Divide them
      for (int i=1; i<nBins+1; i++){
        if (Sherpa->GetBinContent(i) > 0){
          ratio_up->SetBinContent(i, Sherpa_up->GetBinContent(i)/Sherpa->GetBinContent(i));
          ratio_down->SetBinContent(i, Sherpa_down->GetBinContent(i)/Sherpa->GetBinContent(i));
        }
        else {
          ratio_up->SetBinContent(i, 1.);
          ratio_down->SetBinContent(i, 1.);
        }
        ratio_up->SetBinError(i, 0.);
        ratio_down->SetBinError(i, 0.);
      }
      // ratio_up->Divide((TH1F*) Sherpa);
      // ratio_down->Divide((TH1F*) Sherpa);
      ratio_sherpa->Divide((TH1F*) Sherpa);
      ratio_madgraph->Divide((TH1F*) MadGraph);

      ratio_up->SetFillColor(1);
      ratio_up->SetFillStyle(3005);
      ratio_down->SetFillColor(10);
      ratio_down->SetFillStyle(1001);
      ratio_down->SetMarkerColor(10);

      ratio_up->SetLineColor(0);
      ratio_down->SetLineColor(0);
      ratio_sherpa->SetLineColor(857); // blue
      ratio_madgraph->SetLineColor(807); // red
      ratio_sherpa->SetMarkerSize(0);
      ratio_madgraph->SetMarkerSize(0);
      ratio_sherpa->SetLineWidth(2);
      ratio_madgraph->SetLineWidth(2);


      // Define the ratio plot info
      gStyle->SetErrorX(0.5);
      ratio_up->GetYaxis()->SetLabelSize(0.03);
      ratio_up->GetYaxis()->SetTitle("Data/Pred.");
      ratio_up->GetXaxis()->SetLabelSize(0.03);
      ratio_up->GetXaxis()->SetTitleOffset(1.4);
      ratio_up->GetYaxis()->SetTitleOffset(1.4);
      ratio_up->GetYaxis()->SetRangeUser(0.4, 1.4);
      // ratioPlot->GetYaxis()->SetRangeUser(0.3, 1.7);
      // ratio_up->Draw("P");
      // ratio_up->Draw("SAME P");
      ratio_up->Draw("HIST");
      ratio_down->Draw("SAME HIST");
      ratio_sherpa->Draw("SAME");
      ratio_madgraph->Draw("SAME");
      // Draw Black line centered at 1.
      TH1F* ratioOne = (TH1F*) Sherpa->Clone();
      for (int i=1; i<nBins+1;i++){
       ratioOne->SetBinContent(i,1.);
       ratioOne->SetBinError(i,0.);
      }
      ratioOne->SetLineColor(kBlack);
      ratioOne->SetMarkerSize(0.);
      ratioOne->Draw("SAME HIST");
      gPad->RedrawAxis();
      gPad->RedrawAxis("G");

      can->Print("yields"+region+plot+".png");
      // can->Print(pdf);

    } // end of plotList

  } // end of hee or hmm

  return;
}
