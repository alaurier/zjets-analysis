#include "../ZJetsplots.h"
#include "../util.h"

void plotsFromHists(FileVMap sortedSampleFiles, Str plotSet, TEnv* settings, Str channel);
void saveQCDTemplate(TEnv* settings);
void doPlots(TEnv* settings);

int makeMJTemplate(){
  setStyle();
  Str config = "ZJetsplots.data";
  std::cout << "config file: " << config.Data() << std::endl;

  TEnv *settings = openSettingsFile(config);

  saveQCDTemplate(settings);
  doPlots(settings);
  // StrV plotSetList = vectorize(settings->GetValue("plotSetList", " "));
  // StrV inputCategories = vectorize(settings->GetValue("inputCategories", " "));
  // StrV channels = vectorize(settings->GetValue("channels", " "));
  //
  // for (auto chan : channels){
  //   FileVMap sortedSampleFiles;
  //   Str filePath = settings->GetValue("filePath", " ");
  //   for (auto input : inputCategories) {
  //     StrV sampleList = vectorize(settings->GetValue(input.Data(), " "));
  //     for (auto sample : sampleList)
  //       sortedSampleFiles[input].push_back(openFile(filePath + sample + ".root"));
  //   }
  //
  //   for(auto plotSet : plotSetList)
  //     plotsFromHists(sortedSampleFiles, plotSet, settings, chan);
  // }

   return 0;
}

void saveQCDTemplate(TEnv* settings){
  TH1::AddDirectory(kFALSE);


  Str heefilePath = settings->GetValue("ElectronfilePath", " ");
  Str hmmfilePath = settings->GetValue("MuonfilePath", " ");
  TString filename = "MultijetTemplate.root";
  TFile hisfile(filename,"recreate");
  hisfile.Close();

  StrV plotSetList = vectorize(settings->GetValue("plotList", " "));
  StrV channels = vectorize(settings->GetValue("channels", " "));

  TH1F* Template = new TH1F();
  TH1F* temp = new TH1F();
  TH1F* copy = new TH1F();

  for (auto chan : channels){ // do both channels
    TString newPath;
    if (chan == "hee") newPath = heefilePath;
    else if (chan == "hmm") newPath = hmmfilePath;
    else fatal("NOT CORRECT CHANNEL!");
    TFile *data = openFile(newPath + "data.root");
    TFile *Sherpa = openFile(newPath + "SherpaZJets.root");
    TFile *Diboson = openFile(newPath + "Diboson.root");
    TFile *TopQuark = openFile(newPath + "TopQuark.root");
    TFile *Ztt = openFile(newPath + "Ztt.root");
    TFile *WJets = openFile(newPath + "WJets.root");

    for (auto plot : plotSetList){
       // do each plot
      Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
      Template = getHisto(data, chan+var+"0");
      // Remove Sherpa
      temp = getHisto(Sherpa, chan+var+"0");
      Template->Add(temp, -1.);
      // Remove Diboson
      temp = getHisto(Diboson, chan+var+"0");
      Template->Add(temp, -1.);
      // Remove TopQuark
      temp = getHisto(TopQuark, chan+var+"0");
      Template->Add(temp, -1.);
      // Remove WJets
      temp = getHisto(WJets, chan+var+"0");
      Template->Add(temp, -1.);
      // Remove Ztt
      temp = getHisto(Ztt, chan+var+"0");
      Template->Add(temp, -1.);
      // Write template to file
      TFile hisfile(filename,"update");
      Template->Write();
    }
    hisfile.Close();
    data->Close();
    Sherpa->Close();
    Diboson->Close();
    TopQuark->Close();
    Ztt->Close();
    WJets->Close();
  }
  return;
}

void doPlots(TEnv* settings){
  TH1::AddDirectory(kFALSE);


  Str heefilePath = settings->GetValue("ElectronfilePath", " ");
  Str hmmfilePath = settings->GetValue("MuonfilePath", " ");

  StrV plotSetList = vectorize(settings->GetValue("plotList", " "));
  StrV channels = vectorize(settings->GetValue("channels", " "));
  double Totlumi = settings->GetValue("luminosity", 0.0);

  TH1F* data = new TH1F();
  TH1F* sherpa = new TH1F();
  TH1F* diboson = new TH1F();
  TH1F* topQuark = new TH1F();
  TH1F* ztt = new TH1F();
  TH1F* wJets = new TH1F();

  for (auto chan : channels){ // do both channels
    TString newPath;
    if (chan == "hee") newPath = heefilePath;
    else if (chan == "hmm") newPath = hmmfilePath;
    else fatal("NOT CORRECT CHANNEL!");
    TFile *Data = openFile(newPath + "data.root");
    TFile *Sherpa = openFile(newPath + "SherpaZJets.root");
    TFile *Diboson = openFile(newPath + "Diboson.root");
    TFile *TopQuark = openFile(newPath + "TopQuark.root");
    TFile *Ztt = openFile(newPath + "Ztt.root");
    TFile *WJets = openFile(newPath + "WJets.root");

    TCanvas *can = new TCanvas("", "", 800, 800);


    for (auto plot : plotSetList){

      std::cout << "Doing " << plot << " in channel " << chan << std::endl;

      can->Clear();
      can->SetLogy(0);
      can->SetLogx(0);
      can->Modified();
      can->Update();
      gPad->SetTicks();
      // can->SetBottomMargin(0.12);
      // can->SetRightMargin(0.04);



       // do each plot
      Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
      data = getHisto(Data, chan+var+"0");
      // Get Sherpa
      sherpa = getHisto(Sherpa, chan+var+"0");
      // Get Diboson
      diboson = getHisto(Diboson, chan+var+"0");
      // Get TopQuark
      topQuark = getHisto(TopQuark, chan+var+"0");
      // Get Ztt
      ztt = getHisto(Ztt, chan+var+"0");
      // Get WJets
      wJets = getHisto(WJets, chan+var+"0");
      // Add them all for the stack plot: Order from bottom to top should be wjets, ztt, top, diboson, sherpa
      ztt->Add(wJets);
      topQuark->Add(ztt);
      diboson->Add(topQuark);
      sherpa->Add(diboson);

      // Set proper line and fill colors
      sherpa->SetLineColor(857);
      diboson->SetLineColor(1); // Black line
      diboson->SetFillColor(419); // Dark Green fill
      topQuark->SetLineColor(1);
      topQuark->SetFillColor(417);// Light green fill
      ztt->SetLineColor(1);
      ztt->SetFillColor(600);
      wJets->SetLineColor(1);
      wJets->SetFillColor(591);
      // Get rid of markers!
      sherpa->SetMarkerSize(0);
      diboson->SetMarkerSize(0);
      topQuark->SetMarkerSize(0);
      ztt->SetMarkerSize(0);
      wJets->SetMarkerSize(0);
      // For data
      data->SetLineWidth(1);
      data->SetFillStyle(0);
      data->SetLineColor(1);
      data->SetMarkerStyle(20);
      // Draw plot and legend
      TLegend* leg = new TLegend(0.7, 0.70, 0.85, 0.93);
      leg->SetFillColor(0);
      leg->SetBorderSize(0);
      leg->SetTextSize(0.030);
      leg->SetFillStyle(0);

      leg->AddEntry(data,"Data","pe");
      leg->AddEntry(sherpa,"Z#rightarrow ll, S#scale[0.8]{HERPA}#scale[0.9]{ 2.2}","pl");
      leg->AddEntry(diboson,"Diboson","pef");
      leg->AddEntry(topQuark,"Top quark","pef");
      leg->AddEntry(ztt,"Z #rightarrow #tau#tau","pef");
      leg->AddEntry(wJets,"W + jets","pef");

      // Reset y scale
      if (plot == "Mll" && chan == "hee") sherpa->SetMaximum(260.);
      if (plot == "Mll" && chan == "hmm") sherpa->SetMaximum(130.);
      if (plot == "Mll2" && chan == "hee") sherpa->SetMaximum(25.);
      if (plot == "Mll2" && chan == "hmm") sherpa->SetMaximum(20.);
      if (plot == "Mll3" && chan == "hee") sherpa->SetMaximum(4.);
      if (plot == "Mll3" && chan == "hmm") sherpa->SetMaximum(8.);
      if (plot == "MllHighPt" && chan == "hee") sherpa->SetMaximum(13.);
      if (plot == "MllHighPt" && chan == "hmm") sherpa->SetMaximum(8.);
      sherpa->SetMinimum(0.);
      // Set correct labels
      Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
      Str Ylabel = settings->GetValue(plot + ".Ylabel", " ");
      sherpa->SetTitle(";"+Xlabel+";"+Ylabel);
      // set x scale: For some reason, I defined histograms up to 200GeV even though I stop filling at 160
      sherpa->GetXaxis()->SetRangeUser(50., 160.);

      sherpa->Draw("HIST");
      diboson->Draw("SAME HIST");
      topQuark->Draw("SAME HIST");
      ztt->Draw("SAME HIST");
      wJets->Draw("SAME HIST");
      data->Draw("SAME");


      drawPlotInfo(0.15,0.9, plot, chan, Totlumi, 0.03);
      if (plot == "Mll") drawText(0.15, 0.9-4*0.03*1.25, "NJets >= 1",0,1,0.03);
      if (plot == "Mll2") drawText(0.15, 0.9-4*0.03*1.25, "NJets >= 2",0,1,0.03);
      if (plot == "Mll3") drawText(0.15, 0.9-4*0.03*1.25, "NJets >= 3",0,1,0.03);
      if (plot == "MllHighPt") drawText(0.15, 0.9-4*0.03*1.25, "Leading jet p_{T} #geq 500GeV",0,1,0.03);
      leg->Draw();
      gPad->RedrawAxis();
      gPad->RedrawAxis("G");


      can->Print("QCD_CR_"+chan+plot+".png");

    }
    Data->Close();
    Sherpa->Close();
    Diboson->Close();
    TopQuark->Close();
    Ztt->Close();
    WJets->Close();
  }
  return;
}


void plotsFromHists(FileVMap sortedSampleFiles, Str plotSet, TEnv* settings, Str chan){

  TString filename = chan+"MultijetTemplate.root";

  TH1::AddDirectory(kFALSE);

  char linLog = (char)(settings->GetValue(plotSet, 1));

  Str channel;
  int chanNo =-1;
  if(chan=="hem"){
    channel = "#it{Z}#rightarrow#it{ll}";
    chanNo = 2;
  }
  if(chan=="hmm"){
    channel = "#it{Z}#rightarrow#it{#mu#mu}";
    chanNo = 0;
  }
  if(chan=="hee"){
    channel = "#it{Z}#rightarrow#it{ee}";
    chanNo = 1;
  }
  if (chanNo <0) fatal("Is neither ee, mumu or ll!");
  TFile hisfile(filename,"update");


  double Totlumi = settings->GetValue("luminosity", 0.0);

  StrV plotList = vectorize(settings->GetValue(plotSet+".plotList", " "));
  StrV inputCategories = vectorize(settings->GetValue("inputCategories", " "));

  HistVMap sortedHists;

  for(auto plot : plotList){
    sortedHists.clear();
    TString histoName;

    for(auto label : inputCategories)
      sortedHists[label] = new HistV;
    //   for(auto input : inputCategories){
    //
    //     Str inputLabel = settings->GetValue(input+".Label", " ");
    //     //printf("%s %s\n", input.Data(), inputLabel.Data());
    //
    //     for (auto sampleFile : sortedSampleFiles[period+"_"+input]) {
    //       double sf = getScaleFactor(xsecs, sampleFile, sampleFile->GetName(), lumi);
    //       if (!input.Contains("data")) sf = -1 * sf;
    //       TH1F* temp = new TH1F();
    //       Str var = settings->GetValue(plot + ".var", " ");
    //       Str binType = settings->GetValue(plot + ".binType", " ");
    //       Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
    //       Str Ylabel = settings->GetValue(plot + ".Ylabel", " ");
    //       if (chanNo ==0 && plot.Contains("Mll")) Xlabel = "M_{#mu#mu} [GeV]";
    //       if (chanNo ==1 && plot.Contains("Mll")) Xlabel = "M_{ee} [GeV]";
    //
    //       // For rebinning purposes.
    //       int nBins;
    //       NumV bins;
    //       if (binType.Contains("regular")) {
    //         nBins = settings->GetValue(plot + ".nBins", -1);
    //         double min = settings->GetValue(plot + ".min", 0.0);
    //         double max = settings->GetValue(plot + ".max", 0.0);
    //         double step = (max-min)/nBins;
    //         for(int i=0 ; i<nBins+1; i++)
    //            bins.push_back(min+i*step);
    //       }
    //       else if (binType.Contains("variable")) {
    //         bins = numberize(settings->GetValue(plot + ".bins", " "));
    //         nBins = bins.size() - 1;
    //       }
    //       else{fatal("BinType is not regular or variable!");}
    //
    //       Double_t BinsVec[nBins+1];
    //       for (int n=0; n<nBins+1;n++)
    //         BinsVec[n]=bins[n];
    //
    //       histoName=region+var+"0";
    //       temp = getHisto(sampleFile, region+var+"0");
    //       temp->Scale(sf);
    //       temp = (TH1F*)temp->Rebin(nBins, temp->GetName(), BinsVec);
    //       temp->SetTitle(";"+Xlabel+";"+Ylabel);
    //       sortedHists[inputLabel]->push_back((TH1F*) temp->Clone());
    //     }
    //   }
    //
    // /////////
    //
    // HistMap summedHists;
    // for (auto label : inputLabels) {
    //   if (!label.Contains("All Data")){ // "All Data" histograms dont exist yet
    //   TH1F *sum = (TH1F*) sortedHists[label]->at(0)->Clone();
    //      sum->Reset();
    //      for (auto h : *(sortedHists[label]))
    //        sum->Add(h);
    //      summedHists[label] = sum;
    //   }
    // }
    //
    // if (plotPeriod.Contains("all")) { // Add all 3 years
    //   TH1F *sum = (TH1F*) sortedHists["2015-16 Data"]->at(0)->Clone();
    //   TH1F *sum1 = (TH1F*) sortedHists["2017 Data"]->at(0)->Clone();
    //   TH1F *sum2 = (TH1F*) sortedHists["2018 Data"]->at(0)->Clone();
    //   sum->Add(sum1);
    //   sum->Add(sum2);
    //   summedHists["All Data"] = sum;
    // }
    //
    // // This is the equivalent of doStackMC
    // // If all years combined, remove the data 15,17,18 labels
    // if (inputLabels[0] == "All Data") {
    //   inputLabels.erase(std::remove(inputLabels.begin(), inputLabels.end(), "2015-16 Data"), inputLabels.end());
    //   inputLabels.erase(std::remove(inputLabels.begin(), inputLabels.end(), "2017 Data"), inputLabels.end());
    //   inputLabels.erase(std::remove(inputLabels.begin(), inputLabels.end(), "2018 Data"), inputLabels.end());
    // }
    // StrV::iterator it1, it2;
    // TH1F* data;
    // for(auto input : inputLabels){
    // // Data only histos
    //   if (!input.Contains("Data"))
    //     continue;
    //   data = (TH1F*) summedHists[input]->Clone(histoName);
    // }
    // std::cout << "========= for plot " << plot << "   ============ " << std::endl;
    // std::cout << "data integral = " << data->Integral() << std::endl;
    // for (it1 = inputLabels.begin(); it1 != inputLabels.end(); it1++) {
    //   // Not Data
    //   if ((*it1).Contains("Data")) continue;
    //   if ((*it1).Contains("MG")) continue;
    //   if (!(*it1).Contains("Sherpa")) continue;
    //   std::cout << "alex MC label = " << (*it1).Data() << std::endl;
    //   it2 = it1;
    //   TH1F* sum = (TH1F*) summedHists[*it2]->Clone();
    //   sum->Reset();
    //   for (; it2 != inputLabels.end(); it2++) {
    //     std::cout << "alex MC includes = " << (*it2).Data() << std::endl;
    //     if ((*it2).Contains("Data")) continue;
    //     if ((*it2).Contains("MG")) continue;
    //     sum->Add(summedHists[*it2]);
    //   }
    //   std::cout << "MC integral = " << sum->Integral() << std::endl;
    //   data->Add(sum);
    // }
    // std::cout << "Template integral = " << data->Integral() << std::endl;
    // data->Write();


    // drawStackMC(settings, plot, summedHists, inputLabels, colours, can, leg, region, channel, Totlumi, pdf, logX, 1, chanNo);
  }

}
