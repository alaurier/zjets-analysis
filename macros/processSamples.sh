export ZJetsdata=/afs/cern.ch/work/a/alaurier/private/ZJets/source/zjets-analysis/data
hadd data17.root *data17*_13TeV*channel3*/*.root
hadd data18.root *data18*_13TeV*channel3*/*.root
rm -rf user.alaurier*data17*_13TeV*
rm -rf user.alaurier*data18*_13TeV*
hadd data15-16.root *data*_13TeV*channel3*/*.root
rm -rf user.alaurier*data*_13TeV*
mkdir mc16a mc16d mc16e
for s in $(ls | grep -v data | cut -f5,6,7,8 -d. | rev | cut -c8- | rev); do hadd $s.root *$s*/root; done
rm -rf *_hist
mv *2016.mc16*root mc16a
mv *2017.mc16*root mc16d
mv *2018.mc16*root mc16e
for line in $(more $ZJetsdata/mc16a_samples.config); do if [[ $line = *mc16* ]]; then mv mc16a/$(ls mc16a/ | grep $(echo $line | cut -d. -f2)) mc16a/$(echo $line | cut -d. -f3).root; fi; done
for line in $(more $ZJetsdata/mc16d_samples.config); do if [[ $line = *mc16* ]]; then mv mc16d/$(ls mc16d/ | grep $(echo $line | cut -d. -f2)) mc16d/$(echo $line | cut -d. -f3).root; fi; done
for line in $(more $ZJetsdata/mc16e_samples.config); do if [[ $line = *mc16* ]]; then mv mc16e/$(ls mc16e/ | grep $(echo $line | cut -d. -f2)) mc16e/$(echo $line | cut -d. -f3).root; fi; done
