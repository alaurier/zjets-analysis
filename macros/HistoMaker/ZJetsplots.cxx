#define ZJetsplots_cxx
#include "ZJetsplots.h"
#include "util.h"

void cutFlow(FileVMap sortedSampleFiles, TEnv* settings);
void plotsFromHists(FileVMap sortedSampleFiles, Str plotSet, TEnv* settings);
void doEfficiencyPlots(FileVMap sortedSampleFiles, Str plotSet, TEnv* settings);
void drawStackMC(TEnv* settings, Str plot, HistMap hists, StrV labels, IntV colours, TCanvas *can, TLegend* leg, Str region, Str channel, double lumi, Str pdf, int logX, int logY, int chanNo);

void doHistograms(FileVMap sortedSampleFiles, TEnv* settings, StrVMap filenameMap);
void doHistograms(TEnv* settings, StrVMap filenameMap);

// int main(){
int doHistos(){
   setStyle();
   Str config = "ZJetsplots.data";
   std::cout << "ALEX!" << std::endl;

   std::cout << "config file: " << config.Data() << std::endl;
   TEnv *settings = openSettingsFile(config);
   StrV periods = vectorize(settings->GetValue("periods", " "));

   bool makeCutFlow = (bool)settings->GetValue("makeCutFlow", 0);
   bool makePlots = (bool)settings->GetValue("makePlots", 0);
   bool makeHistos = (bool)settings->GetValue("makeHistos", 0);
   bool makeEfficiency = (bool)settings->GetValue("makeEff", 0);

   FileVMap sortedSampleFiles;
   StrVMap sortedFileNames;
   if(makePlots){
     for (auto period : periods) {
       Str filePath = settings->GetValue("filePath."+period, " ");
       StrV inputCategories = vectorize(settings->GetValue("inputCategories."+period, " "));
       for (auto input : inputCategories) {
         StrV sampleList = vectorize(settings->GetValue(input.Data(), " "));
         for (auto sample : sampleList) {
           //printf("opening file %-30s : %-70s\r", (period+"_"+input+"_hist").Data(), sample.Data());
           sortedSampleFiles[period+"_"+input].push_back(openFile(filePath + sample + "_hist.root"));
         }
       }
       //printf("\n");
     }
   }
   else if (makeHistos || makeCutFlow) {
      for (auto period : periods) {
         Str filePath = settings->GetValue("filePath."+period, " ");
         StrV inputCategories = vectorize(settings->GetValue("inputCategories."+period, " "));
         for (auto input : inputCategories) {
            StrV sampleList = vectorize(settings->GetValue(input.Data(), " "));
            for (auto sample : sampleList) {
               // printf("opening file %s : %s\r", (period+"_"+input).Data(), sample.Data());
               if (makeCutFlow) sortedSampleFiles[period+"_"+input].push_back(openFile(filePath + sample + "_hist.root"));
               if (makeHistos) sortedFileNames[period+"_"+input].push_back(filePath +sample + ".root");
            }
         }
      }
   }
   else if (makeEfficiency){
      for (auto period : periods) {
        Str filePath = settings->GetValue("filePath."+period, " ");
        bool includeMG = (bool)settings->GetValue("includeMG", 0);
        StrV inputCategories {"SherpaZJets"};
        if (includeMG) inputCategories.push_back("MGZJets");
        for (auto input : inputCategories) {
          StrV sampleList = vectorize(settings->GetValue(input.Data(), " "));
          for (auto sample : sampleList) {
            sortedSampleFiles[period+"_"+input].push_back(openFile(filePath + sample + "_hist.root"));
          }
        }
        //printf("\n");
      }

   }

   //if (makeHistos) doHistograms(sortedSampleFiles, settings, sortedFileNames);
   if (makeHistos) doHistograms(settings, sortedFileNames);
   if (makeCutFlow) { cutFlow(sortedSampleFiles, settings); }
   if (makePlots) {
     StrV plotSetList = vectorize(settings->GetValue("plotSetList", " "));
     for(auto plotSet : plotSetList)
      plotsFromHists(sortedSampleFiles, plotSet, settings);
   }
   if (makeEfficiency) {
     StrV plotSetList = vectorize(settings->GetValue("EffSetList", " "));
     for(auto plotSet : plotSetList)
      doEfficiencyPlots(sortedSampleFiles, plotSet, settings);
   }


   return 0;
}

void ZJetsplots::Loop(){
   if (fChain == 0) return;

   std::string sysString = to_string(sys);
   std::cout << "Debug:: sys #"<< sys << std::endl;
   bool isMG = false;
   bool isSherpa = false;
   bool isData = false;
   if (Filename.Contains("data")) isData = true;
   if (Filename.Contains("MG")) isMG = true;
   if (Filename.Contains("Sherpa")) isSherpa = true;
   std::cout << "Is data sample? " << isData << std::endl;
   std::cout << "Is MG sample? " << isMG << std::endl;
   std::cout << "Is Sherpa sample? " << isSherpa << std::endl;

   // Define number of Control plots;
   const int npl = 37;
   string histvec[npl] = {string("Avmu"),string("ZMassX"),string("ZJpt"),        // 0 - 2
   string("ZPt"),string("Njets"),string("Ptj0"),                                 // 3 - 5
   string("HT1j"), string("ZPt1j"),string("Mjj"),string("DRZj"),                 // 6 - 19
   string("ZMass1j"),string("ZJptlDR"),string("ZJpthDR"),                        // 10 - 12
   string("MjjlDR"),string("MjjhDR"),string("NjexlDR"),                          // 13 - 15
   string("NjexhDR"),string("Njex"),string("Ptjc"), string("Ptj1"),              // 16 - 19
   string("DPjjlDR"), string("DPjjhDR"),                                         // 20 - 21
   string("NJetsInclu"),string("NJetsInclu500"),                                 // 22 - 23
   string("NJetsExclu"),string("NJetsExclu500"),                                 // 24 - 25
   string("ZJpt_l0.5"),string("ZJpt_h0.5"),                                      // 26 - 27
   string("ZJpt_l1.0"),string("ZJpt_h1.0"),                                      // 28 - 29
   string("ZJpt_l1.5"),string("ZJpt_h1.5"),                                      // 30 - 31
   string("ZMass0-250"),string("ZMass250-500"),string("ZMass500"),               // 32 - 34
   string("Lep1PT"),string("Lep2PT")};                                           // 35 - 36
   int nbin[npl]    = {80,  75, 60,
      200, 11, 150,
      200, 200, 200, 80,
      75,  60, 60,
      200, 200, 11,
      11,  11, 75, 75,
      32,  32,
      11, 11,
      11, 11,
      60, 60,
      60, 60,
      60, 60,
      75, 75, 75,
      100, 100};
   float dlow[npl]  = {-0.25, 50.,  0.,
      0., -0.5, 60.,
      0.,  0. ,  0., 0.,
      50. ,  0.,  0. ,
      0.,  0. , -0.5,
      -0.5, -0.5, 30., 30.,
      0., 0.,
      -0.5, -0.5,
      -0.5, -0.5,
      0, 0,
      0, 0,
      0, 0,
      50., 50., 50.,
      25., 25.};
   float dhigh[npl] = {79.75, 200., 3.,
      2000., 10.5,  1560.,
      4000., 2000., 4000., 4.,
      200., 3.,  3.,
      4000.,4000.,10.5,
      10.5, 10.5, 1530.,1530.,
      3.2, 3.2,
      10.5, 10.5,
      10.5, 10.5,
      3., 3.,
      3., 3.,
      3., 3.,
      200., 200., 200.,
      2025, 2025.};

   // Unfolding
   const int nunf = 14;
   int   unbin[nunf]   =  {100,    150,    200,   200,   80,  45,    45,    45,   11,   11,   11,   11 , 100,  100    };
   float udlow[nunf]   =  {0.,    60.,    0.,    0.,    0.,  0.5,   0.5,   0.5,  -0.5, -0.5, -0.5, -0.5, 25,  25 };
   float udhigh[nunf]  =  {2000., 1560.,  4000., 4000., 4.,  45.5,  45.5,  45.5, 10.5, 10.5, 10.5, 10.5, 2025,2025   };

   // matrixes:
   std::string histvecm[nunf] = {string("respZpt1j"),string("respPtj0"),string("respHT1j"), string("respMjj"),
      string("respDRZj"), string("respZJpt"), string("respZJptlDR"),string("respZJpthDR"),
      string("respNJetsInclu"),string("respNJetsExclu"),string("respNJetsInclu500"),string("respNJetsExclu500"),
      string("respLep1Pt"),string("respLep2Pt")};
   std::string histvecm2[nunf] = {string("resp2Zpt1j"),string("resp2Ptj0"),string("resp2HT1j"), string("resp2Mjj"),
      string("resp2DRZj"), string("resp2ZJpt"), string("resp2ZJptlDR"),string("resp2ZJpthDR"),
      string("resp2NJetsInclu"),string("resp2NJetsExclu"),string("resp2NJetsInclu500"),string("resp2NJetsExclu500"),
      string("resp2Lep1Pt"),string("resp2Lep2Pt")};
   // reco
   std::string histvecr[nunf] = {string("measZpt1j"),string("measPtj0"),string("measHT1j"), string("measMjj"),
      string("measDRZj"), string("measZJpt"), string("measZJptlDR"),string("measZJpthDR"),
      string("measNJetsInclu"),string("measNJetsExclu"),string("measNJetsInclu500"),string("measNJetsExclu500"),
      string("measLep1Pt"),string("measLep2Pt")};
   std::string histvecr2[nunf] = {string("meas2Zpt1j"),string("meas2Ptj0"),string("meas2HT1j"), string("meas2Mjj"),
      string("meas2DRZj"), string("meas2ZJpt"), string("meas2ZJptlDR"),string("meas2ZJpthDR"),
      string("meas2NJetsInclu"),string("meas2NJetsExclu"),string("meas2NJetsInclu500"),string("meas2NJetsExclu500"),
      string("meas2Lep1Pt"),string("meas2Lep2Pt")};
   std::string histvecd[nunf] = {string("dataZpt1j"),string("dataPtj0"),string("dataHT1j"), string("dataMjj"),
      string("dataDRZj"), string("dataZJpt"), string("dataZJptlDR"),string("dataZJpthDR"),
      string("dataNJetsInclu"),string("dataNJetsExclu"),string("dataNJetsInclu500"),string("dataNJetsExclu500"),
      string("dataLep1Pt"),string("dataLep2Pt")};
   //truth
   std::string histvect[nunf] = {string("trueZpt1j"),string("truePtj0"),string("trueHT1j"), string("trueMjj"),
      string("trueDRZj"), string("trueZJpt"), string("trueZJptlDR"),string("trueZJpthDR"),
      string("trueNJetsInclu"),string("trueNJetsExclu"),string("trueNJetsInclu500"),string("trueNJetsExclu500"),
      string("trueLep1Pt"),string("trueLep2Pt")};
   std::string histvect2[nunf] = {string("true2Zpt1j"),string("true2Ptj0"),string("true2HT1j"), string("true2Mjj"),
      string("true2pDRZj"), string("true2ZJpt"), string("true2ZJptlDR"),string("true2ZJpthDR"),
      string("true2NJetsInclu"),string("true2NJetsExclu"),string("true2NJetsInclu500"),string("true2NJetsExclu500"),
      string("true2Lep1Pt"), string("true2Lep2Pt")};
   ///////////////////////////////////////////////////////////////
   double weightSum=0;

   // control plots
   TH1F *vhist[npl*3];
   for (int j=0;j<npl;j++){
      for (int k=0;k<3;k++){
         int i=j+(k*npl);
         string chan = string("hee");
         if(i>npl-1 && i<npl*2) chan = string("hmm");
         if(i>npl*2-1 && i<npl*3) chan = string("hem");
         TString hname = chan+histvec[j]+sysString;
         vhist[i]  =  new TH1F(hname,hname, nbin[j], dlow[j], dhigh[j]);
         vhist[i]->Sumw2();
      }
   }
   // Response Matrixes
   TH2F *vhistm[nunf*2];
   for (int j=0;j<nunf;j++){
      for (int k=0;k<2;k++){
         if (j==0 & k==0) std::cout << "Debug:: Defining Response matrixes!"<< std::endl;
         int i=j+(k*nunf);
         string chan = string("hee");
         if(i>nunf-1 && i<nunf*2) chan = string("hmm");
         TString hname;
         if(!isMG) { // Not MG
            hname = chan+histvecm[j]+sysString;
         }else if (isMG){ // Madgraph signal
            hname = chan+histvecm2[j]+sysString;
         }
         vhistm[i]  =  new TH2F(hname,hname, unbin[j], udlow[j], udhigh[j], unbin[j], udlow[j], udhigh[j]);
         vhistm[i]->Sumw2();
      }
   }
   // unfolding response
   TH1F *vhistr[nunf*2];
   for (int j=0;j<nunf;j++){
      for (int k=0;k<2;k++){
         if (j==0 & k==0) std::cout << "Debug:: Defining Unfolding histograms!" << std::endl;
         int i=j+(k*nunf);
         string chan = string("hee");
         if(i>nunf-1 && i<nunf*2) chan = string("hmm");
         TString hname;
         if(isData) { // Data
            hname = chan+histvecd[j]+sysString;
         }else if (!isMG){ // Not MG = Sherpa
            hname = chan+histvecr[j]+sysString;
         }else if (isMG){ // MG
            hname = chan+histvecr2[j]+sysString;
         }
         //cout<<"declaring histogram "<<hname<<endl;
         vhistr[i]  =  new TH1F(hname,hname, unbin[j], udlow[j], udhigh[j]);
         vhistr[i]->Sumw2();
      }
   }
   // unfolding truth
   TH1F *vhistt[nunf*2];
   if(sys==0){
      string hname;
      for (int j=0;j<nunf;j++){
         for (int k=0;k<2;k++){
            if (j==0 & k==0) std::cout << "Debug:: Defining Truth histograms!" << std::endl;
            int i=j+(k*nunf);
            string chan = string("hee");
            if(i>nunf-1 && i<nunf*2) chan = string("hmm");
            if(!isMG) {
               hname = chan+histvect[j]+sysString;
            }else if (isMG){
               hname = chan+histvect2[j]+sysString;
            }
            vhistt[i]  =  new TH1F(hname.data(),hname.data(), unbin[j], udlow[j], udhigh[j]);
            vhistt[i]->Sumw2();
         }
      }
   }


   // Pre Evnt Loop definitions
   // Output File
   TFile hisfile(mout,"update");

   //
   Double_t unskimmedEvts=0;
   // Event Loop
   Long64_t nentries = fChain->GetEntriesFast();
   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) { // Start of Event loop
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      //if (jentry%20000 == 0) std::cout << "Event # " << jentry/1000 << " thousand" << std::endl;

      if(sys==0) { // If truth and !dilepton, continue
         if(lep_E->size()!=2 && truth_lep_E->size()!=2)
            continue;
      }
      else { // If not truth and !dilepton, continue
         if(lep_E->size()!=2) {
            continue;
         }
      }

      // Phase space regions (search)
      // rch = 0 : Electron channel
      // rch = 1 : Muon channel
      // rch = 2 : e-mu channel
      int rch = 3;
      if(lep_E->size()==2){
         int id0 = lep_id->at(0);
         int id1 = lep_id->at(1);
         if (id0 * id1 > 0) continue; // same charge leptons
         if( std::abs(id0)==11 && std::abs(id1)==11 ) {
            rch = 0; // electrons
            // fake muons: v2: drop isolation requirement
         }else if (std::abs(id0)==13 && std::abs(id1)==13 ) {
            rch = 1; // muons
         }else if ((std::abs(id0)==13 && std::abs(id1)==11) || (std::abs(id0)==11 && std::abs(id1)==13)) {
            rch = 2; // e-mu
         }
      }
      // Truth Phase Space region
      // tch = 0 : electron channel
      // tch = 1 : muon channel
      // tch = 2 : e-mu channel
      int tch = 3;
      if(sys==0){
         if(truth_lep_E->size()==2){
            if (truth_lep_id->at(0) * truth_lep_id->at(1) > 0) continue; // Same charge leptons
            if(std::abs(truth_lep_id->at(0))==11 && std::abs(truth_lep_id->at(1))==11) {
               tch = 0; // electron
            }else if (std::abs(truth_lep_id->at(0))==13 && std::abs(truth_lep_id->at(1))==13) {
               tch = 1; // muon
            }else if ((std::abs(truth_lep_id->at(0))==11 && std::abs(truth_lep_id->at(1))==13) || (std::abs(truth_lep_id->at(0))==13 && std::abs(truth_lep_id->at(1))==11)) {
               tch = 2; // e-mu
            }else{
               fatal("Warning: unexpected truth lepton combination!!");
            }
         }
      }

      if (isSherpa && std::abs(initialWeight)>=100) initialWeight=1;
      double wl0 = lep1TotalSF; // Total SF
      double wl1 = lep2TotalSF; // Total SF
      double wgrec = wl0 * wl1 * initialWeight; // Reco weight
      // std::cout << "Alex trig SF = " << trigSF << std::endl;
      double wgtrue = initialWeight; // Truth weight : MC x Pileup Weight

      // ALEX IMPORTANT INFORMATION ABOUT TOP WEIGHTS
      // In the framework the weights work like this :
      // lep1(2)TotalSF are the reco * ID * TTVA
      // m_trigSF is the trigger SF
      // m_w = event weight * pileupWeight : no use really
      // initial Weight = event weith * pileup weight
      // m_pileupW = pileup weight
      // for top systematics, should do : (top1 * top 2 / nominal) * pileupW * SF1 * SF2 * trigSF

      // jet, lepton  and Z 4-vectors
      TLorentzVector mu1, mu2, zet, jet, cljet, cljetr, cljett, jet1, jet2, jmother;
      TLorentzVector mu1t, mu2t, zett, jett, jet1t, jet2t, jmothert;
      //reco and truth variables for unfolding
      float dpjj = 99., mjj = -1., mjjr = -1., mjjt = -1., ht = -1., htr = -1., htt = -1., zetptr = -1., zetptt = -1., jetptr = -1., jetptt=-1.;
      float lep1ptr=-1., lep2ptr=-1.;
      float cljptr = 0.000001, cljptt = 0.000001, mdrzjr = 99., mdrzjt = 99.,fzjbinr=-1., fzjbint=-1., lep1ptt=-1., lep2ptt=-1.;

      if(lep_px->size()>1){
         mu1.SetPxPyPzE(lep_px->at(0),lep_py->at(0),lep_pz->at(0),lep_E->at(0));
         mu2.SetPxPyPzE(lep_px->at(1),lep_py->at(1),lep_pz->at(1),lep_E->at(1));
         zet = mu1+mu2;
      }
      if(sys==0){
         if (truth_lep_px->size()>1){
            mu1t.SetPxPyPzE(truth_lep_px->at(0),truth_lep_py->at(0),truth_lep_pz->at(0),truth_lep_E->at(0));
            mu2t.SetPxPyPzE(truth_lep_px->at(1),truth_lep_py->at(1),truth_lep_pz->at(1),truth_lep_E->at(1));
            zett = mu1t+mu2t;
         }
      }

      bool truth_isGoodZ = false;
      bool truth_isSelected = false;

      bool isGoodZ       = lep_px->size()==2 && rch<3 && PassTrig && mu1.Pt()>25. &&  mu2.Pt()>25 && fabs(mu1.Eta())<2.4 &&  fabs(mu2.Eta())<2.4 && lep_id->at(0)*lep_id->at(1)<0;
      bool isSelected    = isGoodZ && 71. < zet.M() &&  zet.M() < 111.;

      if(sys==0){
        truth_isGoodZ    = truth_lep_px->size()==2  && tch<2 && mu1t.Pt()>25. &&  mu2t.Pt()>25 && fabs(mu1t.Eta())<2.4 &&  fabs(mu2t.Eta())<2.4;
        truth_isSelected = truth_isGoodZ && 71. < zett.M() &&  zett.M() < 111.;
      }

      int nPreJets  = jet_px->size();
      int truth_nPreJets = 0;
      if(sys==0) truth_nPreJets = truth_jet_pT->size();
      int nJets = 0, nJetsLoose = 0, truth_nJets = 0, truth_nJetsLoose = 0;

      vjet_E.clear();
      vjet_px.clear();
      vjet_py.clear();
      vjet_pz.clear();
      vjetl_E.clear();
      vjetl_px.clear();
      vjetl_py.clear();
      vjetl_pz.clear();
      truth_vjet_E.clear();
      truth_vjet_px.clear();
      truth_vjet_py.clear();
      truth_vjet_pz.clear();
      truth_vjetl_E.clear();
      truth_vjetl_px.clear();
      truth_vjetl_py.clear();
      truth_vjetl_pz.clear();


      //define final jets (tight and loose)
      //if (isSelected){
      if(isGoodZ){
         for (int i = 0;i<nPreJets;i++){
            jet.SetPxPyPzE(jet_px->at(i),jet_py->at(i),jet_pz->at(i),jet_E->at(i));
            if(jet.Pt()>100. && fabs(jet.Rapidity())<2.5){ // I dont have a pass JVT flag like Ulla. All my jets are required to pass JVT
               vjet_px.push_back(jet_px->at(i));
               vjet_py.push_back(jet_py->at(i));
               vjet_pz.push_back(jet_pz->at(i));
               vjet_E.push_back(jet_E->at(i));
               // ALEX SHOULD I ADD THIS Scaling factor?
               // JVT SF for jets to aleready dilepton SF?
               //wgrec *= jvtSF;
            }
         }
         // Looser jets for unfolding purposes
         for (int i = 0;i<nPreJets;i++){
            jet.SetPxPyPzE(jet_px->at(i),jet_py->at(i),jet_pz->at(i),jet_E->at(i));
            if(jet.Pt()>60. && fabs(jet.Rapidity())<2.5){ // I dont have a pass JVT flag like Ulla. All my jets are required to pass JVT
               vjetl_px.push_back(jet_px->at(i));
               vjetl_py.push_back(jet_py->at(i));
               vjetl_pz.push_back(jet_pz->at(i));
               vjetl_E.push_back(jet_E->at(i));
               // ALEX SHOULD I ADD THIS Scaling factor?
               // JVT SF for jets to aleready dilepton SF?
               //wgrec *= jvtSF;
            }
         }
      }
      // Truth Jet Selection
      //if(truth_isSelected && sys==0){
      if(truth_isGoodZ && sys==0){
         for (int i = 0;i<truth_nPreJets;i++){
            jet.SetPxPyPzE(truth_jet_px->at(i),truth_jet_py->at(i),truth_jet_pz->at(i),truth_jet_E->at(i));
            if(jet.Pt()>100. && fabs(jet.Rapidity())<2.5){ // I dont have a pass JVT flag like Ulla. All my jets are required to pass JVT
               truth_vjet_px.push_back(truth_jet_px->at(i));
               truth_vjet_py.push_back(truth_jet_py->at(i));
               truth_vjet_pz.push_back(truth_jet_pz->at(i));
               truth_vjet_E.push_back(truth_jet_E->at(i));
               // ALEX SHOULD I ADD THIS Scaling factor?
               // JVT SF for jets to aleready dilepton SF?
               //wgrec *= jvtSF;
            }
         }
         // Looser jets for unfolding purposes
         for (int i = 0;i<truth_nPreJets;i++){
            jet.SetPxPyPzE(truth_jet_px->at(i),truth_jet_py->at(i),truth_jet_pz->at(i),truth_jet_E->at(i));
            if(jet.Pt()>60. && fabs(jet.Rapidity())<2.5){ // I dont have a pass JVT flag like Ulla. All my jets are required to pass JVT
               truth_vjetl_px.push_back(truth_jet_px->at(i));
               truth_vjetl_py.push_back(truth_jet_py->at(i));
               truth_vjetl_pz.push_back(truth_jet_pz->at(i));
               truth_vjetl_E.push_back(truth_jet_E->at(i));
               // ALEX SHOULD I ADD THIS Scaling factor?
               // JVT SF for jets to aleready dilepton SF?
               //wgrec *= jvtSF;
            }
         }
      }
      nJets = vjet_E.size();
      nJetsLoose = vjetl_E.size();
      truth_nJets = truth_vjet_E.size();
      truth_nJetsLoose = truth_vjetl_E.size();

      ////////////////////////////////
      // Unfolding: reco selection  //
      ////////////////////////////////

      float zjbins[16] = {0.,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5};
      float jbins[4] = {50.,400.,550.,5000.};

      if(isSelected && rch<2){

         // additional bin for jetpt
         if(nJetsLoose>0){
            jet.SetPxPyPzE(vjetl_px.at(0),vjetl_py.at(0),vjetl_pz.at(0),vjetl_E.at(0));
            jetptr = jet.Pt();
            vhistr[1+(rch*nunf)]->Fill(jetptr,wgrec);
         }
         lep1ptr = mu1.Pt();
         lep2ptr = mu2.Pt();
         if (lep1ptr<lep2ptr){
           lep1ptr = mu2.Pt();
           lep2ptr = mu1.Pt();
         }
         if (lep1ptr < lep2ptr) fatal("LEPTONS NOT ORDERED!");
         // histograms with normal jets
         if(nJets>0){
            jet1.SetPxPyPzE(vjet_px.at(0),vjet_py.at(0),vjet_pz.at(0),vjet_E.at(0));
            zetptr = zet.Pt();
            vhistr[0+(rch*nunf)]->Fill(zetptr,wgrec);
            vhistr[12+(rch*nunf)]->Fill(lep1ptr,wgrec);
            vhistr[13+(rch*nunf)]->Fill(lep2ptr,wgrec);
            // Jet multiplicities, jet pt > 100 GeV
            for (int i = 0;i<nJets+1;i++){
               vhistr[8+(rch*nunf)]->Fill(float(i),wgrec);//incl jet multiplicity
            }
            vhistr[9+(rch*nunf)]->Fill(nJets,wgrec);//excl jet multiplicity
            if(nJets>1){
               jet2.SetPxPyPzE(vjet_px.at(1),vjet_py.at(1),vjet_pz.at(1),vjet_E.at(1));
               jmother = jet1+jet2; mjjr = jmother.M();
               vhistr[3+(rch*nunf)]->Fill(mjjr,wgrec);
            }
         }
         mdrzjr = 99.;
         htr = lep1ptr + lep2ptr;
         for (int i = 0;i<nJets;i++){
            jet.SetPxPyPzE(vjet_px.at(i),vjet_py.at(i),vjet_pz.at(i),vjet_E.at(i));
            htr += jet.Pt();
            float mindr = jet.DeltaR(zet);
            if(mindr < mdrzjr) {
               cljetr.SetPxPyPzE(vjet_px.at(i),vjet_py.at(i),vjet_pz.at(i),vjet_E.at(i));
               mdrzjr = mindr;
            }
            cljptr = cljetr.Pt()>0 ? cljetr.Pt() : 0.000001;
         }
         if (nJets)  vhistr[2+(rch*nunf)]->Fill(htr,wgrec);
         if (mdrzjr > 0.4)
         if(jet1.Pt()>500.){ // ALEX NEW CUT
            // Jet multiplicities, leading jet pt > 500 GeV
            for (int i = 0;i<nJets+1;i++){
               vhistr[10+(rch*nunf)]->Fill(float(i),wgrec);//incl jet multiplicity
            }
            vhistr[11+(rch*nunf)]->Fill(nJets,wgrec);//excl jet multiplicity

            vhistr[4+(rch*nunf)]->Fill(mdrzjr,wgrec);
            float fzjbin1 = 16.,fzjbin2=0.;
            //cout<<"Test val: "<<zet.Pt()/cljptr<<" = "<<cljptr<<endl;
            fzjbinr=0.;
            for (int i=0;i<15;i++){
               if (zet.Pt()/cljptr> zjbins[i] && zet.Pt()/cljptr< zjbins[i+1]) fzjbin1 = float(i);
                  for (int j=0;j<3;j++){
                     if (cljptr> jbins[j] && cljptr< jbins[j+1]) fzjbin2 = float(j);
                  }
            }
            //0,1,2, ...15 --> 0,3,6,...-45
            fzjbinr = fzjbin1*3.+fzjbin2;
            //cout<<"Test bin: "<<fzjbin1<<" = "<<fzjbin2<<" = "<<fzjbinr<<endl;
            vhistr[5+(rch*nunf)]->Fill(fzjbinr,wgrec);
            if(mdrzjr<1.4) vhistr[6+(rch*nunf)]->Fill(fzjbinr,wgrec);
            if(mdrzjr>2.0) vhistr[7+(rch*nunf)]->Fill(fzjbinr,wgrec);
         }
      }//reco selection

      ////////////////////////////////////////////
      // Unfolding: truth and  response matrix  //
      ////////////////////////////////////////////

      // curently only for nominal
      // ToDo: read in auxiliary file with truth values
      if(truth_isSelected){
         if(sys==0){
            // additional bin for jetpt
            if(truth_nJetsLoose>0){
               jett.SetPxPyPzE(truth_vjetl_px.at(0),truth_vjetl_py.at(0),truth_vjetl_pz.at(0),truth_vjetl_E.at(0));
               jetptt = jett.Pt();
               vhistt[1+(tch*nunf)]->Fill(jetptt,wgtrue);
            }
            // histograms with normal jets
            if(truth_nJets>0){
               jet1t.SetPxPyPzE(truth_vjet_px.at(0),truth_vjet_py.at(0),truth_vjet_pz.at(0),truth_vjet_E.at(0));
               zetptt= zett.Pt();
               lep1ptt=mu1t.Pt();
               lep2ptt=mu2t.Pt();
               if (lep1ptt < lep2ptt){
                  lep1ptt = lep2ptt;
                  lep2ptt = mu1t.Pt();
               }
               if (lep1ptt <lep2ptt) fatal("NOT LEPTONS NOT ORDERED");
               vhistt[0+(tch*nunf)]->Fill(zetptt,wgtrue);
               vhistt[12+(tch*nunf)]->Fill(lep1ptt,wgtrue);
               vhistt[13+(tch*nunf)]->Fill(lep2ptt,wgtrue);
               if(truth_nJets>1){
                  jet2t.SetPxPyPzE(truth_vjet_px.at(1),truth_vjet_py.at(1),truth_vjet_pz.at(1),truth_vjet_E.at(1));
                  jmothert = jet1t+jet2t; mjjt = jmothert.M();
                  vhistt[3+(tch*nunf)]->Fill(mjjt,wgtrue);
               }
               for (int i = 0;i<truth_nJets+1;i++){
                  vhistt[8+(tch*nunf)]->Fill(float(i),wgtrue);//incl jet multiplicity
               }
               vhistt[9+(tch*nunf)]->Fill(truth_nJets,wgtrue);//excl jet multiplicity


               htt = mu1t.Pt() + mu2t.Pt();
               mdrzjt = 99.;
               for (int i = 0;i<truth_nJets;i++){
                  jett.SetPxPyPzE(truth_vjet_px.at(i),truth_vjet_py.at(i),truth_vjet_pz.at(i),truth_vjet_E.at(i));
                  htt += jett.Pt();
                  float mindr = jett.DeltaR(zett);
                  if(mindr < mdrzjt) {
                     cljett.SetPxPyPzE(truth_vjet_px.at(i),truth_vjet_py.at(i),truth_vjet_pz.at(i),truth_vjet_E.at(i));
                     mdrzjt = mindr;
                  }
                  cljptt = cljett.Pt()>0 ? cljett.Pt() : 0.000001;
               }
               vhistt[2+(tch*nunf)]->Fill(htt,wgtrue);

               if (mdrzjt > 0.4)
               if(jet1t.Pt()>500.){ // ALEX NEW CUT
                  for (int i = 0;i<truth_nJets+1;i++){
                     vhistt[10+(tch*nunf)]->Fill(float(i),wgtrue);//incl jet multiplicity
                  }
                  vhistt[11+(tch*nunf)]->Fill(truth_nJets,wgtrue);//excl jet multiplicity

                  vhistt[4+(tch*nunf)]->Fill(mdrzjt,wgtrue);
                  float fzjbin1 = 0.,fzjbin2=0.;
                  fzjbint=0.;
                  for (int i=0;i<15;i++){
                     if (zett.Pt()/cljptt> zjbins[i] && zett.Pt()/cljptt< zjbins[i+1]) fzjbin1 = float(i);
                     for (int j=0;j<3;j++){
                        if (cljptt> jbins[j] && cljptt< jbins[j+1]) fzjbin2 = float(j);
                     }
                  }
                  fzjbint = fzjbin1*3.+fzjbin2;
                  vhistt[5+(tch*nunf)]->Fill(fzjbint,wgtrue);
                  if(mdrzjt<1.4) vhistt[6+(tch*nunf)]->Fill(fzjbint,wgtrue);
                  if(mdrzjt>2.0) vhistt[7+(tch*nunf)]->Fill(fzjbint,wgtrue);
               }
            }

            //writing auxiliary file
            //fout<<tch<<" "<<evtnum<<" "<<wgtrue<<" "<<zetptt<<" "<<htt<<" "<<mjjt<<" "<<mdrzjt<<" "<<fzjbint<<endl;
            vector<float> valvec;
            valvec.push_back(float(tch));
            valvec.push_back(wgtrue);
            valvec.push_back(float(truth_nJets));
            valvec.push_back(float(truth_nJetsLoose));
            valvec.push_back(zetptt);
            valvec.push_back(jetptt);
            valvec.push_back(htt);
            valvec.push_back(mjjt);
            valvec.push_back(mdrzjt);
            valvec.push_back(fzjbint);
            valvec.push_back(lep1ptt);
            valvec.push_back(lep2ptt);

            //cout<<"inserting"<<evtnum<<", "<<valvec[0]<<endl;
            genmap.insert(std::make_pair(EventNumber, valvec));
         }
      }
      bool hastrue = 0;
      if(sys>0){
         vector<float> valvec;
         map<int,vector<float>>::iterator  it = genmap.find(EventNumber);
         if(it != genmap.end()){
            valvec  = it->second;
            hastrue = 1;
            tch = int(valvec[0]);
            wgtrue = valvec[1];
            truth_nJets = int(valvec[2]);
            truth_nJetsLoose = int(valvec[3]);
            zetptt = valvec[4];
            jetptt = valvec[5];
            htt = valvec[6];
            mjjt = valvec[7];
            mdrzjt = valvec[8];
            fzjbint = valvec[9];
            lep1ptt  = valvec[10];
            lep2ptt  = valvec[11];
         }else{
            //cout<<"no true"<<endl;
         }
      }

      if(truth_isSelected || hastrue){
         // Fill response matrices
         if(isSelected && tch==rch){

            // start w/o truth matching, later do truth matching
            if(nJets>0 && truth_nJets>0){
               weightSum+=wgrec;
               vhistm[9+(tch*nunf)]->Fill(nJets,truth_nJets,wgrec); // Exclusive nJets
               vhistm[0+(tch*nunf)]->Fill(zetptr,zetptt,wgrec); // Z pT
               vhistm[12+(tch*nunf)]->Fill(lep1ptr,lep1ptt,wgrec); // lep 1 pT
               vhistm[13+(tch*nunf)]->Fill(lep2ptr,lep2ptt,wgrec); // lep 2 pT
               vhistm[2+(tch*nunf)]->Fill(htr,htt,wgrec); // HT
               if(nJets>1 && truth_nJets>1){
                  vhistm[3+(tch*nunf)]->Fill(mjjr,mjjt,wgrec);
               }
               if (mdrzjr > 0.4 && mdrzjt > 0.4)
               if (jetptr> 500. && jetptt> 500.){ // ALEX NEW CUT
                  vhistm[11+(tch*nunf)]->Fill(nJets,truth_nJets,wgrec); // Exclusive nJets
                  vhistm[4+(tch*nunf)]->Fill(mdrzjr,mdrzjt,wgrec);
                  vhistm[5+(tch*nunf)]->Fill(fzjbinr,fzjbint,wgrec);
                  if(mdrzjt<1.4) vhistm[6+(tch*nunf)]->Fill(fzjbinr,fzjbint,wgrec);
                  if(mdrzjt>2.0) vhistm[7+(tch*nunf)]->Fill(fzjbinr,fzjbint,wgrec);
               }
            }
            if(nJetsLoose>0 && truth_nJetsLoose>0){
               vhistm[1+(tch*nunf)]->Fill(jetptr,jetptt,wgrec);
            }
         }
      }//truth selection

      ////////////////////
      // Control plots  //
      ////////////////////

       //off-peak
       if(isGoodZ){
          vhist[1+(rch*npl)]->Fill(zet.M(),wgrec);
          if (zet.Pt()<250.) vhist[32+(rch*npl)]->Fill(zet.M(),wgrec);
          else if (zet.Pt()>500.) vhist[34+(rch*npl)]->Fill(zet.M(),wgrec);
          else vhist[33+(rch*npl)]->Fill(zet.M(),wgrec);
          if(nJets)
            vhist[10+(rch*npl)]->Fill(zet.M(),wgrec);
       }

      // on-peak
      if(isSelected){

         // This should be in selected Z, not just any mass Z
         //vhist[1+(rch*npl)]->Fill(zet.M(),wgrec);
         //if(nJets) vhist[10+(rch*npl)]->Fill(zet.M(),wgrec);
         for (int i = 0;i<nJets+1;i++){
            vhist[4+(rch*npl)]->Fill(float(i),wgrec);//incl jet multiplicity
            vhist[22+(rch*npl)]->Fill(float(i),wgrec);//incl jet multiplicity
         }
         vhist[24+(rch*npl)]->Fill(nJets,wgrec);
         // no jet preselection
         vhist[3+(rch*npl)]->Fill(zet.Pt(),wgrec); //incl Zpt
         vhist[0+(rch*npl)]->Fill(AverageMu,wgrec);//inclusive average mu


         ht = mu1.Pt() + mu2.Pt();
         float minDRZj = 10.;
         for (int i = 0;i<nJets;i++){
            jet.SetPxPyPzE(vjet_px.at(i),vjet_py.at(i),vjet_pz.at(i),vjet_E.at(i));
            ht += jet.Pt();
            float mindr = jet.DeltaR(zet);
            if(mindr < minDRZj) {
               cljet.SetPxPyPzE(vjet_px.at(i),vjet_py.at(i),vjet_pz.at(i),vjet_E.at(i));
               minDRZj = mindr;
            }
         }

         // events with jets
         if(nJets>0){
            // inclusive Ptj, pTZ HT
            jet1.SetPxPyPzE(vjet_px.at(0),vjet_py.at(0),vjet_pz.at(0),vjet_E.at(0));
            vhist[7+(rch*npl)]->Fill(zet.Pt(),wgrec);
            vhist[5+(rch*npl)]->Fill(jet1.Pt(),wgrec);
            vhist[6+(rch*npl)]->Fill(ht,wgrec);
            vhist[35+(rch*npl)]->Fill(lep1ptr,wgrec);
            vhist[36+(rch*npl)]->Fill(lep2ptr,wgrec);

            if(nJets>1){
               jet2.SetPxPyPzE(vjet_px.at(1),vjet_py.at(1),vjet_pz.at(1),vjet_E.at(1));
               jmother = jet1+jet2; mjj = jmother.M(); dpjj = jet1.DeltaPhi(jet2);
            }

            // events with large jet-pT
            if (minDRZj > 0.4)
            if(jet1.Pt()>500.){ // ALEX NEW CUT
               for (int i = 0;i<nJets+1;i++){
                  vhist[23+(rch*npl)]->Fill(float(i),wgrec);//incl jet multiplicity
               }
               vhist[25+(rch*npl)]->Fill(nJets,wgrec);
               //closest jet pT and 2nd-jet pT
               vhist[18+(rch*npl)]->Fill(cljet.Pt(),wgrec);
               if(nJets>1) vhist[19+(rch*npl)]->Fill(jet2.Pt(),wgrec);
               //excl jet multiplicity for collinear/noncollinear
               vhist[17+(rch*npl)]->Fill(nJets,wgrec);
               if(minDRZj<1.4){
                  vhist[15+(rch*npl)]->Fill(nJets,wgrec);
               }else if (minDRZj > 2.0){
                  vhist[16+(rch*npl)]->Fill(nJets,wgrec);
               }
               // key collinear plots
               double pTZJ=zet.Pt()/cljet.Pt();
               vhist[2+(rch*npl)]->Fill(pTZJ,wgrec);
               vhist[9+(rch*npl)]->Fill(minDRZj,wgrec);
               if(minDRZj < 2.){
                  vhist[11+(rch*npl)]->Fill(pTZJ,wgrec);
               }else{
                  vhist[12+(rch*npl)]->Fill(pTZJ,wgrec);
               }
               if(minDRZj < 0.5){
                  vhist[26+(rch*npl)]->Fill(pTZJ,wgrec);
               }else{
                  vhist[27+(rch*npl)]->Fill(pTZJ,wgrec);
               }
               if(minDRZj < 1.4){
                  vhist[28+(rch*npl)]->Fill(pTZJ,wgrec);
               }else{
                  vhist[29+(rch*npl)]->Fill(pTZJ,wgrec);
               }
               if(minDRZj < 1.5){
                  vhist[30+(rch*npl)]->Fill(pTZJ,wgrec);
               }else{
                  vhist[31+(rch*npl)]->Fill(pTZJ,wgrec);
               }

               if(nJets>1){
                  //mjj cor collinear/noncollinear
                  vhist[8+(rch*npl)]->Fill(mjj,wgrec);
                  if(minDRZj<2.){
                     vhist[13+(rch*npl)]->Fill(mjj,wgrec);
                     vhist[20+(rch*npl)]->Fill(dpjj,wgrec);
                  } else{
                     vhist[14+(rch*npl)]->Fill(mjj,wgrec);
                     vhist[21+(rch*npl)]->Fill(dpjj,wgrec);
                  }
               }//2jets
            }// events with large jet-PT
         }// >+1jet
      }//isSelected

      if (pileupWeight !=0)
        unskimmedEvts += (initialWeight / pileupWeight);
   } // End of Event loop

   // Write histos
   // All reco histos
   for(int i=0;i<nunf*2;i++){
      // Write truth histos
      if(sys==0 && !isData){
         vhistt[i]->Write();
         delete vhistt[i];
      }
      // Write histos detector response
      vhistr[i]->Write();
      delete vhistr[i];
      // Write reco histos
      if(!isData){
         vhistm[i]->Write();
         delete vhistm[i];
      }
   }
   // Define event weights histo and write it
   std::cout << "Unskimmed event sum = " << unskimmedEvts << std::endl;
   if(sys==0){
      TH1F *hsumwgh;
      hsumwgh = new TH1F("hsumwgh", "hsumwgh", 2,0.5,2.5);
      std::cout << EvtSum << " : " << unskimmedEvts << std::endl;
      hsumwgh->Fill(1.,EvtSum);
      hsumwgh->Fill(2.,unskimmedEvts);
      hsumwgh->Write();
      delete hsumwgh;

      TH1F* CFhisto;
      CFhisto = new TH1F("cutFlow_nominal", "cutFlow_nominal", 99,0.5,99.5);
      for (int i=0; i<100; i++)
         CFhisto->Fill(i+1,cutFlowValues[i]);
      CFhisto->Write();
      delete CFhisto;
   }
   // Write all other histos
   for(int i=0;i<npl*3;i++){
      vhist[i]->Write();
      delete vhist[i];
   }
   std::cout << "FILE NAME = " << Filename << std::endl;
   std::cout << "ALEXANDRE SYST # = " << sys << " HAS WEIGHT " << weightSum << std::endl;
} // Event of ZJetsplots::Loop

double getScaleFactorCF(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi){
  TObjArray *tx = sample.Tokenize("/");
  if((((TObjString *)(tx->Last()))->String()).Contains("data")){
    delete tx;
    return 1.0;
  }
  sample = (((TObjString *)(tx->Last()))->String()).ReplaceAll(".root", "");
  delete tx;

  double sumW = getHisto(sampleFile, "sum_of_weights")->GetBinContent(4);
  double sigma = xsecs->GetValue(sample + ".Xsec", 0.0);
  double k = xsecs->GetValue(sample + ".Kfactor", 0.0);
  double eff = xsecs->GetValue(sample + ".Filter", 0.0);

  return sigma * k * eff * lumi / sumW;
}

void doHistograms(TEnv* settings, StrVMap filenameMap){
   // Definitions for all the different files
   std::string Treename = "Ztree";
   IntV Systematics = interize(settings->GetValue("Systematics", " "));
   int nSys = Systematics[0];

   for (auto obj : filenameMap){
      int counter = 0;
      for (auto fname : obj.second){
         // Open sum of weights histo
         TString filename = (filenameMap.find(obj.first)->second)[counter];
         Str outname = filename;
         TFile *f = TFile::Open(filename);
         outname.ReplaceAll(".root","_hist.root");
         TH1F* hsumw = (TH1F*)f->Get("sum_of_weights");
         TH1F* cutflowtable = (TH1F*)f->Get("CutFlow_nominal");
         std::vector<int> cutflowvalues;
         for (int bin=1; bin < 100; bin++)
            cutflowvalues.push_back(cutflowtable->GetBinContent(bin));
         Double_t nevt = 1.;
         nevt = hsumw->GetBinContent(4); // Sum Of Weights
         std::cout << "Sum of Weights Skimmed: " << nevt << std::endl;
         if (obj.first.Contains("data")) nSys = 1;
         for (int i=0; i<nSys; i++){
            TString SysTree = Treename+to_string(i);
            std::cout << "Debug:: TreeName = " << SysTree << std::endl;
            TChain *ZChain  = new TChain(SysTree);
            ZChain->Add(filename);
            if (i==0) {
               Long64_t nEntries = Long64_t(ZChain->GetEntries());
               std::cout << "Number of Events: " << nEntries << std::endl;
            }
            std::cout << "Doing Systematic variation # " << i << std::endl;
            ZJetsplots* Ana = new ZJetsplots((TChain *) ZChain, nevt, i, filename,outname,cutflowvalues);
            Ana->Loop();
            delete Ana;
            delete ZChain;
         }
         std::cout << "-----" << std::endl;
         counter++;
         f->Close();
      }
      std::cout << "=====" << std::endl;
   }
   // end of do Histos
}

void doHistograms(FileVMap sortedSampleFiles, TEnv* settings, StrVMap filenameMap){
   // Definitions for all the different files
   std::string Treename = "Ztree";
   IntV Systematics = interize(settings->GetValue("Systematics", " "));
   int nSys = Systematics[0];

   for (auto obj : sortedSampleFiles){
      int counter = 0;
      for (auto fname : obj.second){
         // Open sum of weights histo
         TString filename = (filenameMap.find(obj.first)->second)[counter];
         Str outname = filename;
         outname.ReplaceAll(".root","_hist.root");
         TH1F* hsumw = (TH1F*)fname->Get("sum_of_weights");
         TH1F* cutflowtable = (TH1F*)fname->Get("CutFlow_nominal");
         std::vector<int> cutflowvalues;
         for (int bin=1; bin < 100; bin++)
            cutflowvalues.push_back(cutflowtable->GetBinContent(bin));
         Double_t nevt = 1.;
         nevt = hsumw->GetBinContent(4); // Sum Of Weights
         std::cout << "Sum of Weights Skimmed: " << nevt << std::endl;
         if (obj.first.Contains("data")) nSys = 1;
         for (int i=0; i<nSys; i++){
            TString SysTree = Treename+to_string(i);
            std::cout << "Debug:: TreeName = " << SysTree << std::endl;
            TChain *ZChain  = new TChain(SysTree);
            ZChain->Add(filename);
            if (i==0) {
               Long64_t nEntries = Long64_t(ZChain->GetEntries());
               std::cout << "Number of Events: " << nEntries << std::endl;
            }
            std::cout << "Doing Systematic variation # " << i << std::endl;
            ZJetsplots* Ana = new ZJetsplots((TChain *) ZChain, nevt, i, filename,outname,cutflowvalues);
            Ana->Loop();
            delete Ana;
            delete ZChain;
         }
         std::cout << "-----" << std::endl;
         counter++;
         fname->Close();
      }
      std::cout << "=====" << std::endl;
   }
   // end of do Histos
}

void drawStackMC(TEnv* settings, Str plot, HistMap hists, StrV inputLabels, IntV colours, TCanvas *can, TLegend* leg, Str region, Str channel, double lumi, Str pdf, int logX, int logY, int chanNo) {
  bool includeMG = (bool)settings->GetValue("includeMG", 1);
  std::cout << "ALEX MADE IT DRAWSTACK" << std::endl;

  IntV CRs = interize(settings->GetValue("doCR", " "));
  int CR=CRs[0];

  if (logY)
    can->SetLogy(1);
  else
    can->SetLogy(0);
  // if (CR!=0) can->SetLogy(0);
  if (logX)
    can->SetLogx(1);
  else
    can->SetLogx(0);
  can->Modified();
  can->Update();
  // If all years combined, remove the data 15,17,18 labels
  if (inputLabels[0] == "All Data") {
    inputLabels.erase(std::remove(inputLabels.begin(), inputLabels.end(), "2015-16 Data"), inputLabels.end());
    inputLabels.erase(std::remove(inputLabels.begin(), inputLabels.end(), "2017 Data"), inputLabels.end());
    inputLabels.erase(std::remove(inputLabels.begin(), inputLabels.end(), "2018 Data"), inputLabels.end());
  }
  Str Chan;
  if (channel.Contains("ee")) Chan = "ee";
  if (channel.Contains("mu")) Chan = "#mu#mu";
  if (channel.Contains("ll")) Chan = "ll";
  Str effWP = settings->GetValue("Efficiency.WP", " ");
  Str newSHLabel = "#it{Z}#rightarrow " + Chan + ", Sherpa2.2";
  Str newMGLabel = "#it{Z}#rightarrow " + Chan + ", MGPy8EG";
  StrV::iterator it1, it2;
  bool first = true;
  int col = 1;
  double ratio = 0.36, margin = 0.02;
  can->cd();
  // ALEX CHANGE THIS FOR PNG!
  bool doPNG = false;
  gPad->SetBottomMargin(ratio);
  gPad->SetTicks();
  TH1F* data;
  TH1F* totalSum, *totalSumAltStrong;
  for(auto input : inputLabels){
  // Data only histos
    if (!input.Contains("Data"))
      continue;
    //printf("%s\n", input.Data());
    data = (TH1F*) hists[input]->Clone();
    data->SetMarkerStyle(20);
    data->GetYaxis()->SetLabelSize(0.03);
    data->GetYaxis()->SetTitleOffset(1.4);
    // ALEX doPNG
    data->GetXaxis()->SetLabelSize(0);
    if(logY){
      data->SetMaximum(data->GetMaximum()*100);
      if(data->GetMinimum() > 100)
        data->SetMinimum(data->GetMinimum()*0.05);
    }
    else
      data->SetMaximum(data->GetMaximum()*2.0);
    double Ymin = settings->GetValue(plot+".Ymin", -99.9);
    double Ymax = settings->GetValue(plot+".Ymax", -99.9);
    if(Ymin != -99.9)
      data->SetMinimum(Ymin);
    if(Ymax != -99.9)
      data->SetMaximum(Ymax);

    // if (CR!=0){
    //   data->SetMinimum(0.);
    //   data->SetMaximum(1.2*data->GetMaximum()/2.0);
    //   // data->SetMaximum(200);
    // }

    data->Draw();
    //Str inputLabel = settings->GetValue(input + ".Label", " ");
    leg->AddEntry(data, input, "plf");
  }
  double totalSumW = 0.0;
  for (it1 = inputLabels.begin(); it1 != inputLabels.end(); it1++) {
    // Not Data
    //printf("%s\n", (*it1).Data());
    if ((*it1).Contains("Data")) continue;
    if ((*it1).Contains("MG")) continue;
    it2 = it1;
    TH1F* sum = (TH1F*) hists[*it2]->Clone();
    sum->Reset();
    sum->SetMarkerStyle(20);
    sum->SetMarkerSize(0);
    //sum->SetLineColor(colours[col]);
    //sum->SetFillColor(colours[col++]);
    sum->SetLineColor(1);
    if (col==1)
      sum->SetLineColor(colours[col]);
    else
      sum->SetFillColor(colours[col]);
    col++;

    sum->GetXaxis()->SetTitleOffset(1.4);
    for (; it2 != inputLabels.end(); it2++) {
      //printf("%s\n", (*it2).Data());
      if ((*it2).Contains("Data")) continue;
      if ((*it2).Contains("MG")) continue;
      sum->Add(hists[*it2]);
    }
    //Str inputLabel = settings->GetValue(*it1 + ".Label", " ");
    //leg->AddEntry(sum, *it1, "plf");
    if ((*it1).Contains("Sherpa")) leg->AddEntry(sum,newSHLabel,"pf");
    else leg->AddEntry(sum, *it1, "pf");
    sum->SetMinimum(0.5);
    sum->Draw("HIST SAME");
    if (first){
      totalSum = (TH1F*) sum->Clone();
      first = false;
    }
    totalSumW = sum->GetSumOfWeights();
    if (col==2){ //quick dumb hack for MG ordering in table
      TH1F* dumby = (TH1F*) sum->Clone();
      dumby->SetLineColor(2);
      // leg->AddEntry(dumby, "#it{Z}#rightarrow ll, MGPy8EG", "pf");
      if (includeMG) leg->AddEntry(dumby, newMGLabel, "pf");
    }
  }
  first = true;
  //make sum for ratio with other strong Zjj
  //Now that I'm making both at once, this part is only for MadGraph
  for (it1 = inputLabels.begin(); it1 != inputLabels.end(); it1++) {
    if ((*it1).Contains("Data")) continue;
    if ((*it1).Contains("Sherpa")) continue;
    it2 = it1;
    TH1F* sum = (TH1F*) hists[*it1]->Clone();
    sum->Reset();
    sum->SetMarkerStyle(20);
    sum->SetMarkerSize(0);
    sum->GetXaxis()->SetTitleOffset(1.4);
    sum->SetLineColor(2);
    for (; it2 != inputLabels.end(); it2++) {
      if ((*it2).Contains("Data")) continue;
      if ((*it2).Contains("Sherpa")) continue;
      sum->Add(hists[*it2]);
    }
    sum->SetMinimum(0.5);
    if (includeMG) sum->Draw("HIST SAME");
    totalSumAltStrong = (TH1F*) sum->Clone();
    break;
  }
  data->Draw("SAME E1");
  data->Draw("AXIS SAME E1");
  leg->Draw();
  // If using text size =0.04 (as big as ATLAS INTERNAL); use y spacing of 0.05 between lines.
  //ATLASLabel(0.58, 0.9, "Work in progress");
  ATLASLabel(0.25, 0.9, "Internal");
  drawText(0.25, 0.875, Form("#sqrt{#it{s}} = 13 TeV, %.1f fb^{-1}", lumi / 1000),0,1,0.02);
  if (CR == 0 || CR == 1){ // SR or CR1 (opposite sign)
    if (chanNo == 0){
      if (plot.Contains("NoJet"))
        drawText(0.25, 0.85, "Z#rightarrow #mu^{+}#mu^{-} + #geq 0 jet",0,1,0.02);
      else if (plot.Contains("Mll2"))
        drawText(0.25, 0.85, "Z#rightarrow #mu^{+}#mu^{-} + #geq 2 jet",0,1,0.02);
      else if (plot.Contains("Mll3"))
        drawText(0.25, 0.85, "Z#rightarrow #mu^{+}#mu^{-} + #geq 3 jet",0,1,0.02);
      else if (plot.Contains("MllHighPt"))
        drawText(0.25, 0.85, "Z#rightarrow #mu^{+}#mu^{-} + #geq 1 500GeV pT jet",0,1,0.02);
      else
        drawText(0.25, 0.85, "Z#rightarrow #mu^{+}#mu^{-} + #geq 1 jet",0,1,0.02);
    }
    else if (chanNo == 1){
      if (plot.Contains("NoJet"))
        drawText(0.25, 0.85, "Z#rightarrow e^{+}e^{-} + #geq 0 jet",0,1,0.02);
      else if (plot.Contains("Mll2"))
        drawText(0.25, 0.85, "Z#rightarrow e^{+}e^{-} + #geq 2 jet",0,1,0.02);
      else if (plot.Contains("Mll3"))
        drawText(0.25, 0.85, "Z#rightarrow e^{+}e^{-} + #geq 3 jet",0,1,0.02);
      else if (plot.Contains("MllHighPt"))
        drawText(0.25, 0.85, "Z#rightarrow e^{+}e^{-} + #geq 1 500GeV pT jet",0,1,0.02);
      else
        drawText(0.25, 0.85, "Z#rightarrow e^{+}e^{-} + #geq 1 jet",0,1,0.02);
    }
    else if (chanNo == 2){
      if (plot.Contains("NoJet"))
        drawText(0.25, 0.85, "Z#rightarrow #ell^{+}#ell^{-} + #geq 0 jet",0,1,0.02);
      else if (plot.Contains("Mll2"))
        drawText(0.25, 0.85, "Z#rightarrow #ell^{+}#ell^{-} + #geq 2 jet",0,1,0.02);
      else if (plot.Contains("Mll3"))
        drawText(0.25, 0.85, "Z#rightarrow #ell^{+}#ell^{-} + #geq 3 jet",0,1,0.02);
      else if (plot.Contains("MllHighPt"))
        drawText(0.25, 0.85, "Z#rightarrow #ell^{+}#ell^{-} + #geq 1 500GeV pT jet",0,1,0.02);
      else
        drawText(0.25, 0.85, "Z#rightarrow #ell^{+}#ell^{-} + #geq 1 jet",0,1,0.02);
    }
    else fatal("Not ee,mumu or ll!");
  }
  else if (CR == 2){ // same signed CR
    if (chanNo == 0){
      if (plot.Contains("NoJet"))
        drawText(0.25, 0.85, "Z#rightarrow #mu^{#pm}#mu^{#pm} + #geq 0 jet",0,1,0.02);
      else if (plot.Contains("Mll2"))
        drawText(0.25, 0.85, "Z#rightarrow #mu^{#pm}#mu^{#pm} + #geq 2 jet",0,1,0.02);
      else if (plot.Contains("Mll3"))
        drawText(0.25, 0.85, "Z#rightarrow #mu^{#pm}#mu^{#pm} + #geq 3 jet",0,1,0.02);
      else if (plot.Contains("MllHighPt"))
        drawText(0.25, 0.85, "Z#rightarrow #mu^{#pm}#mu^{#pm} + #geq 1 500GeV pT jet",0,1,0.02);
      else
        drawText(0.25, 0.85, "Z#rightarrow #mu^{#pm}#mu^{#pm} + #geq 1 jet",0,1,0.02);
    }
    else if (chanNo == 1){
      if (plot.Contains("NoJet"))
        drawText(0.25, 0.85, "Z#rightarrow e^{#pm}e^{#pm} + #geq 0 jet",0,1,0.02);
      else if (plot.Contains("Mll2"))
        drawText(0.25, 0.85, "Z#rightarrow e^{#pm}e^{#pm} + #geq 2 jet",0,1,0.02);
      else if (plot.Contains("Mll3"))
        drawText(0.25, 0.85, "Z#rightarrow e^{#pm}e^{#pm} + #geq 3 jet",0,1,0.02);
      else if (plot.Contains("MllHighPt"))
        drawText(0.25, 0.85, "Z#rightarrow e^{#pm}e^{#pm} + #geq 1 500GeV pT jet",0,1,0.02);
      else
        drawText(0.25, 0.85, "Z#rightarrow e^{#pm}e^{#pm} + #geq 1 jet",0,1,0.02);
    }
    else if (chanNo == 2){
      if (plot.Contains("NoJet"))
        drawText(0.25, 0.85, "Z#rightarrow #ell^{#pm}#ell^{#pm} + #geq 0 jet",0,1,0.02);
      else if (plot.Contains("Mll2"))
        drawText(0.25, 0.85, "Z#rightarrow #ell^{#pm}#ell^{#pm} + #geq 2 jet",0,1,0.02);
      else if (plot.Contains("Mll3"))
        drawText(0.25, 0.85, "Z#rightarrow #ell^{#pm}#ell^{#pm} + #geq 3 jet",0,1,0.02);
      else if (plot.Contains("MllHighPt"))
        drawText(0.25, 0.85, "Z#rightarrow #ell^{#pm}#ell^{#pm} + #geq 1 500GeV pT jet",0,1,0.02);
      else
        drawText(0.25, 0.85, "Z#rightarrow #ell^{#pm}#ell^{#pm} + #geq 1 jet",0,1,0.02);
    }
    else fatal("Not ee,mumu or ll!");
  }
  else if (CR==3){ // All signs CR
    if (chanNo == 0){
      if (plot.Contains("NoJet"))
        drawText(0.25, 0.85, "Z#rightarrow #mu#mu + #geq 0 jet",0,1,0.02);
      else if (plot.Contains("Mll2"))
        drawText(0.25, 0.85, "Z#rightarrow #mu#mu + #geq 2 jet",0,1,0.02);
      else if (plot.Contains("Mll3"))
        drawText(0.25, 0.85, "Z#rightarrow #mu#mu + #geq 3 jet",0,1,0.02);
      else if (plot.Contains("MllHighPt"))
        drawText(0.25, 0.85, "Z#rightarrow #mu#mu + #geq 1 500GeV pT jet",0,1,0.02);
      else
        drawText(0.25, 0.85, "Z#rightarrow #mu#mu + #geq 1 jet",0,1,0.02);
    }
    else if (chanNo == 1){
      if (plot.Contains("NoJet"))
        drawText(0.25, 0.85, "Z#rightarrow ee + #geq 0 jet",0,1,0.02);
      else if (plot.Contains("Mll2"))
        drawText(0.25, 0.85, "Z#rightarrow ee + #geq 2 jet",0,1,0.02);
      else if (plot.Contains("Mll3"))
        drawText(0.25, 0.85, "Z#rightarrow ee + #geq 3 jet",0,1,0.02);
      else if (plot.Contains("MllHighPt"))
        drawText(0.25, 0.85, "Z#rightarrow ee + #geq 1 500GeV pT jet",0,1,0.02);
      else
        drawText(0.25, 0.85, "Z#rightarrow ee + #geq 1 jet",0,1,0.02);
    }
    else if (chanNo == 2){
      if (plot.Contains("NoJet"))
      drawText(0.25, 0.85, "Z#rightarrow #ell#ell + #geq 0 jet",0,1,0.02);
      else if (plot.Contains("Mll2"))
        drawText(0.25, 0.85, "Z#rightarrow #ell#ell + #geq 2 jet",0,1,0.02);
      else if (plot.Contains("Mll3"))
        drawText(0.25, 0.85, "Z#rightarrow #ell#ell + #geq 3 jet",0,1,0.02);
      else if (plot.Contains("MllHighPt"))
        drawText(0.25, 0.85, "Z#rightarrow #ell#ell + #geq 1 500GeV pT jet",0,1,0.02);
      else
        drawText(0.25, 0.85, "Z#rightarrow #ell#ell + #geq 1 jet",0,1,0.02);
    }
    else fatal("Not ee,mumu or ll!");
  }
  else fatal("CR UNKNOWN!");

  if (plot == "Mll") std::cout << "Sherpa Integral = " << totalSum->Integral() << std::endl;
  drawText(0.25, 0.775, Form("Sherpa Integral = %.1f Events", totalSum->Integral()),0,1,0.02);
  drawText(0.25, 0.75, Form("Data Integral = %.1f Events", data->Integral()),0,1,0.02);
  if (CR!=0) drawText(0.25, 0.725, Form("Control Region = %1.i", CR),0,1,0.02);


  drawText(0.25, 0.825, "p_{T}^{jet}#geq 100GeV, |y^{jet}| < 2.5",0,1,0.02);
  if (plot == "Mjj" || plot.Contains("ZJpT") || plot == "minDR" || plot == "NJets" || plot == "MllHighPt")
    drawText(0.25, 0.8, "Leading jet p_{T} #geq 500GeV",0,1,0.02);
  if (effWP != " ")
    drawText(0.25, 0.775, effWP,0,1,0.02);

  if (doPNG){
  // Add this for the DR plots!
  if (plot == "ZJpThighDR")
    drawText(0.625, 0.45, "#DeltaR #geq 2.0",0,1,0.04);
  if (plot == "ZJpTlowDR")
    drawText(0.625, 0.45, "#DeltaR #leq 2.0",0,1,0.04);
  if (plot == "ZJpThigh05")
    drawText(0.625, 0.45, "#DeltaR #geq 0.5",0,1,0.04);
  if (plot == "ZJpTlow05")
    drawText(0.625, 0.45, "#DeltaR #leq 0.5",0,1,0.04);
  if (plot == "ZJpThigh1")
    drawText(0.625, 0.45, "#DeltaR #geq 1.0",0,1,0.04);
  if (plot == "ZJpTlow1")
    drawText(0.625, 0.45, "#DeltaR #leq 1.0",0,1,0.04);
  if (plot == "ZJpThigh15")
    drawText(0.625, 0.45, "#DeltaR #geq 1.5",0,1,0.04);
  if (plot == "ZJpTlow15")
    drawText(0.625, 0.45, "#DeltaR #leq 1.5",0,1,0.04);
  }
  TString pngTitle= plot + ".png";
  //can->Print(pngTitle);
  TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
  if (logX)
    p->SetLogx(1);
  else
    p->SetLogx(0);
  p->Modified();
  p->Update();
  p->SetTopMargin(1.0 - ratio);
  p->SetFillStyle(0);
  p->SetTicks();
  p->SetGridy();
  p->Draw();
  p->cd();
  //TH1F* ratioHist = (TH1F*) data->Clone();
  //ratioHist->Divide((TH1*) totalSum);
  // Swap ratio to be MC / data
  TH1F* ratioHist = (TH1F*) totalSum->Clone();
  ratioHist->Divide((TH1F*) data);

  TLegend* legRatio = new TLegend(0.74, 0.28, 0.92, 0.34);
  legRatio->SetFillColor(0);
  legRatio->SetBorderSize(0);
  legRatio->SetTextSize(0.020);

  // ratioHist->SetLineColor(857);
  // ratioHist->SetMarkerColor(857);
  // ratioHist->SetMarkerStyle(20);
  ratioHist->GetYaxis()->SetLabelSize(0.03);
  ratioHist->GetYaxis()->SetTitle("Pred./Data");
  //ratioHist->GetYaxis()->SetTitle("Data/MC");
  ratioHist->GetXaxis()->SetLabelSize(0.03);
  ratioHist->GetXaxis()->SetTitleOffset(1.4);
  ratioHist->GetYaxis()->SetTitleOffset(1.4);

  float ratioMin = settings->GetValue("ratioMin", 0.3);
  float ratioMax = settings->GetValue("ratioMax", 1.7);

  ratioHist->GetYaxis()->SetRangeUser(ratioMin, ratioMax);
  ratioHist->GetXaxis()->SetMoreLogLabels();
  ratioHist->SetLineColor(857);
  // ratioHist->SetMarkerStyle();
  ratioHist->Draw("E");
  // ratioHist->Draw("same E2");


  // TH1F* ratioHistAltStrong = (TH1F*) data->Clone();
  // ratioHistAltStrong->Divide((TH1*) totalSumAltStrong);
  // Swap raito to be MC / data
  TH1F* ratioHistAltStrong;
  legRatio->AddEntry(ratioHist, newSHLabel, "plf");
  if (includeMG){
    TH1F* ratioHistAltStrong = (TH1F*) totalSumAltStrong->Clone();
    ratioHistAltStrong->Divide((TH1F*) data);
    ratioHistAltStrong->Draw("same");
    legRatio->AddEntry(ratioHistAltStrong, newMGLabel , "plf");
  }
  // Draw black line at value equal to 1
  TH1F* ratioOne = (TH1F*) totalSumAltStrong->Clone();
  for (int i=1; i<data->GetNbinsX()+1;i++){
    ratioOne->SetBinContent(i,1);
    ratioOne->SetBinError(i,0);
  }
  ratioOne->SetLineColor(kBlack);
  ratioOne->Draw("same");


  legRatio->Draw();
  if (plot == "Mll0-250NoJet"){
    drawText(0.25, 0.75, "0#leqpT(Z)#leq250GeV",0,1,0.02);
  }
  if (plot == "Mll250-500NoJet"){
    drawText(0.25, 0.75, "250#leqpT(Z)#leq500GeV",0,1,0.02);
  }
  if (plot == "Mll500NoJet"){
    drawText(0.25, 0.75, "pT(Z)#geq500GeV",0,1,0.02);
  }
  // if (plot == "Mll"){
  //   std::cout << "ALEX Mll data - MC" << std::endl;
  //   for (int i=1; i<data->GetNbinsX()+1;i++)
  //     std::cout << "bin " << i << " has diff " << data->GetBinContent(i)-totalSum->GetBinContent(i) << std::endl;
  // }
  can->Print(region+plot+".png");
  can->Print(pdf);
}

void plotsFromHists(FileVMap sortedSampleFiles, Str plotSet, TEnv* settings){

  TH1::AddDirectory(kFALSE);

  IntV colours = interize(settings->GetValue("colours", " "));
  Str plotPeriod = settings->GetValue(plotSet+".period", " ");
  char linLog = (char)(settings->GetValue(plotSet+".linLog", 1));
  //bool doLin = linLog & 1;
  //bool doLog = linLog & 2;
  bool doLin = false;
  bool doLog = false;
  if (linLog == 1) doLin = true;
  else if (linLog == 2) doLog = true;
  else fatal("not valid lin-log value");

  Str channel;
  int chanNo =-1;
  if(plotSet.Contains("all")){
    channel = "#it{Z}#rightarrow#it{ll}";
    chanNo = 2;
  }
  if(plotSet.Contains("mm")){
    channel = "#it{Z}#rightarrow#it{#mu#mu}";
    chanNo = 0;
  }
  if(plotSet.Contains("ee")){
    channel = "#it{Z}#rightarrow#it{ee}";
    chanNo = 1;
  }
  if (chanNo <0) fatal("Is neither ee, mumu or ll!");

  double Totlumi = settings->GetValue("luminosity."+plotPeriod, 0.0);
  Str region = settings->GetValue(plotSet+".region", " ");
  TEnv *xsecs = openSettingsFile(settings->GetValue("xsecFile", " "));

  StrV periods;
  if(plotPeriod.Contains("all"))
    periods = vectorize(settings->GetValue("periods", ""));
  else
    periods.push_back(plotPeriod);

  StrV plotList = vectorize(settings->GetValue(plotSet+".plotList", " "));

  TCanvas *can = new TCanvas("", "", 800, 800);
  Str pdf = plotSet+".pdf";
  can->Print(pdf + "[");

  HistVMap sortedHists;

  StrV inputLabels, inputCategories;
  for(auto period : periods){
    StrV inputCategories_period = vectorize(settings->GetValue("inputCategories."+period, " "));
    for(auto input : inputCategories_period){
      if(std::find(inputCategories.begin(), inputCategories.end(), input) == inputCategories.end())
        inputCategories.push_back(input);
    }
  }
  for(auto input : inputCategories){
    Str inputLabel = settings->GetValue(input+".Label", " ");
    if(std::find(inputLabels.begin(), inputLabels.end(), inputLabel) == inputLabels.end())
      inputLabels.push_back(inputLabel);
  }
  // if all . Add all Data, and we will remove seperate years later
  if(plotPeriod.Contains("all"))
    inputLabels.insert(inputLabels.begin(),"All Data");

  Str plotListStr;
  for(auto plot : plotList){
    sortedHists.clear();
    plotListStr += plot;
    plotListStr += " ";
    //printf("plot set %-25s : plotting variable %s\r", plotSet.Data(), plotListStr.Data());
    for(auto label : inputLabels)
      sortedHists[label] = new HistV;
    for(auto period : periods){
      inputCategories = vectorize(settings->GetValue("inputCategories."+period, " "));
      double lumi = settings->GetValue("luminosity."+period, 0.0);
      for(auto input : inputCategories){

        Str inputLabel = settings->GetValue(input+".Label", " ");
        //printf("%s %s\n", input.Data(), inputLabel.Data());

        for (auto sampleFile : sortedSampleFiles[period+"_"+input]) {
          double sf = getScaleFactor(xsecs, sampleFile, sampleFile->GetName(), lumi);
          TH1F* temp = new TH1F();
          Str var = settings->GetValue(plot + ".var", " ");
          Str binType = settings->GetValue(plot + ".binType", " ");
          Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
          Str Ylabel = settings->GetValue(plot + ".Ylabel", " ");
          if (chanNo ==0 && plot.Contains("Mll")) Xlabel = "M_{#mu#mu} [GeV]";
          if (chanNo ==1 && plot.Contains("Mll")) Xlabel = "M_{ee} [GeV]";

          // For rebinning purposes.
          int nBins;
          NumV bins;
          if (binType.Contains("regular")) {
            nBins = settings->GetValue(plot + ".nBins", -1);
            double min = settings->GetValue(plot + ".min", 0.0);
            double max = settings->GetValue(plot + ".max", 0.0);
            double step = (max-min)/nBins;
            for(int i=0 ; i<nBins+1; i++)
               bins.push_back(min+i*step);
          }
          else if (binType.Contains("variable")) {
            bins = numberize(settings->GetValue(plot + ".bins", " "));
            nBins = bins.size() - 1;
          }
          else{fatal("BinType is not regular or variable!");}

          Double_t BinsVec[nBins+1];
          for (int n=0; n<nBins+1;n++)
            BinsVec[n]=bins[n];

          // Do Rebinning and save new histo
          // Get histogram and scale it.
          // if (inputLabel.Contains("MG")){
          //   if (var=="Lep1PT") var = "measLep1Pt";
          //   else if (var=="Lep2PT") var = "measLep2Pt";
          //   else var = "meas"+var;
          //   if (var == "measZPt1j") var = "measZpt1j";
          //   std::cout << "ALEXANDRE MG VAR = " << var<< std::endl;
          // }
          // if (inputLabel.Contains("Data"))
          //   if (var == "measZpt1j") var="dataZpt1j";

          // else std::cout << "ALEXANDRE SH VAR = " << var << std::endl;
          // std::cout <<" REGION = " << region << std::endl;
          temp = getHisto(sampleFile, region+var+"0");
          temp->Scale(sf);
          temp = (TH1F*)temp->Rebin(nBins, temp->GetName(), BinsVec);
          temp->SetTitle(";"+Xlabel+";"+Ylabel);
          // sortedHists[inputLabel]->push_back(temp);
          sortedHists[inputLabel]->push_back((TH1F*) temp->Clone());
        }
      }
    }

    HistMap summedHists;
    for (auto label : inputLabels) {
      if (!label.Contains("All Data")){ // "All Data" histograms dont exist yet
      TH1F *sum = (TH1F*) sortedHists[label]->at(0)->Clone();
         sum->Reset();
         for (auto h : *(sortedHists[label]))
           sum->Add(h);
         summedHists[label] = sum;
         std::cout << "Sample " << label << "for plot " << plot << " has integral " << sum->Integral() << std::endl;
      }
    }

    if (plotPeriod.Contains("all")) { // Add all 3 years
      // TH1F *Sum = (TH1F*) sortedHists["2015-16 Data"]->at(0)->Clone();
      // Sum->Reset();
      TH1F *sum = (TH1F*) sortedHists["2015-16 Data"]->at(0)->Clone();
      TH1F *sum1 = (TH1F*) sortedHists["2017 Data"]->at(0)->Clone();
      TH1F *sum2 = (TH1F*) sortedHists["2018 Data"]->at(0)->Clone();
      // for (int n=0; n<sum->GetNbinsX()+1;n++){
      //   Sum->SetBinContent(n,sum->GetBinContent(n)+sum1->GetBinContent(n)+sum2->GetBinContent(n));
      // }
      sum->Add(sum1);
      sum->Add(sum2);
      summedHists["All Data"] = sum;
      // summedHists["All Data"] = Sum;
    }

    TLegend* leg = new TLegend(0.75, 0.80, 0.85, 0.93);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.020);
    int logX = settings->GetValue(plot+".logX", 0);

    if(doLin)
      drawStackMC(settings, plot, summedHists, inputLabels, colours, can, leg, region, channel, Totlumi, pdf, logX, 0, chanNo);
    leg->Clear();
    if(doLog)
      drawStackMC(settings, plot, summedHists, inputLabels, colours, can, leg, region, channel, Totlumi, pdf, logX, 1, chanNo);
  }

  //printf("\n");
  can->Print(pdf + "]");
}

void doEfficiencyPlots(FileVMap sortedSampleFiles, Str plotSet, TEnv* settings){
  Str plotPeriod = settings->GetValue(plotSet+".period", " ");
  bool includeMG = (bool)settings->GetValue("includeMG", 0);
  Str effWP = settings->GetValue("Efficiency.WP", " ");

  Str channel;
  int chanNo =-1;
  if(plotSet.Contains("all")){
    channel = "#it{Z}#rightarrow#it{ll}";
    chanNo = 2;
  }
  if(plotSet.Contains("mm")){
    channel = "#it{Z}#rightarrow#it{#mu#mu}";
    chanNo = 0;
  }
  if(plotSet.Contains("ee")){
    channel = "#it{Z}#rightarrow#it{ee}";
    chanNo = 1;
  }
  if (chanNo <0) fatal("Is neither ee, mumu or ll!");

  double Totlumi = settings->GetValue("luminosity."+plotPeriod, 0.0);
  Str region = settings->GetValue(plotSet+".region", " ");
  TEnv *xsecs = openSettingsFile(settings->GetValue("xsecFile", " "));

  StrV periods;
  if(plotPeriod.Contains("all"))
    periods = vectorize(settings->GetValue("periods", ""));
  else
    periods.push_back(plotPeriod);

  StrV plotList = vectorize(settings->GetValue(plotSet+".plotList", " "));

  TCanvas *can = new TCanvas("", "", 800, 800);
  Str pdf = plotSet+".pdf";
  can->Print(pdf + "[");

  HistVMap sortedTH1;
  HistVMap2 sortedTH2;

  StrV inputLabels {"SherpaZJetsResp","SherpaZJetsTrue"};
  StrV inputCategories {"SherpaZJets"};
  if (includeMG){
    inputCategories.push_back("MGZJets");
    inputLabels.push_back("MGZJetsResp");
    inputLabels.push_back("MGZJetsTrue");
  }

  Str plotListStr;
  for(auto plot : plotList){
    Str var = settings->GetValue(plot + ".var", " "); // Get the variable to plot
    sortedTH1.clear(); // Reset map of histograms
    sortedTH2.clear(); // Reset map of histograms
    for(auto label : inputLabels){ // Make new vectors in each map element (Response + truth x2)
      sortedTH1[label] = new HistV;
      sortedTH2[label] = new HistV2;
    }
    for(auto period : periods){
      double lumi = settings->GetValue("luminosity."+period, 0.0);
      for(auto input : inputCategories){ // For Sherpa & MG
        for (auto sampleFile : sortedSampleFiles[period+"_"+input]) { // Loop through SH or MG file
          double sf = getScaleFactor(xsecs, sampleFile, sampleFile->GetName(), lumi);
          // Get histogram and scale it.
          TH1F* temp = new TH1F();
          TH2F* temp2 = new TH2F();

          if (input == "SherpaZJets"){
            temp2 = getHisto2(sampleFile, region+"resp"+var+"0");
            temp2->Scale(sf);
            sortedTH2["SherpaZJetsResp"]->push_back((TH2F*) temp2->Clone());
            temp = getHisto(sampleFile, region+"true"+var+"0");
            temp->Scale(sf);
            sortedTH1["SherpaZJetsTrue"]->push_back((TH1F*) temp->Clone());
          }
          else if (input == "MGZJets"){
            TH1F* temp = new TH1F();
            TH2F* temp2 = new TH2F();
            temp2 = getHisto2(sampleFile, region+"resp2"+var+"0");
            temp2->Scale(sf);
            sortedTH2["MGZJetsResp"]->push_back((TH2F*) temp2->Clone());
            temp = getHisto(sampleFile, region+"true2"+var+"0");
            temp->Scale(sf);
            sortedTH1["MGZJetsTrue"]->push_back((TH1F*) temp->Clone());
          }
          else fatal("NOT SHERPA OR MG FOR EFFICIENCY PLOTS!!");
        }
      }
    }

  Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
  // Str Ylabel = settings->GetValue(plot + ".Ylabel", " ");
  Str Ylabel = "Reco efficiency";

  int nBins;
  NumV bins;
  Str binType = settings->GetValue(plot + ".binType", " ");
  if (binType.Contains("regular")) {
    nBins = settings->GetValue(plot + ".nBins", -1);
    double min = settings->GetValue(plot + ".min", 0.0);
    double max = settings->GetValue(plot + ".max", 0.0);
    double step = (max-min)/nBins;
    for(int i=0 ; i<nBins+1; i++)
      bins.push_back(min+i*step);
  }
  else if (binType.Contains("variable")) {
    bins = numberize(settings->GetValue(plot + ".bins", " "));
    nBins = bins.size() - 1;
  }
  else{fatal("BinType is not regular or variable!");}

  Double_t BinsVec[nBins+1];
  for (int n=0; n<nBins+1;n++)
    BinsVec[n]=bins[n];

  // Make new Histo.
  TString hname1 = "Sh Z#rightarrow ll " + var + "reco efficiency";
  TString hname2 = "MG Z#rightarrow ll " + var + "reco efficiency";
  TH1F* effhistSH = new TH1F(hname1 ,hname1, nBins, BinsVec);
  TH1F* effhistMG = new TH1F(hname2 ,hname2, nBins, BinsVec);
  effhistSH->SetTitle(";"+Xlabel+";"+Ylabel);
  effhistSH->SetMinimum(-0.05);
  effhistSH->SetMaximum(1.0);
  effhistSH->SetLineColor(857);
  effhistSH->SetMarkerStyle(3);
  effhistMG->SetLineColor(2);
  effhistMG->SetMarkerStyle(25);
  // std::cout << "ALEX DOING " << plot << std::endl;


  // ALEXANDRE WERE HERE NOW.
  for (int i=1; i<nBins+1; i++){
    double sumResp=0., sumTrue=0.; // Reset sums
    // Reco && Truth events
    auto xAxis = sortedTH2["SherpaZJetsResp"]->at(0)->GetXaxis();
    for (auto h : *(sortedTH2["SherpaZJetsResp"])){ // Get the Reco && Truth
      for (int j=1; j<xAxis->GetNbins()+1; j++){ // Loop through the TH2F bins
        if ((xAxis->GetBinCenter(j) < effhistSH->GetBinLowEdge(i+1)) && (xAxis->GetBinCenter(j) > effhistSH->GetBinLowEdge(i)))
          for (int k=1; k<xAxis->GetNbins()+1; k++) // Loop through the TH1F bins
            sumResp += h->GetBinContent(k,j);
      }
    }
    // Truth only events
    // xAxis = sortedTH1["SherpaZJetsTrue"]->at(0)->GetXaxis();
    for (auto h : *(sortedTH1["SherpaZJetsTrue"])) // Get the Truth
      for (int j=1; j<xAxis->GetNbins()+1; j++) // Loop through the TH1F bins
        if ((h->GetBinCenter(j) < effhistSH->GetBinLowEdge(i+1)) && (h->GetBinCenter(j) > effhistSH->GetBinLowEdge(i))){
          sumTrue += h->GetBinContent(j);
        }

    // std::cout << "For bin " << i << ", reco = " << sumResp << ", truth = " << sumTrue << std::endl;
    if (sumTrue==0) sumTrue=1;
    // if (sumResp<=0) sumResp=0.;
    // if (sumTrue<=0) sumTrue=1;
    // if (sumResp<=0) sumResp=0.;
    // std::cout << "Sherpa Efficiency for bin " << i << " is " << sumResp/sumTrue << std::endl;
    // std::cout << "Sum response = " << sumResp << std::endl;
    // std::cout << "Sum True = " << sumTrue << std::endl;
    effhistSH->SetBinContent(i,sumResp/sumTrue);
    effhistSH->SetBinError(i,0.000001);

    if (includeMG) {
      sumResp=0., sumTrue=0.; // Reset sums
      // Reco && Truth events
      xAxis = sortedTH2["MGZJetsResp"]->at(0)->GetXaxis();
      for (auto h : *(sortedTH2["MGZJetsResp"])) // Get the Reco && Truth
        for (int j=1; j<xAxis->GetNbins()+1; j++) // Loop through the TH2F bins
          if (xAxis->GetBinCenter(j) < effhistMG->GetBinLowEdge(i+1) && xAxis->GetBinCenter(j) > effhistMG->GetBinLowEdge(i))
            for (int k=1; k<xAxis->GetNbins()+1; k++) // Loop through the TH1F bins
              sumResp += h->GetBinContent(k,j);

      // Truth only events
      // xAxis = sortedTH1["MGZJetsTrue"]->at(0)->GetXaxis();
      for (auto h : *(sortedTH1["MGZJetsTrue"])) // Get the Truth
        for (int j=1; j<xAxis->GetNbins()+1; j++) // Loop through the TH1F bins
           if (h->GetBinCenter(j) < effhistMG->GetBinLowEdge(i+1) && h->GetBinCenter(j) > effhistMG->GetBinLowEdge(i))
            sumTrue += h->GetBinContent(j);

      if (sumTrue==0) sumTrue=1;
      // if (sumResp<0) sumResp=0.;
      // std::cout << "MG Efficiency for bin " << i << " is " << sumResp/sumTrue << std::endl;
      // std::cout << "Sum response = " << sumResp << std::endl;
      // std::cout << "Sum True = " << sumTrue << std::endl;

      effhistMG->SetBinContent(i,sumResp/sumTrue);
      effhistMG->SetBinError(i,0.000001);
    }
  }
  effhistSH->Draw("E");
  if (includeMG) effhistMG->Draw("ESAME");

  double xTitle=0.5, yTitle=0.9;
  if (effWP == " " || effWP == "HighPt_LooseVar"){
    xTitle = 0.55;
    yTitle = 0.90;
  }
  else if(effWP == "Medium_LooseVar" || effWP == "Medium_TightVar"){
    xTitle = 0.20;
    yTitle = 0.45;
  }
  else if(effWP.Contains("Smear") || effWP.Contains("smear")){
    xTitle = 0.20;
    yTitle = 0.45;
  }
  // else fatal("WP not known.");
  ATLASLabel(xTitle, yTitle, "Internal");
  drawText(xTitle, yTitle-0.05, Form("#sqrt{#it{s}} = 13 TeV, %.1f fb^{-1}", Totlumi / 1000),0,1,0.04);
  if (chanNo == 0)
    drawText(xTitle, yTitle-0.10, "Z#rightarrow #mu#mu + #geq 1 jet",0,1,0.04);
  else if (chanNo == 1)
    drawText(xTitle, yTitle-0.10, "Z#rightarrow ee + #geq 1 jet",0,1,0.04);
  else if (chanNo == 2)
    drawText(xTitle, yTitle-0.10, "Z#rightarrow #ell#ell + #geq 1 jet",0,1,0.04);
  else fatal("Not ee,mumu or ll!");
  drawText(xTitle, yTitle-0.15, "p_{T}^{jet}#geq 100GeV, |y^{jet}| < 2.5",0,1,0.04);
  if (effWP != " ")
    drawText(xTitle, yTitle-0.20, effWP,0,1,0.04);

  TLegend* leg = new TLegend(xTitle, yTitle-0.3, xTitle+0.3, yTitle-0.22);
  leg->SetFillColor(0);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.020);
  leg->SetFillStyle(0);


  Str newSHLabel = channel + ", Sherpa2.2";
  Str newMGLabel = channel + ", MGPy8EG";
  leg->AddEntry(effhistSH,newSHLabel,"pl");
  if (includeMG)  leg->AddEntry(effhistMG,newMGLabel,"pl");
  leg->Draw();



  can->Print(region+var+".png");
  can->Print(pdf);
  delete effhistSH;
  delete effhistMG;
  }

  can->Print(pdf + "]");
  delete can;
}

void cutFlow(FileVMap sortedSampleFiles, TEnv* settings){
  StrV cutFlowLabels = vectorize(settings->GetValue("cutFlowLabels", " "), ";");
  StrV cutFlowLabelsWithCuts = vectorize(settings->GetValue("cutFlowLabelsWithCuts", " "), ";");
  IntV cutFlowBins = interize(settings->GetValue("cutFlowBins", " "));

  TEnv *xsecs = openSettingsFile(settings->GetValue("xsecFile", " "));

  StrV periods = vectorize(settings->GetValue("periods", " "));

  for(auto period : periods){
    double lumi = settings->GetValue("luminosity."+period, 0.0);

    Str cutFlowOutFile = "cutFlow_"+period+".root";

    StrV inputCategories = vectorize(settings->GetValue("inputCategories."+period, " "));

    NumVMap cutFlowPerSample;

    for(auto input : inputCategories){
      bool first = true;
      for (auto sampleFile : sortedSampleFiles[period+"_"+input]) {
        TH1F *cutFlowWeighted = getHisto(sampleFile, "CutFlow_weighted");
        double sf = getScaleFactor(xsecs, sampleFile, sampleFile->GetName(), lumi);
        cutFlowWeighted->Scale(sf);
        if (first) {
          first = false;
          for (int i = 0; i <cutFlowBins.size();  i++) {
            cutFlowPerSample[input].push_back(cutFlowWeighted->GetBinContent(cutFlowBins[i]));
          }
        } else {
          for (int i = 0; i <cutFlowBins.size(); i++)
            cutFlowPerSample[input][i] += cutFlowWeighted->GetBinContent(cutFlowBins[i]);
        }
        //if (first) {
        //  first = 0;
        //  for (int i = 1; i <= cutFlowWeighted->GetNbinsX(); i++) {
        //    cutFlowPerSample[input].push_back(cutFlowWeighted->GetBinContent(i));
        //  }
        //} else {
        //  for (int i = 1; i <= cutFlowWeighted->GetNbinsX(); i++)
        //    cutFlowPerSample[input][i - 1] += cutFlowWeighted->GetBinContent(i);
        //}
      }
    }

    //Print cutFlow
    printf("%30s ", "");
    for(auto input : inputCategories)
      //std::cout << "& " << input.Data() << " ";
      //printf("& %15s ", input.Data());
      printf("& %11s ", input.Data());
    //std::cout << "\\\\" << std::endl;
    printf("\\\\\n");
    int iLabel = 0;
    //for (auto label : cutFlowLabels) {
    //  //printf("%30s ", label.ReplaceAll(" ", "").Data());
    //  printf("%30s ", label.Data());
    //  for(auto input : inputCategories)
    //    if (iLabel < cutFlowPerSample[input].size())
    //      std::cout << "& " << cutFlowPerSample[input][iLabel] << " \\\\" << std::endl;
    //      //printf("& %15.2e ", cutFlowPerSample[input][iLabel]);
    //    else
    //      std::cout << "& 0.0 \\\\" << std::endl;
    //      //printf("& %15.1f ", 0.0);
    //  //printf("\\\\\n");
    //  iLabel++;
    //}
    for (auto label : cutFlowLabels) {
      printf("%30s ", label.Data());
      //printf("%30s ", label.ReplaceAll(" ", "").Data());
      for(auto input : inputCategories)
        if (iLabel < cutFlowPerSample[input].size())
          //std::cout << "& " << cutFlowPerSample[input][iLabel] << std::endl;
          printf("& %11.2e ", cutFlowPerSample[input][iLabel]);
          //printf("& %15.2e ", cutFlowPerSample[input][iLabel]);
        else
          printf("& %11.1f ", 0.0);
          //printf("& %15.1f ", 0.0);
      printf("\\\\\n");
      iLabel++;
    }

    //Write cutFlow to file
    TFile *cutFlowFile = new TFile(cutFlowOutFile.Data(), "RECREATE");
    for(auto input : inputCategories){
      int iCut = 1;
      Str cut = "";
      //printf("cutflow: %-70s\r", input.Data());
      TH1F* cutFlowHist = new TH1F(Form("%s_cutFlow", input.Data()), Form("%s_cutFlow", input.Data()), cutFlowLabelsWithCuts.size(), 0, cutFlowLabelsWithCuts.size());
      for (auto cutName : cutFlowLabelsWithCuts) {
        cutFlowHist->GetXaxis()->SetBinLabel(iCut, cutName.Data());
        int bin = cutFlowBins[iCut-1];
        float cutFlowSum = 0.0;
        for (auto sampleFile : sortedSampleFiles[period+"_"+input]) {
          TH1F *cutFlow = getHisto(sampleFile, "CutFlow_weighted");
          double sf = getScaleFactor(xsecs, sampleFile, sampleFile->GetName(), lumi);
          // double sf = getScaleFactorCF(xsecs, sampleFile, sampleFile->GetName(), lumi);
          cutFlow->Scale(sf);
          float cutSumW;
          if(bin > 0){
            cutSumW = cutFlow->GetBinContent(bin);
            cutFlowSum += cutSumW * sf;
          }
          else{
            cutName.Remove(TString::kLeading, ' ');
            TH1F* temp = getHisto(sampleFile, "Mll_"+cutName);
            cutSumW = temp->GetSumOfWeights();
            cutFlowSum += cutSumW * sf;
          }
        }
        cutFlowHist->SetBinContent(iCut, cutFlowSum);
        //printf("%.3f\n", cutFlowSum);
        iCut++;
      }
      cutFlowFile->cd();
      cutFlowHist->Write();
    }
    printf("\n");
  }
}
