#include "../util.h"

void yields(){
  setStyle();
  TEnv *settings = openSettingsFile("ZJetsSystplots.data");
  std::cout.precision(8);


  // Define systematics to run over.
  Str systematics = settings->GetValue("systematics", " "); // Systematics set to run over
  StrV systList = vectorize(settings->GetValue(systematics+".syst", " ")); // Gets the list of systematic names
  IntV Systematics = interize(settings->GetValue("Systematics", " ")); // Total number of systematics
  int nSys = Systematics[0]; // Total number of systematics
  bool doQuadrature = false;
  bool doSymmetrize = false;
  bool doMax = true;

  int nColumns = 8; // Number of regions + error (4x2)
  int nLines = 9; // Number of Sources (sherpa, MG, top, diboson, ztt, wjets, sherpaSum, MGSum, data)
  std::vector<std::vector<double>> MuonYields(nLines);
  std::vector<std::vector<double>> ElecYields(nLines);
  for (int i=0; i<nLines; i++){
    MuonYields[i].resize(nColumns);
    ElecYields[i].resize(nColumns);
  }

  // Open all the required samples
  FileVMap sortedSampleFiles;
  Str filePath = settings->GetValue("filePath", " ");
  StrV inputCategories = vectorize(settings->GetValue("inputCategories", " "));
  for (auto input : inputCategories) {
    StrV sampleList = vectorize(settings->GetValue(input.Data(), " "));
    for (auto sample : sampleList)
      sortedSampleFiles[input].push_back(openFile(filePath + sample + ".root"));
  }
  // Define list of plots to verify
  StrV plotList = vectorize(settings->GetValue("PlotList", " "));
  TH1F* nominal = new TH1F();
  TH1F* Syst = new TH1F();
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "=============" << std::endl;
  std::cout << "Muon Channel " << std::endl;
  int line=0;
  int column=0;
  for (auto plot : plotList){
    line = 0;
    std::cout << std::endl;
    if (plot == "ZPt") std::cout << "Good Z, on shell" << std::endl;
    if (plot == "ZPt1j") std::cout << "Good Z, >= 1 jet, on shell" << std::endl;
    if (plot == "DRZj") std::cout << "Good Z, leading jet, on shell" << std::endl;
    if (plot == "ZJpt") std::cout << "Good Z, leading jet, on shell" << std::endl;
    if (plot == "ZJpt_l1.0") std::cout << "Good Z, lowDR, on shell" << std::endl;
    if (plot == "ZJpthDR") std::cout << "Good Z, highDR, on shell" << std::endl;

    for (auto input : inputCategories){
      for (auto file : sortedSampleFiles[input]){

        nominal = getHisto(file, "hmm"+plot+"0");
        // Calculate variance
        double mean = nominal->Integral();
        double variance = 0;
        // V = 1/n * Sum (x_i - mu)^2
        if (doQuadrature){
          for (int i=1; i<nSys; i++){
            Syst = getHisto(file, "hmm"+plot+i);
            variance = variance + std::pow((Syst->Integral() - mean),2);
          }
        }
        else if (doSymmetrize){
          for (int i=1; i<(nSys+1)/2; i++){
            double val1=0, val2=0;
            Syst = getHisto(file, "hmm"+plot+(2*i-1));
            val1 = std::abs(Syst->Integral() - mean);
            Syst = getHisto(file, "hmm"+plot+(2*i));
            val2 = std::abs(Syst->Integral() - mean);
            variance = variance + std::pow( (val1+val2)/2 ,2);
          }
        }
        else if (doMax){
          for (int i=1; i<(nSys+1)/2; i++){
            double val1=0, val2=0;
            Syst = getHisto(file, "hmm"+plot+(2*i-1));
            val1 = std::pow(Syst->Integral() - mean,2);
            Syst = getHisto(file, "hmm"+plot+(2*i));
            val2 = std::pow(Syst->Integral() - mean,2);
            variance = variance + std::max(val1,val2);
          }
        }
        else fatal("Not 1 of 3 uncertainty methods!");
        // if (nSys>1)
        //   variance = variance / (nSys-1);
        // Sigma = sqrt(V)
        double syst = std::sqrt(variance);
        double stat = std::sqrt(mean);
        double sigma = std::sqrt(stat*stat + syst*syst);
        std::cout << input << " = " << mean << " +- " << sigma << std::endl;
        // std::cout << input << " = " << mean << " +- " << syst << " +- " << stat << std::endl;
        if (input == "data"){
          MuonYields[8][2*column] = mean;
          MuonYields[8][2*column+1] = sigma;
        }
        else{
          MuonYields[line][2*column] = mean;
          MuonYields[line][2*column+1] = sigma;
        }
      }
      line = line + 1;
      if (input == "data") line = 0;
    }
    // Calculate total values
    TH1F* total = (TH1F*) nominal->Clone();
    TH1F* total2 = (TH1F*) nominal->Clone();
    TH1F* totalSyst = (TH1F*) nominal->Clone();
    TH1F* totalSyst2 = (TH1F*) nominal->Clone();
    total->Reset();
    total2->Reset();
    totalSyst->Reset();
    totalSyst2->Reset();
    for (auto input : inputCategories){
      if (input=="data") continue;
      if (input!="MGZJets")
      for (auto file : sortedSampleFiles[input]){
        nominal = getHisto(file, "hmm"+plot+"0");
        total->Add(nominal);
      }
      if (input!="SherpaZJets")
      for (auto file : sortedSampleFiles[input]){
        nominal = getHisto(file, "hmm"+plot+"0");
        total2->Add(nominal);
      }
    }
    double mean = total->Integral();
    double variance = 0;
    double mean2 = total2->Integral();
    double variance2 = 0;
    for (int i=1; i<nSys; i++){
      totalSyst->Reset();
      totalSyst2->Reset();
      for (auto input : inputCategories){
        if (input=="data") continue;
        if (input!="MGZJets")
        for (auto file : sortedSampleFiles[input]){
          Syst = getHisto(file, "hmm"+plot+i);
          totalSyst->Add(Syst);
        }
        if (input!="SherpaZJets")
        for (auto file : sortedSampleFiles[input]){
          Syst = getHisto(file, "hmm"+plot+i);
          totalSyst2->Add(Syst);
        }
      }
      variance = variance + std::pow((totalSyst->Integral() - mean),2);
      variance2 = variance2 + std::pow((totalSyst2->Integral() - mean2),2);
    }
    // if (nSys>1){
    //   variance = variance / (nSys-1);
    //   variance2 = variance2 / (nSys-1);
    // }
    // double syst = std::sqrt(variance);
    // double stat = std::sqrt(mean);
    // double sigma = std::sqrt(stat*stat + syst*syst);
    double sigma =0.;
    for (int i=0; i<5; i++){
      int a=0;
      if (i>0) a=i+1;
      sigma += std::pow(MuonYields[a][2*column+1],2);
    }
    std::cout << "Total Sherpa Contribution = " << mean << " +- " << std::sqrt(sigma) << std::endl;
    MuonYields[6][2*column] = mean;
    MuonYields[6][2*column+1] = std::sqrt(sigma);
    // std::cout << "Total Sherpa Contribution = " << alexTotal << " +- " << std::sqrt(alexSigma) << std::endl;

    // syst = std::sqrt(variance2);
    // stat = std::sqrt(mean2);
    // sigma = std::sqrt(stat*stat + syst*syst);
    sigma =0.;
    for (int i=1; i<6; i++){
      sigma += std::pow(MuonYields[i][2*column+1],2);
    }
    std::cout << "Total MadGraph Contribution = " << mean2 << " +- " << std::sqrt(sigma) << std::endl;
    MuonYields[7][2*column] = mean2;
    MuonYields[7][2*column+1] = std::sqrt(sigma);
    column = column + 1;
  }
  std::cout << "-----------------" << std::endl;
  std::cout << "Electron Channel " << std::endl;
  line=0;
  column=0;
  for (auto plot : plotList){
    line = 0;
    std::cout << std::endl;
    if (plot == "ZPt") std::cout << "Good Z, on shell" << std::endl;
    if (plot == "ZPt1j") std::cout << "Good Z, >= 1 jet, on shell" << std::endl;
    if (plot == "DRZj") std::cout << "Good Z, leading jet, on shell" << std::endl;
    if (plot == "ZJpt_l1.0") std::cout << "Good Z, lowDR, on shell" << std::endl;
    if (plot == "ZJpthDR") std::cout << "Good Z, highDR, on shell" << std::endl;
    if (plot == "ZJptmedDR") std::cout << "Good Z, medDR, on shell" << std::endl;
    for (auto input : inputCategories){
      for (auto file : sortedSampleFiles[input]){
        nominal = getHisto(file, "hee"+plot+"0");
        // Calculate total values
        // Calculate variance
        double mean = nominal->Integral();
        double variance = 0;
        // V = 1/n * Sum (x_i - mu)^2
        if (doQuadrature){
          for (int i=1; i<nSys; i++){
            Syst = getHisto(file, "hee"+plot+i);
            variance = variance + std::pow((Syst->Integral() - mean),2);
          }
        }
        else if (doSymmetrize){
          for (int i=1; i<(nSys+1)/2; i++){
            double val1=0, val2=0;
            Syst = getHisto(file, "hee"+plot+(2*i-1));
            val1 = std::abs(Syst->Integral() - mean);
            Syst = getHisto(file, "hee"+plot+(2*i));
            val2 = std::abs(Syst->Integral() - mean);
            variance = variance + std::pow( (val1+val2)/2 ,2);
          }
        }
        else if (doMax){
          for (int i=1; i<(nSys+1)/2; i++){
            double val1=0, val2=0;
            Syst = getHisto(file, "hee"+plot+(2*i-1));
            val1 = std::abs(Syst->Integral() - mean);
            Syst = getHisto(file, "hee"+plot+(2*i));
            val2 = std::abs(Syst->Integral() - mean);
            variance = variance + std::pow( std::max(val1,val2) ,2);
          }
        }
        else fatal("Not 1 of 3 uncertainty methods!");
        // if (nSys>1)
        //   variance = variance / (nSys-1);
        // Sigma = sqrt(V)
        double syst = std::sqrt(variance);
        double stat = std::sqrt(mean);
        double sigma = std::sqrt(stat*stat + syst*syst);
        std::cout << input << " = " << mean << " +- " << sigma << std::endl;
        if (input == "data"){
          ElecYields[8][2*column] = mean;
          ElecYields[8][2*column+1] = sigma;
        }
        else{
          ElecYields[line][2*column] = mean;
          ElecYields[line][2*column+1] = sigma;
        }
      }
      line = line + 1;
      if (input == "data") line = 0;
    }
    // Calculate total values
    TH1F* total = (TH1F*) nominal->Clone();
    TH1F* total2 = (TH1F*) nominal->Clone();
    TH1F* totalSyst = (TH1F*) nominal->Clone();
    TH1F* totalSyst2 = (TH1F*) nominal->Clone();
    total->Reset();
    total2->Reset();
    totalSyst->Reset();
    totalSyst2->Reset();
    for (auto input : inputCategories){
      if (input=="data") continue;
      if (input!="MGZJets")
      for (auto file : sortedSampleFiles[input]){
        nominal = getHisto(file, "hee"+plot+"0");
        total->Add(nominal);
      }
      if (input!="SherpaZJets")
      for (auto file : sortedSampleFiles[input]){
        nominal = getHisto(file, "hee"+plot+"0");
        total2->Add(nominal);
      }
    }
    double mean = total->Integral();
    double variance = 0;
    double mean2 = total2->Integral();
    double variance2 = 0;
    for (int i=1; i<nSys; i++){
      totalSyst->Reset();
      totalSyst2->Reset();
      for (auto input : inputCategories){
        if (input=="data") continue;
        if (input!="MGZJets")
        for (auto file : sortedSampleFiles[input]){
          Syst = getHisto(file, "hee"+plot+i);
          totalSyst->Add(Syst);
        }
        if (input!="SherpaZJets")
        for (auto file : sortedSampleFiles[input]){
          Syst = getHisto(file, "hee"+plot+i);
          totalSyst2->Add(Syst);
        }
      }
      variance = variance + std::pow((totalSyst->Integral() - mean),2);
      variance2 = variance2 + std::pow((totalSyst2->Integral() - mean2),2);
    }
    // if (nSys>1){
    //   variance = variance / (nSys-1);
    //   variance2 = variance2 / (nSys-1);
    // }
    // double syst = std::sqrt(variance);
    // double stat = std::sqrt(mean);
    // double sigma = std::sqrt(stat*stat + syst*syst);
    double sigma =0.;
    for (int i=0; i<5; i++){
      int a=0;
      if (i>0) a=i+1;
      sigma += std::pow(ElecYields[a][2*column+1],2);
    }
    std::cout << "Total Sherpa Contribution = " << mean << " +- " << std::sqrt(sigma) << std::endl;
    ElecYields[6][2*column] = mean;
    ElecYields[6][2*column+1] = std::sqrt(sigma);
    // syst = std::sqrt(variance2);
    // stat = std::sqrt(mean2);
    // sigma = std::sqrt(stat*stat + syst*syst);
    sigma =0.;
    for (int i=1; i<6; i++){
      sigma += std::pow(ElecYields[i][2*column+1],2);
    }
    std::cout << "Total MadGraph Contribution = " << mean2 << " +- " << std::sqrt(sigma) << std::endl;
    ElecYields[7][2*column] = mean2;
    ElecYields[7][2*column+1] = std::sqrt(sigma);
    column = column + 1;
  }

  /// Draw line " Muon Channel "
  for (int i=0; i<77; i++)
    if (i==39) std::cout << " Muon Channel ";
    else std::cout << "=";
  std::cout << std::endl;

  std::cout << setw(16) << "Sherpa Z+jets" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << MuonYields[0][2*j] << " +- " << setw(4) << MuonYields[0][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "MadGraph Z+jets" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << MuonYields[1][2*j] << " +- " << setw(4) << MuonYields[1][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "Top" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << MuonYields[2][2*j] << " +- " << setw(4) << MuonYields[2][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "Diboson" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << MuonYields[3][2*j] << " +- " << setw(4) << MuonYields[3][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "Z -> tt" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << MuonYields[4][2*j] << " +- " << setw(4) << MuonYields[4][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "W+jets" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << MuonYields[5][2*j] << " +- " << setw(4) << MuonYields[5][2*j+1] << " | ";
  std::cout << std::endl;
  /// Draw line
  for (int i=0; i<90; i++)
    std::cout << "-";
  std::cout << std::endl;


  std::cout << setw(16) << "Total (Sherpa)" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << MuonYields[6][2*j] << " +- " << setw(4) << MuonYields[6][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "Total (MadGraph)" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << MuonYields[7][2*j] << " +- " << setw(4) << MuonYields[7][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "Data" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << MuonYields[8][2*j] << " +- " << setw(4) << MuonYields[8][2*j+1] << " | ";
  std::cout << std::endl;

  /// Draw line
  for (int i=0; i<90; i++)
    std::cout << "=";
  std::cout << std::endl;

  /// Draw line " Muon Channel "
  for (int i=0; i<77; i++)
    if (i==39) std::cout << " Elec Channel ";
    else std::cout << "=";
  std::cout << std::endl;

  std::cout << setw(16) << "Sherpa Z+jets" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << ElecYields[0][2*j] << " +- " << setw(4) << ElecYields[0][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "MadGraph Z+jets" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << ElecYields[1][2*j] << " +- " << setw(4) << ElecYields[1][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "Top" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << ElecYields[2][2*j] << " +- " << setw(4) << ElecYields[2][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "Diboson" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << ElecYields[3][2*j] << " +- " << setw(4) << ElecYields[3][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "Z -> tt" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << ElecYields[4][2*j] << " +- " << setw(4) << ElecYields[4][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "W+jets" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << ElecYields[5][2*j] << " +- " << setw(4) << ElecYields[5][2*j+1] << " | ";
  std::cout << std::endl;
  /// Draw line
  for (int i=0; i<90; i++)
    std::cout << "-";
  std::cout << std::endl;


  std::cout << setw(16) << "Total (Sherpa)" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << ElecYields[6][2*j] << " +- " << setw(4) << ElecYields[6][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "Total (MadGraph)" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << ElecYields[7][2*j] << " +- " << setw(4) << ElecYields[7][2*j+1] << " | ";
  std::cout << std::endl;

  std::cout << setw(16) << "Data" << " | ";
  for (int j=0; j<4; j++)
    std::cout << setw(7) << ElecYields[8][2*j] << " +- " << setw(4) << ElecYields[8][2*j+1] << " | ";
  std::cout << std::endl;

  /// Draw line
  for (int i=0; i<90; i++)
    std::cout << "=";
  std::cout << std::endl;

  /// Do the latex formatting
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "DOING LATEX FORMATTING FOR MUON CHANNEL!" << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "\\hline" << std::endl;
  std::cout << "Sherpa \\Zjets      & ";
  for (int j=0; j<4; j++){
    if (j==0) std::cout << "\\numRF{" << MuonYields[0][2*j] << "}{4}\\phantom{0}"; // Yield
    else if (j<4) std::cout << "\\numRF{" << MuonYields[0][2*j] << "}{3}\\phantom{0}"; // Yield
    else fatal("failed Sherpa yield");
    std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[0][2*j+1] << "}{2}"; // Uncertainty
    // std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[0][2*j+1] << "}{2} (\\numRP{" << MuonYields[0][2*j+1]/MuonYields[0][2*j] << "}{2})"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "MadGraph \\Zjets      & ";
  for (int j=0; j<4; j++){
    if (j==0) std::cout << "\\numRF{" << MuonYields[1][2*j] << "}{4}\\phantom{0}"; // Yield
    else if (j<4) std::cout << "\\numRF{" << MuonYields[1][2*j] << "}{3}\\phantom{0}"; // Yield
    else fatal("failed MadGraph yield");
    std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[1][2*j+1] << "}{2}"; // Uncertainty
    // std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[1][2*j+1] << "}{2} (\\numRP{" << MuonYields[1][2*j+1]/MuonYields[1][2*j] << "}{2})"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "Top         & ";
  for (int j=0; j<4; j++){
    std::cout << "\\numRF{" << MuonYields[2][2*j] << "}{3}\\phantom{0}";
    std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[2][2*j+1] << "}{2}"; // Uncertainty
    // std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[2][2*j+1] << "}{2} (\\numRP{" << MuonYields[2][2*j+1]/MuonYields[2][2*j] << "}{2})"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "Diboson         & ";
  for (int j=0; j<4; j++){
    std::cout << "\\numRF{" << MuonYields[3][2*j] << "}{3}\\phantom{0}"; // Yield
    std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[3][2*j+1] << "}{2}"; // Uncertainty
    // std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[3][2*j+1] << "}{2} (\\numRP{" << MuonYields[3][2*j+1]/MuonYields[3][2*j] << "}{2})"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "Z $\\rightarrow \\tau\\tau$     & ";
  for (int j=0; j<4; j++){
    if (j==0){
      std::cout << "\\numRF{" << MuonYields[4][2*j] << "}{3}\\phantom{0}"; // Yield
      // std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[4][2*j+1] << "}{2} (\\numRP{" << MuonYields[4][2*j+1]/MuonYields[4][2*j] << "}{2})"; // Uncertainty
      std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[4][2*j+1] << "}{2}"; // Uncertainty
    }
    else if (j<4){
      std::cout << "\\numRP{" << MuonYields[4][2*j] << "}{1}\\phantom{0}"; // Yield
      std::cout << "  & \\phantom{0}\\numRP{" << MuonYields[4][2*j+1] << "}{1}"; // Uncertainty
      // std::cout << "  & \\phantom{0}\\numRP{" << MuonYields[4][2*j+1] << "}{1} (\\numRP{" << MuonYields[4][2*j+1]/MuonYields[4][2*j] << "}{2})"; // Uncertainty
    }
    else fatal("failed Ztt yields");
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "\\Wjets     & ";
  for (int j=0; j<4; j++){
    if (j==0){
      std::cout << "\\numRF{" << MuonYields[5][2*j] << "}{3}\\phantom{0}"; // Yield
      // std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[5][2*j+1] << "}{2} (\\numRP{" << MuonYields[5][2*j+1]/MuonYields[5][2*j] << "}{2})"; // Uncertainty
      std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[5][2*j+1] << "}{2}"; // Uncertainty
    }
    else if (j<4){
      std::cout << "\\numRP{" << MuonYields[5][2*j] << "}{1}\\phantom{0}"; // Yield
      // std::cout << "  & \\phantom{0}\\numRP{" << MuonYields[5][2*j+1] << "}{1} (\\numRP{" << MuonYields[5][2*j+1]/MuonYields[5][2*j] << "}{2})"; // Uncertainty
      std::cout << "  & \\phantom{0}\\numRP{" << MuonYields[5][2*j+1] << "}{1}"; // Uncertainty
    }
    else fatal("failed Wjets yields");
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "\\hline" << std::endl;
  std::cout << "Total (Sherpa)       & ";
  for (int j=0; j<4; j++){
    if (j<2) std::cout << "\\numRF{" << MuonYields[6][2*j] << "}{4}\\phantom{0}"; // Yield
    else if (j<4) std::cout << "\\numRF{" << MuonYields[6][2*j] << "}{3}\\phantom{0}"; // Yield
    else fatal("failed sherpa Sum yield");
    std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[6][2*j+1] << "}{2}"; // Uncertainty
    // std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[6][2*j+1] << "}{2} (\\numRP{" << MuonYields[6][2*j+1]/MuonYields[6][2*j] << "}{2})"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "Total (MadGraph)       & ";
  for (int j=0; j<4; j++){
    if (j==0) std::cout << "\\numRF{" << MuonYields[7][2*j] << "}{3}\\phantom{0}"; // Yield
    else if (j==1) std::cout << "\\numRF{" << MuonYields[7][2*j] << "}{4}\\phantom{0}"; // Yield
    else if (j<4) std::cout << "\\numRF{" << MuonYields[7][2*j] << "}{3}\\phantom{0}"; // Yield
    else fatal("failed MadGraph Sum yield");
    // std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[7][2*j+1] << "}{2} (\\numRP{" << MuonYields[7][2*j+1]/MuonYields[7][2*j] << "}{2})"; // Uncertainty
    std::cout << "  & \\phantom{0}\\numRF{" << MuonYields[7][2*j+1] << "}{2}"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "Data        & \\multicolumn{2}{c|}{";
  for (int j=0; j<4; j++){
    std::cout << "\\num{" << std::round(MuonYields[8][2*j]);
    if (j<2) std::cout << "}} & \\multicolumn{2}{c|}{";
    else if (j==2) std::cout << "}} & \\multicolumn{2}{c}{";
    else std::cout << "}} \\\\" << std::endl;
  }
  std::cout << "\\hline\\hline" << std::endl;

  /// Do the latex formatting
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "DOING LATEX FORMATTING FOR Elec CHANNEL!" << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "\\hline" << std::endl;
  std::cout << "Sherpa \\Zjets      & ";
  for (int j=0; j<4; j++){
    if (j==0) std::cout << "\\numRF{" << ElecYields[0][2*j] << "}{4}\\phantom{0}"; // Yield
    else if (j<4) std::cout << "\\numRF{" << ElecYields[0][2*j] << "}{3}\\phantom{0}"; // Yield
    else fatal("failed Sherpa yield");
    std::cout << "  & \\phantom{0}\\numRF{" << ElecYields[0][2*j+1] << "}{2}"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "MadGraph \\Zjets      & ";
  for (int j=0; j<4; j++){
    if (j==0) std::cout << "\\numRF{" << ElecYields[1][2*j] << "}{4}\\phantom{0}"; // Yield
    else if (j==1) std::cout << "\\numRF{" << ElecYields[1][2*j] << "}{4}\\phantom{0}"; // Yield
    else if (j<4) std::cout << "\\numRF{" << ElecYields[1][2*j] << "}{3}\\phantom{0}"; // Yield
    else fatal("failed MadGraph yield");
    std::cout << "  & \\phantom{0}\\numRF{" << ElecYields[1][2*j+1] << "}{2}"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "Top         & ";
  for (int j=0; j<4; j++){
    if (j<3) std::cout << "\\numRF{" << ElecYields[2][2*j] << "}{3}\\phantom{0}"; // Yield
    else if (j<4) std::cout << "\\numRF{" << ElecYields[2][2*j] << "}{2}\\phantom{0}"; // Yield
    else fatal("failed Top yield");
    std::cout << "  & \\phantom{0}\\numRF{" << ElecYields[2][2*j+1] << "}{2}"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "Diboson         & ";
  for (int j=0; j<4; j++){
    if (j==0) std::cout << "\\numRF{" << ElecYields[3][2*j] << "}{4}\\phantom{0}"; // Yield
    else if (j<4) std::cout << "\\numRF{" << ElecYields[3][2*j] << "}{3}\\phantom{0}"; // Yield
    else fatal("failed Diboson yield");
    std::cout << "  & \\phantom{0}\\numRF{" << ElecYields[3][2*j+1] << "}{2}"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "Z $\\rightarrow \\tau\\tau$     & ";
  for (int j=0; j<4; j++){
    if (j==0){
      std::cout << "\\numRF{" << ElecYields[4][2*j] << "}{3}\\phantom{0}"; // Yield
      std::cout << "  & \\phantom{0}\\numRF{" << ElecYields[4][2*j+1] << "}{2}"; // Uncertainty
    }
    else if (j<4){
      std::cout << "\\numRP{" << ElecYields[4][2*j] << "}{1}\\phantom{0}"; // Yield
      std::cout << "  & \\phantom{0}\\numRP{" << ElecYields[4][2*j+1] << "}{1}"; // Uncertainty
    }
    else fatal("failed Ztt yield");
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "\\Wjets     & ";
  for (int j=0; j<4; j++){
    if (j==0){
      std::cout << "\\numRF{" << ElecYields[5][2*j] << "}{2}\\phantom{0}"; // Yield
      std::cout << "  & \\phantom{0}\\numRF{" << ElecYields[5][2*j+1] << "}{2}"; // Uncertainty
    }
    else if (j<4){
      std::cout << "\\numRP{" << ElecYields[5][2*j] << "}{1}\\phantom{0}"; // Yield
      std::cout << "  & \\phantom{0}\\numRP{" << ElecYields[5][2*j+1] << "}{1}"; // Uncertainty
    }
    else fatal("failed WJets yield");
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "\\hline" << std::endl;
  std::cout << "Total (Sherpa)       & ";
  for (int j=0; j<4; j++){
    if (j==0) std::cout << "\\numRF{" << ElecYields[6][2*j] << "}{4}\\phantom{0}"; // Yield
    else if (j<4) std::cout << "\\numRF{" << ElecYields[6][2*j] << "}{3}\\phantom{0}"; // Yield
    else fatal("failed sherpa sum yields");
    std::cout << "  & \\phantom{0}\\numRF{" << ElecYields[6][2*j+1] << "}{2}"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "Total (MadGraph)       & ";
  for (int j=0; j<4; j++){
    if (j<2) std::cout << "\\numRF{" << ElecYields[7][2*j] << "}{4}\\phantom{0}"; // Yield
    else if (j<4) std::cout << "\\numRF{" << ElecYields[7][2*j] << "}{3}\\phantom{0}"; // Yield
    else fatal("failed sherpa sum yields");
    std::cout << "  & \\phantom{0}\\numRF{" << ElecYields[7][2*j+1] << "}{2}"; // Uncertainty
    if (j!=3) std::cout << " & ";
    else std::cout << "  \\\\" << std::endl;
  }
  std::cout << "Data        & \\multicolumn{2}{c|}{";
  for (int j=0; j<4; j++){
    std::cout << "\\num{" << std::round(ElecYields[8][2*j]);
    if (j<2) std::cout << "}} & \\multicolumn{2}{c|}{";
    else if (j==2) std::cout << "}} & \\multicolumn{2}{c}{";
    else std::cout << "}} \\\\" << std::endl;
  }
  std::cout << "\\hline\\hline" << std::endl;






  return;
}
