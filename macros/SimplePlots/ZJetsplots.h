//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Oct 18 19:59:57 2019 by ROOT version 6.16/00
// from TTree Ztree0/ZJets_tree
// found on file: /eos/user/a/alaurier/ZJets/2019-10-20/data18.root
//////////////////////////////////////////////////////////

#ifndef ZJetsplots_h
#define ZJetsplots_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include <TH1F.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <iostream>
#include "vector"

  // Truth values
  map<int, vector<float>> genmap;

class ZJetsplots {
public :



   double EvtSum = 1;
   int    sys = 0;
   TString mout = "";
   TString Filename = "";
   std::vector<int> cutFlowValues;

   std::vector<float> vjet_E;
   std::vector<float> vjet_px;
   std::vector<float> vjet_py;
   std::vector<float> vjet_pz;
   std::vector<float> vjetl_E;
   std::vector<float> vjetl_px;
   std::vector<float> vjetl_py;
   std::vector<float> vjetl_pz;
   std::vector<float> truth_vjet_E;
   std::vector<float> truth_vjet_px;
   std::vector<float> truth_vjet_py;
   std::vector<float> truth_vjet_pz;
   std::vector<float> truth_vjetl_E;
   std::vector<float> truth_vjetl_px;
   std::vector<float> truth_vjetl_py;
   std::vector<float> truth_vjetl_pz;




   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           EventNumber;
   Int_t           RunNumber;
   Float_t         initialWeight;
   Float_t         pileupWeight;
   Float_t         finalWeight;
   bool         PassTrig;
   Float_t         trigSF;
   Float_t         lep1TotalSF;
   Float_t         lep2TotalSF;
   Float_t         jvtSF;
   Float_t         fjvtSF;
   Float_t         AverageMu;
   Int_t           Njets;
   Bool_t          isZEvent;
   vector<float>   *lep_E;
   vector<float>   *lep_pT;
   vector<float>   *lep_px;
   vector<float>   *lep_py;
   vector<float>   *lep_pz;
   vector<float>   *lep_id;
   vector<float>   *jet_E;
   vector<float>   *jet_pT;
   vector<float>   *jet_px;
   vector<float>   *jet_py;
   vector<float>   *jet_pz;
   Int_t           truth_Njets;
   vector<float>   *truth_lep_E;
   vector<float>   *truth_lep_pT;
   vector<float>   *truth_lep_px;
   vector<float>   *truth_lep_py;
   vector<float>   *truth_lep_pz;
   vector<float>   *truth_lep_id;
   vector<float>   *truth_jet_E;
   vector<float>   *truth_jet_pT;
   vector<float>   *truth_jet_px;
   vector<float>   *truth_jet_py;
   vector<float>   *truth_jet_pz;

   // List of branches
   TBranch        *b_EventNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_initialWeight;   //!
   TBranch        *b_pileupWeight;   //!
   TBranch        *b_finalWeight;   //!
   TBranch        *b_passTrigger;   //!
   TBranch        *b_trigSF;   //!
   TBranch        *b_lep1TotalSF;   //!
   TBranch        *b_lep2TotalSF;   //!
   TBranch        *b_jvtSF;   //!
   TBranch        *b_fjvtSF;   //!
   TBranch        *b_AverageMu;   //!
   TBranch        *b_Njets;   //!
   TBranch        *b_isZEvent;   //!
   TBranch        *b_lep_E;   //!
   TBranch        *b_lep_pT;   //!
   TBranch        *b_lep_px;   //!
   TBranch        *b_lep_py;   //!
   TBranch        *b_lep_pz;   //!
   TBranch        *b_lep_id;   //!
   TBranch        *b_jet_E;   //!
   TBranch        *b_jet_pT;   //!
   TBranch        *b_jet_px;   //!
   TBranch        *b_jet_py;   //!
   TBranch        *b_jet_pz;   //!
   TBranch        *b_truth_Njets;   //!
   TBranch        *b_truth_lep_E;   //!
   TBranch        *b_truth_lep_pT;   //!
   TBranch        *b_truth_lep_px;   //!
   TBranch        *b_truth_lep_py;   //!
   TBranch        *b_truth_lep_pz;   //!
   TBranch        *b_truth_lep_id;   //!
   TBranch        *b_truth_jet_E;   //!
   TBranch        *b_truth_jet_pT;   //!
   TBranch        *b_truth_jet_px;   //!
   TBranch        *b_truth_jet_py;   //!
   TBranch        *b_truth_jet_pz;   //!

   ZJetsplots(TTree *tree=0,double nev=0,int Nsys=0, TString filename="", TString oname="",std::vector<int> cutflowValues={99});
   virtual ~ZJetsplots();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree,int sys);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ZJetsplots_cxx
ZJetsplots::ZJetsplots(TTree *tree,double nev,int Nsys,TString filename, TString oname, std::vector<int> cutflowValues) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/afs/cern.ch/user/a/alaurier/private/data18.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/afs/cern.ch/user/a/alaurier/private/data18.root");
      }
      f->GetObject("Ztree0",tree);

   }
   EvtSum = nev;
   sys = Nsys;
   mout = oname;
   Filename = filename;
   for (int i=0; i<cutflowValues.size();i++)
      cutFlowValues.push_back(cutflowValues[i]);
   Init(tree,Nsys);
}

ZJetsplots::~ZJetsplots()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ZJetsplots::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ZJetsplots::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ZJetsplots::Init(TTree *tree,int sys)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   lep_E = 0;
   lep_pT = 0;
   lep_px = 0;
   lep_py = 0;
   lep_pz = 0;
   lep_id = 0;
   jet_E = 0;
   jet_pT = 0;
   jet_px = 0;
   jet_py = 0;
   jet_pz = 0;
   truth_lep_E = 0;
   truth_lep_pT = 0;
   truth_lep_px = 0;
   truth_lep_py = 0;
   truth_lep_pz = 0;
   truth_lep_id = 0;
   truth_jet_E = 0;
   truth_jet_pT = 0;
   truth_jet_px = 0;
   truth_jet_py = 0;
   truth_jet_pz = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("initialWeight", &initialWeight, &b_initialWeight);
   fChain->SetBranchAddress("pileupWeight", &pileupWeight, &b_pileupWeight);
   fChain->SetBranchAddress("finalWeight", &finalWeight, &b_finalWeight);
   fChain->SetBranchAddress("PassTrig", &PassTrig, &b_passTrigger);
   fChain->SetBranchAddress("trigSF", &trigSF, &b_trigSF);
   fChain->SetBranchAddress("lep1TotalSF", &lep1TotalSF, &b_lep1TotalSF);
   fChain->SetBranchAddress("lep2TotalSF", &lep2TotalSF, &b_lep2TotalSF);
   fChain->SetBranchAddress("jvtSF", &jvtSF, &b_jvtSF);
   fChain->SetBranchAddress("fjvtSF", &fjvtSF, &b_fjvtSF);
   fChain->SetBranchAddress("AverageMu", &AverageMu, &b_AverageMu);
   fChain->SetBranchAddress("Njets", &Njets, &b_Njets);
   fChain->SetBranchAddress("isZEvent", &isZEvent, &b_isZEvent);
   fChain->SetBranchAddress("lep_E", &lep_E, &b_lep_E);
   fChain->SetBranchAddress("lep_pT", &lep_pT, &b_lep_pT);
   fChain->SetBranchAddress("lep_px", &lep_px, &b_lep_px);
   fChain->SetBranchAddress("lep_py", &lep_py, &b_lep_py);
   fChain->SetBranchAddress("lep_pz", &lep_pz, &b_lep_pz);
   fChain->SetBranchAddress("lep_id", &lep_id, &b_lep_id);
   fChain->SetBranchAddress("jet_E", &jet_E, &b_jet_E);
   fChain->SetBranchAddress("jet_pT", &jet_pT, &b_jet_pT);
   fChain->SetBranchAddress("jet_px", &jet_px, &b_jet_px);
   fChain->SetBranchAddress("jet_py", &jet_py, &b_jet_py);
   fChain->SetBranchAddress("jet_pz", &jet_pz, &b_jet_pz);
   if (sys == 0){
      fChain->SetBranchAddress("truth_Njets", &truth_Njets, &b_truth_Njets);
      fChain->SetBranchAddress("truth_lep_E", &truth_lep_E, &b_truth_lep_E);
      fChain->SetBranchAddress("truth_lep_pT", &truth_lep_pT, &b_truth_lep_pT);
      fChain->SetBranchAddress("truth_lep_px", &truth_lep_px, &b_truth_lep_px);
      fChain->SetBranchAddress("truth_lep_py", &truth_lep_py, &b_truth_lep_py);
      fChain->SetBranchAddress("truth_lep_pz", &truth_lep_pz, &b_truth_lep_pz);
      fChain->SetBranchAddress("truth_lep_id", &truth_lep_id, &b_truth_lep_id);
      fChain->SetBranchAddress("truth_jet_E", &truth_jet_E, &b_truth_jet_E);
      fChain->SetBranchAddress("truth_jet_pT", &truth_jet_pT, &b_truth_jet_pT);
      fChain->SetBranchAddress("truth_jet_px", &truth_jet_px, &b_truth_jet_px);
      fChain->SetBranchAddress("truth_jet_py", &truth_jet_py, &b_truth_jet_py);
      fChain->SetBranchAddress("truth_jet_pz", &truth_jet_pz, &b_truth_jet_pz);
   }
   Notify();
}

Bool_t ZJetsplots::Notify()
{
   return kTRUE;
}

void ZJetsplots::Show(Long64_t entry)
{
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ZJetsplots::Cut(Long64_t entry)
{
   return 1;
}
#endif // #ifdef ZJetsplots_cxx
