#define ZJetsplots_cxx
#include "ZJetsplots.h"
#include <TH2.h>
#include "TFile.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TEnv.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TTree.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TError.h"
#include "atlasStyle/AtlasStyle.C"
#include "atlasStyle/AtlasUtils.C"
#include "atlasStyle/AtlasLabels.C"

typedef TString Str;
typedef std::vector<TString> StrV;
typedef std::map<TString, StrV> StrVMap;
typedef std::vector<TFile*> FileV;
typedef std::map<TString, FileV> FileVMap;
typedef std::vector<double> NumV;
typedef std::map<TString, NumV> NumVMap;
typedef std::vector<int> IntV;
typedef std::vector<TH1F*> HistV;
typedef std::map<TString, HistV*> HistVMap;
typedef std::map<TString, TH1F*> HistMap;
typedef std::vector<TH2F*> HistV2;
typedef std::map<TString, HistV2*> HistVMap2;
typedef std::map<TString, TH2F*> HistMap2;
typedef vector<TF1*> FuncV;
double maxHT=0;
double maxNJets=0;
double maxRun=0;
double maxEvent=0;

void fatal(Str msg) {
  std::cout << "FATAL: " << msg.Data() << std::endl;
  abort();
}
void setStyle();
TEnv *openSettingsFile(Str fileName);
TFile *openFile(Str fn);
TH1F *getHisto(TFile *f, Str hn);
TH1F *getHisto(Str fn, Str hn) {
  return getHisto(openFile(fn), hn);
}
StrV vectorize(Str str, Str sep = " ");
NumV numberize(Str str, Str sep = " ");
IntV interize(Str str, Str sep = " ");
void drawText(double x, double y, Str txt, bool alignRight = 0, int col = kBlack, double tsize = 0.04);
void drawLine(double x1, double y1, double x2, double y2, int col = kBlack, int ls = 1, int lw = 1);

void cutFlow(FileVMap sortedSampleFiles, TEnv* settings);
void yields(FileVMap sortedSampleFiles, TEnv* settings);
void plotsFromHists(FileVMap sortedSampleFiles, Str plotSet, TEnv* settings);
NumV* makeLinearBins(int nBins, float min, float max);
void drawStackMC(TEnv* settings, Str plot, HistMap hists, StrV labels, Str strongSample, IntV colours, TCanvas *can, TLegend* leg, Str region, Str channel, double lumi, Str pdf, int logX, int logY, int chanNo);

double getScaleFactorCF(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi);
double getScaleFactor(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi);

void doHistograms(FileVMap sortedSampleFiles, TEnv* settings, StrVMap filenameMap);
void dataPlots(FileV dataFiles, Str plotSet, TEnv* settings, Str channel);

int getEvent(Str config = "ZJetsplots.data"){
   setStyle();

   std::cout << "config file: " << config.Data() << std::endl;
   TEnv *settings = openSettingsFile(config);
   StrV periods = vectorize(settings->GetValue("periods", " "));

   bool makeCutFlow = (bool)settings->GetValue("makeCutFlow", 0);
   bool makeYields = (bool)settings->GetValue("makeYields", 0);
   bool makePlots = (bool)settings->GetValue("makePlots", 0);
   bool makeDataPlots = (bool)settings->GetValue("makeDataPlots", 0);
   bool makeHistos = (bool)settings->GetValue("makeHistos", 0);

   FileVMap sortedSampleFiles;
   StrVMap sortedFileNames;
   if(makeYields || makePlots){
     for (auto period : periods) {
       Str filePath = settings->GetValue("filePath."+period, " ");
       StrV inputCategories = vectorize(settings->GetValue("inputCategories."+period, " "));
       for (auto input : inputCategories) {
         StrV sampleList = vectorize(settings->GetValue(input.Data(), " "));
         for (auto sample : sampleList) {
           //printf("opening file %-30s : %-70s\r", (period+"_"+input+"_hist").Data(), sample.Data());
           sortedSampleFiles[period+"_"+input].push_back(openFile(filePath + sample + "_hist.root"));
         }
       }
       //printf("\n");
     }
   }
   else if (makeHistos || makeCutFlow) {
      for (auto period : periods) {
         Str filePath = settings->GetValue("filePath."+period, " ");
         StrV inputCategories = vectorize(settings->GetValue("inputCategories."+period, " "));
         for (auto input : inputCategories) {
            StrV sampleList = vectorize(settings->GetValue(input.Data(), " "));
            for (auto sample : sampleList) {
               // printf("opening file %s : %s\r", (period+"_"+input).Data(), sample.Data());
               sortedSampleFiles[period+"_"+input].push_back(openFile(filePath + sample + ".root"));
               if (makeHistos) sortedFileNames[period+"_"+input].push_back(filePath +sample + ".root");
            }
         }
      }
   }
   else if (makeDataPlots){
      StrV dataYears = vectorize(settings->GetValue("dataYears", " "));
      StrV channels = vectorize(settings->GetValue("dataPlots.channels", " "));
      for(auto channel : channels){
         FileV dataFiles;
         for(auto year : dataYears){
            Str dataFilePath = settings->GetValue("filePath."+year, " ");
            // printf("opening file %-70s\n", (dataFilePath+channel+"/"+year+"_"+channel+".root").Data());
            dataFiles.push_back(openFile(dataFilePath+year+"_hist.root"));
         }
         StrV plotSetList = vectorize(settings->GetValue("dataPlots.plotSetList", " "));
         for(auto plotSet : plotSetList)
            dataPlots(dataFiles, plotSet, settings, channel);
      }
   }

   if (makeHistos) doHistograms(sortedSampleFiles, settings, sortedFileNames);
   if (makeCutFlow) { cutFlow(sortedSampleFiles, settings); }
   if (makePlots) {
     StrV plotSetList = vectorize(settings->GetValue("plotSetList", " "));
     for(auto plotSet : plotSetList)
      plotsFromHists(sortedSampleFiles, plotSet, settings);
   }


   return 0;
}

void ZJetsplots::Loop()
{
   if (fChain == 0) return;

   // Event Loop
   Long64_t nentries = fChain->GetEntriesFast();
   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) { // Start of Event loop
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;

      if(sys==0) { // If truth and !dilepton, continue
         if(lep_E->size()!=2 && truth_lep_E->size()!=2)
            continue;
      }
      else { // If not truth and !dilepton, continue
         if(lep_E->size()!=2) {
            continue;
         }
      }

      // Phase space regions (search)
      // rch = 0 : Electron channel
      // rch = 1 : Muon channel
      // rch = 2 : e-mu channel
      int rch = 3;
      if(lep_E->size()==2){
         int id0 = lep_id->at(0);
         int id1 = lep_id->at(1);
         if (id0 * id1 > 0) continue; // same charge leptons
         if( std::abs(id0)==11 && std::abs(id1)==11 ) {
            rch = 0; // electrons
            // fake muons: v2: drop isolation requirement
         }else if (std::abs(id0)==13 && std::abs(id1)==13 ) {
            rch = 1; // muons
         }else if ((std::abs(id0)==13 && std::abs(id1)==11) || (std::abs(id0)==11 && std::abs(id1)==13)) {
            rch = 2; // e-mu
         }
      }
      if (rch!=1) continue;

      // jet, lepton  and Z 4-vectors
      TLorentzVector mu1, mu2, zet, jet, cljet, cljetr, cljett, jet1, jet2, jmother;
      if(lep_px->size()>1){
         mu1.SetPxPyPzE(lep_px->at(0),lep_py->at(0),lep_pz->at(0),lep_E->at(0));
         mu2.SetPxPyPzE(lep_px->at(1),lep_py->at(1),lep_pz->at(1),lep_E->at(1));
         zet = mu1+mu2;
      }

      bool isGoodZ       = lep_px->size()==2 && rch<3 && PassTrig && mu1.Pt()>25. &&  mu2.Pt()>25 && fabs(mu1.Eta())<2.4 &&  fabs(mu2.Eta())<2.4 && lep_id->at(0)*lep_id->at(1)<0;
      bool isSelected    = isGoodZ && 71. < zet.M() &&  zet.M() < 111.;

      int nPreJets  = jet_px->size();
      int nJets = 0, nJetsLoose = 0, truth_nJets = 0, truth_nJetsLoose = 0;

      vjet_E.clear();
      vjet_px.clear();
      vjet_py.clear();
      vjet_pz.clear();
      vjetl_E.clear();
      vjetl_px.clear();
      vjetl_py.clear();
      vjetl_pz.clear();
      double HT = 0.;
      HT+=mu1.Pt();
      HT+=mu2.Pt();
      
      //define final jets (tight and loose)
      if(isGoodZ){
         for (int i = 0;i<nPreJets;i++){
            jet.SetPxPyPzE(jet_px->at(i),jet_py->at(i),jet_pz->at(i),jet_E->at(i));
            if(jet.Pt()>100. && fabs(jet.Rapidity())<2.5){ // I dont have a pass JVT flag like Ulla. All my jets are required to pass JVT
               HT+=jet.Pt();
               vjet_px.push_back(jet_px->at(i));
               vjet_py.push_back(jet_py->at(i));
               vjet_pz.push_back(jet_pz->at(i));
               vjet_E.push_back(jet_E->at(i));
               // ALEX SHOULD I ADD THIS Scaling factor?
               // JVT SF for jets to aleready dilepton SF?
               //wgrec *= jvtSF;
            }
         }
      }
      nJets = vjet_E.size();
      if (HT > maxHT){
        maxHT = HT;
        maxNJets=nJets;
        maxRun=RunNumber;
        maxEvent=EventNumber;
      }
      if (nJets == 9){
        std::cout << "~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+" << std::endl;
        std::cout << "===============================" << std::endl;
        std::cout << "ALEXANDRE RUN EVENT NUMBER PAIR = " << RunNumber << " " << EventNumber << std::endl;
        std::cout << "ALEXANDRE RUN EVENT NUMBER PAIR = " << RunNumber << " " << int(EventNumber) << std::endl;
        std::cout << "===============================" << std::endl;
        std::cout << " with HT = " << HT << std::endl;
        std::cout << "~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+" << std::endl;
        //return;
      }
   } // End of Event loop
std::cout << "maxHT: " << maxHT << std::endl;
std::cout << "has " << maxNJets << "jets with pT > 100GeV" << std::endl;
std::cout << "RUN EVENT NUMBER PAIR" << maxRun << " " << maxEvent << std::endl;

} // Event of ZJetsplots::Loop

TH1F *getHisto(TFile *f, Str hn) {
  TH1F *h = (TH1F*) f->Get(hn);
  if (h == NULL)
    fatal("Cannot access " + hn + " in " + f->GetName());
  return h;
}

TEnv *openSettingsFile(Str fileName) {
  if (fileName == "")
    fatal("No config file name specified. Cannot open file!");
  TEnv *settings = new TEnv();
  int status = settings->ReadFile(fileName.Data(), EEnvLevel(0));
  if (status != 0)
    fatal(Form("Cannot read file %s", fileName.Data()));
  return settings;
}

StrV vectorize(Str str, Str sep) {
  StrV result;
  TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries() == 0)
    return result;
  TIter istr(strings);
  while (TObjString* os = (TObjString*) istr())
    if (os->GetString()[0] != '#')
      result.push_back(os->GetString());
    else
      break;
  return result;
}

NumV numberize(Str str, Str sep) {
  NumV result;
  TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries() == 0)
    return result;
  TIter istr(strings);
  while (TObjString* os = (TObjString*) istr())
    if (os->GetString()[0] != '#')
      result.push_back((os->GetString()).Atof());
    else
      break;
  return result;
}

IntV interize(Str str, Str sep) {
  IntV result;
  TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries() == 0)
    return result;
  TIter istr(strings);
  while (TObjString* os = (TObjString*) istr())
    if (os->GetString()[0] != '#')
      result.push_back((os->GetString()).Atoi());
    else
      break;
  return result;
}

TFile *openFile(Str fn) {
  TFile *f = TFile::Open(fn);
  if (f == NULL || f->IsZombie())
    fatal("Cannot open " + fn);
  return f;
}

void setStyle() {
  double tsize = 0.035;
  gStyle->SetTextSize(tsize);
  gStyle->SetLabelSize(tsize, "x");
  gStyle->SetTitleSize(tsize, "x");
  gStyle->SetLabelSize(tsize, "y");
  gStyle->SetTitleSize(tsize, "y");

  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.04);
  gStyle->SetPadBottomMargin(0.12);
  gStyle->SetPadTopMargin(0.05);
  TH1::SetDefaultSumw2();
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  gStyle->SetHistMinimumZero();
  gStyle->SetTitleOffset(1.4, "y");
  gStyle->SetTitleOffset(1.4, "x");
}

void drawText(double x, double y, Str txt, bool alignRight, int col, double tsize) {
  static TLatex *tex = new TLatex();
  if(alignRight)
    tex->SetTextAlign(31);
  tex->SetTextColor(col);
  tex->SetNDC();
  tex->SetTextSize(tsize);
  tex->SetTextFont(42);
  tex->DrawLatex(x, y, txt);
}

void drawLine(double x1, double y1, double x2, double y2, int col, int ls, int lw) {
  static TLine *line = new TLine();
  line->SetLineColor(col);
  line->SetLineStyle(ls);
  line->SetLineWidth(lw);
  line->DrawLine(x1, y1, x2, y2);
}

NumV* makeLinearBins(int nBins, float min, float max){
  NumV* bins = new NumV;
  float step = (max-min)/nBins;
  for(int i=0 ; i<=nBins; i++)
    bins->push_back(min+i*step);
  return bins;
}

double getScaleFactorCF(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi){
  TObjArray *tx = sample.Tokenize("/");
  if((((TObjString *)(tx->Last()))->String()).Contains("data")){
    delete tx;
    return 1.0;
  }
  sample = (((TObjString *)(tx->Last()))->String()).ReplaceAll(".root", "");
  delete tx;

  double sumW = getHisto(sampleFile, "sum_of_weights")->GetBinContent(4);
  double sigma = xsecs->GetValue(sample + ".Xsec", 0.0);
  double k = xsecs->GetValue(sample + ".Kfactor", 0.0);
  double eff = xsecs->GetValue(sample + ".Filter", 0.0);

  return sigma * k * eff * lumi / sumW;
}

double getScaleFactor(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi){
  TObjArray *tx = sample.Tokenize("/");
  if((((TObjString *)(tx->Last()))->String()).Contains("data")){
    delete tx;
    return 1.0;
  }
  sample = (((TObjString *)(tx->Last()))->String()).ReplaceAll("_hist.root", "");
  delete tx;

  double sumW = getHisto(sampleFile, "hsumwgh")->GetBinContent(1);
  double sigma = xsecs->GetValue(sample + ".Xsec", 0.0);
  double k = xsecs->GetValue(sample + ".Kfactor", 0.0);
  double eff = xsecs->GetValue(sample + ".Filter", 0.0);

  return sigma * k * eff * lumi / sumW;
}

void doHistograms(FileVMap sortedSampleFiles, TEnv* settings, StrVMap filenameMap){
   // Definitions for all the different files
   std::string Treename = "Ztree";
   IntV Systematics = interize(settings->GetValue("Systematics", " "));
   int nSys = Systematics[0];

   for (auto obj : sortedSampleFiles){
      int counter = 0;
      for (auto fname : obj.second){
         // Open sum of weights histo
         TString filename = (filenameMap.find(obj.first)->second)[counter];
         Str outname = filename;
         outname.ReplaceAll(".root","_hist.root");
         TH1F* hsumw = (TH1F*)fname->Get("sum_of_weights");
         TH1F* cutflowtable = (TH1F*)fname->Get("CutFlow_nominal");
         std::vector<int> cutflowvalues;
         for (int bin=1; bin < 100; bin++)
            cutflowvalues.push_back(cutflowtable->GetBinContent(bin));
         Double_t nevt = 1.;
         nevt = hsumw->GetBinContent(4); // Sum Of Weights
         std::cout << "Sum of Weights Skimmed: " << nevt << std::endl;
         // if (obj.first.Contains("data")) nSys = 1;
         for (int i=0; i<nSys; i++){
            TString SysTree = Treename+to_string(i);
            std::cout << "Debug:: TreeName = " << SysTree << std::endl;
            TChain *ZChain  = new TChain(SysTree);
            ZChain->Add(filename);
            if (i==0) {
               Long64_t nEntries = Long64_t(ZChain->GetEntries());
               std::cout << "Number of Events: " << nEntries << std::endl;
            }
            std::cout << "Doing Systematic variation # " << i << std::endl;
            ZJetsplots* Ana = new ZJetsplots((TChain *) ZChain, nevt, i, filename,outname,cutflowvalues);
            Ana->Loop();
            delete Ana;
            delete ZChain;
         }
         std::cout << "-----" << std::endl;
         counter++;
      }
      std::cout << "=====" << std::endl;
   }
   // end of do Histos
}

void drawStackMC(TEnv* settings, Str plot, HistMap hists, StrV inputLabels, Str strongSample, IntV colours, TCanvas *can, TLegend* leg, Str region, Str channel, double lumi, Str pdf, int logX, int logY, int chanNo) {
  if (logY)
    can->SetLogy(1);
  else
    can->SetLogy(0);
  if (logX)
    can->SetLogx(1);
  else
    can->SetLogx(0);
  can->Modified();
  can->Update();
  // If all years combined, remove the data 15,17,18 labels
  if (inputLabels[0] == "All Data") {
    inputLabels.erase(std::remove(inputLabels.begin(), inputLabels.end(), "2015-16 Data"), inputLabels.end());
    inputLabels.erase(std::remove(inputLabels.begin(), inputLabels.end(), "2017 Data"), inputLabels.end());
    inputLabels.erase(std::remove(inputLabels.begin(), inputLabels.end(), "2018 Data"), inputLabels.end());
  }
  Str Chan;
  if (channel.Contains("ee")) Chan = "ee";
  if (channel.Contains("mu")) Chan = "#mu#mu";
  if (channel.Contains("ll")) Chan = "ll";
  Str newSHLabel = "#it{Z}#rightarrow " + Chan + ", Sherpa2.2";
  Str newMGLabel = "#it{Z}#rightarrow " + Chan + ", MGPy8EG";
  StrV::iterator it1, it2;
  bool first = true;
  int col = 1;
  double ratio = 0.36, margin = 0.02;
  can->cd();
  // ALEX CHANGE THIS FOR PNG!
  bool doPNG = false;
  gPad->SetBottomMargin(ratio);
  gPad->SetTicks();
  TH1F* data;
  TH1F* totalSum, *totalSumAltStrong;
  for(auto input : inputLabels){
  // Data only histos
    if (!input.Contains("Data"))
      continue;
    //printf("%s\n", input.Data());
    data = (TH1F*) hists[input]->Clone();
    data->SetMarkerStyle(20);
    data->GetYaxis()->SetLabelSize(0.03);
    data->GetYaxis()->SetTitleOffset(1.4);
    // ALEX doPNG
    data->GetXaxis()->SetLabelSize(0);
    if(logY){
      data->SetMaximum(data->GetMaximum()*100);
      if(data->GetMinimum() > 100)
        data->SetMinimum(data->GetMinimum()*0.05);
    }
    else
      data->SetMaximum(data->GetMaximum()*2.0);
    double Ymin = settings->GetValue(plot+".Ymin", -99.9);
    double Ymax = settings->GetValue(plot+".Ymax", -99.9);
    if(Ymin != -99.9)
      data->SetMinimum(Ymin);
    if(Ymax != -99.9)
      data->SetMaximum(Ymax);
    data->Draw();
    //Str inputLabel = settings->GetValue(input + ".Label", " ");
    leg->AddEntry(data, input, "plf");
  }
  double totalSumW = 0.0;
  for (it1 = inputLabels.begin(); it1 != inputLabels.end(); it1++) {
    // Not Data
    //printf("%s\n", (*it1).Data());
    if ((*it1).Contains("Data")) continue;
    if((*it1).Contains("Sherpa") && !strongSample.CompareTo("MG")) continue;
    if((*it1).Contains("MG") && !strongSample.CompareTo("SP")) continue;
    if((*it1).Contains("MG") && (strongSample == "Both")) continue;
    it2 = it1;
    TH1F* sum = (TH1F*) hists[*it2]->Clone();
    sum->Reset();
    sum->SetMarkerStyle(20);
    sum->SetMarkerSize(0);
    //sum->SetLineColor(colours[col]);
    //sum->SetFillColor(colours[col++]);
    sum->SetLineColor(1);
    if (col==1)
      sum->SetLineColor(colours[col]);
    else
      sum->SetFillColor(colours[col]);
    col++;

    sum->GetXaxis()->SetTitleOffset(1.4);
    for (; it2 != inputLabels.end(); it2++) {
      //printf("%s\n", (*it2).Data());
      if ((*it2).Contains("Data")) continue;
      if((*it2).Contains("Sherpa") && !strongSample.CompareTo("MG")) continue;
      if((*it2).Contains("MG") && !strongSample.CompareTo("SP")) continue;
      if((*it2).Contains("MG") && (strongSample == "Both")) continue;
      sum->Add(hists[*it2]);
    }
    //Str inputLabel = settings->GetValue(*it1 + ".Label", " ");
    //leg->AddEntry(sum, *it1, "plf");
    if ((*it1).Contains("Sherpa")) leg->AddEntry(sum,newSHLabel,"pf");
    else leg->AddEntry(sum, *it1, "pf");
    sum->SetMinimum(0.5);
    sum->Draw("HIST SAME");
    if (first){
      totalSum = (TH1F*) sum->Clone();
      first = false;
    }
    totalSumW = sum->GetSumOfWeights();
    if (col==2){ //quick dumb hack for MG ordering in table
      TH1F* dumby = (TH1F*) sum->Clone();
      dumby->SetLineColor(2);
      // leg->AddEntry(dumby, "#it{Z}#rightarrow ll, MGPy8EG", "pf");
      leg->AddEntry(dumby, newMGLabel, "pf");
    }
  }
  first = true;
  //make sum for ratio with other strong Zjj
  //Now that I'm making both at once, this part is only for MadGraph
  for (it1 = inputLabels.begin(); it1 != inputLabels.end(); it1++) {
    if ((*it1).Contains("Data")) continue;
    if((*it1).Contains("MG") && !strongSample.CompareTo("MG")) continue;
    if((*it1).Contains("Sherpa") && !strongSample.CompareTo("SP")) continue;
    if((*it1).Contains("Sherpa") && (strongSample == "Both")) continue;
    it2 = it1;
    TH1F* sum = (TH1F*) hists[*it1]->Clone();
    sum->Reset();
    sum->SetMarkerStyle(20);
    sum->SetMarkerSize(0);
    sum->GetXaxis()->SetTitleOffset(1.4);
    sum->SetLineColor(2);
    for (; it2 != inputLabels.end(); it2++) {
      if ((*it2).Contains("Data")) continue;
      if((*it2).Contains("MG") && !strongSample.CompareTo("MG")) continue;
      if((*it2).Contains("Sherpa") && !strongSample.CompareTo("SP")) continue;
      if((*it2).Contains("Sherpa") && (strongSample == "Both")) continue;
      sum->Add(hists[*it2]);
    }
    sum->SetMinimum(0.5);
    sum->Draw("HIST SAME");
    totalSumAltStrong = (TH1F*) sum->Clone();
    break;
  }
  data->Draw("SAME E1");
  data->Draw("AXIS SAME E1");
  leg->Draw();
  // If using text size =0.04 (as big as ATLAS INTERNAL); use y spacing of 0.05 between lines.
  //ATLASLabel(0.58, 0.9, "Work in progress");
  ATLASLabel(0.25, 0.9, "Internal");
  drawText(0.25, 0.875, Form("#sqrt{#it{s}} = 13 TeV, %.1f fb^{-1}", lumi / 1000),0,1,0.02);
  drawText(0.25, 0.85, "p_{T}^{jet}#geq 100GeV, |y^{jet}| < 2.5",0,1,0.02);
  if (chanNo == 0)
    drawText(0.25, 0.825, "Z#rightarrow #mu#mu + #geq 1 jet",0,1,0.02);
  else if (chanNo == 1)
    drawText(0.25, 0.825, "Z#rightarrow ee + #geq 1 jet",0,1,0.02);
  else if (chanNo == 2)
    drawText(0.25, 0.825, "Z#rightarrow #ell#ell + #geq 1 jet",0,1,0.02);
  else fatal("Not ee,mumu or ll!");
  if (plot == "Mjj" || plot.Contains("ZJpT") || plot == "minDR")
    drawText(0.25, 0.8, "Leading jet p_{T} #geq 500GeV",0,1,0.02);
  if (doPNG){
  // Add this for the DR plots!
  if (plot == "ZJpThighDR")
    drawText(0.625, 0.45, "#DeltaR #geq 2.0",0,1,0.04);
  if (plot == "ZJpTlowDR")
    drawText(0.625, 0.45, "#DeltaR #leq 2.0",0,1,0.04);
  if (plot == "ZJpThigh05")
    drawText(0.625, 0.45, "#DeltaR #geq 0.5",0,1,0.04);
  if (plot == "ZJpTlow05")
    drawText(0.625, 0.45, "#DeltaR #leq 0.5",0,1,0.04);
  if (plot == "ZJpThigh1")
    drawText(0.625, 0.45, "#DeltaR #geq 1.0",0,1,0.04);
  if (plot == "ZJpTlow1")
    drawText(0.625, 0.45, "#DeltaR #leq 1.0",0,1,0.04);
  if (plot == "ZJpThigh15")
    drawText(0.625, 0.45, "#DeltaR #geq 1.5",0,1,0.04);
  if (plot == "ZJpTlow15")
    drawText(0.625, 0.45, "#DeltaR #leq 1.5",0,1,0.04);
  }
  TString pngTitle= plot + ".png";
  //can->Print(pngTitle);
  TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
  if (logX)
    p->SetLogx(1);
  else
    p->SetLogx(0);
  p->Modified();
  p->Update();
  p->SetTopMargin(1.0 - ratio);
  p->SetFillStyle(0);
  p->SetTicks();
  p->SetGridy();
  p->Draw();
  p->cd();
  //TH1F* ratioHist = (TH1F*) data->Clone();
  //ratioHist->Divide((TH1*) totalSum);
  // Swap ratio to be MC / data
  TH1F* ratioHist = (TH1F*) totalSum->Clone();
  ratioHist->Divide((TH1F*) data);

  TLegend* legRatio = new TLegend(0.74, 0.28, 0.92, 0.34);
  legRatio->SetFillColor(0);
  legRatio->SetBorderSize(0);
  legRatio->SetTextSize(0.020);

  // ratioHist->SetLineColor(857);
  // ratioHist->SetMarkerColor(857);
  // ratioHist->SetMarkerStyle(20);
  ratioHist->GetYaxis()->SetLabelSize(0.03);
  ratioHist->GetYaxis()->SetTitle("MC/Data");
  //ratioHist->GetYaxis()->SetTitle("Data/MC");
  ratioHist->GetXaxis()->SetLabelSize(0.03);
  ratioHist->GetXaxis()->SetTitleOffset(1.4);
  ratioHist->GetYaxis()->SetTitleOffset(1.4);

  float ratioMin = settings->GetValue("ratioMin", 0.3);
  float ratioMax = settings->GetValue("ratioMax", 1.7);

  ratioHist->GetYaxis()->SetRangeUser(ratioMin, ratioMax);
  ratioHist->GetXaxis()->SetMoreLogLabels();
  ratioHist->SetLineColor(857);
  // ratioHist->SetMarkerStyle();
  ratioHist->Draw("E");
  // ratioHist->Draw("same E2");


  // TH1F* ratioHistAltStrong = (TH1F*) data->Clone();
  // ratioHistAltStrong->Divide((TH1*) totalSumAltStrong);
  // Swap raito to be MC / data
  TH1F* ratioHistAltStrong = (TH1F*) totalSumAltStrong->Clone();
  ratioHistAltStrong->Divide((TH1F*) data);
  ratioHistAltStrong->Draw("same");

  legRatio->AddEntry(ratioHist, newSHLabel, "plf");
  legRatio->AddEntry(ratioHistAltStrong, newMGLabel , "plf");
  legRatio->Draw();
  can->Print(pdf);
  if (doPNG) can->Print(pngTitle);
}

void plotsFromHists(FileVMap sortedSampleFiles, Str plotSet, TEnv* settings){
  IntV colours = interize(settings->GetValue("colours", " "));
  Str plotPeriod = settings->GetValue(plotSet+".period", " ");
  char linLog = (char)(settings->GetValue(plotSet+".linLog", 1));
  //bool doLin = linLog & 1;
  //bool doLog = linLog & 2;
  bool doLin = false;
  bool doLog = false;
  if (linLog == 1) doLin = true;
  else if (linLog == 2) doLog = true;
  else fatal("not valid lin-log value");

  Str strongSample;
  if (plotSet.Contains("MG")) strongSample = "MG";
  else if (plotSet.Contains("Sherpa")) strongSample = "SP";
  else if (plotSet.Contains("Both")) strongSample = "Both";
  else fatal("plotSetList does not contain Sherpa,MG or Both");

  Str channel;
  int chanNo =-1;
  if(plotSet.Contains("all")){
    channel = "#it{Z}#rightarrow#it{ll}";
    chanNo = 2;
  }
  if(plotSet.Contains("mm")){
    channel = "#it{Z}#rightarrow#it{#mu#mu}";
    chanNo = 0;
  }
  if(plotSet.Contains("ee")){
    channel = "#it{Z}#rightarrow#it{ee}";
    chanNo = 1;
  }
  if (chanNo <0) fatal("Is neither ee, mumu or ll!");

  double Totlumi = settings->GetValue("luminosity."+plotPeriod, 0.0);
  Str region = settings->GetValue(plotSet+".region", " ");
  TEnv *xsecs = openSettingsFile(settings->GetValue("xsecFile", " "));

  StrV periods;
  if(plotPeriod.Contains("all"))
    periods = vectorize(settings->GetValue("periods", ""));
  else
    periods.push_back(plotPeriod);

  StrV plotList = vectorize(settings->GetValue(plotSet+".plotList", " "));

  TCanvas *can = new TCanvas("", "", 800, 800);
  Str pdf = plotSet+".pdf";
  can->Print(pdf + "[");

  HistVMap sortedHists;

  StrV inputLabels, inputCategories;
  for(auto period : periods){
    StrV inputCategories_period = vectorize(settings->GetValue("inputCategories."+period, " "));
    for(auto input : inputCategories_period){
      if(std::find(inputCategories.begin(), inputCategories.end(), input) == inputCategories.end())
        inputCategories.push_back(input);
    }
  }
  for(auto input : inputCategories){
    Str inputLabel = settings->GetValue(input+".Label", " ");
    if(std::find(inputLabels.begin(), inputLabels.end(), inputLabel) == inputLabels.end())
      inputLabels.push_back(inputLabel);
  }
  // if all . Add all Data, and we will remove seperate years later
  if(plotPeriod.Contains("all"))
    inputLabels.insert(inputLabels.begin(),"All Data");

  Str plotListStr;
  for(auto plot : plotList){
    sortedHists.clear();
    plotListStr += plot;
    plotListStr += " ";
    //printf("plot set %-25s : plotting variable %s\r", plotSet.Data(), plotListStr.Data());
    for(auto label : inputLabels)
      sortedHists[label] = new HistV;
    for(auto period : periods){
      inputCategories = vectorize(settings->GetValue("inputCategories."+period, " "));
      double lumi = settings->GetValue("luminosity."+period, 0.0);
      for(auto input : inputCategories){

        Str inputLabel = settings->GetValue(input+".Label", " ");
        //printf("%s %s\n", input.Data(), inputLabel.Data());

        for (auto sampleFile : sortedSampleFiles[period+"_"+input]) {
          double sf = getScaleFactor(xsecs, sampleFile, sampleFile->GetName(), lumi);
          TH1F* temp = new TH1F();
          Str var = settings->GetValue(plot + ".var", " ");
          Str binType = settings->GetValue(plot + ".binType", " ");
          Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
          Str Ylabel = settings->GetValue(plot + ".Ylabel", " ");

          // For rebinning purposes.
          int nBins;
          NumV bins;
          if (binType.Contains("regular")) {
            nBins = settings->GetValue(plot + ".nBins", -1);
            double min = settings->GetValue(plot + ".min", 0.0);
            double max = settings->GetValue(plot + ".max", 0.0);
            double step = (max-min)/nBins;
            for(int i=0 ; i<nBins+1; i++)
               bins.push_back(min+i*step);
          }
          else if (binType.Contains("variable")) {
            bins = numberize(settings->GetValue(plot + ".bins", " "));
            nBins = bins.size() - 1;
          }
          else{fatal("BinType is not regular or variable!");}

          Double_t BinsVec[nBins+1];
          for (int n=0; n<nBins+1;n++)
            BinsVec[n]=bins[n];

          // Do Rebinning and save new histo
          // Get histogram and scale it.
          temp = getHisto(sampleFile, region+var+"0");
          temp->Scale(sf);
          temp = (TH1F*)temp->Rebin(nBins, temp->GetName(), BinsVec);
          temp->SetTitle(";"+Xlabel+";"+Ylabel);
          // sortedHists[inputLabel]->push_back(temp);
          sortedHists[inputLabel]->push_back((TH1F*) temp->Clone());
        }
      }
    }

    HistMap summedHists;
    for (auto label : inputLabels) {
      if (!label.Contains("All Data")){ // "All Data" histograms dont exist yet
      TH1F *sum = (TH1F*) sortedHists[label]->at(0)->Clone();
         sum->Reset();
         for (auto h : *(sortedHists[label]))
           sum->Add(h);
         summedHists[label] = sum;
      }
    }

    if (plotPeriod.Contains("all")) { // Add all 3 years
      // TH1F *Sum = (TH1F*) sortedHists["2015-16 Data"]->at(0)->Clone();
      // Sum->Reset();
      TH1F *sum = (TH1F*) sortedHists["2015-16 Data"]->at(0)->Clone();
      TH1F *sum1 = (TH1F*) sortedHists["2017 Data"]->at(0)->Clone();
      TH1F *sum2 = (TH1F*) sortedHists["2018 Data"]->at(0)->Clone();
      // for (int n=0; n<sum->GetNbinsX()+1;n++){
      //   Sum->SetBinContent(n,sum->GetBinContent(n)+sum1->GetBinContent(n)+sum2->GetBinContent(n));
      // }
      sum->Add(sum1);
      sum->Add(sum2);
      summedHists["All Data"] = sum;
      // summedHists["All Data"] = Sum;
    }

    TLegend* leg = new TLegend(0.75, 0.80, 0.85, 0.93);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.020);
    int logX = settings->GetValue(plot+".logX", 0);

    if(doLin)
      drawStackMC(settings, plot, summedHists, inputLabels, strongSample, colours, can, leg, region, channel, Totlumi, pdf, logX, 0, chanNo);
    leg->Clear();
    if(doLog)
      drawStackMC(settings, plot, summedHists, inputLabels, strongSample, colours, can, leg, region, channel, Totlumi, pdf, logX, 1, chanNo);
  }

  //printf("\n");
  can->Print(pdf + "]");
}

void dataPlots(FileV dataFiles, Str plotSet, TEnv* settings, Str channel){
   IntV colours = interize(settings->GetValue("colours", " "));
   IntV markers = interize(settings->GetValue("markers", " "));
   Str channelString = channel.Contains("mu") ? "#it{Z}#rightarrow#it{#mu#mu}" : "#it{Z}#rightarrow#it{ee}";
   Str chan = channel.Contains("mu") ? "mm" : "ee";

   StrV plotList = vectorize(settings->GetValue("dataPlots."+plotSet+".plotList", " "));
   StrV dataYears = vectorize(settings->GetValue("dataYears", " "));

   TCanvas *can = new TCanvas("", "", 800, 800);
   // Str pdf = plotSet+"_"+channel+".pdf";
   // std::cout << pdf << std::endl;
   // can->Print(pdf + "[");

   // can->SetLogy(1);
   // double ratio = 0.36, margin = 0.02;
   // can->cd();
   // gPad->SetBottomMargin(ratio);
   // gPad->SetTicks();
   //
   // HistV dataHists;
   // TH1F* temp;
   // for(auto plot : plotList){
   //    dataHists.clear();
   //    can->cd();
   //    printf("plot set %-25s : plotting variable %s\n", plotSet.Data(), plot.Data());
   //    int iFile = 0;
   //    for(auto dataYear : dataYears){
   //       TFile* dataFile = dataFiles[iFile++];
   //       Str var = "h" + chan + settings->GetValue(plot + ".var", " ") + "0";
   //       Str binType = settings->GetValue(plot + ".binType", " ");
   //       Str Xlabel = settings->GetValue(plot + ".Xlabel", " ");
   //       Str Ylabel = settings->GetValue(plot + ".Ylabel", " ");
   //       Double_t *bins;
   //       int nBins;
   //       NumV binsVecVar;
   //       if (binType.Contains("regular")){
   //          nBins = settings->GetValue(plot + ".nBins", -1);
   //          float min = settings->GetValue(plot + ".min", 0.0);
   //          float max = settings->GetValue(plot + ".max", 0.0);
   //          NumV* binsVec = makeLinearBins(nBins, min, max);
   //          bins = &(*binsVec)[0];
   //       }
   //       else if (binType.Contains("variable")){
   //          binsVecVar = numberize(settings->GetValue(plot + ".bins", " "));
   //          bins = &binsVecVar[0];
   //          nBins = binsVecVar.size() - 1;
   //       }
   //       temp = getHisto(dataFile, var);
   //       TH1F* rebin = (TH1F*)temp->Rebin(nBins, dataFile->GetName(), bins);
   //       rebin->Scale(1.0/temp->Integral());
   //       rebin->SetTitle(";"+Xlabel+";");
   //       dataHists.push_back(rebin);
   //    }
   //    TLegend* leg = new TLegend(0.75, 0.83, 0.9, 0.93);
   //    leg->SetFillColor(0);
   //    leg->SetBorderSize(0);
   //    leg->SetTextSize(0.025);
   //    int logX = settings->GetValue(plot+".logX", 0);
   //    if (logX)
   //    can->SetLogx(1);
   //    else
   //    can->SetLogx(0);
   //    can->Modified();
   //    can->Update();
   //    int first = 1;
   //    int col = 0;
   //    TH1F* dataHist;
   //    for(auto dataYear : dataYears){
   //       dataHist = dataHists[col];
   //       dataHist->SetLineColor(colours[col]);
   //       dataHist->SetMarkerColor(colours[col]);
   //       dataHist->SetMarkerStyle(markers[col++]);
   //       dataHist->SetMarkerSize(1);
   //       Str dataLabel = settings->GetValue(dataYear+".Label", " ");
   //       leg->AddEntry(dataHist, dataLabel, "plf");
   //       dataHist->GetXaxis()->SetLabelSize(0);
   //       if(first){
   //          first = 0;
   //          double logDiff = log10(dataHist->GetMaximum()) - log10(dataHist->GetMinimum());
   //          if(dataHist->GetMinimum() > 0)
   //             dataHist->SetMaximum(dataHist->GetMaximum()*(1+2*logDiff));
   //          else
   //             dataHist->SetMaximum(dataHist->GetMaximum()*100);
   //          dataHist->Draw("HIST E");
   //       }
   //       else
   //          dataHist->Draw("HIST E SAME");
   //    }
   //    leg->Draw();
   //    // ATLASLabel(0.15, 0.9, "Work in progress");
   //    drawText(0.15, 0.85, channelString);
   //    TPad *p = new TPad("p_test", "", 0, 0, 1, 1.0 - margin, 0, 0, 0.0);
   //    if (logX)
   //       p->SetLogx(1);
   //    else
   //       p->SetLogx(0);
   //    p->Modified();
   //    p->Update();
   //    p->SetTopMargin(1.0 - ratio);
   //    p->SetFillStyle(0);
   //    p->SetTicks();
   //    p->SetGridy();
   //    p->Draw();
   //    p->cd();
   //    first = 1;
   //    TH1F* ratioHist;
   //    for(int i = 1; i<dataHists.size(); i++){
   //       TH1F* ratioHist = (TH1F*) dataHists[i]->Clone();
   //       ratioHist->Divide(dataHists[0]);
   //
   //       ratioHist->SetLineColor(colours[i]);
   //       ratioHist->SetMarkerStyle(markers[i]);
   //       ratioHist->SetMarkerColor(colours[i]);
   //       ratioHist->GetYaxis()->SetLabelSize(0.03);
   //       Str dataLabelDenom = settings->GetValue(dataYears[0]+".Label", " ");
   //       ratioHist->GetYaxis()->SetTitle(Form("DataXX/%s", dataLabelDenom.Data()));
   //       ratioHist->GetXaxis()->SetLabelSize(0.03);
   //       ratioHist->GetXaxis()->SetTitleOffset(1.4);
   //
   //       float ratioMin = settings->GetValue("ratioMin", 0.6);
   //       float ratioMax = settings->GetValue("ratioMax", 1.4);
   //
   //       ratioHist->GetYaxis()->SetRangeUser(ratioMin, ratioMax);
   //       ratioHist->GetXaxis()->SetMoreLogLabels();
   //       if(first){
   //          first = 0;
   //          ratioHist->Draw("HIST E");
   //       }
   //       else
   //          ratioHist->Draw("HIST E SAME");
   //    }
   //
   //    can->Print(pdf);
   // }
   // can->Print(pdf + "]");
   // delete can;
}

void cutFlow(FileVMap sortedSampleFiles, TEnv* settings){
  StrV cutFlowLabels = vectorize(settings->GetValue("cutFlowLabels", " "), ";");
  StrV cutFlowLabelsWithCuts = vectorize(settings->GetValue("cutFlowLabelsWithCuts", " "), ";");
  IntV cutFlowBins = interize(settings->GetValue("cutFlowBins", " "));

  TEnv *xsecs = openSettingsFile(settings->GetValue("xsecFile", " "));

  StrV periods = vectorize(settings->GetValue("periods", " "));

  for(auto period : periods){
    double lumi = settings->GetValue("luminosity."+period, 0.0);

    Str cutFlowOutFile = "cutFlow_"+period+".root";

    StrV inputCategories = vectorize(settings->GetValue("inputCategories."+period, " "));

    NumVMap cutFlowPerSample;

    for(auto input : inputCategories){
      bool first = true;
      for (auto sampleFile : sortedSampleFiles[period+"_"+input]) {
        TH1F *cutFlowWeighted = getHisto(sampleFile, "CutFlow_weighted");
        if (first) {
          first = false;
          for (int i = 0; i <cutFlowBins.size();  i++) {
            cutFlowPerSample[input].push_back(cutFlowWeighted->GetBinContent(cutFlowBins[i]));
          }
        } else {
          for (int i = 0; i <cutFlowBins.size(); i++)
            cutFlowPerSample[input][i] += cutFlowWeighted->GetBinContent(cutFlowBins[i]);
        }
        //if (first) {
        //  first = 0;
        //  for (int i = 1; i <= cutFlowWeighted->GetNbinsX(); i++) {
        //    cutFlowPerSample[input].push_back(cutFlowWeighted->GetBinContent(i));
        //  }
        //} else {
        //  for (int i = 1; i <= cutFlowWeighted->GetNbinsX(); i++)
        //    cutFlowPerSample[input][i - 1] += cutFlowWeighted->GetBinContent(i);
        //}
      }
    }

    //Print cutFlow
    printf("%30s ", "");
    for(auto input : inputCategories)
      //std::cout << "& " << input.Data() << " ";
      //printf("& %15s ", input.Data());
      printf("& %11s ", input.Data());
    //std::cout << "\\\\" << std::endl;
    printf("\\\\\n");
    int iLabel = 0;
    //for (auto label : cutFlowLabels) {
    //  //printf("%30s ", label.ReplaceAll(" ", "").Data());
    //  printf("%30s ", label.Data());
    //  for(auto input : inputCategories)
    //    if (iLabel < cutFlowPerSample[input].size())
    //      std::cout << "& " << cutFlowPerSample[input][iLabel] << " \\\\" << std::endl;
    //      //printf("& %15.2e ", cutFlowPerSample[input][iLabel]);
    //    else
    //      std::cout << "& 0.0 \\\\" << std::endl;
    //      //printf("& %15.1f ", 0.0);
    //  //printf("\\\\\n");
    //  iLabel++;
    //}
    for (auto label : cutFlowLabels) {
      printf("%30s ", label.Data());
      //printf("%30s ", label.ReplaceAll(" ", "").Data());
      for(auto input : inputCategories)
        if (iLabel < cutFlowPerSample[input].size())
          //std::cout << "& " << cutFlowPerSample[input][iLabel] << std::endl;
          printf("& %11.2e ", cutFlowPerSample[input][iLabel]);
          //printf("& %15.2e ", cutFlowPerSample[input][iLabel]);
        else
          printf("& %11.1f ", 0.0);
          //printf("& %15.1f ", 0.0);
      printf("\\\\\n");
      iLabel++;
    }

    //Write cutFlow to file
    TFile *cutFlowFile = new TFile(cutFlowOutFile.Data(), "RECREATE");
    for(auto input : inputCategories){
      int iCut = 1;
      Str cut = "";
      //printf("cutflow: %-70s\r", input.Data());
      TH1F* cutFlowHist = new TH1F(Form("%s_cutFlow", input.Data()), Form("%s_cutFlow", input.Data()), cutFlowLabelsWithCuts.size(), 0, cutFlowLabelsWithCuts.size());
      for (auto cutName : cutFlowLabelsWithCuts) {
        cutFlowHist->GetXaxis()->SetBinLabel(iCut, cutName.Data());
        int bin = cutFlowBins[iCut-1];
        float cutFlowSum = 0.0;
        for (auto sampleFile : sortedSampleFiles[period+"_"+input]) {
          TH1F *cutFlow = getHisto(sampleFile, "CutFlow_weighted");
          double sf = getScaleFactorCF(xsecs, sampleFile, sampleFile->GetName(), lumi);
          float cutSumW;
          if(bin > 0){
            cutSumW = cutFlow->GetBinContent(bin);
            cutFlowSum += cutSumW * sf;
          }
          else{
            cutName.Remove(TString::kLeading, ' ');
            TH1F* temp = getHisto(sampleFile, "Mll_"+cutName);
            cutSumW = temp->GetSumOfWeights();
            cutFlowSum += cutSumW * sf;
          }
        }
        cutFlowHist->SetBinContent(iCut, cutFlowSum);
        //printf("%.3f\n", cutFlowSum);
        iCut++;
      }
      cutFlowFile->cd();
      cutFlowHist->Write();
    }
    printf("\n");
  }
}
