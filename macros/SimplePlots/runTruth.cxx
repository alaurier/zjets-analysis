#define ZJetsplots_cxx
#include "ZJetsplots.h"
#include <TH2.h>
#include "TFile.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TEnv.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TTree.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TError.h"
#include "atlasStyle/AtlasStyle.C"
#include "atlasStyle/AtlasUtils.C"
#include "atlasStyle/AtlasLabels.C"

typedef TString Str;
typedef std::vector<TString> StrV;
typedef std::map<TString, StrV> StrVMap;
typedef std::vector<TFile*> FileV;
typedef std::map<TString, FileV> FileVMap;
typedef std::vector<double> NumV;
typedef std::map<TString, NumV> NumVMap;
typedef std::vector<int> IntV;
typedef std::vector<TH1F*> HistV;
typedef std::map<TString, HistV*> HistVMap;
typedef std::map<TString, TH1F*> HistMap;
typedef std::vector<TH2F*> HistV2;
typedef std::map<TString, HistV2*> HistVMap2;
typedef std::map<TString, TH2F*> HistMap2;
typedef vector<TF1*> FuncV;

void fatal(Str msg) {
  std::cout << "FATAL: " << msg.Data() << std::endl;
  abort();
}
void setStyle();
TEnv *openSettingsFile(Str fileName);
TFile *openFile(Str fn);
TH1F *getHisto(TFile *f, Str hn);
TH1F *getHisto(Str fn, Str hn) {
  return getHisto(openFile(fn), hn);
}
StrV vectorize(Str str, Str sep = " ");
NumV numberize(Str str, Str sep = " ");
IntV interize(Str str, Str sep = " ");
void drawText(double x, double y, Str txt, bool alignRight = 0, int col = kBlack, double tsize = 0.04);
void drawLine(double x1, double y1, double x2, double y2, int col = kBlack, int ls = 1, int lw = 1);

double getScaleFactorCF(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi);
double getScaleFactor(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi);

void doPlots(FileVMap sortedSampleFiles, TEnv* settings, StrVMap filenameMap);

// Define the histograms
TH1F *NJets = new TH1F("Exclusive NJets","Exclusive NJets", 8, 0.5, 8.5);
TH1F *NJets500 = new TH1F("Exclusive NJets, leading jet pT 500","Exclusive NJets, leading jet pT 500", 8, 0.5, 8.5);
TH1F *lepton_DR = new TH1F("Lepton DR, leading jet pT 500GeV","Lepton DR, leading jet pT 500GeV", 40, 0., 4.);
TH1F *jet_lep_DR = new TH1F("DR(jet,lep), leading jet pT 500GeV","DR(jet,lep), leading jet pT 500GeV", 40, 0., 4.);
TH1F *jet_Z_DR = new TH1F("DR(jet,Z), leading jet pT 500GeV","DR(jet,Z), leading jet pT 500GeV", 40, 0., 4.);
TH1F *jetPt = new TH1F("leading jet pT","leading jet pT", 70, 100., 1500.);

int runTruth(Str config = "ZJetsplots.data"){
   setStyle();

   std::cout << "config file: " << config.Data() << std::endl;
   TEnv *settings = openSettingsFile(config);
   StrV periods = vectorize(settings->GetValue("periods", " "));

   FileVMap sortedSampleFiles;
   StrVMap sortedFileNames;
   for (auto period : periods) {
      Str filePath = settings->GetValue("filePath."+period, " ");
      StrV inputCategories = vectorize(settings->GetValue("inputCategories."+period, " "));
      for (auto input : inputCategories) {
         StrV sampleList = vectorize(settings->GetValue(input.Data(), " "));
         for (auto sample : sampleList) {
            sortedSampleFiles[period+"_"+input].push_back(openFile(filePath + sample + ".root"));
            sortedFileNames[period+"_"+input].push_back(filePath +sample + ".root");
         }
      }
   }

   doPlots(sortedSampleFiles, settings, sortedFileNames);
   TCanvas *can = new TCanvas();
   // TCanvas *can = new TCanvas("", "", 800, 800);
   Str pdf = "ORPlots.pdf";
   can->Print(pdf + "["); // Open File
   NJets->GetXaxis()->SetTitle("NJets exclusive");
   NJets->Draw();
   TPaveLabel *t = new TPaveLabel(0.6, 0.9, 1.0, 1.0, "Exclusive NJets", "brNDC"); // top right
   t->Draw();
   can->Print(pdf);
   NJets500->GetXaxis()->SetTitle("NJets exclusive");
   NJets500->Draw();
   TPaveLabel *tt = new TPaveLabel(0.6, 0.9, 1.0, 1.0, "Exclusive NJets,j0 pT > 500GeV", "brNDC"); // top right
   tt->Draw();
   can->Print(pdf);
   lepton_DR->GetXaxis()->SetTitle("DR(ll)");
   lepton_DR->Draw();
   TPaveLabel *t1 = new TPaveLabel(0.6, 0.9, 1.0, 1.0, "DR(ll), j0 pT > 500GeV", "brNDC"); // top right
   t1->Draw();
   can->Print(pdf);
   jet_lep_DR->GetXaxis()->SetTitle("minDR(lep,jet)");
   jet_lep_DR->Draw();
   TPaveLabel *t2 = new TPaveLabel(0.6, 0.9, 1.0, 1.0, "minDR(lep,jet), j0 pT > 500GeV", "brNDC"); // top right
   t2->Draw();
   can->Print(pdf);
   jet_Z_DR->GetXaxis()->SetTitle("minDR(Z,jet)");
   jet_Z_DR->Draw();
   TPaveLabel *t3 = new TPaveLabel(0.6, 0.9, 1.0, 1.0, "minDR(Z,jet), j0 pT > 500GeV", "brNDC"); // top right
   t3->Draw();
   can->Print(pdf);
   jetPt->GetXaxis()->SetTitle("j0 pT (GeV)");
   jetPt->Draw();
   TPaveLabel *t4 = new TPaveLabel(0.6, 0.9, 1.0, 1.0, "leading jet pT", "brNDC"); // top right
   t4->Draw();
   can->Print(pdf);
   can->Print(pdf+"]"); // Close File

   return 0;
}

void ZJetsplots::Loop()
{
   if (fChain == 0) return;

   bool isMG = false;
   bool isSherpa = false;
   bool isData = false;
   if (Filename.Contains("data")) isData = true;
   if (Filename.Contains("MG")) isMG = true;
   if (Filename.Contains("Sherpa")) isSherpa = true;
   std::cout << "Is data sample? " << isData << std::endl;
   std::cout << "Is MG sample? " << isMG << std::endl;
   std::cout << "Is Sherpa sample? " << isSherpa << std::endl;


   // Pre Evnt Loop definitions
   //
   Double_t unskimmedEvts=0;
   // Event Loop
   Long64_t nentries = fChain->GetEntriesFast();
   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) { // Start of Event loop
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      if (jentry%20000 == 0) std::cout << "Event # " << jentry/1000 << " thousand" << std::endl;

      if(truth_lep_E->size()!=2)
         continue;

      // Phase space regions (search)
      // rch = 0 : Electron channel
      // rch = 1 : Muon channel
      // rch = 2 : e-mu channel
      int tch = 3;
      int id0 = truth_lep_id->at(0);
      int id1 = truth_lep_id->at(1);
      if (id0 * id1 >= 0) continue; // same charge leptons
      if( std::abs(id0)==11 && std::abs(id1)==11 ) {
         tch = 0; // electrons
         // fake muons: v2: drop isolation requirement
      }else if (std::abs(id0)==13 && std::abs(id1)==13 ) {
         tch = 1; // muons
      }else if ((std::abs(id0)==13 && std::abs(id1)==11) || (std::abs(id0)==11 && std::abs(id1)==13)) {
         tch = 2; // e-mu
      }
      if (tch > 1) continue;

      if (isSherpa && std::abs(initialWeight)>=100) initialWeight=1;
      double wl0 = lep1TotalSF; // Total SF
      double wl1 = lep2TotalSF; // Total SF
      double wgrec = wl0 * wl1 * initialWeight; // Reco weight
      double wgtrue = initialWeight; // Truth weight : MC x Pileup Weight

      // jet, lepton  and Z 4-vectors
      TLorentzVector mu1, mu2, zet, jet;
      TLorentzVector mu1t, mu2t, zett, jett;


      // Define leptons and Z
      mu1t.SetPxPyPzE(truth_lep_px->at(0),truth_lep_py->at(0),truth_lep_pz->at(0),truth_lep_E->at(0));
      mu2t.SetPxPyPzE(truth_lep_px->at(1),truth_lep_py->at(1),truth_lep_pz->at(1),truth_lep_E->at(1));
      zett = mu1t+mu2t;
      float zPt=zett.Pt();
      float dr_ll = mu1t.DeltaR(mu2t);

      bool truth_isGoodZ = false;
      bool truth_isSelected = false;

      truth_isGoodZ    = mu1t.Pt()>25. &&  mu2t.Pt()>25 && fabs(mu1t.Eta())<2.4 &&  fabs(mu2t.Eta())<2.4;
      truth_isSelected = truth_isGoodZ && 71. < zett.M() &&  zett.M() < 111.;

      int truth_nPreJets = 0;
      truth_nPreJets = truth_jet_pT->size();

      truth_vjet_E.clear();
      truth_vjet_px.clear();
      truth_vjet_py.clear();
      truth_vjet_pz.clear();

      bool leadingJet = false;
      float maxPt=0.;
      float jetpT;
      for (int i = 0;i<truth_nPreJets;i++){
         jett.SetPxPyPzE(truth_jet_px->at(i),truth_jet_py->at(i),truth_jet_pz->at(i),truth_jet_E->at(i));
         if(jett.Pt()>=100. && fabs(jett.Rapidity())<2.5){
            truth_vjet_px.push_back(truth_jet_px->at(i));
            truth_vjet_py.push_back(truth_jet_py->at(i));
            truth_vjet_pz.push_back(truth_jet_pz->at(i));
            truth_vjet_E.push_back(truth_jet_E->at(i));
            jetpT = jett.Pt();
            if (jetpT>=500) leadingJet=true;
            if (jetpT>maxPt) maxPt=jetpT;
         }
      }

      NJets->Fill(truth_vjet_E.size());
      jetPt->Fill(maxPt);

      float minDR_Z=99.;
      float minDR_lep=99.;
      float drZ, drL;
      if (leadingJet) {
         for (int i = 0;i<truth_vjet_E.size();i++){
            jett.SetPxPyPzE(truth_vjet_px.at(i),truth_vjet_py.at(i),truth_vjet_pz.at(i),truth_vjet_E.at(i));
            // find minDR Z, jet
            drZ=jett.DeltaR(zett);
            if (drZ < minDR_Z) minDR_Z = drZ;
            // find minDR lep, jet
            drL=jett.DeltaR(mu1t);
            if (drL < minDR_lep) minDR_lep = drL;
            drL=jett.DeltaR(mu2t);
            if (drL < minDR_lep) minDR_lep = drL;
         }
         NJets500->Fill(truth_vjet_E.size());
         lepton_DR->Fill(dr_ll);
         jet_lep_DR->Fill(drL);
         jet_Z_DR->Fill(drZ);
      }

      if (pileupWeight !=0)
        unskimmedEvts += (initialWeight / pileupWeight);
   } // End of Event loop

} // Event of ZJetsplots::Loop

TH1F *getHisto(TFile *f, Str hn) {
  TH1F *h = (TH1F*) f->Get(hn);
  if (h == NULL)
    fatal("Cannot access " + hn + " in " + f->GetName());
  return h;
}

TEnv *openSettingsFile(Str fileName) {
  if (fileName == "")
    fatal("No config file name specified. Cannot open file!");
  TEnv *settings = new TEnv();
  int status = settings->ReadFile(fileName.Data(), EEnvLevel(0));
  if (status != 0)
    fatal(Form("Cannot read file %s", fileName.Data()));
  return settings;
}

StrV vectorize(Str str, Str sep) {
  StrV result;
  TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries() == 0)
    return result;
  TIter istr(strings);
  while (TObjString* os = (TObjString*) istr())
    if (os->GetString()[0] != '#')
      result.push_back(os->GetString());
    else
      break;
  return result;
}

NumV numberize(Str str, Str sep) {
  NumV result;
  TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries() == 0)
    return result;
  TIter istr(strings);
  while (TObjString* os = (TObjString*) istr())
    if (os->GetString()[0] != '#')
      result.push_back((os->GetString()).Atof());
    else
      break;
  return result;
}

IntV interize(Str str, Str sep) {
  IntV result;
  TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries() == 0)
    return result;
  TIter istr(strings);
  while (TObjString* os = (TObjString*) istr())
    if (os->GetString()[0] != '#')
      result.push_back((os->GetString()).Atoi());
    else
      break;
  return result;
}

TFile *openFile(Str fn) {
  TFile *f = TFile::Open(fn);
  if (f == NULL || f->IsZombie())
    fatal("Cannot open " + fn);
  return f;
}

void setStyle() {
  double tsize = 0.035;
  gStyle->SetTextSize(tsize);
  gStyle->SetLabelSize(tsize, "x");
  gStyle->SetTitleSize(tsize, "x");
  gStyle->SetLabelSize(tsize, "y");
  gStyle->SetTitleSize(tsize, "y");

  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.04);
  gStyle->SetPadBottomMargin(0.12);
  gStyle->SetPadTopMargin(0.05);
  TH1::SetDefaultSumw2();
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  gStyle->SetHistMinimumZero();
  gStyle->SetTitleOffset(1.4, "y");
  gStyle->SetTitleOffset(1.4, "x");
}

void drawText(double x, double y, Str txt, bool alignRight, int col, double tsize) {
  static TLatex *tex = new TLatex();
  if(alignRight)
    tex->SetTextAlign(31);
  tex->SetTextColor(col);
  tex->SetNDC();
  tex->SetTextSize(tsize);
  tex->SetTextFont(42);
  tex->DrawLatex(x, y, txt);
}

void drawLine(double x1, double y1, double x2, double y2, int col, int ls, int lw) {
  static TLine *line = new TLine();
  line->SetLineColor(col);
  line->SetLineStyle(ls);
  line->SetLineWidth(lw);
  line->DrawLine(x1, y1, x2, y2);
}

NumV* makeLinearBins(int nBins, float min, float max){
  NumV* bins = new NumV;
  float step = (max-min)/nBins;
  for(int i=0 ; i<=nBins; i++)
    bins->push_back(min+i*step);
  return bins;
}

double getScaleFactorCF(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi){
  TObjArray *tx = sample.Tokenize("/");
  if((((TObjString *)(tx->Last()))->String()).Contains("data")){
    delete tx;
    return 1.0;
  }
  sample = (((TObjString *)(tx->Last()))->String()).ReplaceAll(".root", "");
  delete tx;

  double sumW = getHisto(sampleFile, "sum_of_weights")->GetBinContent(4);
  double sigma = xsecs->GetValue(sample + ".Xsec", 0.0);
  double k = xsecs->GetValue(sample + ".Kfactor", 0.0);
  double eff = xsecs->GetValue(sample + ".Filter", 0.0);

  return sigma * k * eff * lumi / sumW;
}

double getScaleFactor(TEnv *xsecs, TFile* sampleFile, Str sample, double lumi){
  TObjArray *tx = sample.Tokenize("/");
  if((((TObjString *)(tx->Last()))->String()).Contains("data")){
    delete tx;
    return 1.0;
  }
  sample = (((TObjString *)(tx->Last()))->String()).ReplaceAll("_hist.root", "");
  delete tx;

  double sumW = getHisto(sampleFile, "hsumwgh")->GetBinContent(1);
  double sigma = xsecs->GetValue(sample + ".Xsec", 0.0);
  double k = xsecs->GetValue(sample + ".Kfactor", 0.0);
  double eff = xsecs->GetValue(sample + ".Filter", 0.0);

  return sigma * k * eff * lumi / sumW;
}

void doPlots(FileVMap sortedSampleFiles, TEnv* settings, StrVMap filenameMap){
   // Definitions for all the different files
   std::string Treename = "Ztree";
   IntV Systematics = interize(settings->GetValue("Systematics", " "));
   int nSys = Systematics[0];
   nSys=1; // do nominal only

   for (auto obj : sortedSampleFiles){
      int counter = 0;
      for (auto fname : obj.second){
         // Open sum of weights histo
         TString filename = (filenameMap.find(obj.first)->second)[counter];
         Str outname = filename;
         outname.ReplaceAll(".root","_hist.root");
         TH1F* hsumw = (TH1F*)fname->Get("sum_of_weights");
         std::vector<int> cutflowvalues;
         Double_t nevt = 1.;
         nevt = hsumw->GetBinContent(4); // Sum Of Weights
         std::cout << "Sum of Weights Skimmed: " << nevt << std::endl;
         for (int i=0; i<nSys; i++){
            TString SysTree = Treename+to_string(i);
            std::cout << "Debug:: TreeName = " << SysTree << std::endl;
            TChain *ZChain  = new TChain(SysTree);
            ZChain->Add(filename);
            if (i==0) {
               Long64_t nEntries = Long64_t(ZChain->GetEntries());
               std::cout << "Number of Events: " << nEntries << std::endl;
            }
            std::cout << "Doing Systematic variation # " << i << std::endl;
            ZJetsplots* Ana = new ZJetsplots((TChain *) ZChain, nevt, i, filename,outname,cutflowvalues);
            Ana->Loop();
            delete Ana;
            delete ZChain;
         }
         std::cout << "-----" << std::endl;
         counter++;
      }
      std::cout << "=====" << std::endl;
   }
   // end of do Histos
}
