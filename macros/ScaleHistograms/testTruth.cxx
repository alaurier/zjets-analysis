#include "../util.h"

void scaleHistograms(FileVMap sortedSampleFiles, TEnv* settings);

int testTruth(){
  setStyle();
  Str config = "ZJetsplots.data";
  std::cout << "ALEX!" << std::endl;

  std::cout << "config file: " << config.Data() << std::endl;
  TEnv *settings = openSettingsFile(config);
  StrV periods = vectorize(settings->GetValue("periods", " "));

  FileVMap sortedSampleFiles;
  StrVMap sortedFileNames;
  for (auto period : periods) {
    Str filePath = settings->GetValue("filePath."+period, " ");
    StrV inputCategories = vectorize(settings->GetValue("inputCategories."+period, " "));
    for (auto input : inputCategories) {
      StrV sampleList = vectorize(settings->GetValue(input.Data(), " "));
      for (auto sample : sampleList) {
        //printf("opening file %-30s : %-70s\r", (period+"_"+input+"_hist").Data(), sample.Data());
        sortedSampleFiles[period+"_"+input].push_back(openFile(filePath + sample + ".root"));
      }
    }
  }

  scaleHistograms(sortedSampleFiles, settings);

  return 0;
}

void scaleHistograms(FileVMap sortedSampleFiles, TEnv* settings){
  // TFile hisfile(mout,"update");
  TH1::AddDirectory(kFALSE);
  StrV periods = vectorize(settings->GetValue("periods", " "));
  TEnv *xsecs = openSettingsFile(settings->GetValue("xsecFile", " "));
  HistVMap sortedHists;

  StrV inputLabels, inputCategories;
  for(auto period : periods){
    StrV inputCategories_period = vectorize(settings->GetValue("inputCategories."+period, " "));
    for(auto input : inputCategories_period){
      if(std::find(inputCategories.begin(), inputCategories.end(), input) == inputCategories.end())
        inputCategories.push_back(input);
    }
  }
  for(auto input : inputCategories){
    Str inputLabel = settings->GetValue(input+".Label", " ");
    if(std::find(inputLabels.begin(), inputLabels.end(), inputLabel) == inputLabels.end())
      inputLabels.push_back(inputLabel);
  }

  //printf("plot set %-25s : plotting variable %s\r", plotSet.Data(), plotListStr.Data());
  for(auto label : inputLabels)
    sortedHists[label] = new HistV;
  for(auto period : periods){
    inputCategories = vectorize(settings->GetValue("inputCategories."+period, " "));
    double lumi = settings->GetValue("luminosity."+period, 0.0);
    for(auto input : inputCategories){

      Str inputLabel = settings->GetValue(input+".Label", " ");
      //printf("%s %s\n", input.Data(), inputLabel.Data());

      for (auto sampleFile : sortedSampleFiles[period+"_"+input]) {
        // std::cout << "Starting scaling of " << sampleFile->GetName() << std::endl;
        TH1F* temp = new TH1F();
        double Zee = 1., Zmm = 1., DRee = 1., DRmm = 1.;
        temp = getHisto(sampleFile, "heetrueZpt1j0");
        Zee = temp->Integral();
        temp = getHisto(sampleFile, "heetrueDRZj0");
        DRee = temp->Integral();
        temp = getHisto(sampleFile, "hmmtrueZpt1j0");
        Zmm = temp->Integral();
        temp = getHisto(sampleFile, "hmmtrueDRZj0");
        DRmm = temp->Integral();
        std::cout << Zee << "     " << Zmm << "     " << DRee << "     " << DRmm << std::endl;
        // std::cout << "Finished Scaling file" << std::endl;
      }
    }
  }
}
