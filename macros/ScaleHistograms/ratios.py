f = open ("mc16_xsecs.conf", "r")
arrayMuon = []
arrayElec = []
for line in f:
    if "xs" in line:
        if ("Sherpa_221_NNPDF30NNLO_Zmumu") in line:
            arrayMuon.append(float(line.split()[1]))

        if ("Sherpa_221_NNPDF30NNLO_Zee") in line:
            arrayElec.append(float(line.split()[1]))
    # print line
sumElec = 0
sumMuon = 0
for x in range(0, len(arrayMuon)):
#for x in range(4, 9):
#for x in range(0, 4):
#for x in range(9, len(arrayMuon)):
    # print(x)
    sumElec = sumElec + arrayElec[x]
    sumMuon = sumMuon + arrayMuon[x]
    # print arrayElec[x]/arrayMuon[x]
print("AMI Cross sections ratios")
for x in range(0, len(arrayMuon)):
    # print arrayElec[x]/arrayMuon[x]
    #print arrayElec[x]/sumElec
    print("%.3f" % round(arrayElec[x]/arrayMuon[x], 3))
print("Sigma slice / sigma total")
for x in range(0, len(arrayMuon)):
    # print arrayElec[x]/arrayMuon[x]
    #print arrayElec[x]/sumElec
    # print("%.3f" % round(arrayElec[x]/arrayMuon[x], 3))
    print("%.3f" % round(arrayElec[x]/sumElec, 3))

print "final ratio = "
print sumElec/sumMuon
