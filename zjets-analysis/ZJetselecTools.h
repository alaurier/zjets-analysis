#ifndef ZJetselecTools_H
#define ZJetselecTools_H

//Electron tools
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "IsolationCorrections/IsolationCorrectionTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"

//Trigger tools
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"

//TTVA
#include "xAODTracking/TrackParticlexAODHelpers.h"

//xAOD
#include "xAODEgamma/ElectronContainer.h"

#include "zjets-analysis/Config.h"
#include "zjets-analysis/ZJetscommon.h"

using namespace ZJets;

class ZJetselecTools : public asg::AsgMessaging{
public:
  ZJetselecTools(const char *name, bool isMC);
  StatusCode Initialize(Config &config);
  StatusCode InitializeElectronTools(Config &config);
  StatusCode ElectronSelection(xAOD::TEvent *event, const xAOD::Vertex* primaryVtx, std::vector<xAOD::Electron*> *calibElecs, std::vector<xAOD::Electron*> *selected,int& count1, int& count2, int& count3);
  StatusCode GetAllCalibratedElectrons(xAOD::TEvent *event, xAOD::TStore* store, std::vector<xAOD::Electron*> *allElecs);

  StatusCode ApplySystematicVariation(const CP::SystematicSet &syst);


  bool passTTVA(const xAOD::Electron* e, const xAOD::Vertex *vtxHS, const xAOD::EventInfo *eventInfo);

  double getRecoSF(const xAOD::Electron* e1, const xAOD::Electron* e2, const CP::SystematicSet &syst = CP::SystematicSet());
  double getIsoSF(const xAOD::Electron* e1, const xAOD::Electron* e2, const CP::SystematicSet &syst = CP::SystematicSet());
  double getIDSF(const xAOD::Electron* e1, const xAOD::Electron* e2, const CP::SystematicSet &syst = CP::SystematicSet());
  double getTrigSF(const xAOD::Electron* e1, const xAOD::Electron* e2, const CP::SystematicSet &syst = CP::SystematicSet());

  // product of reco * iso * ID * trig. Trig only included if m_doTriggerSF is true
  double getDielectronSF(const xAOD::Electron* e1, const xAOD::Electron* e2, const CP::SystematicSet &syst = CP::SystematicSet());

  double m_pTCut, m_MaxAbsEta, m_BarrelMaxAbsEta, m_EndcapMinAbsEta, m_d0BySigd0Max, m_z0Max;
  Str m_electronLikelihoodWP;
  bool m_doElecTriggerSF;

  CP::EgammaCalibrationAndSmearingTool* m_elecCalibTool; //!
  AsgElectronEfficiencyCorrectionTool* m_elecRecoSFTool; //!
  AsgElectronEfficiencyCorrectionTool* m_elecIdSFTool; //!
  AsgElectronEfficiencyCorrectionTool* m_elecIsoSFTool; //!
  AsgElectronEfficiencyCorrectionTool* m_elecTrigSFTool; //!
  AsgElectronEfficiencyCorrectionTool* m_elecTrigMCEffTool; //!
  CP::IsolationSelectionTool* m_isoSelectionTool; //!
  CP::IsolationCorrectionTool* m_isoCorrectionTool; //!
  
  StatusCode FinalizeTools();

private:
  Str m_name;
  bool m_isMC;

};

#endif
