#ifndef ZJetsjetTools_H
#define ZJetsjetTools_H

//Jet tools
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetResolution/JERTool.h"
#include "JetResolution/JERSmearingTool.h"
#include "JetUncertainties/JetUncertaintiesTool.h"
#include "JetInterface/IJetUpdateJvt.h"
#include "JetJvtEfficiency/JetJvtEfficiency.h"

//Asg
#include "AsgTools/AnaToolHandle.h"

//xAOD
#include "xAODJet/JetContainer.h"

#include "zjets-analysis/Config.h"
#include "zjets-analysis/ZJetscommon.h"

using namespace ZJets;

class ZJetsjetTools : public asg::AsgMessaging{
public:
  Str m_jetCollectionName;
  JetCalibrationTool *m_jetCalibTool; //!
  JetUncertaintiesTool *m_jetUncertainties; //!
  asg::AnaToolHandle<IJetModifier> m_fJvtTool; //!
  CP::JetJvtEfficiency *m_jvtEffTool; //!
  CP::JetJvtEfficiency *m_fjvtEffTool; //!
  ZJetsjetTools(const char *name, bool isMC);
  StatusCode Initialize(Config &config);
  StatusCode InitializeJetTools(Config &config);
  void GetJVTscaleFactors(float& jvtSF, float& fjvtSF);
  
  double m_pTCut, m_rapidityCut;
  bool m_dofJVT;
  double m_jvtSF, m_fjvtSF;

  double GetMaxRapidity() { return m_rapidityCut; }

  StatusCode ApplySystematicVariation(const CP::SystematicSet &syst);

  StatusCode FinalizeTools();

private:
  Str m_name;
  Str m_jetAlg;
  bool m_isMC;
  bool m_doPFlow;


};

#endif
