#ifndef ZJetsmuonTools_H
#define ZJetsmuonTools_H

//Muon tools
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"

//Trigger tools
#include "AsgTools/AnaToolHandle.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"

//TTVA
#include "xAODTracking/TrackParticlexAODHelpers.h"

//xAOD
#include "xAODMuon/MuonContainer.h"

#include "zjets-analysis/Config.h"
#include "zjets-analysis/ZJetscommon.h"

using namespace ZJets;

class ZJetsmuonTools : public asg::AsgMessaging{
public:
  ZJetsmuonTools(const char *name, bool isMC);
  StatusCode Initialize(Config &config);
  StatusCode InitializeMuonTools(Config &config);
  StatusCode MuonSelection(xAOD::TEvent *event, const xAOD::Vertex* primaryVtx, std::vector<xAOD::Muon*> *calibMuons, std::vector<xAOD::Muon*> *selected,int& count1,int& count2,int& count3);

  StatusCode ApplySystematicVariation(const CP::SystematicSet &syst);
  void checkSyst(const CP::SystematicSet &syst);

  double GetMuonTriggerEfficiency(std::vector<xAOD::Muon*> *SelectedMuons);
  double GetMuonTriggerEfficiencyEmu(std::vector<xAOD::Muon*> *SelectedMuons);

  StatusCode FinalizeTools();

  bool passTTVA(const xAOD::Muon* muon, const xAOD::Vertex *vtxHS, const xAOD::EventInfo *eventInfo);

  double getRecoSF(const xAOD::Muon* mu1, const xAOD::Muon* mu2, const CP::SystematicSet &syst = CP::SystematicSet());
  double getIsoSF(const xAOD::Muon* mu1, const xAOD::Muon* mu2, const CP::SystematicSet &syst = CP::SystematicSet());
  double getTTVASF(const xAOD::Muon* mu1, const xAOD::Muon* mu2, const CP::SystematicSet &syst = CP::SystematicSet());
  double getDimuonTriggerSF(const xAOD::Muon* mu1, const xAOD::Muon* mu2);
  double getMuonTriggerSF(const xAOD::Muon* mu1, const xAOD::Muon* mu2);

  // recoSF * isoSF * TTVASF * triggerSF
  double getDimuonSF(const xAOD::Muon* mu1, const xAOD::Muon* mu2, const CP::SystematicSet &syst = CP::SystematicSet());

  double m_pTCut, m_MaxAbsEta, m_d0BySigd0Max, m_z0Max; //!

  CP::MuonCalibrationAndSmearingTool *m_muonCalibrationAndSmearingTool; //!
  CP::MuonSelectionTool *m_muonSelectionTool; //!
  CP::IsolationSelectionTool *m_isoSelectionTool; //!
  CP::MuonEfficiencyScaleFactors *m_muonRecoSFTool; //!
  CP::MuonEfficiencyScaleFactors *m_muonIsoSFTool; //!
  CP::MuonEfficiencyScaleFactors *m_muonTTVASFTool; //!
  CP::MuonTriggerScaleFactors *m_muonTriggerSFTool; //!

private:
  Str m_name;
  bool m_isMC;

  bool m_applyTriggerSF; //!
  Str m_diMuonTriggerLeg1, m_diMuonTriggerLeg2; //!
  Str m_emuTrigger1; //!

};

#endif
