#ifndef ZJetsstudy_ZJetsStudy_H
#define ZJetsstudy_ZJetsStudy_H

// #include <AnaAlgorithm/AnaAlgorithm.h>
#include <EventLoop/Algorithm.h>

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODCore/ShallowCopy.h"

#include <AsgTools/MessageCheck.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include "xAODTracking/VertexContainer.h"

#include "MCUtils/PIDUtils.h"

#include "zjets-analysis/ZJetscommon.h"
#include "zjets-analysis/OutputTree.h"
#include "zjets-analysis/ZJetseventTools.h"
#include "zjets-analysis/ZJetsjetTools.h"
#include "zjets-analysis/ZJetselecTools.h"
#include "zjets-analysis/ZJetsmuonTools.h"

// OR
#include "AssociationUtils/IOverlapRemovalTool.h"
#include "AssociationUtils/OverlapRemovalDefs.h"
#include "AssociationUtils/OverlapRemovalInit.h"
#include "AssociationUtils/ToolBox.h"
#include <TString.h>

using namespace ZJets;

class ZJetsStudy: public EL::Algorithm {
  public:
    static const int nsys = 11.; //!
    std::vector<CP::SystematicSet> m_sysList; //!

    // this is a standard constructor
    ZJetsStudy();

    virtual EL::StatusCode setupJob(EL::Job &job);
    virtual EL::StatusCode histInitialize();
    virtual EL::StatusCode initialize();
    virtual EL::StatusCode InitializeOR();
    virtual EL::StatusCode execute();
    virtual EL::StatusCode fileExecute();
    virtual EL::StatusCode finalize();

    inline void SetConfig(const Config &config){
      m_config = config;
    }

    // this is needed to distribute the algorithm to the workers
    ClassDef(ZJetsStudy, 1);

    float m_mmin      = 71.;  //!
    float m_mmax      = 111.; //!

    // OR options
    std::string m_orTitle = "OverlapRemovalTool";
    std::string m_selectionLabel = "selected";
    std::string m_outputLabel = "overlaps";
    ORUtils::ORFlags m_flags; //!
    ORUtils::ToolBox m_toolBox; //!

  private:
    EL::StatusCode FillTruthBranches();

    ZJetseventTools* m_eventTools; //!
    ZJetsjetTools* m_jetTools; //!
    ZJetsmuonTools* m_muTools; //!
    ZJetselecTools* m_elTools; //!

    bool m_isMC; //!
    int m_channel; // muon = 1, electron = 2
    Config m_config;

    bool m_doPFlow;

    int EvtNbr; //!
    int RunNbr; //!
    float m_w; //!
    float m_pileupW; //!
    float m_initialW; //!

    float m_pileupW_up; //!
    float m_pileupW_down; //!

    std::set<std::pair<UInt_t, UInt_t> > m_processedEvents; //!

    float m_mu; //!

    bool m_pass_trigger; //!

    int  m_Nmuons; //!
    int m_Nelecs; //!
    int m_Njets; //!
    int m_NTruthJets; //!
    bool m_trigger_matched; //!
    float m_lep1TotalSF; //!
    float m_lep2TotalSF; //!
    float m_trigSF; //!
    float m_jvtSF; //!
    float m_fjvtSF; //!

    // Histo
    TTree *ZjTree[nsys]; //!

    // Truth
    // Truth Leptons
    std::vector<float> m_truth_lep_E; //!
    std::vector<float> m_truth_lep_pT; //!
    std::vector<float> m_truth_lep_px; //!
    std::vector<float> m_truth_lep_py; //!
    std::vector<float> m_truth_lep_pz; //!
    std::vector<float> m_truth_lep_id; //!
    // Truth Jet
    std::vector<float> m_truth_jet_E; //!
    std::vector<float> m_truth_jet_pT; //!
    std::vector<float> m_truth_jet_px; //!
    std::vector<float> m_truth_jet_py; //!
    std::vector<float> m_truth_jet_pz; //!

    TH1F* m_cutFlow_weighted; //!
    TH1F* m_cutFlow_nominal; //!
    TH1F* m_sum_of_weights; //!

    std::vector<TLV> m_jets; //!
    TLV m_lepton1; //!
    TLV m_lepton2; //!
    float m_lepton1ID;
    float m_lepton2ID;
    std::vector<xAOD::Muon*> *m_finalMuons; //!
    std::vector<xAOD::Electron*> *m_finalElectrons; //!
    // Leptons
    std::vector<float> m_lep_E; //!
    std::vector<float> m_lep_pT; //!
    std::vector<float> m_lep_px; //!
    std::vector<float> m_lep_py; //!
    std::vector<float> m_lep_pz; //!
    std::vector<float> m_lep_id; //!
    // Jets
    std::vector<float> m_jet_E; //!
    std::vector<float> m_jet_pT; //!
    std::vector<float> m_jet_px; //!
    std::vector<float> m_jet_py; //!
    std::vector<float> m_jet_pz; //!

    TruthParticleStruct m_truthPtcls; //!

    bool isDiMuon=false, isDiElec=false, isElMu=false; //!
    bool isTruth=false; //!
    bool m_isZEvent=false; //!
};

#endif
