#ifndef OUTPUT_TREE_H
#define OUTPUT_TREE_H

#include <vector>
#include <map>

#include <EventLoop/Worker.h>

#include <TTree.h>
#include <TString.h>

  // Small class for creation of an output tree, D. Gillberg
  class OutputTree {
  public:
  OutputTree() : m_tree(0) {};
    ~OutputTree(){};

    void createAndRegister(TString name, EL::IWorker *wk) {
      //      TFile *outputFile = wk->getOutputFile ("outputLabel");
      m_tree = new TTree(name,name);
      wk->addOutput(m_tree);
      //m_tree->SetDirectory (outputFile);
    }

    // access and fill the TTree
    TTree *tree() { return m_tree; }
    void fill() { m_tree->Fill(); }

    // maps for holding the integers and floats
    std::map<TString,int> ints;
    std::map<TString,float> floats;
    std::map<TString,std::vector<float> > fvecs;
    std::map<TString,std::vector<double> > dvecs;

    // creation of float branches
    void createFBranch(TString name) { m_tree->Branch(name,&floats[name],name+"/F"); };
    void createFBranches(std::vector<TString> names) { for (TString n:names) createFBranch(n); }
    // creation of int branches
    void createIBranch(TString name) { m_tree->Branch(name,&ints[name],name+"/I"); };
    void createIBranches(std::vector<TString> names) { for (TString n:names) createIBranch(n); }
    // creation of float vector branches
    void createFVecBranch(TString name) { m_tree->Branch(name,&fvecs[name]); }
    void createFVecBranches(std::vector<TString> names) { for (TString n:names) createFVecBranch(n); }
    // creation of double vector branches
    void createDVecBranch(TString name) { m_tree->Branch(name,&dvecs[name]); }
    void createDVecBranches(std::vector<TString> names) { for (TString n:names) createDVecBranch(n); }
    // reset all branches - prior to filling a new entry
    void resetBranches() {
      for (auto &e:ints) e.second = -99;
      for (auto &e:floats) e.second = -99.0;
      for (auto &e:fvecs) e.second.clear();
      for (auto &e:dvecs) e.second.clear();
    }
  private:
    TTree *m_tree;
  };

#endif
