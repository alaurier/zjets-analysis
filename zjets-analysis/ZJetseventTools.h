#ifndef ZJetseventTools_H
#define ZJetseventTools_H

#include <xAODMetaDataCnv/FileMetaDataTool.h>
#include "xAODMetaData/FileMetaData.h"

#include "PileupReweighting/PileupReweightingTool.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "AsgTools/AnaToolHandle.h"
#include "TPRegexp.h"

//Trigger Tools
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"

#include "zjets-analysis/Config.h"
#include "zjets-analysis/ZJetscommon.h"

using namespace ZJets;

class ZJetseventTools : public asg::AsgMessaging{
public:
  ZJetseventTools(const char *name, bool isMC);
  StatusCode Initialize(Config &config);
  StatusCode InitializeTools(Config &config);
  StatusCode InitializeTriggerTools();
  StatusCode FinalizeTools();

  StatusCode FillSumOfWeights(xAOD::TEvent* event, TH1F* sum_of_weights);

  int getYear(int runNbr);

  float GetDataPRWsf();
  float GetCorrectedMu(const xAOD::EventInfo* eventInfo);
  bool PassRunLB(const xAOD::EventInfo* eventInfo);
  bool PassEventCleaning(const xAOD::EventInfo* eventInfo);

  // Triggers
  bool PassTrigger();
  bool PassMuonTrigger();
  bool PassElecTrigger();
  bool PassEMuTrigger();
  bool TrigMatchElectron(xAOD::Electron &elec1, xAOD::Electron &elec2);
  bool TrigMatchMuon(xAOD::Muon &muon1, xAOD::Muon &muon2);
  bool TrigMatchElectronEmu(xAOD::Electron &elec1, xAOD::Electron &elec2);
  bool TrigMatchMuonEmu(xAOD::Muon &muon1, xAOD::Muon &muon2);

  void removeJetsThatOverlapWithLeptons(std::vector<xAOD::Jet*> *jets, std::vector<xAOD::Electron*> *elecs, std::vector<xAOD::Muon*> *muons);
  void removeLeptonsThatOverlapWithJets(std::vector<xAOD::Jet*> *jets, std::vector<xAOD::Electron*> *elecs, std::vector<xAOD::Muon*> *muons);
  bool jetOverlapsMuon(xAOD::Jet* jet, xAOD::Muon* muon, double DRcut);

  GoodRunsListSelectionTool *m_grlTool; //!
  asg::AnaToolHandle<CP::IPileupReweightingTool> m_PileupReweightingTool; //!

private:
  Str m_name;
  bool m_isMC;
  Str m_mcType; //!
  bool m_doBadBatman; //!

  // Triggers
  StrV m_triggers;
  StrV m_emu_triggers;
  StrV m_mu_triggers;
  StrV m_el_triggers;
  double m_trigMatchElecdR;
  double m_trigMatchMuondR; //!
  Str m_diMuonTriggerLeg1, m_diMuonTriggerLeg2; //!
  TrigConf::xAODConfigTool *m_trigConfigTool; //!
  Trig::TrigDecisionTool *m_trigDecisionTool; //!
  asg::AnaToolHandle<Trig::IMatchingTool> m_trigMatching; //!

  double m_jet_DR_e, m_jet_DR_mu, m_e_DR_jet, m_mu_DR_jet; //!

  double m_prwSF; //!

  template <class T> bool overlap(const xAOD::IParticle *ptcl, std::vector<T*> *ptcls, double DRcut){
    if (DRcut < 0)
      return false;
    return ZJets::minDRrap(ptcl, ptcls) < DRcut;
  }

  template <class T1, class T2> void removeOverlap(std::vector<T1*> *ptcls, std::vector<T2*> *refPtcls, double DRcut){
    for (auto p = ptcls->rbegin(); p != ptcls->rend(); ++p) {
      // If the current probe-particle overlaps with any reference particle, remove it
      if (overlap(*p, refPtcls, DRcut))
        ptcls->erase(p.base() - 1);
    }
  }
};

#endif
