#ifndef ZJetsExtraSysts_ZJetsExtraSysts_H
#define ZJetsExtraSysts_ZJetsExtraSysts_H

// #include <AnaAlgorithm/AnaAlgorithm.h>
#include <EventLoop/Algorithm.h>

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODCore/ShallowCopy.h"

#include <AsgTools/MessageCheck.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include "xAODTracking/VertexContainer.h"

#include "MCUtils/PIDUtils.h"
#include "PMGTools/PMGTruthWeightTool.h"

#include "zjets-analysis/ZJetscommon.h"
#include "zjets-analysis/OutputTree.h"
#include "zjets-analysis/ZJetseventTools.h"
#include "zjets-analysis/ZJetsjetTools.h"
#include "zjets-analysis/ZJetselecTools.h"
#include "zjets-analysis/ZJetsmuonTools.h"

// OR
#include "AssociationUtils/IOverlapRemovalTool.h"
#include "AssociationUtils/OverlapRemovalDefs.h"
#include "AssociationUtils/OverlapRemovalInit.h"
#include "AssociationUtils/ToolBox.h"
#include <TString.h>

using namespace ZJets;

class ZJetsExtraSysts: public EL::Algorithm {
  public:
    // static const int nsys = 1.; //!
    // static const int nsys = 31.; //!
    // 8 extra histograms!
    static const int nsys = 111.; //!

    // this is a standard constructor
    ZJetsExtraSysts();

    virtual EL::StatusCode setupJob(EL::Job &job);
    virtual EL::StatusCode histInitialize();
    virtual EL::StatusCode initialize();
    virtual EL::StatusCode InitializeOR();
    virtual EL::StatusCode InitializeEventWeights();
    virtual EL::StatusCode execute();
    virtual EL::StatusCode fileExecute();
    virtual EL::StatusCode finalize();

    inline void SetConfig(const Config &config){
      m_config = config;
    }

    TString sampleName;

    // this is needed to distribute the algorithm to the workers
    ClassDef(ZJetsExtraSysts, 1);

    float m_mmin      = 71.;  //!
    float m_mmax      = 111.; //!

    // OR options
    std::string m_orTitle = "OverlapRemovalTool";
    std::string m_selectionLabel = "selected";
    std::string m_outputLabel = "overlaps";
    ORUtils::ORFlags m_flags; //!
    ORUtils::ToolBox m_toolBox; //!

    // PMG Truth : 230 top weights
    asg::AnaToolHandle<PMGTools::IPMGTruthWeightTool> m_weightTool; //!
    const static int nWeights = 300;
    std::vector<double> vector_of_weights; //!
    std::vector<double> vector_sum_of_weights; //!
    std::vector<std::string> vector_name_of_weights; //!

  private:
    EL::StatusCode FillTruthVariables();
    EL::StatusCode FillTruthHistos();

    ZJetseventTools* m_eventTools; //!
    ZJetsjetTools* m_jetTools; //!
    ZJetsmuonTools* m_muTools; //!
    ZJetselecTools* m_elTools; //!

    bool m_isMC; //!
    Config m_config;

    int EvtNbr; //!
    int RunNbr; //!
    double m_w; //!
    double m_pileupW; //!
    double m_initialW; //!
    double m_weight; //!

    double m_pileupW_up; //!
    double m_pileupW_down; //!

    std::set<std::pair<UInt_t, UInt_t> > m_processedEvents; //!

    double m_mu; //!

    bool m_pass_trigger; //!

    int  m_Nmuons; //!
    int m_Nelecs; //!
    bool m_trigger_matched; //!
    double m_lep1TotalSF; //!
    double m_lep2TotalSF; //!
    double m_trigSF; //!
    double m_jvtSF; //!
    double m_fjvtSF; //!

    // Histogram values
    // Number of control histograms
    const static int npl = 41; //!
    TH1F *vhist[npl*3*nsys]; //!
    // Unfolding matrices: 2 per variable per systematic
    const static int nunf = 15; //!
    TH2F *vhistm[nunf*2*nsys]; //!
    TH1F *vhistr[nunf*2*nsys]; //!
    TH1F *vhistt[nunf*2*nsys]; //!
    float zjbins[16] = {0.,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5}; //!
    float jbins[4] = {50.,400.,550.,5000.}; //!



    // Truth
    bool m_isSelectedTruth; //!
    int tch=3; //!
    // Truth Leptons
    TLorentzVector m_truth_lep1, m_truth_lep2, m_truthZ, m_truth_dijet,m_truth_clJet; //!
    int m_truth_lep1ID=0,m_truth_lep2ID=0,m_NTruthJets=0; //!
    double m_truth_HT=0.,m_truth_minDR=0.,m_truth_fzjbin=0.; //!
    // Truth Jets
    std::vector<TLorentzVector> m_truth_loose_jets,m_truth_jets; //!

    // Bookkeeping
    TH1F* m_cutFlow_weighted; //!
    TH1F* m_cutFlow_nominal; //!
    TH1F* m_sum_of_weights; //!
    TH1F* hsumwgh; //!



    std::vector<TLV> m_jets; //!
    TLV m_lepton1; //!
    TLV m_lepton2; //!
    double m_lepton1ID; //!
    double m_lepton2ID; //!
    std::vector<xAOD::Muon*> *m_finalMuons; //!
    std::vector<xAOD::Electron*> *m_finalElectrons; //!

    TruthParticleStruct m_truthPtcls; //!

    bool isDiMuon=false, isDiElec=false, isElMu=false; //!
    bool isTruth=false; //!
    bool m_isZEvent=false; //!
};

#endif
