#ifndef ZJetscommon_H
#define ZJetscommon_H

#include <AsgTools/MessageCheck.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/JetContainer.h"

#include "MCUtils/PIDUtils.h"

#include "PathResolver/PathResolver.h"

#include <TSystem.h>
#include <TROOT.h>
#include <TH1F.h>

#include <TObjString.h>
#include <TObjArray.h>
#include <TString.h>
#include <TMath.h>
#include <TRandom3.h>

struct DescendingPt: std::function<bool(const xAOD::IParticle*, const xAOD::IParticle*)> {
  bool operator()(const xAOD::IParticle* l, const xAOD::IParticle* r) const {
    return l->pt() > r->pt();
  }
};

struct DescendingPtTLV: std::function<bool(const TLorentzVector &l, const TLorentzVector &r)> {
  bool operator()(const TLorentzVector &l, const TLorentzVector &r) const {
    return l.Pt() > r.Pt();
  }
};

namespace ZJets {
  /*
   *  General helper methods
   */

  void fatal(TString msg);

  // Simple class that is TLV + pdgId. Needed for dressed leptons.
  class Lepton : public TLorentzVector {
  public:
    // default constructor
    Lepton() { }
    // Copy construcotr. E.g. Lepton ptcl = tlv;
    Lepton(const TLorentzVector &tlv):TLorentzVector(tlv) {}
    Lepton(const TLorentzVector &tlv, int id, double fd=0.0, int nd=0)
      :TLorentzVector(tlv),pdgId(id),fracDressing(fd),nDressing(nd) {}
    int pdgId = 0;
    double fracDressing = 0.0;
    int nDressing=0;
  };

  typedef TString Str;
  typedef std::vector<double> NumV;
  typedef std::vector<int> IntV;
  typedef std::vector<TString> StrV;
  typedef std::vector<const xAOD::TruthParticle*> TruthPtcls;
  typedef TLorentzVector TLV;
  typedef std::vector<TLorentzVector> TLVs;
  typedef std::vector<Lepton> Leptons;
  typedef std::vector<xAOD::Jet*> Jets;

  // 1 GeV = 1000 MeV
  static const double GeV(1000), invGeV(1.0 / GeV);

  // templated methods

  // 1. add element(s) to vector
  template<typename T> void add(std::vector<T> &v, T a) {
    v.push_back(a);
  }
  template<typename T, typename ... Args> void add(std::vector<T> &v, T a, Args ... args) {
    add(v, a);
    add(v, args...);
  }

  // 2. create a vector out of a list of elements
  template<typename T, typename ... Args>
  std::vector<T> vec(T a, Args ... args) {
    std::vector<T> v;
    add(v, a, args...);
    return v;
  }

  // 3. merge vectors
  template<typename T> void add(std::vector<T> &v, std::vector<T> v2) {
    for (auto e : v2)
      add(v, e);
  }
  template<typename T, typename ... Args> std::vector<T> merge(std::vector<T> a, Args ... args) {
    std::vector<T> v(a);
    add(v, args...);
    return v;
  }

  template<typename T> TLVs makeTLVsGeV(std::vector<T*> objs) {
    TLVs tlvs;
    for (auto o:objs) add(tlvs,o->p4()*invGeV);
    return tlvs;
  }

  StrV vectorize(TString str, TString sep = "");

  // convert a text line containing a list of numbers to a vector<double>
  NumV vectorizeNum(TString str, TString sep);
  TString fourVecAsText(const TLorentzVector &p4);
  TString fourVecAsText(const xAOD::IParticle *p);

  // checks if a given file or directory exist
  bool fileExist(TString fn);

  /*
   *  Methods for histogram creation
   */

  TH1F* createHist1D(TString hname, TString title, int nbins, int xlow, int xhigh);

  /*
   *  Truth methods
   */

  /// print details about the truth particle to the screen
  void printTruthPtcl(const xAOD::TruthParticle *ptcl, TString comment, int childDepth = 0, int parentDepth = 0, int currentDepth = 0);

  bool notFromHadron(const xAOD::TruthParticle *ptcl);
  bool isStable(const xAOD::TruthParticle *ptcl);
  bool isFromZ(const xAOD::TruthParticle *ptcl);
  bool isLepton(const xAOD::TruthParticle *ptcl);
  bool isZdecayLepton(const xAOD::TruthParticle *ptcl);
  bool isGoodTruthElectron(const xAOD::TruthParticle *ptcl);
  bool isGoodTruthMuon(const xAOD::TruthParticle *ptcl);

  double DRrap(const TLorentzVector &p1, const TLorentzVector &p2);
  double DRrap(const xAOD::IParticle *p1, const xAOD::IParticle *p2);
  // Minimal DR
  double DRrap_min(const TLorentzVector &p, const TLVs &bs);

  // scalar pT sum
  double HT(const TLVs &ps);

  // minimal DR between ptcl and ptcls
  template <class T> double minDRrap(const xAOD::IParticle *ptcl, std::vector<T*> *ptcls){
    double mindr = 99;

    for (auto p : *ptcls)
      if (p != ptcl && DRrap(ptcl, p) < mindr){
        mindr = DRrap(ptcl, p);
      }

    return mindr;
  }

  //  struct TruthParticles
  struct TruthParticleStruct {
    TruthPtcls Zs, leptons, Zleptons, gams;
    Leptons dressedLeptons, fidLeptons, weirdLeptons;

    // Leading dressed leptons and their sum
    TLorentzVector lep1, lep2, ll;

    // Information about the dressing
    int NdressingLep1, NdressingLep2;
    double fracDressingLep1, fracDressingLep2;

    // Jets after overlap removal
    TLVs jets, jets40gev;
  };

  //  struct RecoParticles
  struct RecoParticleStruct {
    TLVs muons, elecs, jets;
    TLorentzVector jet1, jet2, gapJet, lep1, lep2, zboson;
    NumV Qmuons, Qelecs;
  };

  TruthParticleStruct identifyTruthParticles(xAOD::TEvent *event);

  // input TruthElectrons or TruthMuons
  Leptons getDressedLeptons(const xAOD::TruthParticleContainer *leptons);

  // Fiducial electron: pT>27 GeV, |eta|<2.47 and not in crack |eta| in (1.37,1.52)
  bool isFiducialElectron(const TLV &e);

  // Fiducial muon: pT>27 GeV, |eta|<2.4
  bool isFiducialMuon(const TLV &mu);

  // checks pdgId and returns either of the two above
  bool isFiducialLepton(const Lepton &lep);

} // namespace ZJets

#endif
