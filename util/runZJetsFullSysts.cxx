#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include <SampleHandler/SampleLocal.h>
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include <EventLoopGrid/PrunDriver.h>
#include "SampleHandler/DiskListLocal.h"
#include "PathResolver/PathResolver.h"
#include <AnaAlgorithm/AnaAlgorithmConfig.h>
#include <TSystem.h>
#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>

#include "zjets-analysis/ZJetsFullSysts.h"
#include "zjets-analysis/Config.h"
#include "zjets-analysis/ZJetscommon.h"

StrV parseArguments(Config *conf, int argc, char **argv);

int main( int argc, char* argv[] ) {

  Config conf;

  StrV files = parseArguments(&conf, argc, argv);

  if (conf.isDefined("Include"))
    for (TString cfg : conf.getStrV("Include"))
      { conf.addFile(cfg); }

  if (!conf.isDefined("BaseConfig")){
    ZJets::fatal("You must specify a base configuration file, option: BaseConfig. Exiting.");
  }
  else {
    conf.addFile(PathResolverFindCalibFile(conf.getStr("BaseConfig").Data()));
  }

  TString sampleName = conf.getStr("SampleName", "sample");
  TString submitDir = conf.getStr("OutputDir", "ZJetsanalysis_DATE");
  if (submitDir.Contains("DATE")) {
    TDatime now = TDatime();
    submitDir.ReplaceAll("DATE",Form("%d.%.2d.%.2d_%.2d.%.2d.%.2d",
                                     now.GetYear(),now.GetMonth(),now.GetDay(),
                                     now.GetHour(),now.GetMinute(),now.GetSecond()));
    conf.setValue("OutputDir", submitDir.Data());
  }

  if (ZJets::fileExist(submitDir))
    ZJets::fatal("Output directory " + submitDir + " already exists.\n" +
          "  Rerun after deleting it, or specify a new one, like below.\n" +
          "    OutputDir: NEWDIR");

  if (conf.isDefined("InputFile")) {
    for (auto file : conf.getStrV("InputFile"))
      files.push_back(file);
  }

  printf("\n");
  printf("  %20s  %s\n", "SampleName:", sampleName.Data());
  printf("  %20s  %s\n", "OutputDir:", submitDir.Data());

  EL::Job job;
  SH::SampleHandler sh;

  sh.setMetaString( "nc_tree", "CollectionTree" );

  if (files.size()) { //Run Locally
    // If one or several root files are specified as arugment, run over these
    std::auto_ptr<SH::SampleLocal> sample (new SH::SampleLocal (sampleName.Data()));
    printf("\n  %s\n","InputFiles:");
    for ( TString file : files ) {
      printf("    %s\n",file.Data());
      sample->add (file.Data());
    }
    printf("\n");

    // add the files to the sample handler
    sh.add (sample.release());

    // Alex turn this on to set max events to 1k locally
    // job.options()->setDouble(EL::Job::optMaxEvents, 50000);

    if (conf.isDefined("NumEvents"))
      job.options()->setDouble(EL::Job::optMaxEvents, conf.getInt("NumEvents"));

    if (conf.isDefined("SkipEvents"))
      job.options()->setDouble(EL::Job::optSkipEvents, conf.getInt("SkipEvents"));

    job.sampleHandler( sh );

    ZJetsFullSysts *alg = new ZJetsFullSysts();
    alg->SetConfig(conf);
    job.algsAdd( alg );


    // Run the job using the local/direct driver:
    EL::DirectDriver driver;
    driver.submit( job, submitDir.Data() );
  }
  if(conf.isDefined("GridDS")){ //Run on Grid
    for (TString sample: conf.getStrV("GridDS"))
      SH::addGrid(sh, sample.Data());

    if (!conf.isDefined("OutputDS")) {
       fatal("To submit to the grid, you MUST define an OutputDS of the form: user.<UserName>.<UniqueString>");
    }

    TString outDS = conf.getStr("OutputDS");
    printf("  %20s  %s\n", "OutputDS:", outDS.Data());

    EL::PrunDriver driver;
    driver.options()->setString("nc_outputSampleName", (outDS).Data());

    for (auto opt : {"nc_nFiles", "nc_nFilesPerJob", "nc_nJobs"})
      if (conf.isDefined(opt))
        driver.options()->setDouble(opt, conf.getInt(opt));

    for (auto opt : {"nc_excludedSite", "nc_EventLoop_SubmitFlags", "nc_mergeOutput", "nc_official", "nc_voms", "nc_destSE"})
      if (conf.isDefined(opt))
        driver.options()->setString(opt, conf.getStr(opt).Data());
    // driver.options()->setString(EL::Job::optSubmitFlags, "--noEmail --osMatching --excludedSite=ANALY_FZU");
    driver.options()->setString(EL::Job::optSubmitFlags, "--noEmail --osMatching");
    driver.options()->setDouble(EL::Job::optGridMergeOutput, 1);

    ZJetsFullSysts *alg = new ZJetsFullSysts();
    alg->SetConfig(conf);

    job.algsAdd( alg );

    job.sampleHandler( sh );

    driver.submitOnly(job, submitDir.Data());
  }
  if(conf.isDefined("GridRunAll")){ //Run the full sample on the grid
    if(conf.isDefined(sampleName.Data())){
      for (TString sample: conf.getStrV(sampleName.Data())){
        printf("  %20s  %s\n", "Sample:", sample.Data());
        SH::addGrid(sh, sample.Data());
      }
    }
    else
      fatal("Sample " + sampleName + " not defined");

    if (!conf.isDefined("OutputDS")) {
       fatal("To submit to the grid, you MUST define an OutputDS of the form: user.<UserName>.<UniqueString>");
    }

    TString outDS = conf.getStr("OutputDS");
    printf("  %20s  %s\n", "OutputDS:", outDS.Data());

    EL::PrunDriver driver;
    driver.options()->setString("nc_outputSampleName", (outDS).Data());

    for (auto opt : {"nc_nFiles", "nc_nFilesPerJob", "nc_nJobs"})
      if (conf.isDefined(opt))
        driver.options()->setDouble(opt, conf.getInt(opt));

    for (auto opt : {"nc_excludedSite", "nc_EventLoop_SubmitFlags", "nc_mergeOutput", "nc_official", "nc_voms", "nc_destSE"})
      if (conf.isDefined(opt))
        driver.options()->setString(opt, conf.getStr(opt).Data());
    driver.options()->setString(EL::Job::optSubmitFlags, "--noEmail --osMatching");
    driver.options()->setDouble(EL::Job::optGridMergeOutput, 1);

    ZJetsFullSysts *alg = new ZJetsFullSysts();
    alg->SetConfig(conf);

    job.algsAdd( alg );

    job.sampleHandler( sh );

    driver.submitOnly(job, submitDir.Data());
  }

return 0;
}

StrV parseArguments(Config *conf, int argc, char **argv) {
  StrV files;

  for (int argi = 1; argi < argc; ++argi) {
    TString arg = argv[argi];

    // 1. Check if argument is a configuration file. If so add it!
    if (arg.EndsWith(".cfg") || arg.EndsWith(".config")) {
      conf->addFile(arg);
      continue;
    }

    // 2. If the argument contains ".root", i.e. *.root*, add it to files to run over
    if (arg.Contains(".root")) {
      files.push_back(arg);
      continue;
    }

    // 3. If the arguemnt ends with colon, add a new argument
    if (arg.EndsWith(":")) {
      TString key(arg);
      key.ReplaceAll(":", "");
      conf->setValue(key, argv[++argi]);
      continue;
    }

    // if we get here, the arguemnt cannot be understood
    ZJets::fatal("Cannot interpret argument: " + arg );
  }

  return files;
}
